<?php
Yii::import('application.extensions.yiiGoogleMap.yiiGM');

/**
 * Description of CImageComponent
 *
 * @author Administrator
 */
class GMComponent extends CApplicationComponent
{
    public function f($pos = 'front')
    {
        $class = $pos == 'front' ? 'class="front"' : '';

        echo '<div id="google_map"'.$class.'></div>';
        return new yiiGM($pos);
    }
}