$(document).ready(function() {
    var map;
    var EditForm = '<p><div class="marker-edit">'+
        '<div name="SaveMarker" id="SaveMarker">'+
        '<label for="pName"><span>Place Name :</span><input type="text" name="pName" class="save-name" placeholder="Enter Title" maxlength="40" /></label>'+
        '<label for="pDesc"><span>Description :</span><textarea name="pDesc" class="save-desc" placeholder="Enter Address" maxlength="150"></textarea></label>'+
        '</div>';

    map_initialize(); // load map
    function map_initialize(){
        this.marker = [];
        this.marker['mrk'] = [];
        this.marker['db'] = [];
        this.infowindow = [];
        this.marker_count = 0;

        var data = $.parseJSON(get_param());
        var zoom = data.zoom * 1;
        var center = new google.maps.LatLng(data.x, data.y);

        //Google map option
        var googleMapOptions =
        {
            center: center, // map center
            zoom: zoom, //zoom level, 0 = earth view to higher value
            panControl: true, //enable pan Control
            zoomControl: true, //enable zoom control
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL //zoom control size
            },
            scaleControl: true, // enable scale control
            draggable: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
        };

        map = new google.maps.Map(document.getElementById("google_map"), googleMapOptions);
        this.map = map;

        set_markers(EditForm);
    }
});

//############### Create Marker Function ##############
function create_marker(map, MapPos, MapTitle, MapDesc,  InfoOpenDefault, DragAble, id, saveBtn, iconPath)
{
    var self = this;
    self.marker_count++;

    //new marker
    self.marker['mrk'][self.marker_count] = new google.maps.Marker({
        position: MapPos,
        map: map,
        icon: iconPath,
        draggable:false,
        animation: google.maps.Animation.DROP,
    });

    if(id == null)
        update_marker(self.marker_count);
    else
        self.marker['db'][self.marker_count] = id;

// icon: iconPath

    //Content structure of info Window for the Markers

    var contentString = $('<div class="marker-info-win">'+
        '<div class="marker-inner-win"><span class="info-content">'+
        '<h1 class="marker-heading">'+MapTitle+'</h1>'+
        MapDesc+
        '</div></div>');



    //Create an infoWindow
    self.infowindow[self.marker_count] = new google.maps.InfoWindow();
    //set the content of infoWindow
    self.infowindow[self.marker_count].setContent(contentString[0]);


    //add click listner to remove marker button

    function MakeInfoWindowEvent(cur) {
        return function() {
            self.infowindow[cur].open(map,self.marker['mrk'][cur]); // click on marker opens info window
        }
    }

    //add click listner to save marker button
    if(!saveBtn)
        google.maps.event.addListener(self.marker['mrk'][self.marker_count], 'click', MakeInfoWindowEvent(self.marker_count));


    if(InfoOpenDefault) //whether info window should be open by default
    {
        self.infowindow[self.marker_count].open(map,self.marker['mrk'][self.marker_count]);
    }
}

function set_markers(EditForm)
{
    var self = this;
    $.ajax({
        url: "/site/gm/",
        dataType: 'json',
        success:function(data){
            $.each(data.point, function(key, val) {
                // Создание маркера и позиционирование его на карте
                var id      = val.id;
                if(val.name != null)
                    var name = val.name;
                else
                    var name = 'New Marker';
                var saveBtn = false;
                if(val.address != null)
                    var address   = '<p>'+ val.address +'</p>';
                else
                {
                    var address  = '';
                    saveBtn = true;
                }

                var point     = new google.maps.LatLng(parseFloat(val.x),parseFloat(val.y));

                create_marker(self.map, point, name, address, false, false, id, saveBtn, "/static/images/point.png");
            });
        },
        error:function (xhr, ajaxOptions, thrownError){
            alert(thrownError); //throw any errors
        }
    });
}


function update_marker(MarkerId, mName, mAddress, replaceWin)
{
    //Save new marker using jQuery Ajax
    Marker = self.marker['mrk'][MarkerId];
    var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
    var zoom = this.map.getZoom();
    var center = this.map.getCenter().toUrlValue();
    var myData = {name : mName, address : mAddress, latlang : mLatLang, id : self.marker['db'][MarkerId]}; //post variables

    update_param(zoom, center);

    $.ajax({
        type: "POST",
        url: "/site/gm/",
        data: myData,
        dataType: 'json',
        success:function(data){
            self.marker['db'][MarkerId] = data.id;
            if(replaceWin != null)
                replaceWin.html(data.fields); //replace info window with new html
            /*Marker.setDraggable(false); //set marker to fixed
             Marker.setIcon('http://PATH-TO-YOUR-WEBSITE-ICON/icons/pin_blue.png'); //replace icon*/
        },
        error:function (xhr, ajaxOptions, thrownError){
            alert(thrownError); //throw any errors
        }
    });
}

function get_param()
{
    var myData = {getparam : true}; //post variables
    var jqxhr = $.ajax({
        type: "GET",
        url: "/site/gm/",
        global: false,
        async:false,
        dataType: 'json',
        data: myData,
        success:function(data){
            return data;
        },
        error:function (xhr, ajaxOptions, thrownError){
            alert(thrownError); //throw any errors
        }
    }).responseText;

    return jqxhr;
}

function update_param(zoom, center)
{
    var myData = {zoom : zoom, center : center}; //post variables
    $.ajax({
        type: "POST",
        url: "/site/gm/",
        data: myData,
        success:function(data){
        },
        error:function (xhr, ajaxOptions, thrownError){
            alert(thrownError); //throw any errors
        }
    });
}

//############### Remove Marker Function ##############
function remove_marker(cur)
{
    /* determine whether marker is draggable
     new markers are draggable and saved markers are fixed */
    var Marker = self.marker['mrk'][cur];
    Marker.setMap(null); //just remove new marker
    //Remove saved marker from DB and map using jQuery Ajax
    var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
    var myData = {del : 'true', id : self.marker['db'][cur]}; //post variables
    $.ajax({
        type: "POST",
        url: "/site/gm/",
        data: myData,
        success:function(data){
        },
        error:function (xhr, ajaxOptions, thrownError){
            alert(thrownError); //throw any errors
        }
    });
}