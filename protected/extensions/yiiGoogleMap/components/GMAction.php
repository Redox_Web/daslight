<?php
class GMAction extends CAction
{
    public function run()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_POST['latlang']))
            {
                $mLatLang   = explode(',',$_POST["latlang"]);

                if (!empty($_POST['id']))
                    $model = GmMarkers::model()->findByPk((int)$_POST['id']);
                else
                    $model = new GmMarkers();

                $model->lat = $mLatLang[0];
                $model->lng = $mLatLang[1];

                $output = '';
                if (!empty($_POST['name']) && !empty($_POST['address']))
                {
                    $model->name = $_POST['name'];
                    $model->address = $_POST['address'];
                    $output = '<h1 class="marker-heading">'.$model->name.'</h1><p>'.$model->address.'</p>';
                }

                if($model->save())
                    echo json_encode(array('id' => $model->id, 'fields' => $output));
            }
            elseif(isset($_POST['del']))
            {
                $model = GmMarkers::model()->findByPk((int)$_POST['id']);
                $model->delete();
            }
            elseif(isset($_GET['getparam']))
            {
                $model = GmParam::model()->findAll();
                $arr = array();
                foreach($model as $value)
                    $arr[$value->param] = $value->value;

                if($arr['zoom'] == '')
                    $arr['zoom'] = 17;
                if($arr['center'] == '')
                    $arr['center'] = '0,0';

                $mLatLang   = explode(',', $arr['center']);
                $arr['x'] = $mLatLang[0];
                $arr['y'] = $mLatLang[1];

                echo json_encode($arr);
            }
            elseif(isset($_POST['zoom']) && isset($_POST['center']))
            {
                $model = GmParam::model()->findByAttributes(array('param' => 'zoom'));
                $model->value = (int)$_POST['zoom'];
                $model->save();

                $model = GmParam::model()->findByAttributes(array('param' => 'center'));
                $model->value = $_POST['center'];
                $model->save();
            }
            else
            {
                $model = GmMarkers::model()->findAll();
                $arr = array();
                $arr['zoommy'] = 7;
                if($model)
                {
                    foreach($model as $key => $value)
                    {
                        if($key == 0)
                        {
                            $arr['x'] = $value->lat;
                            $arr['y'] = $value->lng;
                        }
                        $arr['point'][$key]['id'] = $value->id;
                        $arr['point'][$key]['x'] = $value->lat;
                        $arr['point'][$key]['y'] = $value->lng;
                        $arr['point'][$key]['name'] = $value->name;
                        $arr['point'][$key]['address'] = $value->address;
                    }
                    echo json_encode($arr);
                }
            }
            Yii::app()->end();
        }
    }
}