<?php

class yiiGM
{
    protected $_assetsUrl;

    public function __construct($pos)
    {
        $this->registerCSS();
        $this->registerJS($pos);

        $this->createTable();
    }

    public function registerJS($pos)
    {
        $position = CClientScript::POS_END;
        /** @var CClientScript $cs */
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile('http://maps.google.com/maps/api/js?key=AIzaSyCzIS4O5QH3K5VHfpJ1_izN_62SXJrqzis', $position);

        if($pos == 'front')
            $cs->registerScriptFile($this->getAssetsUrl().'/js/gmFront.js', $position);
        elseif($pos == 'back')
            $cs->registerScriptFile($this->getAssetsUrl().'/js/gm.js', $position);
    }

    public function registerCSS()
    {
        /** @var CClientScript $cs */
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($this->getAssetsUrl().'/css/gm.css');
    }

    protected function createTable()
    {
        if (!Yii::app()->getDb()->schema->getTable('gm_markers')) {
            Yii::app()->getDb()->createCommand()->createTable("gm_markers", array(
                'id' => 'pk',
                'name' => 'varchar(60)',
                'address' => 'varchar(80)',
                'lat' => 'float(10,6)',
                'lng' => 'float(10,6)',
            ),'ENGINE=InnoDB');
        }

        if (!Yii::app()->getDb()->schema->getTable('gm_param')) {
            Yii::app()->getDb()->createCommand()->createTable("gm_param", array(
                'id' => 'pk',
                'param' => 'varchar(20)',
                'value' => 'varchar(255)',
            ),'ENGINE=InnoDB');

            $sql = "insert into gm_param (param) values ('zoom'),('center')";
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    protected function getAssetsUrl()
    {
        if (isset($this->_assetsUrl))
            return $this->_assetsUrl;
        else
        {
            $assetsPath = Yii::getPathOfAlias('application.extensions.yiiGoogleMap.assets');
            $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);
            return $this->_assetsUrl = $assetsUrl;
        }
    }
}