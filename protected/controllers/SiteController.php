<?php
Yii::import('store.models.StoreAttributeValue');
Yii::import('store.models.StoreAttribute');
Yii::import('store.models.StoreCategoryAttribute');

class SiteController extends Frontend
{
    public function actions()
    {
        return array(
            'gm' => 'application.extensions.yiiGoogleMap.components.GMAction',
        );
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                /** @var CHttpRequest $request */
                $request = Yii::app()->request;
                $urlArray = explode('/', $request->getPathInfo());
                if (isset($urlArray[0]) && in_array($urlArray[0], Yii::app()->languages->languages)) {
                    Yii::app()->user->language = Yii::app()->language = Yii::app()->session['language'] = $urlArray[0];
                }
                $this->render('error', $error);
            }
        }
    }
}
