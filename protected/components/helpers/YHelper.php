<?php
Yii::import('backend.models.Settings');

class YHelper
{
    public static function yiisetting($name, $default = null)
    {
        if ($setting = Settings::model()->findByAttributes(array("name"=>$name))) {
            if (isset($setting) && $setting != '') {
                return $setting->value;
            }
        }

        return $default;
    }

    public static function formatCurrency($value, $currency = "MDL", $format = '#,##0.00 ¤')
    {
        return Yii::app()->numberFormatter->format($format, $value, $currency);
    }

    public static function formatDate($formatGet, $date = null, $formatSet = 'yyyy-MM-dd')
    {
        if ($date && $formatSet) {
            $newDate = Yii::app()->dateFormatter->format($formatGet, CDateTimeParser::parse($date, $formatSet));
        } else {
            $newDate = Yii::app()->format->date($formatGet);
        }

        return $newDate;
    }

    public static function generateStr($length = 16)
    {
        $chars = str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789");
        $code = "";
        $clen = strlen($chars) - 1;

        while (strlen($code) < $length)
            $code .= $chars[mt_rand(0, $clen)];

        return $code;
    }

    public static function getImagePath($source_image, $width = 0, $height = 0, $default = '')
    {
        if (preg_match('~^(http|https)://~', $source_image)) {
            return $source_image;
        }
        if (!empty($source_image) && file_exists($source_image)) {
            $image_info = getimagesize($source_image);
            if (!is_array($image_info) OR count($image_info) < 3) {
                $source_image = $default ? : Yii::app()->params['noImage'];
            }
        } else {
            $source_image = $default ? : Yii::app()->params['noImage'];
        }
        if (empty($width) && empty($height)) {
            $image = '/' . $source_image;
        } elseif (!empty($width) && empty($height)) {
            $image = Yii::app()->iwi->load($source_image)->resize($width, 0)->cache();
        } elseif (!empty($height) && empty($width)) {
            $image = Yii::app()->iwi->load($source_image)->resize(0, $height, Image::HEIGHT)->cache();
        } else {
            $image = Yii::app()->iwi->load($source_image)
                ->adaptive($width, $height, true)
                ->crop($width, $height, 'top', 'center')
                ->cache();
        }
        return $image;
    }
}
