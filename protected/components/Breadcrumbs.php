<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 3/18/13
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
Yii::import("zii.widgets.CBreadcrumbs");
class Breadcrumbs extends CBreadcrumbs
{
    public $breadcrumbsClass;

    public function run()
    {
        $this->separator = '';
        $this->tagName='ul';
        $this->htmlOptions = array('class'=>'list-unstyled');
        $this->inactiveLinkTemplate = '<li>{label}</li>';
        $this->activeLinkTemplate = '<li>'.CHtml::link("{label}", "{url}").'{arrow}</li>';

        if (empty($this->links)) {
            return;
        }

        if (!$this->breadcrumbsClass) {
            $this->breadcrumbsClass = "breadcrumbs";
        }

        echo CHtml::openTag("nav", array("class"=>$this->breadcrumbsClass)) . "\n";
        echo CHtml::openTag($this->tagName, $this->htmlOptions) . "\n";
        $links = array();
        if ($this->homeLink === null)
            $links[] = '<li>'.CHtml::link(Yii::t('zii', 'Home'), Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)), array("class" => "breadcrumbHome")).'<i class="fa fa-angle-right"></i></li>'; //homeUrl
        elseif ($this->homeLink !== false)
            $links[] = $this->homeLink;
        foreach ($this->links as $label => $url) {
            if (is_string($label) || is_array($url)){
                $links[] = strtr($this->activeLinkTemplate, array(
                    '{url}' => CHtml::normalizeUrl($url),
                    '{label}' => $this->encodeLabel ? CHtml::encode($label) : $label,
                    '{arrow}' => '<i class="fa fa-angle-right"></i>'
                ));
            }else
                $links[] = str_replace('{label}', $this->encodeLabel ? CHtml::encode($url) : $url, $this->inactiveLinkTemplate);

    }
        echo implode($this->separator, $links);
        echo CHtml::closeTag($this->tagName);
        echo CHtml::closeTag("nav");
    }

}
