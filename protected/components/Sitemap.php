<?php
class Sitemap extends CApplicationComponent
{
    public function createSitemap()
    {
        $sitemap = new DSitemap();

        // main menu
        //$sitemap->addCreateUrl('/blog/frontBlog/category', array('id' => 1), DSitemap::WEEKLY);
        $sitemap->addCreateUrl('/main/default/page', array('id' => 2), DSitemap::WEEKLY, 0.5, date('Y-m-d H:i:s'));
        // $sitemap->addCreateUrl('/blog/frontBlog/slidesCategory', array('id' => 7), DSitemap::WEEKLY);
        $sitemap->addCreateUrl('/main/default/partners', array(), DSitemap::WEEKLY, 0.5, date('Y-m-d H:i:s'));
        $sitemap->addCreateUrl('/main/default/feedback', array(), DSitemap::WEEKLY, 0.5, date('Y-m-d H:i:s'));

        // servicii
        $sitemap->addCreateUrl('/main/default/calculator', array(), DSitemap::WEEKLY, 0.5, date('Y-m-d H:i:s'));
        $sitemap->addCreateUrl('/main/default/page', array('id' => 3), DSitemap::WEEKLY, 0.5, date('Y-m-d H:i:s'));

        // categories
        $store_categories = StoreCategory::model()->active()->findAll(array('order'=>'priority,root,lft,id'));
        $sitemap->addModels($store_categories, DSitemap::DAILY, 0.8);

        // products
        $store_products = StoreProduct::model()->findAll('is_active = 1');
        $sitemap->addModels($store_products, DSitemap::DAILY, 0.8);

        // blog categories
        $blog_categories = BlogCategory::model()->findAll();
        $sitemap->addModels($blog_categories, DSitemap::DAILY, 0.8);

        // blog articles
        $blog_article_categories = BlogArticle::model()->findAll();
        $sitemap->addModels($blog_article_categories, DSitemap::DAILY, 0.8);

        $sitemap->render();
    }
}