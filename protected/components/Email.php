<?php
class Email extends CApplicationComponent
{
    public function sendEmail($subject, $body, $to, $senderEmail = null, $blogArticle = null)
    {
        $sImage = null;
        if (!empty($blogArticle['tmp_name']['file'])) {
            $sImage = file_get_contents($blogArticle['tmp_name']['file']);
        }

        if (!is_array($to)) $to = explode(',', $to);
        $senderEmail = $senderEmail ? : Yii::app()->params['no-replyEmail'];

        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->From = Yii::app()->params['no-replyEmail'];
        $mailer->AddReplyTo($senderEmail);

        foreach($to as $toEmail)
            $mailer->AddAddress($toEmail);

        $mailer->FromName = Yii::app()->name;

        if ($sImage) {
            $mailer->AddStringAttachment($sImage, $blogArticle['name']['file']);
        }
        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->IsHTML(true);  // set email format to HTML
        $mailer->Body = $body;

        if($mailer->Send())
            return true;
        else
            return false;
    }
}