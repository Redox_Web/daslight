<?php
/**
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 */

class DSitemap
{
    const ALWAYS = 'always';
    const HOURLY = 'hourly';
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';
    const YEARLY = 'yearly';
    const NEVER = 'never';

    protected $items = array();

    public function __construct(){
        $host = Yii::app()->request->hostInfo;
        $item = array(
            'loc' => $host,
//            'changefreq' => DSitemap::DAILY,
            'lastmod' => $this->dateToW3C(date('Y-m-d H:i:s')),
            'priority' => 0.8
        );
        $this->items[] = $item;
    }

    /**
     * @param $url
     * @param string $changeFreq
     * @param float $priority
     * @param int $lastmod
     */
    public function addUrl($url, $changeFreq=self::DAILY, $priority=0.5, $lastMod=0)
    {
        $host = Yii::app()->request->hostInfo;
        $languages = Yii::app()->languages;
        foreach($languages->languages as $lang){ 
            Yii::app()->language = $lang;
            $item = array(
                'loc' => $host . $url,
//                'changefreq' => $changeFreq,
                'priority' => $priority
            );
            if ($lastMod)
                $item['lastmod'] = $this->dateToW3C($lastMod);
            
            $this->items[] = $item;
        }
    }
    
    /**
     * @param $url
     * @param string $changeFreq
     * @param float $priority
     * @param int $lastmod
     */
    public function addCreateUrl($url, $params, $changeFreq=self::DAILY, $priority=0.5, $lastMod=0)
    {
        $host = Yii::app()->request->hostInfo;
        $languages = Yii::app()->languages;
        foreach($languages->languages as $lang){ 
            Yii::app()->language = $lang;
            //echo '---'.Yii::app()->language."<br>";
            $item = array(
                'loc' => $host.Yii::app()->createUrl($url, $params),
//                'changefreq' => $changeFreq,
                'priority' => $priority
            );
            if ($lastMod) {
                $item['lastmod'] = $this->dateToW3C($lastMod);
            } else {
                $item['lastmod'] = $this->dateToW3C(date('Y-m-d H:i:s'));
            }
            
            $this->items[] = $item;
        }
       // exit;
    }

    /**
     * @param CActiveRecord[] $models
     * @param string $changeFreq
     * @param float $priority
     */
    public function addModels($models, $changeFreq=self::DAILY, $priority=0.5)
    {
        $host = Yii::app()->request->hostInfo;
        $current_lang = Yii::app()->language; 
        $languages = Yii::app()->languages;
        foreach($languages->languages as $lang){ 
            Yii::app()->language = $lang;
            foreach ($models as $model)
            {
                $item = array(
                    'loc' => $host . $model->getUrl($lang),
//                    'changefreq' => $changeFreq,
                    'priority' => $priority
                );

                if ($model->hasAttribute('updated')) {
                    $lastMod = $model->updated;
                    if ((empty($lastMod) || $lastMod == '0000-00-00 00:00:00')
                        && $model->hasAttribute('created')) {
                        $lastMod = $model->created;
                    }
                    $item['lastmod'] = $this->dateToW3C($lastMod);
                } else {
                    $item['lastmod'] = $this->dateToW3C(date('Y-m-d H:i:s'));
                }

                $this->items[] = $item;
            }
        }
        Yii::app()->setLanguage($current_lang);
    }

    /**
     * @return string XML code
     */
    public function render()
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $urlset = $dom->createElement('urlset');
        $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
        foreach($this->items as $item)
        {
            $url = $dom->createElement('url');

            foreach ($item as $key=>$value)
            {
                $elem = $dom->createElement($key);
                $elem->appendChild($dom->createTextNode($value));
                $url->appendChild($elem);
            }

            $urlset->appendChild($url);
        }
        $dom->appendChild($urlset);

        return $dom->save(Yii::app()->basePath . '/../'.'sitemap.xml');
    }

    protected function dateToW3C($date)
    {
        if (is_int($date))
            return date(DATE_W3C, $date);
        else
            return date(DATE_W3C, strtotime($date));
    }
}