<?php
Yii::import('store.components.storeProductRouter');
Yii::import('store.components.searchProductRouter');
Yii::import('blog.components.blogArticleRouter');
Yii::import('main.components.mainPageRouter');
Yii::import('main.components.mainUrlRouter');
/**
 * Created by Idol IT.
 * Date: 10/30/12
 * Time: 12:38 PM
 */


class Frontend extends Controller {
    public $_assetsUrl;
    public $_curNav;
    public $_curCateg;

    public function urltrans($url)
    {
        if($lurl = mainPageRouter::Instance()->urltrans($url)) {
            return $lurl;
        }
        if($lurl = blogArticleRouter::Instance()->urltrans($url)) {
            return $lurl;
        }
        if($lurl = storeProductRouter::Instance()->urltrans($url)) {
            return $lurl;
        }
        if($lurl = mainUrlRouter::Instance()->urltrans($url)) {
            return $lurl;
        }
        if($lurl = searchProductRouter::Instance()->urltrans($url)) {
            return $lurl;
        }

        return $this->defaultPrepareUrl($url);
    }

    protected function defaultPrepareUrl($url)
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $langsPrepared = [];

        foreach (Yii::app()->languages->languages as $key => $language) {
            $langsPrepared[$key] = [
                'alias' => $language,
                'prev' => isset(Yii::app()->languages->languages[$key-1]) ? Yii::app()->languages->languages[$key-1] : '',
                'next' => isset(Yii::app()->languages->languages[$key+1]) ? Yii::app()->languages->languages[$key+1] : '',
            ];
        }

        foreach ($langsPrepared as $language) {
            $diffLang = !empty($language['next']) ? $language['next'] : $language['prev'];

            if ($request->pathInfo == $language['alias'] . '/main/default/change') {
                $url = preg_replace('~'.$request->getBaseUrl(true).'~', '', $url);
                $url = preg_replace('~\/'.$diffLang.'\/~', '/'.$language['alias'].'/', $url);
            }
        }

        return $url;
    }

    public function init()
    {
        if (YII_DEBUG) {
            $this->getAssetsUrl();
        }

        Yii::app()->clientScript->registerPackage('base');

        return parent::init();
    }

    public function behaviors()
    {
        return array(
            'seo' => array('class' => 'webroot.vendor.chemezov.yii-seo.behaviors.SeoBehavior'),
        );
    }

    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null) {
            $this->_assetsUrl =
                Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.static'), false, -1, YII_DEBUG);
        }

        return $this->_assetsUrl;
    }
}
