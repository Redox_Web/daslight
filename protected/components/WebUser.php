<?php
Yii::import('backend.models.User');

class WebUser extends CWebUser {

    // Store model to not repeat query.
    private $_model;

    // Return first name.
    // access it by Yii::app()->user->first_name
    public function getFullName(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->name.' '.$user->surname;
    }

    public function getEmail(){
        $user = $this->loadUser(Yii::app()->user->id);
        if($user)
            return preg_replace('~@.*~', '', $user->email);
    }

    public function getIsSuperAdmin(){
        $user = $this->loadUser(Yii::app()->user->id);

        if($user->id == 1)
            return true;

        return false;
    }

    public function getAvatar(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->UAvatar;
    }

    public function getProfileField($field)
    {
        if (Yii::app()->getUser()->hasState($field)) {
            return Yii::app()->user->getState($field);
        }

        $profile = $this->loadUser(Yii::app()->user->id);

        if (null === $profile) {
            return null;
        }

        $value = $profile->$field;

        Yii::app()->getUser()->setState($field, $value);

        return $value;
    }

    // Load user model.
    private function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }
}