<?php
class MainPageUrlRule extends CBaseUrlRule
{
    public $connectionID = 'db';

    public function createUrl($manager,$route,$params,$ampersand)
    {
        if ($route==='main/default/index') {
            return $params['lang'];
        }

        return false;  // this rule does not apply
    }

    public function parseUrl($manager,$request,$pathInfo,$rawPathInfo)
    {
        $elems = explode("/", $pathInfo);

        if (in_array($elems[0], array('ru', 'ro')) && !isset($elems[1])) {
            $_GET['language'] = $elems[0];
            return "main/default/index";
        }

        return false;  // this rule does not apply
    }
}
