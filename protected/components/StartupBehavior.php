<?php
//Yii::import('landing.models.LandingCountry');

class StartupBehavior extends CBehavior{
    public function attach($owner) {
        $owner->attachEventHandler('onBeginRequest', array($this, 'beginRequest'));
    }

    public function beginRequest(CEvent $event) {
        if(Url::isFrontPage() && (Yii::app()->request->requestUri=="/ro" || Yii::app()->request->requestUri=="/ro/")){
            Yii::app()->language = 'ro';
        }elseif(Url::isFrontPage()){
            Yii::app()->language = "ru";
        }
    }
}

