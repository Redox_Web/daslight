<?php
foreach(Yii::app()->user->getFlashes() as $status => $message) { ?>
    <?php if(strpos($status, 'success') !== false) { ?>
        <div class="col-xs-12 col-sm-12 success-response">
            <p><?php echo $message;?>.</p>
        </div>
    <?php } elseif(strpos($status, 'error') !== false) { ?>
        <div class="col-xs-12 col-sm-12 error-response">
            <p><?php echo $message;?>.</p>
        </div>
    <?php } ?>
<?php } ?>