<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<section class="mt-error-sec dark style3">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="error-holder pull-right">
                    <h1 class="text-uppercase montserrat"><?php echo Yii::t("base", "Page not found");?></h1>
                    <div class="txt">
                        <p><?php echo Yii::t("base", "Could not find page");?>.</p>
                    </div>
                    <a href="/" class="btn-back text-uppercase"><?php echo Yii::t("base", "Home");?></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <span class="error-code2 montserrat">404</span>
            </div>
        </div>
    </div>
</section>