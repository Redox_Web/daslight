﻿<!DOCTYPE html>
<html>
<head>
    <?php Yii::app()->getController()->widget('webroot.vendor.chemezov.yii-seo.widgets.SeoHead'); ?>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- include the site stylesheet -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic%7cMontserrat:400,700%7cOxygen:400,300,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <?php if($favicon_src = YHelper::yiisetting('favicon_site')){ ?>
        <link rel="icon" href="<?php echo Yii::app()->iwi->load($favicon_src)->adaptive(16, 16)->cache(); ?>" type="image/png" />
    <?php } ?>
    <meta property="fb:app_id" content="156317001391446" />
    <meta name="google-site-verification" content="Ln63cKwcaLtOqzt7RhHsQZbxu3NYYcAxMGv10BxoAyE" />
    <?php echo YHelper::yiisetting('header_analitics'); ?>
    <script>
        var curentLanguage = '<?=Yii::app()->language?>';
    </script>
</head>
<body>
<div id="wrapper">
    <!-- Page Loader -->
    <div id="pre-loader" class="loader-container">
        <div class="loader">
            <img src="<?php echo Yii::app()->getBaseUrl(false); ?>/static/images/svg/rings.svg" alt="loader">
        </div>
    </div>
    <!-- W1 start here -->
    <div class="w1">
        <!-- mt header style4 start here -->
        <header id="mt-header" class="style4">
            <!-- mt bottom bar start here -->
            <div class="mt-bottom-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- mt logo start here -->
                            <div class="mt-logo">
                                <a href="<?php echo Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)); ?>">
                                    <?php if($logo_src = YHelper::yiisetting('logo_site')){ ?>
                                        <img src="<?php echo Yii::app()->getBaseUrl(false)."/".$logo_src; ?>" alt="DASLight logo" />
                                    <?php } ?>
                                </a>
                            </div>
                            <?php if($phone = YHelper::yiisetting('phone')){ ?>
                                <div class="mt-phone"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;  <?php echo $phone; ?></div>
                            <?php } ?>
                            <!-- mt icon list start here -->
                            <ul class="mt-icon-list">
                                <li class="hidden-lg hidden-md">
                                    <a href="#" class="bar-opener mobile-toggle">
                                        <span class="bar"></span>
                                        <span class="bar small"></span>
                                        <span class="bar"></span>
                                    </a>
                                </li>
                                <li class="header-icon"><a href="#" class="icon-magnifier"></a></li>

                                <li class="drop header-icon" id="shopping-cart-widget">
                                    <?php $this->widget('store.widgets.ShoppingCartWidget'); ?>
                                </li>
                                <li>
                                    <!--<a href="#" class="bar-opener side-opener">-->
                                    <!--<span class="bar"></span>-->
                                    <!--<span class="bar small"></span>-->
                                    <!--<span class="bar"></span>-->
                                    <!--</a>-->
                                </li>
                                <?php $this->widget('MultiLang'); ?>

                            </ul><!-- mt icon list end here -->
                            <!-- navigation start here -->
                            <nav id="nav">
                                <ul>
                                    <?php $this->renderPartial('main.views.default._menu', array('main' => true)); ?>
                                </ul>
                            </nav>
                            <!-- mt icon list end here -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- mt bottom bar end here -->
            <span class="mt-side-over"></span>
        </header><!-- mt header style4 end here -->
        <!-- mt side menu start here -->
        <div class="mt-side-menu">
            <!-- mt holder start here -->
            <div class="mt-holder">
                <a href="#" class="side-close"><span></span><span></span></a>
                <strong class="mt-side-title">MY ACCOUNT</strong>
                <!-- mt side widget start here -->
                <div class="mt-side-widget">
                    <header>
                        <span class="mt-side-subtitle">SIGN IN</span>
                        <p>Welcome back! Sign in to Your Account</p>
                    </header>
                    <form action="#">
                        <fieldset>
                            <input type="text" placeholder="Username or email address" class="input">
                            <input type="password" placeholder="Password" class="input">
                            <div class="box">
                                <span class="left"><input class="checkbox" type="checkbox" id="check1"><label for="check1">Remember Me</label></span>
                                <a href="#" class="help">Help?</a>
                            </div>
                            <button type="submit" class="btn-type1">Login</button>
                        </fieldset>
                    </form>
                </div>
                <!-- mt side widget end here -->
                <div class="or-divider"><span class="txt">or</span></div>
                <!-- mt side widget start here -->
                <div class="mt-side-widget">
                    <header>
                        <span class="mt-side-subtitle">CREATE NEW ACCOUNT</span>
                        <p>Create your very own account</p>
                    </header>
                    <form action="#">
                        <fieldset>
                            <input type="text" placeholder="Username or email address" class="input">
                            <button type="submit" class="btn-type1">Register</button>
                        </fieldset>
                    </form>
                </div>
                <!-- mt side widget end here -->
            </div>
            <!-- mt holder end here -->
        </div><!-- mt side menu end here -->
        <!-- mt search popup start here -->
        <div class="mt-search-popup">
            <div class="mt-holder">
                <a href="#" class="search-close"><i class="fa fa-times" aria-hidden="true"></i></a>
                <div class="mt-frame">
                    <form>
                        <fieldset>
                            <input id="search" type="text" name="q" value="" placeholder="<?php echo Yii::t("base", "Search").'...';?>">
                            <button onclick="window.location.href =
                                    '<?=Yii::app()->createUrl('store/frontSearch/searchResult'); ?>/?q='+
                                    document.getElementById('search').value"
                                    class="icon-magnifier" type="submit">
                            </button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div><!-- mt search popup end here -->
        <!-- mt main slider start here -->
        <?php if (Url::isFrontPage()) {
            $this->widget('slider.widgets.SliderWidget');
        } ?>
        <!-- mt main start here -->
        <main id="mt-main">
            <?php echo $content; ?>
        </main><!-- mt main end here -->

        <!-- footer of the Page -->
        <footer id="mt-footer" class="style1 wow fadeInUp" data-wow-delay="0.4s">
            <!-- Footer Holder of the Page -->
            <div class="footer-holder dark">
                <div class="container">
                    <div class="">
                        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 mt-paddingbottomsm">
                            <!-- F Widget About of the Page -->
                            <div class="f-widget-about">
                                <div class="logo">
                                    <a href="<?php echo Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)); ?>">
                                        <img class="w100" src="<?php echo Yii::app()->getBaseUrl(true); ?>/static/images/logo/footer-logo.png" alt="DasLight logo">
                                    </a>
                                </div>
                                <p><?php echo YHelper::yiisetting('footer_desc_'.Yii::app()->language); ?></p>
                                <!-- Social Network of the Page -->
                                <ul class="list-unstyled social-network">
                                    <li>
                                        <a href="<?php echo YHelper::yiisetting('twitter_href'); ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo YHelper::yiisetting('facebook_href'); ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo YHelper::yiisetting('pinterest_href'); ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo YHelper::yiisetting('youtube_href'); ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo YHelper::yiisetting('googleplus_href'); ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Social Network of the Page end -->
                            </div>
                            <!-- F Widget About of the Page end -->
                        </div>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<div class="row">
								<?php $this->widget('main.widgets.FooterCategories'); ?>
							</div>
						</div>
                        <!-- Footer Tabs of the Page -->
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 text-right">
                            <!-- F Widget About of the Page -->
                            <h3 class="f-widget-heading"><?php echo Yii::t("base", "Contacts"); ?></h3>
                            <div class="f-widget-about flex flex-end">
                                <div class="child-pb-0_5em">
                                    <div class="flex-0_5em nowrap  flex-end">
                                        <div><?php echo YHelper::yiisetting('address'); ?></div>
                                        <div><i class="fa fa-map-marker"></i></div>
                                    </div>
                                    <div class="flex-0_5em nowrap  flex-end">
                                        <div><a href="tel:<?php echo YHelper::yiisetting('phone'); ?>"><?php echo YHelper::yiisetting('phone'); ?></a></div>
                                        <div><i class="fa fa-phone"></i></div>
                                    </div>
                                    <div class="flex-0_5em nowrap  flex-end">
                                        <div><a href="mailto:<?php echo YHelper::yiisetting('email'); ?>"><?php echo YHelper::yiisetting('email'); ?></a></div>
                                        <div><i class="fa fa-envelope-o"></i></div>
                                    </div>
                                </div>
                            </div>
                            <!-- F Widget About of the Page end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer Holder of the Page end -->
            <!-- Footer Area of the Page -->
            <div class="footer-area">
                <div class="container">
                    <div class="flex space-between items-center">
                        <div>
                            <?php echo YHelper::yiisetting('copyright'); ?>
                        </div>
                        <div class="flex nowrap">
                            <?php echo YHelper::yiisetting('footer_analitics'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer Area of the Page end -->
        </footer><!-- footer of the Page end -->

    </div><!-- W1 end here -->
    <span id="back-top" class="fa fa-arrow-up"></span>
</div>


</body>
<!-- hello -->
<?php $this->widget('main.widgets.ActionPop'); ?>
</html>
