<?php

$dirs = scandir(dirname(__FILE__).'/../modules');
$packages = require_once(dirname(__FILE__).'/packages.php');
$configDir = dirname(__FILE__);

// строим массив модулей
$modules = array();
foreach ($dirs as $name){
    if ($name[0] != '.')
        $modules[$name] = array();
}

define('MODULES_MATCHES', implode('|', array_keys($modules)));

$mainLocalFile = $configDir . DIRECTORY_SEPARATOR . 'main-local.php';
$mainLocalConfiguration = file_exists($mainLocalFile) ? require($mainLocalFile) : array();

$mainEnvFile = $configDir . DIRECTORY_SEPARATOR . 'main-env.php';
$mainEnvConfiguration = file_exists($mainEnvFile) ? require($mainEnvFile) : array();

return CMap::mergeArray(
    array(
        'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
        'name' => 'Daslight',

        'preload' => array('log', 'languages'),

        'import' => array(
            'application.modules.store.models.*',
            'application.modules.blog.models.*',
            'application.components.helpers.*',
            'application.components.*',
        ),

        'behaviors'=> array(
            array(
                'class'=>'application.modules.backend.components.behavior.ModuleUrlRulesBehavior',
                'beforeCurrentModule'=>array(
                    'main',
                    'idea',
                    'store',
                    'blog',
                ),
            ),
        ),

        'modules'=> array(
            'backend',
            'main',
            'blog',
            'store',
        ),

// application components
        'components' => array(
            'log' => array(
                'class' => 'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'logFile'=>'cron.log',
                        'levels'=>'error, warning',
                    ),
                    array(
                        'class'=>'CFileLogRoute',
                        'logFile'=>'cron_trace.log',
                        'levels'=>'trace',
                    ),
                ),
            ),

            'urlManager'=>array(
                'urlFormat'=>'path',
                'showScriptName'=>false,
                'rules'=>array(
                    array(
                        'class' => 'application.components.MainPageUrlRule',
                    ),
                    ''=>'main/default/index',
                    'gii'=>'gii',
                    'backend'=>'backend/default/index',
                    '<module:' . MODULES_MATCHES . '>/default/index'=>'backend/default/error404',
                    '<module:' . MODULES_MATCHES . '>/default'=>'backend/default/error404',
                    '<lang:(ru|ro)>/<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>/<id:\w+>'=>'<module>/<controller>/<action>',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>'=>'<module>/<controller>/index',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),

            'languages' => array(
                'class' => 'Languages',
                'useLanguage' => true,
                'autoDetect' => true,
                'languages' => array('ru', 'ro'),
                'defaultLanguage' => 'ro',
            ),

            'request' => array(
                'hostInfo' => 'http://daslight.md',
                'baseUrl' => '',
                'scriptUrl' => '',
            ),

            'session' => array('cookieParams' => array('httponly' => true)),

            'email' => array(
                'class' => 'application.components.Email',
            ),
        ),

        'params'=>array(
            'adminEmail'=>'jenya@idol-it.com',
            'no-replyEmail'=>'no-reply@spinnaker.com',
            'itemsPerPage' => 21,
            'noImage' => 'images/avatar.png',
            'cacheTime' => 3600,
            'sandbox' => false,
        ),
    ),
    CMap::mergeArray($mainEnvConfiguration, $mainLocalConfiguration)
);