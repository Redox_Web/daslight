<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$dirs = scandir(dirname(__FILE__).'/../modules');
$packages = require_once(dirname(__FILE__).'/packages.php');
$configDir = dirname(__FILE__);

// строим массив модулей
$modules = array();
foreach ($dirs as $name){
    if ($name[0] != '.')
        $modules[$name] = array();
}

define('MODULES_MATCHES', implode('|', array_keys($modules)));

$mainLocalFile = $configDir . DIRECTORY_SEPARATOR . 'main-local.php';
$mainLocalConfiguration = file_exists($mainLocalFile) ? require($mainLocalFile) : array();

$mainEnvFile = $configDir . DIRECTORY_SEPARATOR . 'main-env.php';
$mainEnvConfiguration = file_exists($mainEnvFile) ? require($mainEnvFile) : array();

return CMap::mergeArray(
    array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'DAS Light',
        'language'=>'ru',
        'theme' => 'default',

        // preloading 'log' component
        'preload'=>array('log', 'languages'), //, 'booster'

        // autoloading model and component classes
        'import'=>array(
            'ext.mailer.Emailer',
            'application.models.*',
            'application.components.*',
            'application.widgets.*',
            'application.extensions.yiiGoogleMap.models.*',
            'application.components.helpers.*',
            'application.extensions.*'
        ),

        'modules'=>array_replace($modules, array(
            'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'12345',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'=>array('127.0.0.1','::1'),
                'generatorPaths'=>array(
                    'booster.gii',
                ),
            ),

        )),

        'behaviors'=> array(
            array(
                'class'=>'application.modules.backend.components.behavior.ModuleUrlRulesBehavior',
                'beforeCurrentModule'=>array(
                    'main',
                    'idea',
                    'store',
                    'blog',
                ),
            ),
            'onbeginRequest'=>array('class'=>'application.components.StartupBehavior'),
        ),

        // application components
        'components'=>array(
            'widgetFactory' => array(
                'enableSkin' => true,
                'skinPath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'skins',
            ),

            'booster' => array(
                'class' => 'ext.booster.components.Booster',
                'forceCopyAssets' => false
            ),

            'format' => array(
                'class' => 'backend.components.ExtendedFormatter'
            ),

            'GM' => array(
                'class' => 'application.extensions.yiiGoogleMap.GMComponent',
            ),

            'email' => array(
                'class' => 'application.components.Email',
            ),

            'languages' => array(
                'class' => 'Languages',
                'useLanguage' => true,
                'autoDetect' => true,
                'languages' => array('ru', 'ro'),
                'defaultLanguage' => 'ro',
            ),

            'iwi' => array(
                'class' => 'application.extensions.iwi.IwiComponent',
                // GD or ImageMagick
                'driver' => 'GD',
                // ImageMagick setup path
                //'params'=>array('directory'=>'C:/ImageMagick'),
            ),

            'clientScript' => array(
		        'packages' => $packages,
		        'coreScriptPosition' => CClientScript::POS_END,
		        'scriptMap' => array(
			        'jquery.js' => '/static/js/jquery.js',
                    'jquery.min.js' => '/static/js/jquery.js',
		        ),
	        ),


            'assetManager' => array(
                'newDirMode' => 0755,
                'newFileMode' => 0655
            ),

            'user' => array(
                'allowAutoLogin'=>true,
                'class' => 'WebUser',
            ),

            'errorHandler' => array(
                //'errorAction' => '/main/default/error',
                'errorAction' => 'site/error',
            ),

            // uncomment the following to enable URLs in path-format

            'urlManager'=>array(
                'urlFormat'=>'path',
                'showScriptName'=>false,
                'rules'=>array(
                    array(
                        'class' => 'application.components.MainPageUrlRule',
                    ),
                    ''=>'main/default/index',
                    'gii'=>'gii',
                    'backend'=>'backend/default/index',
                    '<module:' . MODULES_MATCHES . '>/default/index'=>'backend/default/error404',
                    '<module:' . MODULES_MATCHES . '>/default'=>'backend/default/error404',
                    '<lang:(ru|ro)>/<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                    '<lang:(ru|ro)>/<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>/<id:\w+>'=>'<module>/<controller>/<action>',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>'=>'<module>/<controller>/index',
                    '<module:' . MODULES_MATCHES . '>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<language:(ru|ro)>/<action:\w+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),

            'session' => array('cookieParams' => array('httponly' => true)),

            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning, info, trace, profile',
                    ),
                ),
            ),

        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'no-replyEmail' => 'noreply@daslight.md',
            'header_image' => 'static/images/contacts_new.jpg',
            'adminEmail'=>'jenya@idol-it.com',
            'itemsPerPage' => 9,
            'noImage' => 'static/images/no_foto.png',
            'cacheTime' => 3600
        ),
    ),
    CMap::mergeArray($mainEnvConfiguration, $mainLocalConfiguration)
);