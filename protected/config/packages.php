<?php
return array(
    'base' => array(
        'basePath' => 'webroot.static',
        'css' => array(
            'css/bootstrap.css',
            'css/animate.css',
            'css/font-awesome.min.css',
            'css/icon-fonts.css',
            'css/main.css',
            'css/responsive.css',
            'css/flexstrap.min.css',
            'plugins/pines-notify/jquery.pnotify.default.css',
        ),
        'js' => array(
            'js/jquery.js',
            'js/plugins.js',
            'js/jquery.main.js',
            'js/jquery.matchHeight.js',
            'js/store.js?v=1',
            'plugins/pines-notify/jquery.pnotify.min.js',
        ),
      //  'depends' => array('jquery'),
    ),
);