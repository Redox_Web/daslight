<?php

/**
 * Environment config on Djangoeurope
 */

return array(
    'components' => array(
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=daslight_main',
            'emulatePrepare' => true,
            'username' => 'daslight_root',
            'password' => 'qazwsxedcrfv',
            'charset' => 'utf8',
            'initSQLs'=>array("set time_zone='".date('P')."';"),
        ),
    )
);
