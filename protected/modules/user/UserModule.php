<?php
Yii::import('backend.components.WebModule');

class UserModule extends WebModule
{
    public $_localAssetsUrl;

	public function init()
	{
        parent::init();

		$this->setImport(array(
			'store.models.*',
			'store.components.*',
		));
	}

    public static function urlRules()
    {
        return array(

        );
    }
}
