<?php
Yii::import('backend.models.User');

class DefaultController extends Frontend
{
    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = Yii::createComponent('user.models.LoginForm');
        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->request->urlReferrer);
        } else
            throw new CHttpException(404, Yii::t('base', 'Page does not exist'));
    }

    public function actionRegister()
    {
        $register_form = new User('userinsert');

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($register_form);
            Yii::app()->end();
        }

        if (isset($_POST["User"])) {
            $register_form->attributes = $_POST["User"];
            $register_form->is_active = 1;

            if($register_form->save()) {
                $login = Yii::createComponent('user.models.LoginForm');
                $login->email = $register_form->email;
                $login->password = $_POST["User"]['password'];
                if($login->autoLogin()) {
                    Yii::app()->user->setFlash('user_register', Yii::t("base","Congratulations! You have registered successfully!"));
                    $this->redirect(Yii::app()->request->urlReferrer);
                }
            }
        } else
            throw new CHttpException(404, Yii::t('base', 'Page does not exist'));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
       // $this->redirect(Yii::app()->homeUrl);
        $this->redirect(Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)));
    }
}