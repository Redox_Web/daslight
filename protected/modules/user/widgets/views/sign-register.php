<div class="modal fade" id="signin">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sign In</h4>
            </div>
            <div class="modal-body">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'action' => array("/user/default/login"),
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => false
                    ),
                )); ?>
                    <div class="form-group">
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Email'))); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Password'))); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>
                    <button type="submit" class="button-block button-green">Sign in</button>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="register">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Register</h4>
            </div>
            <div class="modal-body">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'register-form',
                    'action' => array("/user/default/register"),
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => false
                    ),
                )); ?>
                    <div class="form-group">
                        <?php echo $form->textField($register_form, 'name', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Name'))); ?>
                        <?php echo $form->error($register_form, 'name'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($register_form, 'surname', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Surname'))); ?>
                        <?php echo $form->error($register_form, 'surname'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($register_form, 'email', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Email'))); ?>
                        <?php echo $form->error($register_form, 'email'); ?>
                    </div>
                <div class="form-group">
                    <?php echo $form->textField($register_form, 'phone', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Phone'))); ?>
                    <?php echo $form->error($register_form, 'phone'); ?>
                </div>
                    <div class="form-group">
                        <?php echo $form->passwordField($register_form, 'password', array('class' => "form-control", 'placeholder' => Yii::t("base", "Password"))); ?>
                        <?php echo $form->error($register_form, 'password'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->passwordField($register_form, 'password_repeat', array('class' => "form-control", 'placeholder' => Yii::t("base", "Password Repeat"))); ?>
                        <?php echo $form->error($register_form, 'password_repeat'); ?>
                    </div>
                    <button type="submit" class="button-block button-green"><?php echo Yii::t("base", "Register");?></button>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>