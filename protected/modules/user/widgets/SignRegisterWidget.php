<?php
Yii::import('user.models.LoginForm');
Yii::import('backend.models.User');

class SignRegisterWidget extends CWidget
{
    /**
     * @var string Id of elements
     */
    public $view = 'sign-register';

    public function run()
    {
        if(!Yii::app()->user->id)
            $this->render($this->view, array('model' => new LoginForm(), 'register_form' => new User()));
    }
}