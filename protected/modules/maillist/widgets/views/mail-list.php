<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-form',
    'action' => array("/maillist/frontMail/mail"),
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    ),
)); ?>
    <fieldset>
        <?php echo $form->textField($model, 'email', array('class' => 'email', 'placeholder' => 'Enter your email address for special offers')); ?>
        <button type="submit" class="emailsubmit"></button>
    </fieldset>
    <?php echo $form->error($model, 'email'); ?>
<?php $this->endWidget(); ?>