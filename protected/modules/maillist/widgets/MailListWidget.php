<?php
Yii::import('maillist.models.Maillist');

class MailListWidget extends CWidget
{
    public $view = 'mail-list';

    public function init()
    {
        $this->render($this->view, array('model' => new Maillist()));
    }
}