<?php
	$this->breadcrumbs=array(
        'Maillists'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage Maillist','url'=>array('admin')),
    );
    $this->title = "Create Maillist";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>