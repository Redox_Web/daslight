<?php
	$this->breadcrumbs=array(
        'Maillists'=>array('admin'),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create Maillist','url'=>array('create')),
	    array('label'=>'View Maillist','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage Maillist','url'=>array('admin')),
	);
    $this->title = "Update Maillist #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>