<?php
	$this->breadcrumbs=array(
        'Maillists'=>array('admin'),
        'Manage',
    );

    $this->title = "Manage Maillists";

    $urlJoin = Yii::app()->urlManager->getUrlFormat() == 'path' ? '?' : '&';
    Yii::app()->clientScript->registerScript('search', "
        $('#exportToExcel').click(function(){
            window.location = '". $this->createUrl('admin')  . $urlJoin . "' + $(this).parents('form').serialize() + '&export=true';
            return false;
        });
    ");
?>

<?php $t = '<i class="fa fa-download"></i>'.Yii::t('MaillistModule.maillist', 'Export to Excel (xls)'); ?>
<?php echo CHtml::htmlButton($t, array('id' => 'exportToExcel', 'class' => 'btn btn-success btn-label')); ?>
<?php $this->widget('DTbExcelView', array(
    'id'                   => 'maillist-grid',
    'dataProvider'         => $model->search(),
    'filter'               => $model,
    'skin'                 => 'sizer',
    'grid_mode'            => $production,

    'title'                => 'All emails - ' . date('d-m-Y H:i:s'),
    'borderColor'          => '000000', // Default: '000000'
    'bgColor'              => 'FFFFFF', // Default: 'FFFFFF'
    'textColor'            => '000000', // Default: '000000'
    'rowHeight'            => 25, // Default: 15
    'headerHeight'         => 25, // Default: 20
    'footerBorderColor'    => '000000', // Default: '000000'
    'columns'=>array(
        array('name'=>'id','headerHtmlOptions'=>array('width'=>'40px')),
        'email',
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>