<?php
	$this->breadcrumbs=array(
        'Maillists'=>array('admin'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'Create Maillist','url'=>array('create')),
        array('label'=>'Update Maillist','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete Maillist','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Maillist','url'=>array('admin')),
    );
    $this->title = "View Maillist # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'email',
),
)); ?>
