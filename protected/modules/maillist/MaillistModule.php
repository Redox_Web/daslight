<?php
Yii::import('backend.components.WebModule');

class MaillistModule extends WebModule
{
    public $_localAssetsUrl;

    public function init()
    {
        parent::init();

        $this->setImport(array(
            'maillist.models.*',
            'maillist.components.*',
            'maillist.widgets.*',
        ));
    }

    public static function menuItem()
    {
        return array(
            'icon' => 'fa fa-envelope',
            'label' => Yii::t('MaillistModule.maillist', 'Mail list'),
            'items' => array(
                array(
                    'label' => Yii::t('StoreModule.store', 'Mails'),
                    'url' => 'maillist/mail/admin',
                    'sidebar_tab' => 'maillist',
                ),
            )
        );
    }

    public static function urlRules()
    {
        return array(

        );
    }
}