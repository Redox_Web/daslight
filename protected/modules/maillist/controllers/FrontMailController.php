<?php
Yii::import('maillist.models.Maillist');

class FrontMailController extends Frontend
{
    public function actionMail()
    {
        $model = new Maillist();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'email-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST["Maillist"])) {
            $model->attributes = $_POST["Maillist"];

            if($model->save()) {
                Yii::app()->getUser()->setFlash(
                    FlashMessages::SUCCESS_MESSAGE(),
                    Yii::t('StoreModule.store', 'Thank you! You are subscribed to the newsletter.')
                );

                $this->redirect(Yii::app()->request->urlReferrer);
            }
        } else
            throw new CHttpException(404, Yii::t('base', 'Page does not exist'));
    }
}