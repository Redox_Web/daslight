<?php
Yii::import('backend.components.BackendController');

class MailController extends BackendController
{
    public $sidebar_tab = "maillist";

    public function actions()
    {
        return array(
            'admin' => array(
                'class' => 'DAdminAction',
                'excel' => true
            ),
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update' => 'DUpdateAction',
        );
    }

    public function createModel()
    {
        return new Maillist();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Maillist::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionExcel() {
        $model = new Maillist('search');
        $factory = new CWidgetFactory();
        $widget = $factory->createWidget($this, 'EExcelView', array(
            'dataProvider' => $model->search(),
            'grid_mode' => 'export',
            'title' => 'Title',
            'filename' => 'report.xlsx',
            'stream' => false,
            'exportType' => 'Excel2007',
            'columns' => array(
                'id',
                'email',
                array(
                    'class' => 'booster.widgets.TbButtonColumn',
                ),
            ),
        ));
        $widget->init();
        $widget->run();
    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='maillist-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}