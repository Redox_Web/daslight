<?php
Yii::import('booster.widgets.TbExtendedGridView');

class ExtendedGridView extends TbExtendedGridView
{
    private $uid;
    private $_modelName;
    /**
     * @var boolean whether to enable sizer
     * Defaults to true.
     */
    public $enableSizer = true;
    /**
     * @var string GET attribute
     */
    public $sizerAttribute = 'pageSize';
    /**
     * @var array items per page sizes variants
     */
    public $sizerVariants = array(10, 20, 50, 100);
    /**
     * @var string CSS class of sorter element
     */
    public $sizerCssClass = 'wraper-pagesizer';
    /**
     * @var string the text shown before sizer links. Defaults to empty.
     */
    public $sizerHeader = 'Show by: ';
    /**
     * @var string the text shown after sizer links. Defaults to empty.
     */
    /**
     * @var bool Hide the line with buttons
     */
    public $hideBulkActions = false;

    public $sizerFooter = ' pages in a row';
    public $inSummary = false;
    public $nested = false;
    public $sortUrl = '';

    public function init()
    {
        $this->_modelName = $this->dataProvider->modelClass;
        $this->uid = uniqid($this->_modelName);

//        $this->bulkActions = empty($this->bulkActions) ? [
//            'class' => 'booster.widgets.TbBulkActions',
//            'align' => 'right',
//            'actionButtons' => [
//                [
//                    'id' => 'delete-' . strtolower($this->_modelName),
//                    'buttonType' => 'button',
//                    'context' => 'danger',
//                    'size' => 'small',
//                    'label' => Yii::t('StoreModule.store', 'Delete'),
//                    'click' => 'js:function (values) { if(!confirm("' . Yii::t(
//                            'StoreModule.store',
//                            'Do you really want to delete selected elements?'
//                        ) . '")) return false; multiaction' . $this->uid . '("delete", values); }',
//                ],
//            ],
//            'checkBoxColumnConfig' => [
//                'name' => 'id'
//            ]
//        ] : $this->bulkActions;
        $this->bulkActions = [];

        if($this->inSummary) {
            $total=$this->dataProvider->getTotalItemCount();
            if($this->summaryText === null)
                $this->summaryText = Yii::t('zii','Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.',$total);
        }

        if (!isset($this->sizerVariants[0]))
            $this->sizerVariants = array(10);
        if ($this->enableSizer) {
            $pageSize = Yii::app()->request->getQuery($this->sizerAttribute, $this->sizerVariants[0]);
            $this->dataProvider->getPagination()->setPageSize($pageSize);
        }

        Booster::getBooster()->registerPackage('select2');

        if($this->enableSizer) {
            Yii::app()->clientScript->registerScript('pageSize',"
                var selectedPageSize = $('.change-pageSize').val();
                $('body').on('change', '.change-pageSize', function() {
                    $.fn.yiiGridView.update('".$this->id."',{ data:{ '".$this->sizerAttribute."': $(this).val() }});
                });
                $('.change-pageSize').select2({'minimumResultsForSearch':'Infinity','width':'resolve'}).select2('val', selectedPageSize);
            ");
        }

        /*if($this->filter) {
            Yii::app()->clientScript->registerScript('pageSize1',"
                $('#".ucfirst($this->dataProvider->model->getClass())."_is_active').select2({'minimumResultsForSearch':'Infinity'});
                $('#".ucfirst($this->dataProvider->model->getClass())."_user_id').select2({'minimumResultsForSearch':'Infinity'});
            ");
        }*/

        if($this->nested) {
            if(empty($this->sortUrl)) {
                $this->sortUrl = '/'.$this->controller->module->id .'/'.Yii::app()->controller->id.'/sort?attribute=ord';
            }

            $this->dataProvider->sort->defaultOrder = 'ord ASC';

            Yii::app()->clientScript->registerScript('nested',"
                var fixHelper = function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                };

                var sortable_table = {
                    init: function(url){
                        $('.grid-view table.items tbody').sortable({
                            forcePlaceholderSize: true,
                            forceHelperSize: true,
                            items: 'tr',
                            handle: '.handle-sortable',
                            update : function () {
                                serial = $('.grid-view table.items tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                                serial+='&onPage=';
                                serial+= $('#pageSize').val();
                                $.ajax({
                                    'url': url,
                                    'type': 'post',
                                    'data': serial,
                                    'success': function(data){
                                    },
                                    'error': function(request, status, error){
                                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                                    }
                                });
                            },
                            helper: fixHelper
                        }).disableSelection();
                    }
                }
                var active_page = $('.pagination.pull-right').find('li.active > a').html();
                sortable_table.init('$this->sortUrl' + '&page=' + active_page);
            ");
        }

        parent::init();
    }
    public function renderSizer()
    {
        if (!$this->enableSizer)
            return;

        $itemCount = $this->dataProvider->getTotalItemCount();
        if ($itemCount <= 0 || $itemCount < $this->sizerVariants[0])
            return;

        $render = null;
        $pageSize = $this->dataProvider->getPagination()->getPageSize();
        $pageVar = $this->dataProvider->getPagination()->pageVar;

        $render .= CHtml::openTag('div', array('class' => $this->sizerCssClass)) . "\n";
        $render .= $this->sizerHeader;

        $data = array();
        foreach($this->sizerVariants as $count)
        {
            $params = array_replace($_GET, array($this->sizerAttribute => $count));
            if (isset($params[$pageVar]))
                unset($params[$pageVar]);

            $data[$count] = $count;
        }

        $render .= CHtml::dropDownList(
            $this->sizerAttribute,
            $pageSize,
            $data,
            array('class'=>'change-pageSize'));

        $render .= $this->sizerFooter;
        $render .= CHtml::closeTag('div');

        if(preg_match('~{summary}~', $this->template) && $this->inSummary)
            $this->summaryText .= $render;
        else
            echo $render;
    }

    public function renderTableHeader()
    {
        echo "<thead>\n";

        if (!$this->hideBulkActions) {
            $this->renderBulkActions();
        }

        if (!$this->hideHeader) {
            if ($this->filterPosition === self::FILTER_POS_HEADER) {
                $this->renderFilter();
            }

            echo "<tr>\n";
            foreach ($this->columns as $column) {
                $column->renderHeaderCell();
            }
            echo "</tr>\n";

            if ($this->filterPosition === self::FILTER_POS_BODY) {
                $this->renderFilter();
            }
        } elseif ($this->filter !== null && ($this->filterPosition === self::FILTER_POS_HEADER || $this->filterPosition === self::FILTER_POS_BODY)) {
            $this->renderFilter();
        }
        echo "</thead>\n";
    }

    public function renderBulkActions()
    {
        Booster::getBooster()->registerAssetJs('jquery.saveselection.gridview.js');
//        $this->componentsAfterAjaxUpdate[] = "$.fn.yiiGridView.afterUpdateGrid('" . $this->id . "');";
//        echo '<tr><td colspan="' . count($this->columns) . '" class="grid-toolbar">';

        if (!empty($this->bulk)) {
            $this->bulk->renderButtons();
        }
        if (!empty($this->actionsButtons)) {
            if (is_array($this->actionsButtons)) {
                foreach ($this->actionsButtons as $button) {
                    echo $button;
                }
            } else {
                echo CHtml::link(
                    Yii::t('StoreModule.store', 'Add'),
                    [
                        '/' . $this->getController()->getModule()->getId() . '/' . lcfirst(
                            $this->_modelName
                        ) . 'Backend/create'
                    ],
                    ['class' => 'btn btn-success pull-right btn-sm']
                );
            }
        }
        echo '</td></tr>';
    }

    /**
     * Function for rendering multiaction:
     *
     *  JS function variables:
     *      status - type of action:
     *          1  - delete
     *          -------------------
     *
     * @return nothing
     */
    public function renderMultiaction()
    {
        Yii::app()->getClientScript()->registerScript(
            __CLASS__ . '#' . $this->id . 'ExMultiaction',
            'var multiaction' . $this->uid . ' = function (action, values) {
                var queryString = "";
                var url = "' . Yii::app()->getController()->createUrl('multiaction') . '";
                $.map(values, function (itemInput) {
                    queryString += ((queryString.length > 0) ? "&" : "") + "items[]=" + itemInput;
                });
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    data: "' . Yii::app()->getRequest()->csrfTokenName . '=' . Yii::app()->getRequest()->getCsrfToken(
            ) . '&model=' . $this->_modelName . '&do=" + action + "&" + queryString,
                    success: function (data) {
                        if (data.result) {
                            $.fn.yiiGridView.update("' . $this->id . '");
                        } else {
                            alert(data.data);
                        }
                    },
                    error: function (data) {alert("' . Yii::t('base', 'Error!') . '")}
                });
            }',
            CClientScript::POS_BEGIN
        );
    }
}