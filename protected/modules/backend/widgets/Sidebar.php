<?php
/**
 * Created by Idol IT.
 * Date: 10/1/12
 * Time: 3:30 PM
 */

class Sidebar extends CWidget
{

    public function init()
    {
        $this->render('sidebar');
    }

    public function renderModuleMenu() {
        $allModule = explode('|', MODULES_MATCHES);

        if(!empty($allModule)) {
            foreach($allModule as $moduleName) {
                $class = ucfirst($moduleName) . 'Module';
                Yii::import($moduleName . '.' . $class);

                if(method_exists($class, 'menuItem'))
                    $this->moduleMenu(call_user_func($class. '::menuItem'));
            }
        }
    }

    protected function moduleMenu($menu) {
        $sidebar_tab_array = array();
        $innrMenu = '';

        foreach($menu['items'] as $item) {
            $sidebar_tab_array[] = $item['sidebar_tab'];

            $innrMenu .= '<li '.$this->is_open($item['sidebar_tab'], 'li').'>';
                $innrMenu .= CHtml::openTag('a', array('href' => Yii::app()->createUrl($item['url'])));
                $innrMenu .= $item['label'];
                $innrMenu .= CHtml::closeTag('a');
            $innrMenu .= '</li>';
        }

        echo '<li '.$this->is_open($sidebar_tab_array).'>';
            echo CHtml::openTag('a', array('href' => 'javascript:;'));
                echo CHtml::openTag('i', array('class' => $menu['icon']));
                echo CHtml::closeTag('i');
                echo CHtml::tag('span', array(), $menu['label']);
            echo CHtml::closeTag('a');

            echo '<ul class="acc-menu" '.$this->is_open($sidebar_tab_array, 'ul').'>';
                echo $innrMenu;
            echo '</ul>';
        echo '</li>';
    }

    public function is_open($sidebarTab, $style=NULL) {
        switch($style) {
            case 'ul' :
                if(is_array($sidebarTab))
                    return in_array($this->controller->sidebar_tab, $sidebarTab) ? 'style="display:block"' : '';
                else
                    return $this->controller->sidebar_tab == $sidebarTab ? 'style="display:block"' : '';
                break;
            case 'li' :
                if(is_array($sidebarTab))
                    return in_array($this->controller->sidebar_tab, $sidebarTab) ? 'class="active"' : '';
                else
                    return $this->controller->sidebar_tab == $sidebarTab ? 'class="active"' : '';
                break;
            default:
                if(is_array($sidebarTab))
                    return in_array($this->controller->sidebar_tab, $sidebarTab) ? 'class="open active"' : '';
                else
                    return $this->controller->sidebar_tab == $sidebarTab ? 'class="open active"' : '';
        }
    }
}