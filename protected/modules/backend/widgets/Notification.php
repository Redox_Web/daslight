<?php
Yii::import('store.models.StoreOrder');

class Notification extends CWidget{

    public function init() {
        $newOrders = StoreOrder::model()->findAll('status = :status', array(':status' => StoreOrder::STATUS_NEW));
        if ($newOrders) {
            $this->render('notification_view', array('newOrders' => $newOrders, 'count' => count($newOrders)));
        }
    }
}