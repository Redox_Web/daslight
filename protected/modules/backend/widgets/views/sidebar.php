<?php
/**
 * @var $c string
 */
?>


<!-- BEGIN SIDEBAR -->
<nav id="page-leftbar" role="navigation">
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="acc-menu" id="sidebar">
        <li class="divider"></li>
        <li <?php echo $this->is_open('dashboard'); ?>>
            <a href="/backend">
                <i class="fa fa-home"></i> <span>Dashboard</span>
            </a>
        </li>

        <?php $this->renderModuleMenu(); ?>

        <li <?php echo $this->is_open(array('custom', 'general')); ?>>
            <a href="javascript:;">
                <i class="fa  fa-gear"></i>
                <span><?php echo Yii::t("backend", "Settings"); ?></span>
            </a>

            <ul class='acc-menu' <?php echo $this->is_open(array('custom', 'general'), 'ul'); ?>>
                <li <?php echo $this->is_open('general', 'li'); ?>>
                    <a href="<?php echo Yii::app()->createUrl("backend/settings/admin", array('id' => Settings::GENERAL)); ?>">
                        <?php echo Yii::t("BackendModule.backend", "General settings"); ?>
                    </a>
                </li>
                <li <?php echo $this->is_open('custom', 'li'); ?>>
                    <a href="<?php echo Yii::app()->createUrl("backend/settings/admin", array('id' => Settings::CUSTOM)); ?>">
                        <?php echo Yii::t("BackendModule.backend", "Custom settings"); ?>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</nav>
