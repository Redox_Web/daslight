<?php
/**
 * @var StoreOrder $order
 */
?>
<li class="dropdown">
    <a href="#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><i class="fa fa-bell"></i><span class="badge"><?php echo $count; ?></span></a>
    <ul class="dropdown-menu notifications arrow">
        <li class="dd-header">
            <span>You have <?php echo $count; ?> new order(s)</span>
        </li>
        <div class="scrollthis">
            <?php foreach($newOrders as $order) { ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl('store/storeOrder/update', array('id' => $order->id)); ?>" class="notification-order">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="msg">New order <?php echo $order->created; ?>. </span>
                </a>
            </li>
            <?php } ?>
        </div>
        <li class="dd-footer">
            <a href="<?php echo Yii::app()->createUrl('store/storeOrder/admin', array('StoreOrder[status]' => 'status_new')); ?>">View All New Orders</a>
        </li>
    </ul>
</li>