<?php
/**
 * Created by Idol IT.
 * Date: 9/29/12
 * Time: 5:57 PM
 */

class BackendController extends Controller
{
    public $layout = "/layouts/layout_panel";
    public $sidebar_tab;
    public $title;
    public $multiaplodImages;

    public $icon;
    public $tabTitle;

    public function init()
    {
        $this->layout = Yii::app()->getModule('backend')->getBackendLayoutAlias('layout_panel');
        if (!empty($this->sidebar_tab))
            Yii::app()->session["sidebar_tab"] = $this->sidebar_tab;
        parent::init();
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function showClip($clip_id) {
        $all_clips = $this->getClips();
        if(isset($all_clips[$clip_id])) {
            echo $all_clips[$clip_id];
        }
    }

    public function actionAjaxTags()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if(isset($_GET['q'])){
                $tags = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('tags')
                    ->where("tag LIKE :search", array(':search' => '%'.$_GET['q'].'%'))
                    ->limit(15)
                    ->queryAll();
                $returnArr = array();
                foreach($tags as $value) {
                    $returnArr[] = array('id' => $value['id'], 'text' => $value['tag']);
                }
                echo json_encode(array('results' => $returnArr));
            }
            Yii::app()->end();
        }
    }

    public function stringErrors($errors) {
        $stringError = array();
        foreach ($errors as $error) {
            foreach ($error as $err) {
                $stringError[] = $err;
            }
        }
        return implode(',<br>', $stringError);
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('upload', 'gallery','logout','login','error', 'additem', 'removeadd'),
                'users' => array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array(
                    'send',
                    'index',
                    'tree',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'admin',
                    'map',
                    'imagedel',
                    'updatepassword',
                    'search',
                    'adminStaff',
                    'adminMembers',
                    'ajaxTags',
                    'titleUpdate',
                    'mainimage',
                    'typeAttributesForm',
                    'related',
                    'add',
                    'widget',
                    'statusUpdate',
                    'sort',
                    'excel',
                    'blockSearch',
                    'category',
                    'quantityUpdate',
                    'ajaxSearch',
                    'productRow',
                    'updateInfo',
                    'createinfo',
                    'clearCache',
                ),
                'expression'=>'isset(Yii::app()->user->id) && User::model()->findByPk(Yii::app()->user->id)->is_staff',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
}