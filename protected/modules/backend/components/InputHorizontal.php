<?php
/**
 * Created by Idol IT.
 * Date: 10/4/12
 * Time: 4:58 PM
 */

Yii::import('booster.widgets.input.TbInputHorizontal');

class InputHorizontal extends TbInputHorizontal
{
    protected function getLabel()
    {
        if (isset($this->labelOptions['class']))
            $this->labelOptions['class'] .= ' col-sm-3 control-label';
        else
            $this->labelOptions['class'] = 'col-sm-3 control-label';

       if(isset($this->htmlOptions["required_element"]))
            $this->labelOptions['required'] = $this->htmlOptions["required_element"];
       if(isset($this->htmlOptions["labelOverride"]))
           return CHtml::label($this->htmlOptions["labelOverride"],CHtml::activeId($this->model,$this->attribute),$this->labelOptions);


        return parent::getLabel();
    }

    protected function textField()
    {
        echo $this->getLabel();
        echo '<div class="col-sm-6">';
        echo $this->getPrepend();
        echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getAppend();
        echo $this->getError().$this->getHint();
        echo '</div>';
    }

    protected function textArea()
    {
        if(!$this->data['tabs']) echo $this->getLabel();
        echo '<div class="controls">';
        if(!$this->data['tabs']) echo CHtml::openTag('div', array('class' => 'col-sm-6'));

        $this->widget('booster.widgets.TbButton', array(
            'label' => 'WYSIWYG Editor',
            'htmlOptions' => array(
                'data-toggle' => 'modal',
                'data-target' => $this->data['unique_name_id'],
                'class' => 'btn-green'
            ),
        ));
        echo '<br />';
        echo '<br />';

        echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
        if(!$this->data['tabs']) echo CHtml::closeTag('div');
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }
}