<?php $id = CHtml::activeId($model, $attribute); ?>
<div id="<?php echo $id; ?>">
    <?php $this->render("uploadifyRow/_images", array('model' => $model, "attribute" => $attribute,'relation'=>$relation)); ?>
</div>

<div class="control-group "><label class="control-label">&nbsp;</label>

    <div class="controls">
        <div class="fupload">

            <div id="file_upload-<?php echo $id; ?>">Select files</div>
            <p></p>

            <div id="file_uploadQueue" class="uploadifyQueue">
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">

    // load uploadify
    $(function () {

        var uploadify_id = "#file_upload-<?php echo $id; ?>";
        var gallery_id = "#<?php echo $id; ?>";

        $(uploadify_id).uploadFile({
            url:'<?php echo Yii::app()->createUrl(
                Yii::app()->controller->module->id . "/" . lcfirst(get_class($model)) . "/upload"
            ); ?>',
            fileName:"Filedata",
            uploadStr: "Select files",
            formData:{id:'<?php echo $model->id; ?>'},
            returnType: 'json',
            onSuccess:function(files,data,xhr,pd) {
                $.ajax({
                    type:"POST",
                    data:{id:'<?php echo $model->id; ?>'},
                    url:"<?php echo Yii::app()->createUrl(
                        Yii::app()->controller->module->id . "/" . lcfirst(get_class($model)) . "/gallery"
                    ); ?>",
                    success:function (output) {
                        $(gallery_id).html(output);
                    }
                });
            },
            onError: function(files,status,errMsg,pd) {
                console.log('error: '+errMsg);
            }
        });

        // common delete ajax method of images

        $("#codeprogress").on("click", '.delete', function () {
            var $this = $(this);
            $.ajax({
                type:"POST",
                data:{id:$this.attr("id")},
                url:"<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . "/" . lcfirst(get_class($model)) . "/imagedel"); ?>",
                success:function (msg) {
                    $this.closest(".element").fadeOut();
                }
            });
        });

        $("#codeprogress").on("click", '.main', function () {
            var self = $(this);
            $(".main").toggleClass('active');
            $.ajax({
                type:"POST",
                data:{id:this.id},
                url:"<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . "/" . lcfirst(get_class($model)) . "/mainimage"); ?>",
                success:function (data) {
                    if(data) {
                        $('.thumbnail-box').removeClass('active');
                        self.closest('.thumbnail-box').toggleClass('active');
                    }
                },
                error:function(data){
                    console.log(data);
                }
            });
        });
    });

</script>