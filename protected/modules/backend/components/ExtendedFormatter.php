<?php
/**
 * Created by Idol IT.
 * Date: 10/3/12
 * Time: 11:51 AM
 */

class ExtendedFormatter extends CFormatter
{
    public $dateFormat = 'd/m/Y';
    public $datetimeFormat='d/m/Y H:i:s';

    public function formatBoolean($value) {

        if ($value > 0)
            return '<i class="fa fa-check"></i>';
        else
            return '<i class="fa fa-minus"></i>';
    }

    public function formatSex($value) {
        if ($value > 0)
            return 'Female';
        else
            return 'Male';

    }

    public function formatImage($value) {
        $path_info = pathinfo($value);
        if(!empty($path_info) && isset($path_info['extension']) && $path_info['extension'] == 'swf')
            return 'swf file';

        return CHtml::image(Yii::app()->iwi->load($value)->adaptive(80, 80)->cache());
    }

    public function formatUser($value) {
        if ($user = User::model()->findByPk($value))
            return CHtml::link($user->name, Yii::app()->createUrl("backend/user/view", array("id" => $user->id)));
        else
            return '<span class="null">System</span>';
    }

    public function formatStoreType($value) {
        $ar = StoreAttribute::getTypesList();
        return $ar[$value];
    }

    public function formatFontAwsome($value) {
        return CHtml::tag('i', array('class' => 'fa '.$value));
    }

    public function formatLang($value) {
        return StoreAttribute::model()->getLangList($value);
    }

    public function formatTarget($value) {
        return Slider::model()->getMenuTargetList($value);
    }

    public function formatStoreProducer($value) {
        if ($producer = StoreProducer::model()->findByPk($value))
            return CHtml::link($producer->name, Yii::app()->createUrl("store/storeProducer/view", array("id" => $producer->id)));
    }

    public function formatStoreCategory($value) {
        if ($category = StoreCategory::model()->findByPk($value))
            return CHtml::link($category->title, Yii::app()->createUrl("store/storeCategory/view", array("id" => $category->id)));
    }

    public function formatOrderstatus($value) {
        return StoreOrder::model()->getStatusList($value);
    }

    public function formatBannerType($value) {
        return Banner::model()->getbannerType($value);
    }

    public function formatBannerPosition($value) {
        return Banner::model()->getBannerPosition($value);
    }
}