<?php
class UploadAction extends DCrudAction
{
    public $_alreadyFirst;
    public $model;

    public function run()
    {
        if (!empty($_FILES)) {
            if (is_uploaded_file($_FILES["Filedata"]["tmp_name"])) {
                $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
                $fileParts = pathinfo($_FILES['Filedata']['name']);
                if (in_array($fileParts['extension'], $fileTypes)) {
                    $id = (int)$_POST["id"];
                    $this->model = $this->createModel();

                    Yii::log("hello", "error");
                    $hash = md5(rand(1, 99999) . $_FILES["Filedata"]["name"]) . "_" . $_FILES["Filedata"]["name"];
                    $dir = "images/site/".$this->model->getClass()."/" . $id . "/";

                    if (!is_dir($dir)) {
                        mkdir($dir, 0755, true);
                    }

                    move_uploaded_file(
                        $_FILES["Filedata"]["tmp_name"],
                        "images/site/".$this->model->getClass()."/" . $id . "/" . $hash
                    );

                    try {
                        $file = new MultipleImages();
                        $file->item_id = $id;
                        $file->content_type = $this->model->getClass();
                        $file->path = "images/site/".$this->model->getClass()."/" . $id . "/" . $hash;

                        if (!$this->checkFirst($id)) {
                            $file->is_main = 1;
                        } else {
                            $file->is_main = 0;
                        }

                        $file->save();
                    } catch (Exception $e) {
                        CHtml::errorSummary('[Exeption] code: ' . $e->getCode() .
                                            ' Line: ' . $e->getLine() . ' ' . $e->getMessage(), "error");
                    }

                    echo '{"status":"success"}';
                    exit;
                } else {
                    header("HTTP/1.1 405"); //any 4XX error will work
                    exit();
                }
            } else {
                echo '{"status":"error"}';
                exit;
            }
        }
    }

    private function checkFirst($item_id)
    {
        $multipleImages = MultipleImages::model()->count(
            'content_type = :contentType && item_id = :item_id',
            array(':contentType' => $this->model->getClass(), ':item_id' => $item_id)
        );

        return $multipleImages > 0 ? true : false;
    }
}
