<?php
class GalleryAction extends DCrudAction
{
    public function run()
    {
        $id = (int)$_POST["id"];
        $model = $this->createModel();
        $model = $model->findByPk($id);
        echo $this->controller->renderPartial('application.modules.backend.views.multiapload._images', array("model" => $model));
    }
}