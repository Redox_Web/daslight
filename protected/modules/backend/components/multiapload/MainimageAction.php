<?php
class MainimageAction extends DCrudAction
{
    public function run()
    {
        if(isset($_POST['id']))
        {
            $id = (int)$_POST["id"];
            $contentType = $this->createModel();
            $model = MultipleImages::model()->findByPk($id);
            if(!$model)
                throw new CHttpException(400,Yii::t("base","No such record"));

            Yii::log($contentType->getClass(), "error");
            $multipleImages = MultipleImages::model()->findAll('content_type = :contentType && item_id = :item_id', array(':contentType' => $contentType->getClass(), ':item_id' => $model->item_id));

            foreach($multipleImages as $image) {
                $image->is_main = 0;
                $image->update();
            }

            $model->is_main = 1;
            if($model->update())
                echo true;
            else
                echo false;
        }
        else
            throw new CHttpException(400,Yii::t("base","POST is empty"));
    }
}