<?php
/**
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 * @version 1.0
 */

class DCreateAction extends DCrudAction
{
    /**
     * @var string view file for rendering
     */
    public $view = 'create';
    public $redirect = 'view';

    public function run()
    {
        $model = $this->createModel();

        $modelName = get_class($model);

        if(isset($_POST[$modelName]))
        {
            $model->attributes = $_POST[$modelName];

            $this->clientCallback('beforeCreate', $model);
            //$this->clientCallback('performAjaxValidation', $model);

            $method = $model->asa('nestedSetBehavior') ? 'saveNode' : 'save';
            if($model->{$method}()) {
                if($this->redirect == 'view')
                    $this->redirectToView($model);
                else
                    $this->redirectToManagePage();
            }
        }

        $this->controller->render($this->view, array(
            'model'=>$model,
        ));
    }
}
