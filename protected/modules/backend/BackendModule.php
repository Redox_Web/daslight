<?php
/**
 * Created by Idol IT.
 * Date: 9/29/12
 * Time: 5:35 PM
 */
Yii::import('backend.components.WebModule');

class BackendModule extends WebModule
{
    public function init()
    {
        parent::init();

        $this->setImport(array(
            'backend.models.*',
            'backend.components.*',
            'backend.widgets.*',
            'backend.controllers.*',
            'backend.actions.*',
            'backend.components.crud.*',
        ));

        $this->setComponents(
            array(
                'user' => array(
                    'class' => 'WebUser',
                    'loginUrl' => Yii::app()->createUrl('backend/default/login'),
                    'returnUrl' => array('backend'),
                ),
            )
        );

        Yii::app()->getComponent('booster');
        Yii::app()->errorHandler->errorAction = '/backend/default/error';

        if (!Yii::app()->request->isAjaxRequest) {
            $packages = require_once(Yii::getPathOfAlias("backend").'/config/packages.php');

            if(is_array($packages)) {
                foreach ($packages as $name => $definition) {
                    $this->cs->addPackage($name, $definition);
                }
            }

            $this->cs->coreScriptPosition = CClientScript::POS_HEAD;
            $this->cs->scriptMap = array(
                'jquery.js' => $this->assetsUrl.'/js/jquery-1.10.2.min.js',
            );
        }
    }

    public static function menuItem()
    {
        return array(
            'icon' => 'fa fa-user',
            'label' => Yii::t('BackendModule.backend', 'User management'),
            'items' => array(
                array(
                    'label' => Yii::t('BackendModule.backend', 'Customers'),
                    'url' => Yii::app()->createUrl("backend/user/admin", array('id' => User::MEMBER)),
                    'sidebar_tab' => 'member',
                ),
                array(
                    'label' => Yii::t('BackendModule.backend', 'Administrators'),
                    'url' => Yii::app()->createUrl("backend/user/admin", array('id' => User::STAFF)),
                    'sidebar_tab' => 'staff',
                ),
            )
        );
    }

    public static function getUrlRules()
    {
        return array(
            'backend'=>'backend/default/index',
        );
    }

    protected function createTable()
    {
        if (!Yii::app()->getDb()->schema->getTable('gm_markers')) {
            Yii::app()->getDb()->createCommand()->createTable("gm_markers", array(
                'id' => 'pk',
                'identity' => 'string',
                'avatar' => 'string',
                'network' => 'string',
                'name' => 'varchar(80)',
                'surname' => 'varchar(80)',
                'email' => 'string',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
                'name' => '	varchar(80)',
            ),'ENGINE=InnoDB');
        }

        if (!Yii::app()->getDb()->schema->getTable('gm_param')) {
            Yii::app()->getDb()->createCommand()->createTable("gm_param", array(
                'id' => 'pk',
                'param' => 'varchar(20)',
                'value' => 'varchar(255)',
            ),'ENGINE=InnoDB');

            $sql = "insert into gm_param (param) values ('zoom'),('center')";
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    public static function getBackendLayoutAlias($layoutName = '')
    {
        return 'application.modules.backend.views.layouts.'.($layoutName ? $layoutName : 'main');
    }
}
