<?php
    $this->title = 'Manage customers';
    $this->breadcrumbs=array(
        'Manage Member'=>array('admin', 'id' => User::MEMBER),
        'Manage',
    );
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'editable_wrapper',
    'itemsCssClass' => 'items table table-striped table-bordered table-condensed',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(User::MEMBER),
    'filter'=>$model,
    'columns'=>array(
        array('name'=>'id','headerHtmlOptions'=>array('width'=>'40px')),
        'name',
        'email',
        'is_active:boolean',
        'last_login',
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
            'template' => '{view} {update} {delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => '/images/icons/delete.png',
                    'visible'=>'$data->is_staff != 1',
                ),
            )
        ),
    ),
)); ?>


