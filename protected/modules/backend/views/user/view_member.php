<?php
    $this->title = 'View Member #'.$model->id;
    $this->menu=array(
        array('label'=>'Update Member','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete Member','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Member','url'=>array('admin', 'id' => User::MEMBER)),
    );
    $this->breadcrumbs=array(
        'Manage Member'=>array('admin', 'param' => User::MEMBER),
        $model->id,
    );
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes' => array(
        'id',
        'name',
        'surname',
        'email',
        'avatar:image',
        'is_active:boolean',
        'is_staff:boolean',
        'last_login:dateTime',
        'date_joined:dateTime',
    )
)); ?>
