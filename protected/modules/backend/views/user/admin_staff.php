<?php
    $this->title = 'Manage administrators';
    $this->breadcrumbs=array(
        'Manage Administrator'=>array('admin', 'id' => User::STAFF),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create Administrator','url'=> array('create')),
    );
?>

<div class="example-box-wrapper">
    <?php $this->widget('booster.widgets.TbGridView', array(
        'id' => 'user-grid',
        'dataProvider' => $model->search(User::STAFF),
        'type' => 'striped bordered condensed',
        'filter' => $model,
        'columns' => array(
            array('name'=>'id','headerHtmlOptions'=>array('width'=>'40px')),
            'name',
            'email',
            'is_active:boolean',
            'last_login',
            array(
                'class' => 'backend.components.ButtonColumn',
                'htmlOptions' => array('width' => '70px'),
                'template' => '{view} {update} {delete}',
                'buttons' => array(
                    'delete' => array(
                        'imageUrl' => '/images/icons/delete.png',
                        'visible'=>'$data->id != 1',
                    ),
                    'update' => array(
                        'visible'=>'$data->id == Yii::app()->user->id || Yii::app()->user->id == 1',
                    ),
                    'delete' => array(
                        'visible'=>'$data->id == Yii::app()->user->id || Yii::app()->user->id == 1',
                    ),
                ),
            ),
        ),
    )); ?>
</div>

