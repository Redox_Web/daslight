<?php
    $this->title = 'View Administrator #'.$model->id;;
    $this->menu=array(
        array('label'=>'Create Admin','url'=>array('create', 'id' => User::STAFF)),
        array('label'=>'Update Admin','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete Admin','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Admin','url'=>array('admin', 'id' => User::STAFF)),
    );
    $this->breadcrumbs=array(
        'Manage Admin'=>array('admin', 'id' => User::STAFF),
        $model->id,
    );
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes' => array(
        'id',
        'name',
        'surname',
        'email',
        'avatar:image',
        'is_active:boolean',
        'is_staff:boolean',
        'last_login:dateTime',
        'date_joined:dateTime',
    )
)); ?>
