<?php
    $this->title = 'View Member #'.$model->id;
    $this->menu=array(
        array('label'=>'Create Member','url'=>array('create', 'id' => User::MEMBER)),
        array('label'=>'View Member','url'=>array('view','id'=>$model->id)),
        array('label'=>'Manage Member','url'=>array('admin', 'id' => User::MEMBER)),
    );
    $this->breadcrumbs=array(
        'Manage Member'=>array('admin', 'id' => User::MEMBER),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );
?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>