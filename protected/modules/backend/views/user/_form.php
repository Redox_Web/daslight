<?php $form = $this->beginWidget('backend.components.ActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
    'type'=>'horizontal',
    'htmlOptions' => array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model, 'name', array('class' => 'form-control', 'maxlength' => 255)); ?>

    <?php echo $form->textFieldGroup($model, 'surname', array('class' => 'form-control', 'maxlength' => 255)); ?>

    <?php echo $form->textFieldGroup($model, 'email', array('class' => 'form-control', 'maxlength' => 255)); ?>

    <?php echo $form->fileFieldGroup($model,'avatar',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php echo $form->checkboxGroup($model, 'is_active'); ?>

    <?php if($model->isNewRecord) : ?>
        <?php echo $form->passwordFieldGroup($model,'password',array('class'=>'span5','maxlength'=>64)); ?>
        <?php echo $form->passwordFieldGroup($model,'password_repeat',array('class'=>'span5','maxlength'=>64)); ?>
    <?php endif; ?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>



