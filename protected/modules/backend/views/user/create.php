<?php
    $this->title = 'Create Administrator';
    $this->menu=array(
        array('label'=>'Manage Admnistrator','url'=>array('admin', 'id' => User::STAFF)),
    );
    $this->breadcrumbs=array(
        'Manage Admnistrator'=>array('admin', 'id' => User::STAFF),
        'Create',
    );
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
