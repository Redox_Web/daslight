<?php
    $this->title = 'View Administrator #'.$model->id;
    $this->menu=array(
        array('label'=>'Change Password','url'=>array('updatePassword','id'=>$model->id)),
        array('label'=>'Create Admin','url'=>array('create', 'id' => User::STAFF)),
        array('label'=>'View Admin','url'=>array('view','id'=>$model->id)),
        array('label'=>'Manage Admin','url'=>array('admin', 'id' => User::STAFF)),
    );
    $this->breadcrumbs=array(
        'Manage Admin'=>array('admin', 'id' => User::STAFF),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );
?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>