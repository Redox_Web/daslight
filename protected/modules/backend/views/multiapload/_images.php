<div class="heading clearfix">
    <h3 class="pull-left">Images</h3>
</div>
<div class="control-group "><label class="control-label">&nbsp;</label>
    <div class="controls">
        <div class="wmk_grid">
            <?php if (count($model->contypeImagesList) > 0) { ?>
                <div class="row">
                    <?php
                        foreach ($model->contypeImagesList as $image)
                            echo $this->renderPartial("application.modules.backend.views.multiapload._image", array('image'=>$image));
                    ?>
                </div>
            <?php } else { ?>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    There is no images. Click <strong>Select files</strong> to upload images.
                </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>