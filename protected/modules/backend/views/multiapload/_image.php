<div class="col-lg-3 col-md-4 col-sm-6 element thumb-image">
    <div class="thumbnail-box <?php echo ($image->is_main ? "active" : "")?>">
        <a id="<?php echo $image->id; ?>" title="" href="#" class="thumb-link delete"></a>
        <div class="thumb-content">
            <div class="center-vertical">
                <div class="center-content">
                    <i class="icon-helper icon-center animated fadeIn font-white glyph-icon icon-linecons-search"></i>
                </div>
            </div>
            <?php if($this->action->id != 'view') { ?>
                <a href="javascript:void(0)" title="Set Main" class="main" id="<?php echo $image->id; ?>">
                    <i class="fa fa-flag" id="a<?php echo $image->id; ?>"></i>
                </a>
                <a href="javascript:void(0)" title="Remove" class="delete" id="<?php echo $image->id; ?>">
                    <i class="fa fa-trash-o"></i>
                </a>
            <?php } ?>
		</div>
        <div class="thumb-overlay bg-black"></div>
        <a rel="group1" href="<?php echo Yii::app()->iwi->load($image->path)->adaptive(600, 600)->cache(); ?>" class="images-group">
            <img alt="" src="<?php echo YHelper::getImagePath($image->path, 358, 358); ?>">
        </a>
    </div>
</div>