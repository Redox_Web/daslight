<?php
return array(
    'sizer' => array(
        'nested' => true,
        'inSummary' => true,
        'type' => 'striped bordered condensed',
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'template'=>"{sizer}\n{summary}\n{items}\n{pager}",
        'afterAjaxUpdate' => "js:function() {
            var selectedPageSize = $('.change-pageSize').val();
            $('.change-pageSize').select2({'minimumResultsForSearch':'Infinity','width':'resolve'}).select2('val', selectedPageSize);
            sortable_table.init('/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/sort?attribute=ord');
        }"
    ),
    'nested' => array(
        'enableSizer' => false,
        'nested' => true,
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'type' => 'striped bordered condensed',
        'afterAjaxUpdate' => "js:function() {
            sortable_table.init('/".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/sort?attribute=ord');
        }"
    ),
);
