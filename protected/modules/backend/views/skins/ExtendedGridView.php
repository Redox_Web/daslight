<?php
return array(
    'nested' => array(
        'enableSizer' => true,
        'nested' => true,
        'rowCssClassExpression'=>'"items[]_{$data->id}"',
        'template'=>"{sizer}\n{summary}\n{items}\n{pager}",
        'inSummary' => true,
        'type' => 'striped bordered condensed',
        'afterAjaxUpdate' => "js:function() {
            var active_page = $('.pagination.pull-right').find('li.active > a').html();
            sortable_table.init('/store/".Yii::app()->controller->id."/sort?attribute=ord&page=' + active_page);
            var selectedPageSize = $('.change-pageSize').val();
            $('.change-pageSize').select2({'minimumResultsForSearch':'Infinity','width':'resolve'}).select2('val', selectedPageSize);
        }"
    ),
);
