<?php
    $this->title = 'Update "'.$model->name.'" Setting';
    $this->menu=array(
        Yii::app()->user->id == 1 ? array('label'=>'Create Setting','url'=>array('create')) : '',
        array('label'=>'View Setting','url'=>array('view','id'=>$model->id)),
        array('label'=>'Manage Settings','url'=>array('admin', 'id' => Settings::CUSTOM)),
    );
    $this->breadcrumbs=array(
        'Manage Settings'=>array('admin', 'id' => Settings::CUSTOM),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );
?>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'group' => $group)); ?>