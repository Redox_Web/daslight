<?php
    $this->title = 'Create Setting';
    $this->menu=array(
        array('label'=>'Manage Custom Settings','url'=>array('admin', 'id' => Settings::CUSTOM)),
    );
    $this->breadcrumbs=array(
        'Custom Settings'=>array('admin', 'id' => Settings::CUSTOM),
        'Create',
    );
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>