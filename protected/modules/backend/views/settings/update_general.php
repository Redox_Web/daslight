<?php
    $this->title = 'Update "'.$model->name.'" Setting';
    $this->menu=array(
        array('label'=>'View Setting','url'=>array('view','id'=>$model->id)),
        array('label'=>'Manage Settings','url'=>array('admin', 'id' => Settings::GENERAL)),
    );
    $this->breadcrumbs=array(
        'Manage Settings'=>array('admin', 'id' => Settings::GENERAL),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );
?>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'group' => $group)); ?>