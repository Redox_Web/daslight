<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php //echo CHtml::encode($this->pageTitle); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Atlant">
    <meta name="author" content="The Red Team">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="assets/js/less.js"></script> -->
</head>

<body class="focusedform">

<?php echo $content;?>

</body>
</html>