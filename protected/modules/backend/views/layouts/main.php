<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Atlant</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Atlant">
    <meta name="author" content="The Red Team">
    <?php if($favicon_src = YHelper::yiisetting('favicon_site')){ ?>
        <link rel="icon" href="<?php echo Yii::app()->iwi->load($favicon_src)->adaptive(16, 16)->cache(); ?>" type="image/png" />
    <?php } ?>

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo $this->module->assetsUrl; ?>/css/ie8.css">
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="">
<div id="headerbar">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-2">
                <a href="#" class="shortcut-tiles tiles-brown">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-pencil"></i></div>
                    </div>
                    <div class="tiles-footer">
                        Create Post
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-sm-2">
                <a href="#" class="shortcut-tiles tiles-grape">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-group"></i></div>
                        <div class="pull-right"><span class="badge">2</span></div>
                    </div>
                    <div class="tiles-footer">
                        Contacts
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-sm-2">
                <a href="#" class="shortcut-tiles tiles-primary">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-envelope-o"></i></div>
                        <div class="pull-right"><span class="badge">10</span></div>
                    </div>
                    <div class="tiles-footer">
                        Messages
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-sm-2">
                <a href="#" class="shortcut-tiles tiles-inverse">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-camera"></i></div>
                        <div class="pull-right"><span class="badge">3</span></div>
                    </div>
                    <div class="tiles-footer">
                        Gallery
                    </div>
                </a>
            </div>

            <div class="col-xs-6 col-sm-2">
                <a href="#" class="shortcut-tiles tiles-midnightblue">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-cog"></i></div>
                    </div>
                    <div class="tiles-footer">
                        Settings
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-sm-2">
                <a href="#" class="shortcut-tiles tiles-orange">
                    <div class="tiles-body">
                        <div class="pull-left"><i class="fa fa-wrench"></i></div>
                    </div>
                    <div class="tiles-footer">
                        Plugins
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>

<?php $this->widget('Nav'); ?>

<div id="page-container">
<!-- BEGIN SIDEBAR -->
<?php $this->widget('Sidebar'); ?>

<!-- BEGIN RIGHTBAR -->
<div id="page-rightbar">

    <div id="chatarea">
        <div class="chatuser">
            <span class="pull-right">Jane Smith</span>
            <a id="hidechatbtn" class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
        </div>
        <div class="chathistory">
            <div class="chatmsg">
                <p>Hey! How's it going?</p>
                <span class="timestamp">1:20:42 PM</span>
            </div>
            <div class="chatmsg sent">
                <p>Not bad... i guess. What about you? Haven't gotten any updates from you in a long time.</p>
                <span class="timestamp">1:20:46 PM</span>
            </div>
            <div class="chatmsg">
                <p>Yeah! I've been a bit busy lately. I'll get back to you soon enough.</p>
                <span class="timestamp">1:20:54 PM</span>
            </div>
            <div class="chatmsg sent">
                <p>Alright, take care then.</p>
                <span class="timestamp">1:21:01 PM</span>
            </div>
        </div>
        <div class="chatinput">
            <textarea name="" rows="2"></textarea>
        </div>
    </div>

    <div id="widgetarea">
        <div class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#accsummary"><h4>Account Summary</h4></a>
            </div>
            <div class="widget-body collapse in" id="accsummary">
                <div class="widget-block" style="background: #7ccc2e; margin-top:10px;">
                    <div class="pull-left">
                        <small>Current Balance</small>
                        <h5>$71,182</h5>
                    </div>
                    <div class="pull-right"><div id="currentbalance"></div></div>
                </div>
                <div class="widget-block" style="background: #595f69;">
                    <div class="pull-left">
                        <small>Account Type</small>
                        <h5>Business Plan A</h5>
                    </div>
                    <div class="pull-right">
                        <small class="text-right">Monthly</small>
                        <h5>$19<small>.99</small></h5>
                    </div>
                </div>
                <span class="more"><a href="#">Upgrade Account</a></span>
            </div>
        </div>


        <div id="chatbar" class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#chatbody"><h4>Online Contacts <small>(5)</small></h4></a>
            </div>
            <div class="widget-body collapse in" id="chatbody">
                <ul class="chat-users">
                    <li data-stats="online"><a href="javascript:;"><img src="<?php echo $this->module->assetsUrl; ?>/demo/avatar/potter.png" alt=""><span>Jeremy Potter</span></a></li>
                    <li data-stats="online"><a href="javascript:;"><img src="<?php echo $this->module->assetsUrl; ?>/demo/avatar/tennant.png" alt=""><span>David Tennant</span></a></li>
                    <li data-stats="online"><a href="javascript:;"><img src="<?php echo $this->module->assetsUrl; ?>/demo/avatar/johansson.png" alt=""><span>Anna Johansson</span></a></li>
                    <li data-stats="busy"><a href="javascript:;"><img src="<?php echo $this->module->assetsUrl; ?>/demo/avatar/jackson.png" alt=""><span>Eric Jackson</span></a></li>
                    <li data-stats="away"><a href="javascript:;"><img src="<?php echo $this->module->assetsUrl; ?>/demo/avatar/jobs.png" alt=""><span>Howard Jobs</span></a></li>
                </ul>
                <span class="more"><a href="#">See all</a></span>
            </div>
        </div>

        <div class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#taskbody"><h4>Pending Tasks <small>(5)</small></h4></a>
            </div>
            <div class="widget-body collapse in" id="taskbody">
                <div class="contextual-progress" style="margin-top:10px;">
                    <div class="clearfix">
                        <div class="progress-title">Backend Development</div>
                        <div class="progress-percentage"><span class="label label-info">Today</span> 25%</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" style="width: 25%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Bug Fix</div>
                        <div class="progress-percentage"><span class="label label-primary">Tomorrow</span> 17%</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-primary" style="width: 17%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Javascript Code</div>
                        <div class="progress-percentage">70%</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" style="width: 70%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">Preparing Documentation</div>
                        <div class="progress-percentage">6%</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" style="width: 6%"></div>
                    </div>
                </div>
                <div class="contextual-progress">
                    <div class="clearfix">
                        <div class="progress-title">App Development</div>
                        <div class="progress-percentage">20%</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-orange" style="width: 20%"></div>
                    </div>
                </div>

                <span class="more"><a href="ui-progressbars.htm">View all Pending</a></span>
            </div>
        </div>



        <div class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#storagespace"><h4>Storage Space</h4></a>
            </div>
            <div class="widget-body collapse in" id="storagespace">
                <div class="clearfix" style="margin-bottom: 5px;margin-top:10px;">
                    <div class="progress-title pull-left">1.31 GB of 1.50 GB used</div>
                    <div class="progress-percentage pull-right">87.3%</div>
                </div>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" style="width: 50%"></div>
                    <div class="progress-bar progress-bar-warning" style="width: 25%"></div>
                    <div class="progress-bar progress-bar-danger" style="width: 12.3%"></div>
                </div>
            </div>
        </div>

        <div class="widget">
            <div class="widget-heading">
                <a href="javascript:;" data-toggle="collapse" data-target="#serverstatus"><h4>Server Status</h4></a>
            </div>
            <div class="widget-body collapse in" id="serverstatus">
                <div class="clearfix" style="padding: 10px 24px;">
                    <div class="pull-left">
                        <div class="easypiechart" id="serverload" data-percent="67">
                            <span class="percent"></span>
                        </div>
                        <label for="serverload">Load</label>
                    </div>
                    <div class="pull-right">
                        <div class="easypiechart" id="ramusage" data-percent="20.6">
                            <span class="percent"></span>
                        </div>
                        <label for="ramusage">RAM: 422MB</label>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END RIGHTBAR -->
<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <?php $this->widget('booster.widgets.TbBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
                'separator'=>'',
                'homeLink'=>"<a href=".Yii::app()->createAbsoluteurl('backend').">". Yii::t("backend", "Main page")."</a>"
            )); ?>
            <h1><?php echo $this->title; ?></h1>

            <div class="options">
                <div class="btn-toolbar">
                    <?php
                    $this->widget('ExtendedMenu', array(
                        'items' => $this->menu,
                    ));
                    ?>
                </div>
            </div>
        </div>

        <div class="container">
            <?php echo $content; ?>
        </div> <!-- container -->
    </div> <!--wrap -->
</div> <!-- page-content -->

<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li>ATLANT &copy; 2015</li>
        </ul>
        <a href="#top" class="pull-right btn btn-inverse-alt btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></a>
    </div>
</footer>

</div> <!-- page-container -->

</body>
<?php $this->widget('FlashMessages'); ?>
</html>