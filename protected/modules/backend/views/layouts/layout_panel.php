<?php $this->beginContent(Yii::app()->getModule('backend')->getBackendLayoutAlias('main')); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-indigo">
                <div class="panel-heading">
                    <h4><?php echo $this->title; ?></h4>
                </div>
                <div class="panel-body" style="border-radius: 0px;">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent() ; ?>