<?php
/**
 * @var $this BackendController
 * @var $model BackendLoginForm
 * @var $form CActiveForm
 */
?>

<div class="verticalcenter">
    <img src="<?php echo $this->module->assetsUrl; ?>/img/logo-big.png" alt="Logo" class="brand" />
    <div class="panel panel-primary">
        <div class="panel-body">
            <h4 class="text-center" style="margin-bottom: 25px;">Log in to administration part</h4>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'htmlOptions' => array(
                    'class' => 'form-horizontal',
                    'style' => 'margin-bottom: 0px !important;'
                ),
            )); ?>
            <?php if($model->hasErrors('password')): ?>
                <div class="alert alert-error alert-login">
                    <?php echo $form->error($model, 'password'); ?>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <label for="username" class="control-label col-sm-4" style="text-align: left;">Username</label>
                <div class="col-sm-8">
                    <?php echo $form->textField($model, 'email', array('placeholder' => 'Email', 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="control-label col-sm-4" style="text-align: left;">Password</label>
                <div class="col-sm-8">
                    <?php echo $form->passwordField($model, 'password', array('placeholder' => 'Password', 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-right">
                    <label>
                        <?php echo $form->checkBox($model, 'rememberMe',array('style'=>'margin-bottom: 20px')); ?>
                        Remember Me
                    </label>
                </div>
            </div>
            <?php echo CHtml::submitButton('Log In', array("class" => "btn btn-primary btn-block")); ?>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>