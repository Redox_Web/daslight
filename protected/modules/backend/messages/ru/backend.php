<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
    ':name was successfully created.' => ':name было успешно создано.',
    ':name was successfully updated.' => ':name было успешно обновлено.',
    'Administrators' => 'Администраторы',
    'Album Images' => 'Альбомные изображения',
    'Create AlbumImage' => 'Добавить изображение',
    'Custom settings' => 'Особые настрйоки',
    'Customers' => 'Клиенты',
    'DToggleAction::attributes is empty' => 'DToggleAction::attributes пуст',
    'General settings' => 'Общие настройки',
    'Howdy' => 'Привет',
    'Manage Album Image' => 'Управление изображениями',
    'Missing attribute {attribute}' => 'Не хватает атрибута {attribute}',
    'Password of user :name was changed successfully.' => 'Пароль пользователя :name был успешно обновлен.',
    'Pictures' => 'Изображения',
    'Settings' => 'Настройки',
    'User management' => 'Пользователи',
    'Main page' => 'Главная страница',
);