<?php
/**
 * Created by Idol IT.
 * Date: 03/15/13
 * Time: 18:22
 */

return array(
    'backendbase' => array(
        'baseUrl' => $this->assetsUrl,
        'css' => array(
            'fonts/font-awesome/css/font-awesome.css',
            'css/styles.css',
            'css/custom.css',
			'demo/variations/default.css',
            'plugins/form-daterangepicker/daterangepicker-bs3.css',
            'plugins/fullcalendar/fullcalendar.css',
            'plugins/form-markdown/css/bootstrap-markdown.min.css',
            'plugins/codeprettifier/prettify.css',
            'plugins/form-toggle/toggles.css',
			'plugins/pines-notify/jquery.pnotify.default.css',
        ),
        'js' => array(
            //'plugins/charts-flot/excanvas.min.js',
            'js/jqueryui-1.10.3.min.js',
            'js/enquire.js',
            'js/jquery.cookie.js',
            'js/jquery.nicescroll.min.js',
            'plugins/codeprettifier/prettify.js',
            'plugins/easypiechart/jquery.easypiechart.min.js',
            'plugins/sparklines/jquery.sparklines.min.js',
            'plugins/form-toggle/toggle.min.js',
            'plugins/fullcalendar/fullcalendar.min.js',
            'plugins/form-daterangepicker/daterangepicker.min.js',
            'plugins/form-daterangepicker/moment.min.js',
            'plugins/pines-notify/jquery.pnotify.min.js',
            //'plugins/charts-flot/jquery.flot.min.js',
            //'plugins/charts-flot/jquery.flot.resize.min.js',
            //'plugins/charts-flot/jquery.flot.orderBars.min.js',
            'plugins/pulsate/jQuery.pulsate.min.js',
            //'demo/demo-index.js',
            'js/placeholdr.js',
            'js/application.js',
            'demo/demo.js',
            'js/jquery.ui.nestedSortable.js',
            'js/jquery.mjs.nestedSortable.js',
        ),
        'depends' => array('jquery'),
    ),

    'login' => array(
        'baseUrl' => $this->assetsUrl,
        'css' => array(
            'css/styles.css',
        ),
    ),
);