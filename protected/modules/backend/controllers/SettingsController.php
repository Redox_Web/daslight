<?php

class SettingsController extends BackendController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);

        $view = 'view_custom';
        $this->sidebar_tab = 'custom';
        if($model->general) {
            $view = 'view_general';
            $this->sidebar_tab = 'general';
        }

		$this->render($view, array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

    public function actionEditable(){
        Yii::import('ext.editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
        $es = new EditableSaver('Settings');  // 'User' is classname of model to be updated
        $es->update();
    }

	public function actionCreate()
	{
		$model=new Settings;
        $this->sidebar_tab = 'custom';

		if(isset($_POST['Settings']))
		{
			$model->attributes=$_POST['Settings'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        $view = 'update_custom';
        $this->sidebar_tab = 'custom';
        if($model->general) {
            $view = 'update_general';
            $this->sidebar_tab = 'general';
        }

        if($model->type == Settings::IMAGE)
            $model->scenario = 'image';

        $group = null;
        if($model->sameGroup)
            $group = $model->sameGroup;

        if(isset($_POST['Settings']))
        {
            $param = $_POST['Settings'];
            if(!empty($param[0]) > 0)
            {
                if($this->saveTabular($param));
                    $this->redirect(array('view','id'=>$model->id));
            } else {
                $model->attributes=$_POST['Settings'];

                if($model->save())
                    $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render($view,array(
            'model'=>$model,
            'group' => $group
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id = Settings::CUSTOM)
	{
		$model=new Settings('search');
        $this->sidebar_tab = $id;

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Settings']))
			$model->attributes=$_GET['Settings'];

		$this->render('admin_'.$id, array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Settings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='settings-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    protected function saveTabular($param)
    {
        $flag = true;
        foreach ($param as $i => $item) {
            $setting = Settings::model()->findByPk($item['cur_id']);
            $setting->attributes = $item;
            $uploadedFile=CUploadedFile::getInstance($setting,'['.$i.']value');
            if(!empty($uploadedFile))
                $setting->dinamicImage($setting, '['.$i.']value');

            if (!empty($item['image_remove']))
                $setting->image = "";

            if(!$setting->save()){
                $flag = false;
                Yii::log(CHtml::errorSummary($setting), "error");
            }
        }
        return $flag;
    }
}
