<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $name
 * @property string $value
 */
class Settings extends ActiveRecord
{
    const GENERAL = 'general';
    const CUSTOM = 'custom';

    const TEXTFIELD = 'textField';
    const TEXTAREA = 'textArea';
    const IMAGE = 'image';
    const WYSIWYG = 'wysiwyg';
    const CHECKFIELD = 'checkField';

    public $data;
    public $cur_id;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return array(
            array('name', 'required'),
            array('name', 'length', 'max'=>255),
            array('type, description, data, group', 'safe'),
            array('value', 'safe', 'except' => 'image'),
            array('value', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'safe' => false, 'on' => 'image'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, value', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'value' => 'Value',
            'description' => 'Hint',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($param)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);

        if($param == self::GENERAL)
            $criteria->addCondition('general = 1');
        else
            $criteria->addCondition('general = 0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getTypes()
    {
        return array(
            'textField' => 'Text Field',
            'textArea' => 'Text Area',
            'image' => 'Image',
            'wysiwyg' => 'WYSIWYG',
            'checkField' => 'Check Field'
        );
    }

    public function getSameGroup()
    {
        $ret = null;
        if(!empty($this->group)) {
            $criteria = new CDbCriteria();
            $criteria->condition = "`group` = :group";
            $criteria->params[':group'] = $this->group;
            $ret = Settings::model()->findAll($criteria);
        }
        return $ret;
    }
}