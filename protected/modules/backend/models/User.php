<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $identity
 * @property string $network
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property integer $is_active
 * @property integer $is_staff
 * @property string $last_login
 * @property string $date_joined
 *
 *
 */
class User extends ActiveRecord
{
    const STAFF = 'staff';
    const MEMBER = 'member';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public $password_repeat;
    private static $instance;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function Instance() {
        if (self::$instance == null) {
            self::$instance = self::model()->findByPk(Yii::app()->user->id);
        }

        return self::$instance;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, surname, email, password, password_repeat', 'required', 'on' => 'insert'),
            array('name, surname, email, address', 'required', 'on' => 'cabinet'),
            array('name, surname, email, password, password_repeat', 'required', 'on' => 'userinsert'),
            array('name, email, is_active', 'required', 'on' => 'update'),
            array('name, surname, email', 'required', 'on' => 'userupdate'),
            array('password, password_repeat', 'required', 'on' => 'updatepassword'),
            array('email', 'noEmail', 'on' => 'changepassward'),
            array('is_active', 'numerical', 'integerOnly' => true),
            array('avatar', 'file', 'types'=>'png, jpg, gif, swf', 'safe' => false, 'allowEmpty'=>true),
            array('password', 'length', 'min' => 5),
            array('name', 'length', 'max' => 80),
            array('password, identity, network', 'length', 'max' => 512),
            array('salt', 'length', 'max' => 255),
            array('email', 'unique'),
            array('email', 'email', 'message' => 'Email is not valid.'),
            array('password', 'compare', 'on' => 'insert, updatepassword, register'),
            array('password_repeat, last_login, date_joined, is_staff, identity, network, comment, hourly_rate', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, surname, email, password, salt, is_active, is_staff, last_login, date_joined', 'safe', 'on' => 'search'),
            array('date_joined', 'default', 'value' => new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on' => 'insert, register, socials'),
            array('is_staff', 'default', 'value' => 1, 'setOnEmpty' => false, 'on' => 'insert')
        );
    }

    public function noEmail()
    {
        $error = true;
        $model = self::model()->find(array(
                'condition'=>'email = :id',
                'params'=>array('id'=>$this->email),
            )
        );

        if(isset($model) && empty($model->identity)) {
            $error = false;
        }

        if ($error == true) {
            $this->addError('video', 'Такого email-a нет в базе данных.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function validatePassword($password)
    {
        return $this->hashPassword($password, $this->salt) === $this->password;
    }

    /**
     * Generates the password hash.
     * @param string password
     * @param string salt
     * @return string hash
     */
    public function hashPassword($password, $salt)
    {
        return md5($salt . $password);
    }

    /**
     * Generates a salt that can be used to generate a password hash.
     * @return string the salt
     */
    public function generateSalt()
    {
        return uniqid('', true);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'identity' => 'Identity',
            'network' => 'Network',
            'name' => Yii::t("base","Name"),
            'surname' => Yii::t("base","Surname"),
            'email' => Yii::t("base","Email"),
            'password' => Yii::t("base","Password"),
            'password_repeat' => Yii::t("base","Repeat password"),
            'salt' => 'Salt',
            'fullName' => Yii::t("base", 'Full name'),
            'is_active' => 'Is Active',
            'is_staff' => 'Is Staff',
            'last_login' => 'Last Login',
            'date_joined' => 'Date Joined',
            'about' => Yii::t("base","About us"),
            'avatar' => Yii::t("base","Avatar")
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */

    public function getIsUserOnline()
    {
        // select five minutes ago
        $five_minutes_ago = mktime(date("H"), date("i") - 5, date("s"), date("m"), date("d"), date("Y"));

        if ($this->last_login > $five_minutes_ago)
            return true;
        else
            return false;
    }

    public function search($param)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('salt', $this->salt, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('is_staff', $this->is_staff);
        $criteria->compare('last_login', $this->last_login, true);
        $criteria->compare('date_joined', $this->date_joined, true);

        if($param == self::STAFF)
            $criteria->addCondition('is_staff = 1');
        else
            $criteria->addCondition('is_staff = 0');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->salt = $this->generateSalt();
            $this->password = $this->hashPassword($this->password, $this->salt);
        }

        return parent::beforeSave();
    }

    public function todayRegistrations()
    {
        $crt = new CDbCriteria();
        $day_ago = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d") - 1, date("Y")));
        $crt->select = '*, UNIX_TIMESTAMP(date_joined) as date_joined';
        $crt->condition = 'date_joined > :param';
        $crt->params = array(':param' => $day_ago);
        return self::model()->findAll($crt);
    }

    public function getUAvatar()
    {
        if(empty($this->avatar))
            return substr('/'.Yii::app()->getModule('backend')->assetsUrl.'/img/profile-no-photo.png', 1);

        return '/'.$this->avatar;
    }

    public function sendEmail($subject, $body, $to) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->From = Yii::app()->name;
        $mailer->AddReplyTo(Yii::app()->params['no-replyEmail']);
        $mailer->AddAddress($to);
        $mailer->FromName = Yii::app()->name;
        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->IsHTML(true);  // set email format to HTML
        $mailer->Body = $body;

        if($mailer->Send()) {
            return true;
        } else {
            return false;
        }
    }
}