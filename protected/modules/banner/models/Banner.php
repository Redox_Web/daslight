<?php

/**
 * This is the model class for table "banner".
 *
 * The followings are the available columns in table 'banner':
 * @property integer $id
 * @property integer $banner_type_id
 * @property integer $banner_category_id
 * @property string $custom_banner
 * @property string $position
 * @property string $file
 * @property string $link
 * @property string $title_ro
 * @property string $title_ru
 * @property integer $views
 * @property string $reffer
 * @property string $rotate
 * @property string $created
 * @property string $updated
 * @property integer $author
 * @property string $is_active
 */
class Banner extends ActiveRecord
{
    const IMAGE_BANNER = 1;
    const GIF_BANNER = 2;
    const FLASH_BANNER = 3;
    const CUSTOM_BANNER = 4;

    const RIGHT = 'right';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('banner_type_id, banner_category_id, position', 'required'),
			array('banner_type_id, banner_category_id, views, author', 'numerical', 'integerOnly'=>true),
			array('custom_banner', 'length', 'max'=>500),
			array('position, file, link, title_ro, title_ru, reffer', 'length', 'max'=>255),
			array('rotate, is_active', 'length', 'max'=>1),
            array('created, updated, reffer, custom_banner, rotate', 'safe'),
            array('file', 'file', 'types'=>'png, jpg, gif, swf','allowEmpty'=>true, 'safe' => false),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, banner_type_id, banner_category_id, custom_banner, position, file, link, title_ro, title_ru, views, reffer, rotate, created, updated, author, is_active', 'safe', 'on'=>'search'),
            array('updated', 'default', 'value' => date('Y-m-d H:i:s'),'setOnEmpty' => false, 'on' => 'update'),
            array('created, updated', 'default', 'value' => date('Y-m-d H:i:s'),
                'setOnEmpty' => false, 'on' => 'insert')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'bannerCategoryRelation' => array(self::BELONGS_TO, 'BannerCategory', 'banner_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'banner_type_id' => 'Banner Type',
			'banner_category_id' => 'Banner Category',
			'custom_banner' => 'Custom Banner',
			'position' => 'Position',
			'file' => 'File',
			'link' => 'Link',
			'title_ro' => 'Title Ro',
			'title_ru' => 'Title Ru',
			'views' => 'Views',
			'reffer' => 'Reffer',
			'rotate' => 'Rotate',
			'created' => 'Created',
			'updated' => 'Updated',
			'author' => 'Author',
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('banner_type_id',$this->banner_type_id);
		$criteria->compare('banner_category_id',$this->banner_category_id);
		$criteria->compare('custom_banner',$this->custom_banner,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('views',$this->views);
		$criteria->compare('reffer',$this->reffer,true);
		$criteria->compare('rotate',$this->rotate,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('author',$this->author);
		$criteria->compare('is_active',$this->is_active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getBannerType($value = null)
    {
        $arr = array(
            self::IMAGE_BANNER => Yii::t("BannerModule.banner", 'Image Banner'),
            self::GIF_BANNER => Yii::t("BannerModule.banner", 'GIF Banner'),
            self::FLASH_BANNER => Yii::t("BannerModule.banner", 'Flash Banner'),
            self::CUSTOM_BANNER => Yii::t("BannerModule.banner", 'Custom Banner'),
        );

        if($value) return $arr[$value];
        return $arr;
    }

    public function getBannerPosition($value = null)
    {
        $arr = array(
            self::RIGHT => Yii::t("BannerModule.banner", 'Right'),
        );

        if($value) return $arr[$value];
        return $arr;
    }

    public static function getBannerCategory()
    {
        //TODO: Прочитать http://www.elisdn.ru/blog/16/dcategorybehavior-rabota-s-kategoriiami-i-spiskami-v-yii
        $models = BannerCategory::model()->findAll('is_active = 1');
        return CHtml::listData($models, 'id', 'title_ru');
    }
}
