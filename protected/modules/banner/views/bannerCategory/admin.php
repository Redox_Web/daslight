<?php
	$this->breadcrumbs=array(
        'Banner Categories'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create BannerCategory','url'=>array('create')),
    );
    $this->title = "Manage Banner Categories";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'banner-category-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'slug',
		'title_ro',
		'title_ru',
		'created',
		'updated',
		/*
		'author',
		'is_active',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
