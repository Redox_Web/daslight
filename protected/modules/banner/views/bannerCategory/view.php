<?php
	$this->breadcrumbs=array(
        'Banner Categories'=>array('admin'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'Create BannerCategory','url'=>array('create')),
        array('label'=>'Update BannerCategory','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete BannerCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage BannerCategory','url'=>array('admin')),
    );
    $this->title = "View BannerCategory # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'slug',
		'title_ro',
		'title_ru',
		'created',
		'updated',
		'author',
		'is_active',
),
)); ?>
