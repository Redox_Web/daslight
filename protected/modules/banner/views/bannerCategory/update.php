<?php
	$this->breadcrumbs=array(
        'Banner Categories'=>array('admin'),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create BannerCategory','url'=>array('create')),
	    array('label'=>'View BannerCategory','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage BannerCategory','url'=>array('admin')),
	);
    $this->title = "Update BannerCategory #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>