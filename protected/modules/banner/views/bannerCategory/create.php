<?php
	$this->breadcrumbs=array(
        'Banner Categories'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage BannerCategory','url'=>array('admin')),
    );
    $this->title = "Create BannerCategory";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>