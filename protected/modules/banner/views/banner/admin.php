<?php
	$this->breadcrumbs=array(
        'Banners'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create Banner','url'=>array('create')),
    );
    $this->title = "Manage Banners";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'banner-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'banner_type_id:bannerType',
        array(
            'name'=>'banner_category_id',
            'value'=>'$data->bannerCategoryRelation->title_ru'
        ),
		'position:bannerPosition',
		'file:image',
		/*
		'link',
		'title_ro',
		'title_ru',
		'views',
		'reffer',
		'rotate',
		'created',
		'updated',
		'author',
		'is_active',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
