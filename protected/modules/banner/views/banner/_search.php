<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'banner_type_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'banner_category_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'custom_banner',array('class'=>'form-control','maxlength'=>500)); ?>

		<?php echo $form->textFieldGroup($model,'position',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'file',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'link',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'title_ro',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'title_ru',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'views',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'reffer',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'rotate',array('class'=>'form-control','maxlength'=>1)); ?>

		<?php echo $form->textFieldGroup($model,'created',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'updated',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'author',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'is_active',array('class'=>'form-control','maxlength'=>1)); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
