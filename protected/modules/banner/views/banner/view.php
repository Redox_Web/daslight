<?php
	$this->breadcrumbs=array(
        'Banners'=>array('admin'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'Create Banner','url'=>array('create')),
        array('label'=>'Update Banner','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete Banner','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Banner','url'=>array('admin')),
    );
    $this->title = "View Banner # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'banner_type_id',
		'banner_category_id',
		'custom_banner',
		'position',
		'file',
		'link',
		'title_ro',
		'title_ru',
		'views',
		'reffer',
		'rotate',
		'created',
		'updated',
		'author',
		'is_active',
),
)); ?>
