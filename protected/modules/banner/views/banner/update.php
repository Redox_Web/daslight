<?php
	$this->breadcrumbs=array(
        'Banners'=>array('admin'),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create Banner','url'=>array('create')),
	    array('label'=>'View Banner','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage Banner','url'=>array('admin')),
	);
    $this->title = "Update Banner #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>