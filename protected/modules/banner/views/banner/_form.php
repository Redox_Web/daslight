<?php
    if($model->banner_type_id == 4) {
        $imageBannerClass = "hide";
        $cuatomBannerClass = "show";
    } else {
        $imageBannerClass = "show";
        $cuatomBannerClass = "hide";
    }

    if($model->banner_type_id == 3)
        $flashBannerClass = "hide";
    else
        $flashBannerClass = "show";
?>

<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'banner-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListGroup($model,'banner_type_id', Banner::model()->bannerType, array('class'=>'form-control')); ?>

    <?php echo $form->dropDownListGroup($model,'banner_category_id', Banner::model()->bannerCategory, array('class'=>'form-control')); ?>

    <?php echo $form->dropDownListGroup($model,'position', Banner::model()->bannerPosition, array('class'=>'form-control','maxlength'=>255)); ?>

    <div id="imageBanner" class="<?php echo $imageBannerClass; ?>">
        <?php echo $form->fileFieldGroup($model,'file',array('class'=>'form-control','maxlength'=>255)); ?>
    </div>

    <div id="cuatomBanner" class="<?php echo $cuatomBannerClass; ?>">
        <?php echo $form->textFieldGroup($model,'custom_banner',array('class'=>'form-control','maxlength'=>500)); ?>
    </div>

    <div id="linkBanner" class="<?php echo $flashBannerClass; ?>">
        <?php echo $form->textFieldGroup($model,'link',array('class'=>'form-control','maxlength'=>255)); ?>
    </div>

    <?php echo $form->checkboxGroup($model, 'is_active',
        array('widgetOptions' =>
            array(
                'htmlOptions' => array(
                    'checked'=>$model->isNewRecord ? true : $model->is_active
                ),
            )
        )
    ); ?>


    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>

<?php
    Yii::app()->clientScript->registerScript('customBanner',"
        $('#Banner_banner_type_id').on('change', function() {
            if($(this).val() == 4) {
                $('#cuatomBanner').show();
                $('#imageBanner').hide();
            } else {
                $('#cuatomBanner').hide();
                $('#imageBanner').show();
            }
        });
    ");

    Yii::app()->clientScript->registerScript('flashBanner',"
        $('#Banner_banner_type_id').on('change', function() {
            if($(this).val() == 3) {
                $('#linkBanner').hide();
            } else {
                $('#linkBanner').show();
            }
        });
    ");

    /*Yii::app()->clientScript->registerCss('cssBanner', "
        .show {display: block;}
        .hide {display: none;}
    ");*/
?>
