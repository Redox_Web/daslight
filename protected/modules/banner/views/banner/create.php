<?php
	$this->breadcrumbs=array(
        'Banners'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage Banner','url'=>array('admin')),
    );
    $this->title = "Create Banner";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>