<?php
Yii::import('backend.components.WebModule');

class BannerModule extends WebModule
{
    public $_localAssetsUrl;

    public function init()
    {
        parent::init();

        $this->setImport(array(
            'banner.models.*',
            'banner.components.*',
            'banner.widgets.*',
        ));
    }

    public static function urlRules()
    {
        return array(

        );
    }
}
