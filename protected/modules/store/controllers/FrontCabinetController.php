<?php
class FrontCabinetController extends Frontend
{
    public function actionIndex()
    {
        if(!Yii::app()->user->isGuest) {
            $model = User::model()->findByPk(Yii::app()->user->id);
            $model->scenario = 'cabinet';

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'cabinet-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['User'])) {
                $model->attributes=$_POST['User'];
                $model->save();
            }

            $this->render('index', array('model' => $model));
        } else
            throw new CHttpException(400,'You are not authorized to perform this action.');
    }

    public function actionOrderHistory()
    {
        if(!Yii::app()->user->isGuest) {
            $userOrders = StoreOrderProduct::model()->with('order', 'product')->findAll(array('condition' => 'order.user_id = :user_id', 'order' => 'order.id DESC', 'params' => array(':user_id' => Yii::app()->user->id)));
            $userOrders = new CArrayDataProvider($userOrders,
                array(
                    'pagination' => array('Pagesize' => Yii::app()->params['itemsPerPage'])
                )
            );
            $this->render('order-history', array('userOrders' => $userOrders));
        } else
            throw new CHttpException(400,'You are not authorized to perform this action.');
    }

    public function actionWishlist()
    {
        if(!Yii::app()->user->isGuest) {
            $userProducts = StoreWishlist::model()->with('product')->findAll(array('condition' => 't.user_id = :user_id', 'order' => 't.id DESC', 'params' => array(':user_id' => Yii::app()->user->id)));
            $userOrders = new CArrayDataProvider($userProducts,
                array(
                    'pagination' => array('Pagesize' => Yii::app()->params['itemsPerPage'])
                )
            );
            $this->render('order-wishlist', array('userOrders' => $userOrders));
        } else
            throw new CHttpException(400,'You are not authorized to perform this action.');
    }
}
