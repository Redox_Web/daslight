<?php
class StoreCartController extends Frontend
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'delete', 'update', 'add', 'widget'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('checkout'),
                'expression'=>'CAuthHelper::isCartAvailable()'
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $positions = Yii::app()->getModule('store')->cart->getPositions();
        $deliveryTypes = StoreDelivery::model()->published()->findAll();
        Yii::import('main.models.MainPages');
        $conditiiLivrare = MainPages::model()->find('slug = "conditii-de-livrare"');
        $this->render(
            'index',
            array('positions' => $positions, 'deliveryTypes' => $deliveryTypes, 'conditiiLivrare' => $conditiiLivrare)
        );
    }

    public function actionCheckout()
    {
        $order = new StoreOrder(StoreOrder::SCENARIO_USER);
        if (!Yii::app()->user->isGuest) {
            $order->name = Yii::app()->user->fullName;
            $order->email = Yii::app()->user->getProfileField('email');
            $order->phone = Yii::app()->user->getProfileField('phone');
            $order->address = Yii::app()->user->getProfileField('address');
        }

        $deliveryTypes = StoreDelivery::model()->published()->findAll();

        $this->render(
            'checkout',
            array('order' => $order, 'deliveryTypes' => $deliveryTypes)
        );
    }

    public function actionAdd()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        $product = Yii::app()->getRequest()->getPost('Product');

        if (empty($product['id'])) {
            throw new CHttpException(404);
        }

        $model = CartProduct::model()->findByAttributes(array('sku' => $product['id']));

        if (null === $model) {
            throw new CHttpException(404);
        }

        $quantity = empty($product['quantity']) ? 1 : (int)$product['quantity'];
        Yii::app()->getModule('store')->cart->put($model, $quantity);
        Yii::app()->ajax->success(Yii::t("StoreModule.store", 'Product successfully added to your basket'));
    }

    public function actionUpdate()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('id')) {
            throw new CHttpException(404);
        }

        $model = CartProduct::model()->findByAttributes(array('sku' => Yii::app()->getRequest()->getPost('id')));
        if($model) {
            $position = Yii::app()->getModule('store')->cart->itemAt($model->getId());
            $quantity = (int)Yii::app()->getRequest()->getPost('quantity');
            Yii::app()->getModule('store')->cart->update($position, $quantity);
            Yii::app()->ajax->success(Yii::t("StoreModule.store", YHelper::formatCurrency(Yii::app()->getModule('store')->cart->getCost())));
        } else
            throw new CHttpException(404);
    }

    public function actionDelete()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->getRequest()->getPost('id')) {
            throw new CHttpException(404);
        }

        $model = CartProduct::model()->findByAttributes(array('sku' => Yii::app()->getRequest()->getPost('id')));
        if($model) {
            Yii::app()->getModule('store')->cart->remove($model->getId());
            Yii::app()->ajax->success(Yii::t("StoreModule.store", Yii::app()->getModule('store')->cart->getCost()));
        } else
            throw new CHttpException(404);
    }

    public function actionClear()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest()) {
            throw new CHttpException(404);
        }

        Yii::app()->cart->clear();
        Yii::app()->ajax->success(Yii::t("StoreModule.store", 'Cart is cleared'));
    }

    public function actionWidget()
    {
        //$this->renderPartial('//cart/widgets/views/shoppingCart');
        // хочет отправить куки новые для авторизации каждый раз
        $this->widget('store.widgets.ShoppingCartWidget');
    }
}
