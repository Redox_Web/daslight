<?php
Yii::import('backend.components.BackendController');

class StoreOrderProductController extends BackendController
{
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $model = $this->loadModel($id);
            // we only allow deletion via POST request
            if($model->delete()) {
                $model->order->total_price = $model->order->getProductsCost();
                $model->order->save();
            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionAjaxSearch()
    {
        if (!Yii::app()->getRequest()->getQuery('q')) {
            throw new CHttpException(404);
        }

        $model = StoreProduct::model()->searchByName(Yii::app()->getRequest()->getQuery('q'));
        $data = array();

        foreach ($model as $product) {
            $image = $product->contypeMainImage ? $product->contypeMainImage->path : '';
            $data[] = array(
                'id' => $product->id,
                'name' => $product->name . ($product->sku ? " ({$product->sku}) " : ' ') . YHelper::formatCurrency($product->getResultPrice()),
                'thumb' => YHelper::getImagePath($image, 50, 50),
            );
        }

        Yii::app()->ajax->raw($data);
    }

    public function actionProductRow($product_id, $order_id)
    {
        if($product_id) {
            $product = StoreProduct::model()->findByPk($product_id);
            $order = StoreOrder::model()->findByPk($order_id);

            $orderProducts = $order->products;
            if($orderProducts) {
                foreach($orderProducts as $orderProduct) {
                    if($orderProduct->product_id == $product->id) {
                        $orderProduct->quantity += 1;
                        if($orderProduct->save()) {
                            $order->total_price = $order->getProductsCost();
                            $order->save();
                            Yii::app()->ajax->success();
                        }
                    }
                }
            }

            $newOrder = new StoreOrderProduct();
            $newOrder->order_id = $order->id;
            $newOrder->product_id = $product->id;
            $newOrder->product_name = $product->name;
            $newOrder->sku = $product->sku;
            $newOrder->quantity = 1;
            $newOrder->price = $product->getResultPrice();
            if($newOrder->save()) {
                $order->total_price = $order->getProductsCost();
                $order->save();
                Yii::app()->ajax->success($newOrder->id);
            }
        } else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function loadModel($id)
    {
        $model = StoreOrderProduct::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}