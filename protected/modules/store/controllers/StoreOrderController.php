<?php
Yii::import('backend.components.BackendController');

class StoreOrderController extends BackendController
{
    public $sidebar_tab = "store-order";

    public function actions()
    {
        return array(
            'statusUpdate' => 'DBoosterEditableAction',
            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update'=>array(
                'class'=>'DUpdateAction',
                'layout'=> Yii::app()->getModule('backend')->getBackendLayoutAlias('layout_tabs'),
            ),
        );
    }

    public function createModel()
    {
        return new StoreOrder();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=StoreOrder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='store-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionQuantityUpdate()
    {
        $id = (int) Yii::app()->request->getPost('pk');
        $attribute = Yii::app()->request->getPost('name');
        $value = Yii::app()->request->getPost('value');

        $es = StoreOrderProduct::model()->findByPk($id);
        $es->{$attribute} = $value;

        if($es->update()) {
            $es->order->total_price = $es->order->getProductsCost();
            if($es->order->save())
                Yii::app()->ajax->success(array('totalPrice' => $es->order->total_price));
        }
        else
            Yii::app()->ajax->rawText($es->getError($attribute), 500);
    }
}