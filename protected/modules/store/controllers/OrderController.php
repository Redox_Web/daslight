<?php
class OrderController extends Frontend
{
    public function actionView($url = null)
    {
        if (!Yii::app()->getModule('order')->showOrder && !Yii::app()->getUser()->isAuthenticated()) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

        $model = Order::model()->findByUrl($url);

        if ($model === null) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }

        $this->render('view', array('model' => $model));
    }

    public function actionCreate()
    {
        $model = new StoreOrder(StoreOrder::SCENARIO_USER);

        if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('StoreOrder')) {
            $order = Yii::app()->getRequest()->getPost('StoreOrder');

            if ($model->saveData($order, StoreOrder::STATUS_NEW)) {
                Yii::app()->getModule('store')->cart->clear();
                $order = StoreOrder::model()->findByPk($model->id);

                //отправить уведомления
                Yii::app()->getModule('store')->orderNotifyService->sendOrderCreatedAdminNotify($model);
                Yii::app()->getModule('store')->orderNotifyService->sendOrderCreatedUserNotify($model);
                
                 if($this->sendMailOrder($order)){
                    Yii::app()->getModule('store')->cart->clear();
                    if (Yii::app()->getModule('store')->showOrder) {
                        $order = StoreOrder::model()->findByPk($model->id);
                        $this->render('view', array('order' => $order));
                        Yii::app()->end();
                    }

                    $this->redirect(array('/store/catalog/index'));
                }else{
                    throw new CException("Error on send email!");
                }

               
            } else {
                Yii::app()->getUser()->setFlash(
                    FlashMessages::ERROR_MESSAGE(),
                    CHtml::errorSummary($model)
                );
            }
        } else
            throw new CHttpException(500, Yii::t('StoreModule.store', 'Page not found!'));

        $this->redirect(Yii::app()->getUser()->getReturnUrl($_SERVER['HTTP_REFERER']));
    }
    
    public function sendMailOrder($order){
        $emails_settings = YHelper::yiisetting('emails_list_order');
        if($emails_settings){
            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->AddReplyTo($order['email']);
            $emails_arr = preg_split("/\\r\\n|\\r|\\n/", $emails_settings);
            foreach($emails_arr as $email){
                $mailer->AddAddress($email);
            }
            $mailer->FromName = Yii::app()->name;
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = "Заказ от клиента {$order['name']} на сайте ".Yii::app()->name;
            $mailer->IsHTML(true);  // set email format to HTML
             
            $body = "Новый заказ на ".Yii::app()->name." <br><br>"
                    . "Данные клиента: <br>"
                    . "Имя: {$order['name']} <br>"
                    . "Email: {$order['email']} <br>"
                    . "Телефон: {$order['phone']} <br>"
                    . "Адрес доставки: {$order['address']} <br>";
                    $ord_comm = trim($order['comment']);
                    if(!empty($ord_comm))
                    $body .= "Комментарий: {$order['comment']} <br>";
            // Products info
            $body .= "<br> Заказанные товары: <br><br>";        
            $orderProducts = Yii::app()->getModule('store')->cart->getAllItems();
            $i = 1;
            foreach ($orderProducts as $product) {
                $body .= "$i) {$product->name} <br>"
                        . "Sku: {$product->sku} <br>"
                        . "Количество: {$product->getQuantity()} <br>"
                        . "Цена: {$product->getResultPrice()} <br><br>";
                $i++;
            }
            $mailer->Body = $body;
            if($mailer->Send()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public function actionCheck()
    {
        if (!Yii::app()->getModule('order')->enableCheck) {
            throw new CHttpException(404);
        }

        $form = new CheckOrderForm();
        $order = null;

        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $form->setAttributes(
                Yii::app()->getRequest()->getPost('CheckOrderForm')
            );

            if ($form->validate()) {
                $order = Order::model()->findByNumber($form->number);
            }
        }

        $this->render('check', array('model' => $form, 'order' => $order));
    }
}
