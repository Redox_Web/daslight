<?php
/**
 * Class CatalogController
 * @property ProductRepository $productRepository
 *
 */
class StoreCatalogController extends Frontend
{
    protected $productRepository;
    protected $attributeFilter;

    /**
     *
     */
    public function init()
    {
        $this->productRepository = Yii::app()->getComponent('productRepository');
        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');

        parent::init();
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'view', 'category', 'download'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionDownload($id) {
        $file = StoreProduct::model()->findByPk($id);
        if(!$file)
            throw new CHttpException(404, 'This file does not exist.');

        $filepath = $file->pdf;
        $fpath = explode("/", $filepath);
        $fileName = end($fpath);

        if (file_exists($filepath))
            return Yii::app()->getRequest()->sendFile($fileName, @file_get_contents($filepath));
        else
            throw new CHttpException(404, 'This file does not exist.');
    }

    /**
     * @param $name
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $product = StoreProduct::model()->findByPk($id);

        if (null === $product || !$product->category || !$product->category->isCatActive()) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Product was not found!'));
        }
        $cartModel = Yii::app()->getModule('store')->cart->itemAt($product->getId());
        if(!$cartModel)
            $cartModel = $product;
        $this->render('product_single', array('product' => $product, 'cartModel' => $cartModel));
    }

    /**
     * @param $path
     * @throws CHttpException
     */
    public function actionCategory($id)
    {
        $itemsPerPage = Yii::app()->request->getParam('itemsPerPage');
        if (empty($itemsPerPage)) $itemsPerPage = Yii::app()->params->itemsPerPage;

        $category = StoreCategory::model()->findByPk($id);
        $this->_curNav = $category->root;
        $this->_curCateg = $category->id;

        if (null === $category || !$category->isCatActive()) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Category was not found!'));
        }

        $data = Yii::app()->getRequest()->getQueryString() ? $this->module->productRepository->getByFilter(
            $category,   
            $this->module->attributesFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest()),
            $this->module->attributesFilter->getEavAttributesForSearchFromQuery(Yii::app()->getRequest())) : $this->module->productRepository->getListForCategory($category, $itemsPerPage);
        
        $this->pageTitle = $category->getMetaTitle();
        $this->metaKeywords = $category->getMetaKeyword();
        $this->metaDescription = $category->getMetaDescription();

        $this->render('index', array('dataProvider' => $data, 'category' => $category));
    }

    /**
     *
     */
    public function actionIndex()
    {
        $data = Yii::app()->getRequest()->getQueryString() ? $this->module->productRepository->getByFilter(
            $this->module->attributesFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest()),
            $this->module->attributesFilter->getEavAttributesForSearchFromQuery(Yii::app()->getRequest())
        ) : $this->module->productRepository->getListForIndexPage();

        $this->render('index', array('dataProvider' => $data, 'category' => 'all'));
    }

    public function actionSearch()
    {
        if (!Yii::app()->getRequest()->getQuery('SearchForm')) {
            throw new CHttpException(404);
        }

        $form = new SearchForm();

        $form->setAttributes(Yii::app()->getRequest()->getQuery('SearchForm'));

        if ($form->validate()) {

            $category = $form->category ? StoreCategory::model()->findByPk($form->category) : null;

            $this->render(
                'search',
                array(
                    'category' => $category,
                    'searchForm' => $form,
                    'dataProvider' => $this->productRepository->getByFilter(
                            $this->attributeFilter->getMainAttributesForSearchFromQuery(Yii::app()->getRequest(), array(AttributeFilter::MAIN_SEARCH_PARAM_NAME => $form->q)),
                            $this->attributeFilter->getEavAttributesForSearchFromQuery(Yii::app()->getRequest())
                        )
                )
            );
        }
    }

    /**
     *
     */
    public function actionAutocomplete()
    {
        $query = Yii::app()->getRequest()->getQuery('term');

        $result = array();

        if (strlen($query) > 2) {
            $result = $this->productRepository->search($query);
        }

        Yii::app()->ajax->raw($result);
    }
}
