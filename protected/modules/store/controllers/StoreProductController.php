<?php
Yii::import('backend.components.BackendController');
Yii::import('backend.components.multiapload.*');

class StoreProductController extends BackendController
{
    public $sidebar_tab = "store-product";

    public function actions()
    {
        return array(
            'sort'=> array(
                'class' => 'DSortAction',
            ),
            'upload' => 'UploadAction',
            'gallery'=>'GalleryAction',
            'imagedel'=>'ImagedelAction',
            'mainimage'=>'MainimageAction',

            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'delete' => 'DDeleteAction',
        );
    }

    public function createModel()
    {
        $model = new StoreProduct();
        $model->disableDefaultScope()->admin();

        return $model;
    }

    public function actionCreate()
    {
        $this->layout = '/layouts/layout_store_product';
        $model = new StoreProduct();

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('StoreProduct')) {

            $attributes = Yii::app()->getRequest()->getPost('StoreProduct');
            $typeAttributes = Yii::app()->getRequest()->getPost('StoreAttribute', array());

            if (!$attributes['old_price']) $attributes['old_price'] = null;

            if ($model->saveData($attributes, $typeAttributes)) {
                $this->redirect(array('update', 'id' => $model->id));
            } else {
                Yii::app()->getUser()->setFlash(
                    FlashMessages::ERROR_MESSAGE(),
                    $this->stringErrors($model->getErrors())
                    /*Yii::t('StoreModule.store', 'Failed to save product!')*/
                );
            }
        }
        $this->render('create', array('model' => $model, 'searchModel' => NULL));
    }

    public function actionUpdate($id)
    {
        $this->layout = '/layouts/layout_store_product';
        /** @var StoreProduct $model */
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('StoreProduct')) {

            $attributes = Yii::app()->getRequest()->getPost('StoreProduct');
            $typeAttributes = Yii::app()->getRequest()->getPost('StoreAttribute', array());

            if (!$attributes['old_price']) $attributes['old_price'] = null;

            if ($model->saveData($attributes, $typeAttributes)) {
                $this->redirect(array('view', 'id' => $model->id));
            } else {
                Yii::app()->getUser()->setFlash(
                    FlashMessages::ERROR_MESSAGE(),
                    Yii::t('StoreModule.store', 'Failed to save product!')
                );
            }
        }

        $searchModel = new ProductSearch('search');
        $searchModel->unsetAttributes();

        $this->render('update', array('model' => $model, 'searchModel' => $searchModel));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            /** @var StoreProduct $model */
            $model = $this->loadModel($id);
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionRelated($id, $related_product, $param)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $storeRelated = StoreRelated::model()->findByAttributes(array('product_id' => $id, 'related_product_id' => $related_product, 'relation' => $param));

            if ($storeRelated) {
                if($storeRelated->delete()) echo true;
            } else {
                $storeRelated = new StoreRelated();
                $storeRelated->product_id = $id;
                $storeRelated->related_product_id = $related_product;
                $storeRelated->relation = $param;

                if($storeRelated->save()) echo true;
            }

            echo false;
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=StoreProduct::model()->disableDefaultScope()->admin()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionTypeAttributesForm($id)
    {
        $category = StoreCategory::model()->findByPk($id);

        if (null === $category) {
            throw new CHttpException(404);
        }

        $this->renderPartial('_attribute_form', array('category' => $category, 'model' => new StoreProduct()));
    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='store-product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}