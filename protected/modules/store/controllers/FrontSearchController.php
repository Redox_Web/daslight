<?php
Yii::import('store.models.Search');

class FrontSearchController extends Frontend
{
    public function actionSearchResult($q)
    {
        $q = trim(htmlspecialchars($q));
        $q = mb_strtolower($q, 'UTF-8');

        if (strlen($q) < 4) {
            $dataSearch = false;
        } else {
            $crt = new CDbCriteria;
            $crt->condition = "LOWER(name_".Yii::app()->language.") LIKE :search";
            $crt->params[":search"] = '%'.strtr($q, array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\', '(' => '', ')' => '')).'%';
            $crt->order = 't.id DESC';

            $dataSearch = StoreProduct::model()->findAll($crt);
            $dataSearch = array_filter($dataSearch, array(new StoreProduct(), "checkActiveProducts"));
            $dataSearch = new CArrayDataProvider(
                $dataSearch,
                [
                    'pagination' => ['Pagesize' => 10]
                ]
            );
        }
        $this->render('search', ['dataSearch' => $dataSearch, 'searchPhrase' => $q]);
    }

    public function actionSearch()
    {
        $q = '';
        $serchPhrase = Yii::app()->request->getParam('Search');
        foreach($serchPhrase as $phrase)
            $q = $phrase['q'];

        $this->performAjaxValidation();

        if (isset($_POST['Search'])) {
            $this->redirect(array('searchResult', 'q' => $q));
        } else
            throw new CHttpException(404, Yii::t("base", "The requested page does not exist!"));
    }

    protected function performAjaxValidation()
    {
        if (isset($_POST['ajax']) && strpos($_POST['ajax'], 'search-form') !== false) {
            $column = array();
            if (isset($_POST['Search'])) {
                foreach ($_POST['Search'] as $key => $value) {
                    $column[$key] = new Search();
                }
            }
            $f2 = json_decode(CActiveForm::validateTabular($column), 1);
            echo json_encode($f2);
            Yii::app()->end();
        }
    }
}