<?php
Yii::import('backend.components.BackendController');

class StoreCategoryController extends BackendController
{
    public $sidebar_tab = "store-category";

    public function actions()
    {
        return array(
            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update'=> 'DUpdateAction',
        );
    }
    
    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
   public function actionCreate()
   {
       $this->layout = '/layouts/layout_store_product_category';
        $model=new StoreCategory;

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);


        if(isset($_POST['StoreCategory']))
        {
            $model->attributes = $_POST['StoreCategory'] ;

            if($model->saveNode()){
                $this->redirect(array('view', 'id'=>$model->id));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->layout = '/layouts/layout_store_product_category';
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['StoreCategory']))
        {
            $model->attributes=$_POST['StoreCategory'];
            if($model->saveNode())
                $this->redirect(array('view', 'id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionTree()
    {
        if (isset($_POST["ids"])) {
            $ids = $_POST["ids"];
            foreach ($ids as $key => $id) {
                $menu = StoreCategory::model()->findByPk((int)$id["item_id"]);
                if(isset($menu->flash_messages))
                    $menu->flash_messages = false;

                if ($menu && (int)$id["item_id"] != 0) {
                    $menu->priority = $key;
                    $menu->saveNode();
                    if (!empty($id["parent_id"]) && $id["parent_id"] != "root") {
                        $root = StoreCategory::model()->findByPk((int)$id["parent_id"]);
                        $menu->moveAsLast($root);
                    } else {
                        if (!$menu->isRoot())
                            $menu->moveAsRoot();
                    }
                }
            }
            echo json_encode($ids);
        }
    }

    public function createModel()
    {
        return new StoreCategory();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=StoreCategory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='store-category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}