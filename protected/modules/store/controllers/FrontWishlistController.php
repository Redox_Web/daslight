<?php
class FrontWishlistController extends Frontend
{
    public function actionAdd()
    {
        if(Yii::app()->user->isGuest)
            throw new CHttpException(404, Yii::t('StoreModule.store', 'You are not authorized to perform this action'));

        if (!Yii::app()->getRequest()->getIsPostRequest())
            throw new CHttpException(404);

        $product = Yii::app()->getRequest()->getPost('Product');

        if (empty($product['id']))
            throw new CHttpException(404);

        $model = CartProduct::model()->findByAttributes(array('sku' => $product['id']));

        if (null === $model)
            throw new CHttpException(404);

        $model = new StoreWishlist();
        $model->user_id = Yii::app()->user->id;
        $model->product_id = Yii::app()->user->id;
        if($model->save()) {
            $sum = StoreWishlist::model()->count('user_id = :user', array(':user'=> Yii::app()->user->id));
            Yii::app()->ajax->raw(
                array(
                    'sum' => $sum,
                    'response' => Yii::t("StoreModule.store", 'Product successfully added to your basket'),
                )
            );
        }
    }
}