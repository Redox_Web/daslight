<?php
Yii::import('backend.components.BackendController');

class RelatedController extends BackendController
{
    public function actionIndex($id)
    {
        $product = StoreProduct::model()->findByPk($id);

        $model = new ProductSearch('search');
        $model->unsetAttributes();
        $model->attributes = Yii::app()->getRequest()->getParam('ProductSearch');
        $this->render('/storeProduct/_related_products_form', array('searchModel' => $model, 'model' => $product));
    }
}
