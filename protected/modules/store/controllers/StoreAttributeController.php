<?php
Yii::import('backend.components.BackendController');

class StoreAttributeController extends BackendController
{
    public $sidebar_tab = "store-attribute";

    public function actions()
    {
        return array(
            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update'=> 'DUpdateAction',
            'sort'=>'DSortAction',
        );
    }

    public function createModel()
    {
        return new StoreAttribute();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=StoreAttribute::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='store-attribute-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}