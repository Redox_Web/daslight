<?php
Yii::import('store.models.Search');

class WishListWidget extends CWidget
{
    public function run()
    {
        $sum = 0;
        if(!Yii::app()->user->isGuest)
            $sum = StoreWishlist::model()->count('user_id = :user', array(':user'=> Yii::app()->user->id));

        echo $sum;
    }
}