<?php
Yii::import('booster.widgets.TbEditableColumn');

class EditableStatusColumn extends TbEditableColumn
{
    public function init()
    {
        $this->editable['options']['display'] = 'js:function(value, sourceData) {
            var sourceData = ' . json_encode($this->editable['source']) . ';
            var itemsOptions = ' . json_encode($this->editable['options']) . ';

            $(this).html("<div class=\"" + itemsOptions[value]["class"] + "\">" + sourceData[value] + "</div>");
        }';
        parent::init();
    }
}