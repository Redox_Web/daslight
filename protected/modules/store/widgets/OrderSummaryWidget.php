<?php
class OrderSummaryWidget extends CWidget
{
    public $showButton = true;
    public $view = 'order-summary';
    /**
     * @var Product
     */

    public function run()
    {
        $this->render($this->view);
    }
}