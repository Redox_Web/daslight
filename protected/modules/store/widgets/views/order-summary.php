<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    <div class="block-table">
        <h6><i class="fa fa-shopping-cart"></i> Order Summary</h6>
        <div class="table-order-summary">
            <table>
                <tbody>
                <tr>
                    <td>Subtotal:</td>
                    <td>$889.99</td>
                </tr>
                <tr>
                    <td>HST:</td>
                    <td>$117.00</td>
                </tr>
                <tr>
                    <td class="color-red"><strong><?php echo Yii::t("StoreModule.store", "Total");?>:*</strong></td>
                    <td class="color-red"><strong>$<span id="priceSumCart"><?php echo Yii::app()->getModule('store')->cart->getCost(); ?></span></strong></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="block-information">
            We’ll email you when your order is ready for pickup
        </div>
        <?php if($this->showButton) { ?>
            <div class="block-button-next">
                <a href="<?php echo Yii::app()->createUrl('store/storeCart/Checkout'); ?>"><?php echo Yii::t("StoreModule.store", "Proceed to Checkout");?></a>
            </div>
        <?php } ?>
    </div>

    <div class="block-info">
        <p><strong>Need help?</strong> Call 1-866-746-7287</p>
        <p><strong>Cart ID:</strong> 1657526154401</p>
        <p>
            <strong>
                Please do not proceed to the store until you receive an email notifying you that your order is ready for pickup.
            </strong>
        </p>
        <p>
            Note that if your order contains items with different pickup times, you will recieve a separate email for each item.
        </p>
        <p class="small-text">* Taxes and fees are subject to change, which may result in a <a href="#">change</a> in your total purchase price.</p>
    </div>
</div>