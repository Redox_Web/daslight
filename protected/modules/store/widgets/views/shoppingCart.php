<?php $allItems = Yii::app()->getModule('store')->cart->getAllItems(); ?>
<?php $total_count = Yii::app()->getModule('store')->cart->getCount(); ?>

<a href="javascript:void(0);" class="cart-opener">
    <span class="icon-handbag"></span>
    <span class="num"><?php echo $total_count; ?></span>
</a>

<!-- mt drop start here -->
<div class="mt-drop" id="style-3">
    <!-- mt drop sub start here -->
    <div class="mt-drop-sub">
        <!-- mt side widget start here -->
        <div class="mt-side-widget">
            <?php if ($total_count>0) { ?>
                <?php foreach ($allItems as $item) { ?>
                    <!-- cart row start here -->
                    <div class="cart-row">
                        <a href="<?php echo $item->url; ?>" class="img">
                            <?php $image = $item->contypeMainImage ? $item->contypeMainImage->path : ''; ?>
                            <img src="<?php echo YHelper::getImagePath($image, 75, 75); ?>" alt="image" class="img-responsive"></a>
                        <div class="mt-h">
                            <span class="mt-h-title"><a href="<?php echo $item->url; ?>"><?php echo $item->name; ?></a></span>
                            <span class="price"><?php echo YHelper::formatCurrency($item->getResultPrice()); ?></span>
                            <span class="mt-h-title">
                        <?php echo Yii::t("StoreModule.store", "Quantity"); ?>: <?php echo $item->getQuantity(); ?></span>
                        </div>
                        <a href="javascript:void(0);" class="close fa fa-times cart-delete-product" data-product-id="<?php echo $item->sku; ?>"></a>
                    </div><!-- cart row end here -->
                <?php } ?>

                <!-- cart row total start here -->
                <div class="cart-row-total">
                    <span class="mt-total"><?php echo Yii::t("StoreModule.store", "Price"); ?></span>
                    <span class="mt-total-txt">
                        <?php echo YHelper::formatCurrency(Yii::app()->getModule('store')->cart->getCost()); ?>
                </span>
                </div>
            <?php } else { ?>
                <div class="cart-row-total">
                    <?php echo Yii::t("StoreModule.store", "No items in cart");?>
                </div>
            <?php } ?>
            <!-- cart row total end here -->
            <div class="cart-btn-row">
                <a href="<?php echo Yii::app()->createUrl('store/storeCart/index'); ?>" class="btn-type2">
                    <?php echo Yii::t("StoreModule.store", "Proceed to Checkout");?>
                </a>
            </div>
        </div><!-- mt side widget end here -->
    </div>
    <!-- mt drop sub end here -->
</div><!-- mt drop end here -->
<script>
//    $(function(){
//        $(document).on('click', ".cart-opener, .mt-mdropover", function(){
//            console.log("open");
//            $(this).parent().toggleClass("open");
//            return false;
//        });
//    });
</script>
