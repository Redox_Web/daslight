<div class="col-md-3 col-sm-6">
    <div class="product-item">
        <div class="product-img">
            <a href="<?php echo $data->url; ?>">
                <?php $image = $data->contypeMainImage ? $data->contypeMainImage->path : ''; ?>
                <img src="<?php echo Yii::app()->iwi->load($image)->adaptive(320, 240, true)->cache(); ?>" alt="image"  />
            </a>
        </div>
        <div class="product-details">
            <a href="<?php echo $data->url; ?>"><h4><?php echo $data->name; ?></h4></a>
            <?php foreach ($data->isShortAttributes as $attribute): { ?>
                <li>
                    <?php echo $attribute->title; ?>:
                    <?php echo $attribute->renderValue($data->attribute($attribute->column_name)); ?>
                </li>
            <?php } endforeach; ?>
            <h5 class="text-color"><?php echo YHelper::formatCurrency($product->resultPrice); ?></h5>
        </div>
        <div class="add-to-cart">
            <a href="javascript:void(0);" class="btn btn-default quick-add-product-to-cart" data-product-id="<?php echo $data->slug; ?>">
                <i class="fa fa-shopping-cart"></i> <?php echo Yii::t("StoreModule.store", "Add to Cart");?>
            </a>
        </div>
    </div>
</div>
<!-- .product -->
