<!-- related products start here -->
<div class="related-products  wow fadeInUp" data-wow-delay="0.4s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2><?php echo Yii::t('StoreModule.store', 'Related products') ?></h2>
                <div class="row">
                    <div class="col-xs-12">
                        <?php foreach($sameProducts as $product) { ?>
                            <!-- mt product1 center start here -->
                            <div class="mt-product1 mt-paddingbottom20">
                                <div class="box">
                                    <div class="b1">
                                        <div class="b2">
                                            <?php $image = $product->contypeMainImage ? $product->contypeMainImage->path : ''; ?>
                                            <a href="<?php echo $product->url; ?>">
                                                <img src="<?php echo YHelper::getImagePath($image, 300, 300); ?>" alt="<?php echo $product->name; ?>">
                                            </a>
                                          <!--  <span class="caption">
                                                <span class="new">NEW</span>
                                            </span>-->
                                            <ul class="mt-stars">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                            <ul class="links">
                                                <li><a href="javascript:void(0);" class="add-product-to-cart" data-product-id="<?php echo $product->sku; ?>">
                                                        <i class="icon-handbag"></i>
                                                        <span><?php echo Yii::t("base", "To Cart"); ?></span>
                                                    </a>
                                                </li>
<!--                                                <li><a href="#"><i class="icomoon icon-heart-empty"></i></a></li>-->
<!--                                                <li><a href="#"><i class="icomoon icon-exchange"></i></a></li>-->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="txt text-center">
                                    <div class="title-box">
                                        <span class="title title-height">
                                            <a href="<?php echo $product->url; ?>">
                                                <?php echo YText::characterLimiter($product->name, 50); ?>
                                            </a>
                                        </span>
                                    </div>
                                    <strong class="title">Code <span class="orange"><?php echo $product->sku; ?> </span></strong>
                                    <?php if ($product->old_price) { ?>
                                        <span class="old-price">
                                            <span><?php echo YHelper::formatCurrency($product->old_price); ?></span>
                                        </span>
                                    <?php } ?>
                                    <span class="price">
                                        <span>
                                            <?php echo YHelper::formatCurrency($product->getResultPrice()); ?>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <!-- mt product1 center end here -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- related products end here -->
</div>