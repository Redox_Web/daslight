<section id="product-scroll" class="page-section light-bg border-tb">
    <div class="container shop">
        <div class="section-title text-left">
            <h2 class="title"><?php echo Yii::t('StoreModule.store', 'Related products') ?></h2>
        </div>
        <div class="row">
            <div class="owl-carousel navigation-1 opacity text-left" data-pagination="false" data-items="4"  data-autoplay="true" data-navigation="true">
                <?php $this->widget(
                    'zii.widgets.CListView',
                    [
                        'dataProvider' => $dataProvider,
                        'template' => '{items}',
                        'itemView' => '_view',
                        'cssFile' => false,
                        'pager' => false,
                    ]
                ); ?>
            </div>
        </div>
    </div>
</section>