<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'search-form'.$count,
    'action' => array('/'.Yii::app()->language.'/store/frontSearch/search'),
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false
    ),
)); ?>
    <?php echo $form->textField($model, "[$count]q", array('class' => 'text-area input', 'placeholder' => Yii::t("MainModule.main", 'Search'))); ?>
    <button type="submit"><i class="fa fa-search"></i></button>
    <?php echo $form->error($model, "[$count]q"); ?>
<?php $this->endWidget(); ?>