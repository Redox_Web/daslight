<!-- mt producttabs start here -->
<div class="mt-producttabs wow fadeInUp" data-wow-delay="0.4s">
    <!-- producttabs start here -->
    <ul class="producttabs">
        <li><a href="#tab1" class="active c-yellow"><?php echo Yii::t("StoreModule.store", "Random products"); ?></a></li>
    </ul>
    <!-- producttabs end here -->
    <div class="tab-content text-center">
        <div id="tab1">
            <!-- tabs slider start here -->
            <div class="tabs-slider">
                <?php $i=0; ?>
                <?php if(count($products)%2==1) array_pop($products);  ?>
                <?php ob_start(); ?>
                <?php foreach($products as $product) { ?>
                    <!-- slide start here -->
                    <?php if (($i%2)==0) { ?>
                        <div class="slide">
                    <?php } ?>
                        <!-- mt product1 center start here -->
                        <div class="mt-product1 p-0_5em">
                            <div class="box">
                                <div class="b1">
                                    <div class="b2">
                                        <?php $image = $product->contypeMainImage ? $product->contypeMainImage->path : ''; ?>
                                        <a class="product-link" href="<?php echo $product->url; ?>">
                                            <img src="<?php echo YHelper::getImagePath($image, 300, 300); ?>" alt="<?php echo $product->name; ?>">
                                        </a>
                                        <!--<span class="caption">
                                            <span class="new">NEW</span>
                                        </span>-->
                                        <ul class="mt-stars">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="links">
                                            <li>
                                                <a href="javascript:void(0);" class="add-product-to-cart" data-product-id="<?php echo $product->sku; ?>" data-loading-text="<span><?php echo Yii::t("base", "Loading..."); ?></span>">
                                                    <i class="icon-handbag"></i><span><?php echo Yii::t("base", "To Cart"); ?></span>
                                                </a>
                                            </li>
<!--                                            <li><a href="#"><i class="icomoon icon-heart-empty"></i></a></li>-->
<!--                                            <li><a href="#"><i class="icomoon icon-exchange"></i></a></li>-->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="txt">
                                <div class="title-box">
                                    <span class="title title-height">
                                        <a href="<?php echo $product->url; ?>">
                                            <?php echo YText::characterLimiter($product->name, 50); ?>
                                        </a>
                                    </span>
                                </div>
                                <span class=" sku-title">Code <span class="orange"><?php echo $product->sku; ?> </span></span>
                                <?php if ($product->old_price) { ?>
                                <span class="old-price">
                                    <span>
                                        <?php echo YHelper::formatCurrency($product->old_price); ?>
                                    </span>
                                </span>
                                <?php } ?>

                                <span class="price">
                                    <span>
                                        <?php echo YHelper::formatCurrency($product->getResultPrice()); ?>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <!-- mt product1 center end here -->
                    <?php if (($i%2)==1) { ?>
                        </div>
                    <?php } ?>
                    <!-- slide end here -->
                    <?php $i++; ?>
                <?php } ?>
            <?php echo ob_get_clean(); ?>
            </div>
            <!-- tabs slider end here -->
        </div>
    </div>
</div>
<!-- mt producttabs end here -->
