<?php if(!empty($producers)):?>
    <h6><?php echo Yii::t('StoreModule.store', 'Manufacturers');?></h6>
    <?php foreach($producers as $producer):?>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('brand[]',$this->controller->module->attributesFilter->isMainSearchParamChecked(AttributeFilter::MAIN_SEARCH_PARAM_PRODUCER, $producer->id, Yii::app()->getRequest()),['value' => $producer->id, 'id' => 'brand_'.$producer->id]);?>
                <?php echo $producer->name;?>
            </label>
        </div>
    <?php endforeach;?>
<?php endif;?>
