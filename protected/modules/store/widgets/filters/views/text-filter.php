<?php $filter = $this->controller->module->getComponent('attributesFilter');?>
<div class="filter-products-block">
		<h6><?php echo $attribute->title; ?></h6>
		<div class="filter-block-body">
				<div class="row">
						<div class="col-xs-12">
								<?php echo CHtml::textField($filter->getFieldName($attribute), $filter->getFieldValue($attribute), array('class' => 'form-control')) ?>
						</div>
				</div>
		</div>
</div>