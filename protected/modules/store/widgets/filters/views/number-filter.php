<?php $filter = $this->controller->module->getComponent('attributesFilter');?>
<div class="filter-products-block">
		<h6><?php echo $attribute->title; ?></h6>
    <div class="row">
        <div class="col-xs-6">
            <?php echo CHtml::textField($filter->getFieldName($attribute, 'from'), $filter->getFieldValue($attribute, 'from'), array('class' => 'form-control')) ?>
        </div>
        <div class="col-xs-6">
            <?php echo CHtml::textField($filter->getFieldName($attribute, 'to'), $filter->getFieldValue($attribute, 'to'), array('class' => 'form-control')) ?>
        </div>
    </div>
</div>