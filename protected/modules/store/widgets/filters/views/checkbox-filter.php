<?php $filter = $this->controller->module->getComponent('attributesFilter'); ?>
<div class="filter-products-block">
		<h6><?php echo $attribute->title; ?></h6>
		<div class="checkbox">
				<label>
						<?php echo CHtml::checkBox(
								$filter->getFieldName($attribute),
								$filter->isFieldChecked($attribute, 1),
								array('value' => 1)
						) ?>
						<?php echo "да/нет"; ?>
				</label>
		</div>
</div>