<?php $filter = $this->controller->module->getComponent('attributesFilter');?>
<div class="filter-products-block">
		<h6><?php echo $attribute->title; ?></h6>
		<?php foreach ($attribute->options as $option): ?>
				<div class="checkbox">
						<label>
								<?php echo CHtml::checkBox($filter->getDropdownOptionName($option), $filter->getIsDropdownOptionChecked($option, $option->id), array('value' => $option->id)) ?>
								<?php echo $option->value ?>
						</label>
				</div>
		<?php endforeach; ?>
</div>
