<?php if(!empty($categories)):?>
    <h6><?php echo Yii::t('StoreModule.store', 'Categories');?></h6>
    <?php foreach($categories as $category):?>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('category[]',$this->controller->module->attributesFilter->isMainSearchParamChecked(AttributeFilter::MAIN_SEARCH_PARAM_CATEGORY, $category->id, Yii::app()->getRequest()),['value' => $category->id, 'id' => 'category_'. $category->id]);?>
                <?php echo $category->title; ?>
            </label>
        </div>
    <?php endforeach;?>
<?php endif;?>