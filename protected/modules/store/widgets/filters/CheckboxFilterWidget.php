<?php

class CheckboxFilterWidget extends CWidget
{
    public $view = 'checkbox-filter';

    public $attribute;

    public function init()
    {
        if (is_string($this->attribute)) {
            $this->attribute = StoreAttribute::model()->findByAttributes(array('name' => $this->attribute));
        }

        if (!($this->attribute instanceof StoreAttribute) || $this->attribute->type != StoreAttribute::TYPE_CHECKBOX) {
            throw new Exception('Attribute not found or the wrong type');
        }

        parent::init();
    }

    public function run()
    {
        $this->render($this->view, ['attribute' => $this->attribute]);
    }
} 
