<?php

class NumberFilterWidget extends CWidget
{
    public $view = 'number-filter';

    public $attribute;

    public function init()
    {
        if (is_string($this->attribute)) {
            $this->attribute = StoreAttribute::model()->findByAttributes(['column_name' => $this->attribute]);
        }

        if (!($this->attribute instanceof StoreAttribute) || $this->attribute->type != StoreAttribute::TYPE_NUMBER) {
            throw new Exception('Attribute not found or the wrong type');
        }

        parent::init();
    }

    public function run()
    {
        $this->render($this->view, array('attribute' => $this->attribute));
    }
} 
