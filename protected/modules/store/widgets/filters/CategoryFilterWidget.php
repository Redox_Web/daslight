<?php
class CategoryFilterWidget extends CWidget
{
    public $view = 'category-filter';
    public $dataProvider = null;

    public function run() {
        $this->render($this->view, array('categories' => StoreCategory::model()->cache(Yii::app()->params['cacheTime'])->findAll('root = :root && id <> :root', array(':root' => 21))));
    }
}
