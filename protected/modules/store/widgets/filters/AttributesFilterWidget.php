<?php

class AttributesFilterWidget extends CWidget
{
    public $attributes;

    public function run()
    {
        if('*' === $this->attributes) {
            $this->attributes = StoreAttribute::model()->with(array('options.attribute'))->cache(Yii::app()->params['cacheTime'])->findAll();
        }

        foreach ($this->attributes as $attribute) {
            $model = is_string($attribute) ? StoreAttribute::model()->findByAttributes(array('column_name' => $attribute)) : $attribute;

            if ($model) {
                switch ($model->type) {
                    case StoreAttribute::TYPE_DROPDOWN:
                        $this->widget('store.widgets.filters.DropdownFilterWidget', array('attribute' => $model));
                        break;
                    case StoreAttribute::TYPE_CHECKBOX:
                        $this->widget('store.widgets.filters.CheckboxFilterWidget', array('attribute' => $model));
                        break;
                    case StoreAttribute::TYPE_NUMBER:
                        $this->widget('store.widgets.filters.NumberFilterWidget', array('attribute' => $model));
                        break;
                    case StoreAttribute::TYPE_SHORT_TEXT:
                        $this->widget('store.widgets.filters.TextFilterWidget', array('attribute' => $model));
                        break;
                }
            }
        }
    }
}
