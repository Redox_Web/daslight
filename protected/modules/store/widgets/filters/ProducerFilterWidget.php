<?php
class ProducerFilterWidget extends CWidget
{
    public $view = 'producer-filter';
    public $dataProvider = null;

    public function run()
    {
        $this->render($this->view, array('producers' => StoreProducer::model()->cache(Yii::app()->params['cacheTime'])->findAll()));
    }
} 
