<?php

class TextFilterWidget extends CWidget
{
    public $view = 'text-filter';

    public $attribute;

    public function init()
    {
        if (is_string($this->attribute)) {
            $this->attribute = StoreAttribute::model()->findByAttributes(array('column_name' => $this->attribute));
        }

        if (!($this->attribute instanceof StoreAttribute) || (int)$this->attribute->type !== StoreAttribute::TYPE_SHORT_TEXT) {
            throw new Exception('Attribute not found or the wrong type');
        }

        parent::init();
    }

    public function run()
    {
        $this->render($this->view, array('attribute' => $this->attribute));
    }
} 
