<?php
class StarRating extends CWidget
{
    public $product = null;
    public $readOnly = false;
    public $dataSize = 'xs';
    public static $count = 1;
    /**
     * @var string Id of elements
     */
    public $view = 'shoppingCart';

    public function run()
    {
        if($this->product) {
            $this->registerScript();

            $disabled = true;
            if(!$this->readOnly)
                $disabled = (!$this->product->ratingLog && !Yii::app()->user->isGuest) ? false : true;

            echo CHtml::tag('input', array('type' => "hidden", 'id' => 'rating'.self::$count++, 'class' => "rating", 'data-step' => "0.5", 'data-disabled' => $disabled, 'data-size' => $this->dataSize, 'data-show-clear' => false, 'data-show-caption' => false, 'data-product-id' => $this->product->sku, 'value' => $this->product->rating), false, false);
        }
    }

    private function registerScript() {
        Yii::app()->clientScript->registerCssFile($this->controller->module->_localAssetsUrl.'/css/bootstrap-rating.css');
        Yii::app()->clientScript->registerScriptFile($this->controller->module->_localAssetsUrl.'/js/bootstrap-rating.min.js', CClientScript::POS_END);

        Yii::app()->clientScript->registerScript('starRatingAjax',"
            $('.rating').on('rating.change', function () {
                var self = $(this);
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    data: {index: self.val(), product: self.data('product-id')},
                    url: '/store/storeCatalog/rating',
                    success: function (data) {
                        if(data.result) {
                            self.rating('update', data.data);
                            self.rating('refresh', {disabled: true, showClear: false, value: data.data});
                        }
                    }
                });
            });
        ");
    }
}