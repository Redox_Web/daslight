<?php
class RandomProducts extends CWidget
{
    /**
     * @var string Id of elements
     */
    public $view = 'random-products';

    public function run()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'RAND()';
        $criteria->limit = 20;

        $products = StoreProduct::model()->findAll($criteria);
        $products = array_filter($products, array(new StoreProduct(), "checkActiveProducts"));

        if($products)
            $this->render($this->view, array('products' => $products));
    }
}