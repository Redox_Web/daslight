<?php
class ShoppingCartWidget extends CWidget
{
    /**
     * @var string Id of elements
     */
    public $view = 'shoppingCart';

    public function run()
    {
        $this->render($this->view);
    }
}
