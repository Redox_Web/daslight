<?php
Yii::import('booster.widgets.TbTabsLang');

class DTabsLang extends TbTabsLang
{
    public function run()
    {
        $id = $this->id;
        $content = array();
        $items = $this->normalizeTabs($this->tabs, $content);

        ob_start();
        $this->controller->widget('bootstrap.widgets.TbMenu', array(
            'type'=>$this->type,
            'encodeLabel'=>$this->encodeLabel,
            'htmlOptions'=>array('style'=>'margin-bottom: 5px;'),
            'items'=>$items,
        ));
        $tabs = ob_get_clean();

        ob_start();
        echo '<div class="tab-content">';
        echo implode('', $content);
        echo '</div>';
        $content = ob_get_clean();

        echo CHtml::openTag('div', $this->htmlOptions);
        //echo CHtml::activeLabelEx($this->labelModel, $this->labelAttribute, array('class' => 'col-sm-3 control-label'));
        //echo CHtml::openTag('div', array('class' => "col-sm-6"));
        echo $this->placement === self::PLACEMENT_BELOW ? $content.$tabs : $tabs.$content;
        echo '</div>';
        //echo '</div>';

        /** @var CClientScript $cs */
        $cs = Yii::app()->getClientScript();
        $cs->registerScript(__CLASS__.'#'.$id, "jQuery('#{$id}').tab();");

        foreach ($this->events as $name => $handler)
        {
            $handler = CJavaScript::encode($handler);
            $cs->registerScript(__CLASS__.'#'.$id.'_'.$name, "jQuery('#{$id}').on('{$name}', {$handler});");
        }
    }
}