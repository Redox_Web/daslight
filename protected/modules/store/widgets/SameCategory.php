<?php
class SameCategory extends CWidget
{
    public $view = 'same-category';
    public $limit = 9;
    /**
     * @var Product
     */
    public $product;

    public function run()
    {
        if (!$this->product)
            return;

        $criteria = new CDbCriteria();
        $criteria->condition = 'category_id = :category_id';
        $criteria->addCondition("id <> :product_id");
        $criteria->limit = $this->limit;
        $criteria->params[':category_id'] = $this->product->category_id;
        $criteria->params[':product_id'] = $this->product->id;

        $sameProducts = StoreProduct::model()->findAll($criteria);
        $category = StoreCategory::model()->findByPk($this->product->category_id);

        if($sameProducts)
            $this->render($this->view, array('sameProducts' => $sameProducts, 'category' => $category));
    }
}