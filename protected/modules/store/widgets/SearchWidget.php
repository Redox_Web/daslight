<?php
Yii::import('store.models.Search');

class SearchWidget extends CWidget
{
    public static $count = 0;

    public function run()
    {
        self::$count++;
        $this->render("search", array('model' => new Search(), 'count' => self::$count));
    }
}