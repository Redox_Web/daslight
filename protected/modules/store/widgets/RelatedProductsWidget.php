<?php
class RelatedProductsWidget extends CWidget
{
    public $view = 'related-products';
    /**
     * @var Product
     */
    public $product;

    public function run()
    {
        if (!$this->product) {
            return;
        }

        $this->render($this->view, ['dataProvider' => $this->product->getRelatedProductsDataProvider()]);
    }
}