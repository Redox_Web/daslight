<?php
Yii::import('backend.components.WebModule');

class StoreModule extends WebModule
{
    public $_localAssetsUrl;
    public $translatedLanguages = array('ru', 'ro');
    public $defaultLanguage = 'ru';
    public $showOrder = true;


	public function init()
	{
        parent::init();

		$this->setImport(array(
			'store.models.*',
			'store.components.*',
            'store.widgets.*',
		));

        $this->setComponents(array(
            'cart' => array(
                'class' => 'application.modules.store.components.extensions.ShoppingCart',
            ),
            'productRepository' => array(
                'class' => 'application.modules.store.components.ProductRepository',
            ),
            'attributesFilter' => array(
                'class' => 'application.modules.store.components.AttributeFilter',
            ),
            'orderNotifyService' => array(
                'class' => 'application.modules.store.components.OrderNotifyService',
            ),
        ));
	}

    public static function menuItem()
    {
        return array(
            'icon' => 'fa fa-shopping-cart',
            'label' => Yii::t('StoreModule.store', 'Store'),
            'items' => array(
                array(
                    'label' => Yii::t('StoreModule.store', 'Manufacturer'),
                    'url' => 'store/storeProducer/admin',
                    'sidebar_tab' => 'store-producer',
                ),
                array(
                    'label' => Yii::t('StoreModule.store', 'Categories'),
                    'url' => 'store/storeCategory/admin',
                    'sidebar_tab' => 'store-category',
                ),
                array(
                    'label' => Yii::t('StoreModule.store', 'Attributes'),
                    'url' => 'store/storeAttribute/admin',
                    'sidebar_tab' => 'store-attribute',
                ),
                array(
                    'label' => Yii::t('StoreModule.store', 'Product'),
                    'url' => 'store/storeProduct/admin',
                    'sidebar_tab' => 'store-product',
                ),
                array(
                    'label' => Yii::t('StoreModule.store', 'Delivery'),
                    'url' => 'store/storeDelivery/admin',
                    'sidebar_tab' => 'store-delivery',
                ),
                array(
                    'label' => Yii::t('StoreModule.store', 'Orders'),
                    'url' => 'store/storeOrder/admin',
                    'sidebar_tab' => 'store-order',
                ),
            )
        );
    }

    public static function urlRules()
    {
        return array(
            array('class' => 'store.components.storeProductRouter'),
            array('class' => 'store.components.searchProductRouter'),
            'store' => 'store/storeCatalog/index',
            'cabinet' => 'store/frontCabinet/index',
        );
    }

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
            if($controller instanceof Frontend) {
                if ($this->_localAssetsUrl === null)
                    $this->_localAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('store.assets'), false,-1, YII_DEBUG);

               // Yii::app()->clientScript->registerScriptFile($this->_localAssetsUrl.'/js/store.js', CClientScript::POS_END);
            }
			return true;
		}
		else
			return false;
	}
}
