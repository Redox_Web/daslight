<section class="products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="heading">
                            <h1>Search Results</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php
                        if ($dataSearch) {
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $dataSearch,
                                'itemView' => '_search_result', // refers to the partial view named '_post'
                                'summaryText' => false,
                                'pager'=> "LinkPager",
                                'cssFile' => false
                            ));
                        } else {
                            echo Yii::t("base", "Your query is too short!");
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>