
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="block-grid-item">
            <div class="block-title"><?php echo $data->name; ?></div>
            <div class="block-number"><?php echo Yii::t("StoreModule.store", "Product"); ?> # <?php echo $data->sku; ?></div>
            <div class="block-stars"><input type="hidden" class="rating" /></div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="block-text"><?php echo Yii::t("StoreModule.store", "Total");?></div>
                </div>
                <div class="col-xs-6">
                    <div class="block-price"><?php echo YHelper::formatCurrency($data->resultPrice); ?></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="color-red"><?php echo Yii::t("StoreModule.store", "Save"); ?> <?php echo $data->savePersent; ?>%</div>
                </div>
                <div class="col-xs-6">
                    <div class="block-old-price"><?php echo YHelper::formatCurrency($data->price); ?></div>
                </div>
            </div>
            <div class="block-button">
                <a href="#" class="quick-add-product-to-cart" data-product-id="<?php echo $data->sku; ?>">
                    <i class="fa fa-shopping-cart"></i> <?php echo Yii::t("StoreModule.store", "Add to Cart");?>
                </a>
            </div>
        </div>
    </div>
