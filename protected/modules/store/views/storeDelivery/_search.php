<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textAreaGroup($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'price',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'free_from',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'available_from',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'status',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'separate_payment',array('class'=>'form-control')); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
