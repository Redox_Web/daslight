<?php
	$this->breadcrumbs=array(
        'Store Deliveries'=>array('admin'),
        $model->name,
    );

    $this->menu=array(
    array('label'=>'Create StoreDelivery','url'=>array('create')),
    array('label'=>'Update StoreDelivery','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete StoreDelivery','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage StoreDelivery','url'=>array('admin')),
    );
    $this->title = "View StoreDelivery # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
		'description',
		'price',
		'free_from',
		'available_from',
		'status',
		'separate_payment',
),
)); ?>
