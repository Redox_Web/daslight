<?php
	$this->breadcrumbs=array(
        'Store Deliveries'=>array('admin'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	array('label'=>'Create StoreDelivery','url'=>array('create')),
	array('label'=>'View StoreDelivery','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage StoreDelivery','url'=>array('admin')),
	);
    $this->title = "Update StoreDelivery <?php echo $model->id; ?>";
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>