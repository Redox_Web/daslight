<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'store-delivery-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php echo $form->tinyMceGroup($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

	<?php echo $form->textFieldGroup($model,'price',array('class'=>'form-control')); ?>

	<?php echo $form->textFieldGroup($model,'free_from',array('class'=>'form-control')); ?>

	<?php echo $form->textFieldGroup($model,'available_from',array('class'=>'form-control')); ?>

    <?php echo $form->dropDownListGroup($model,'status', StoreDelivery::getStatusList(), array('class'=>'form-control')); ?>

    <?php echo $form->checkboxGroup($model, 'separate_payment'); ?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>
