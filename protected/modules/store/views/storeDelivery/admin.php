<?php
	$this->breadcrumbs=array(
        'Store Deliveries'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
    array('label'=>'Create StoreDelivery','url'=>array('create')),
    );
    $this->title = "Manage Store Deliveries";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'store-delivery-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'name',
		'price',
		'free_from',
		'available_from',
		/*
		'status',
		'separate_payment',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
