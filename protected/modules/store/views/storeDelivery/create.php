<?php
	$this->breadcrumbs=array(
        'Store Deliveries'=>array('admin'),
        'Create',
    );

    $this->menu=array(
    array('label'=>'Manage StoreDelivery','url'=>array('admin')),
    );
    $this->title = "Create StoreDelivery";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>