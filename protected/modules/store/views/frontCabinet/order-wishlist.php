<section class="products">
    <div class="container">
        <div class="row">
            <?php $this->renderPartial('_cabinetSidebar'); ?>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="heading">
                            <h2><?php echo Yii::t("StoreModule.store", "Wish list");?></h2>
                        </div>
                    </div>
                </div>
                <div class="table-list">
                    <?php
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider' => $userOrders,
                        'itemView' => '_wishlist-product', // refers to the partial view named '_post'
                        'summaryText' => false,
                        'pager'=> "LinkPager",
                        'cssFile' => false
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>