<div class="cart-item">
    <div class="row">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hidden-xs">
            <div class="block-image-table">
                <?php $image = $data->product->contypeMainImage ? $data->product->contypeMainImage->path : ''; ?>
                <img src="<?php echo YHelper::getImagePath($image, 211, 211); ?>" alt="image" />
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-6">
            <div class="block-title"><?php echo $data->product->name; ?></div>
            <div class="block-number"><?php echo Yii::t("StoreModule.store", "Product"); ?> # <?php echo $data->product->sku; ?></div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
            <div class="block-price"><?php echo $data->product->quantity; ?> x <?php echo YHelper::formatCurrency($data->product->resultPrice); ?></div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
            <?php echo Yii::app()->format->date($data->order->created); ?>
            <?php $color = ''; if($data->order->status == StoreOrder::STATUS_NEW)
                        $color = 'color-yellow';
                elseif($data->order->status == StoreOrder::STATUS_ACCEPTED)
                    $color = 'color-blue';
                elseif($data->order->status == StoreOrder::STATUS_FINISHED)
                    $color = 'color-green';
                 ?>
            <p class="<?php echo $color; ?>">
                <?php echo StoreOrder::model()->getStatusList($data->order->status); ?>
            </p>
        </div>
    </div>
</div>