<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
    <div class="block-menu">
        <ul>
            <li class="active">
                <a href="<?php echo $this->createUrl('/store/frontCabinet/index'); ?>">
                    <i class="fa fa-star-o"></i> <?php echo Yii::t("StoreModule.store", "My Profile");?>
                </a>
            </li>
            <li>
                <a href="<?php echo $this->createUrl('/store/frontCabinet/orderHistory'); ?>">
                    <i class="fa fa-car"></i> <?php echo Yii::t("StoreModule.store", "Order history");?>
                </a>
            </li>
        </ul>
    </div>
</div>