<section class="products">
    <div class="container">
        <div class="row">
            <?php $this->renderPartial('_cabinetSidebar'); ?>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="heading">
                            <h2>My Account</h2>
                        </div>
                    </div>
                </div>
                <div class="block-white">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'cabinet-form',
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                        ),
                    )); ?>
                        <h5 class="text-center">Billing info</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'First Name')); ?>
                                    <?php echo $form->error($model, 'name'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $form->textField($model, 'surname', array('class' => 'form-control', 'placeholder' => 'Last Name')); ?>
                                    <?php echo $form->error($model, 'surname'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => 'E-mail')); ?>
                                    <?php echo $form->error($model, 'email'); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                        $this->widget('CMaskedTextField', array(
                                            'model' => $model,
                                            'attribute' => 'phone',
                                            'mask' => '(999)-999-999',
                                            'htmlOptions' => array('class' => 'form-control', 'placeholder' => '(***)-***-***')
                                        ));
                                    ?>
                                    <?php echo $form->error($model, 'phone'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php echo $form->textField($model, 'address', array('class' => 'form-control', 'placeholder' => 'Address')); ?>
                                    <?php echo $form->error($model, 'address'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="nav-buttons">
                            <div class="row">
                                <div class="col-xs-offset-0 col-xs-12 col-sm-offset-8 col-sm-4 col-md-offset-9 col-md-3 col-lg-offset-10 col-lg-2">
                                    <a href="#" class="button-block button-red" onclick="$('#cabinet-form').submit();">
                                        <i class="fa fa-check"></i> Save
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php $this->endWidget(); ?>
                </div>

            </div>

        </div>
    </div>
</section>