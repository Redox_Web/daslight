<?php $this->beginContent(Yii::app()->getModule('backend')->getBackendLayoutAlias('main')); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-indigo">
                <div class="panel-heading">
                    <h4><?php echo $this->title; ?></h4>
                    <div class="options">
                        <div class="options">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#domprogress"><i class="fa fa-file-text-o"></i> General </a></li>
                                <li class=""><a data-toggle="tab" href="#attributeprogress"><i class="fa fa-picture-o"></i> Attributes</a></li>
                                <?php if($this->action->id != 'create') { ?>
                                    <!--<li class=""><a data-toggle="tab" href="#relatedproducts"><i class="fa fa-picture-o"></i> Related Products</a></li>-->
                                    <li class=""><a data-toggle="tab" href="#codeprogress"><i class="fa fa-picture-o"></i> Pictures</a></li>
                                <?php } ?>
                                <li class=""><a data-toggle="tab" href="#metatags"><i class="fa fa-tags"></i> Seo tags</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="border-radius: 0px;">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $form=$this->beginWidget('backend.components.ActiveForm',array(
                                'id'=>'store-producer-form',
                                'enableAjaxValidation'=>false,
                                'type'=>'horizontal',
                                'htmlOptions'=>array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
                            )); ?>
                                <div class="tab-content">
                                    <div id="domprogress" class="tab-pane active">
                                        <?php echo $content; ?>
                                    </div>
                                    <div id="attributeprogress" class="tab-pane">
                                        <?php $this->showClip('store_attributes'); ?>
                                    </div>
                                    <?php if($this->action->id != 'create') { ?>
                                        <div id="relatedproducts" class="tab-pane">
                                            <?php $this->showClip('related_products'); ?>
                                        </div>
                                        <div id="codeprogress" class="tab-pane">
                                            <?php $this->showClip('pictures_content'); ?>
                                        </div>
                                    <?php } ?>
                                    <div id="metatags" class="tab-pane">
                                        <?php $this->showClip('meta_tags'); ?>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
		Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/fancybox/jquery.fancybox.css');
		Yii::app()->clientScript->registerScriptFile($this->module->assetsUrl.'/fancybox/jquery.fancybox.pack.js', CClientScript::POS_END);
	?>
    <script>
        $(document).ready(function() {
            $("a.images-group").fancybox();
        });
    </script>

<?php $this->endContent() ; ?>