<?php
	$this->breadcrumbs=array(
        'Store Producers'=>array('admin'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	array('label'=>'Create StoreProducer','url'=>array('create')),
	array('label'=>'View StoreProducer','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage StoreProducer','url'=>array('admin')),
	);
    $this->title = "Update StoreProducer <?php echo $model->id; ?>";
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>