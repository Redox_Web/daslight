<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'slug',array('class'=>'form-control','maxlength'=>150)); ?>

		<?php echo $form->textFieldGroup($model,'image',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textAreaGroup($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'meta_title',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'meta_keywords',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'meta_description',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'status',array('class'=>'form-control','maxlength'=>10)); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
