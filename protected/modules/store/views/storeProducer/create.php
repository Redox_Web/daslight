<?php
	$this->breadcrumbs=array(
        'Store Producers'=>array('admin'),
        'Create',
    );

    $this->menu=array(
    array('label'=>'Manage StoreProducer','url'=>array('admin')),
    );
    $this->title = "Create StoreProducer";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>