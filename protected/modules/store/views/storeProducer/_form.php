<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'store-producer-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
)); ?>

    <?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>250)); ?>

    <?php echo $form->fileFieldGroup($model,'image',array('class'=>'form-control','maxlength'=>250)); ?>

    <?php echo $form->tinyMceGroup($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

	<?php /*echo $form->textFieldGroup($model,'meta_title',array('class'=>'form-control','maxlength'=>250)); */?><!--

	<?php /*echo $form->textFieldGroup($model,'meta_keywords',array('class'=>'form-control','maxlength'=>250)); */?>

	--><?php /*echo $form->textFieldGroup($model,'meta_description',array('class'=>'form-control','maxlength'=>250)); */?>

    <?php echo $form->dropDownListGroup($model,'status', $model->activeStatusList ,array('class'=>'span5')); ?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>
