<?php
	$this->breadcrumbs=array(
        'Store Producers'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
    array('label'=>'Create StoreProducer','url'=>array('create')),
    );
    $this->title = "Manage Store Producers";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'store-producer-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'name',
		'slug',
		'image:image',
        'status',
		/*
		'meta_title',
		'description',
		'meta_keywords',
		'meta_description',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
