<?php
	$this->breadcrumbs=array(
        'Store Producers'=>array('admin'),
        $model->name,
    );

    $this->menu=array(
    array('label'=>'Create StoreProducer','url'=>array('create')),
    array('label'=>'Update StoreProducer','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete StoreProducer','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage StoreProducer','url'=>array('admin')),
    );
    $this->title = "View StoreProducer # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'name',
        'image:image',
        'description',
        'status',
    ),
)); ?>
