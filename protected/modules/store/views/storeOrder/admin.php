<?php
	$this->breadcrumbs=array(
        'Store Orders'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
    array('label'=>'Create StoreOrder','url'=>array('create')),
    );
    $this->title = "Manage Store Orders";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'store-order-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'created',
		'user_id:user',
        array(
            'name' => 'total_price',
            'value' => 'Yii::app()->numberFormatter->formatCurrency($data->total_price, "MDL")'
        ),
        array(
            'class' => 'store.widgets.EditableStatusColumn',
            'name' => 'status',
            'filter' => false,
            'editable' => array(
                'type' => 'select2',
                'url' => $this->createUrl('/store/storeOrder/statusUpdate'),
                'placement' => 'left',
                'source' => StoreOrder::model()->getStatusList(),
                'options' => StoreOrder::model()->getLabels(),
            ),
        ),
        array(
            'class' => 'store.widgets.EditableStatusColumn',
            'name' => 'paid',
            'filter' => false,
            'editable' => array(
                'type' => 'select2',
                'url' => $this->createUrl('/store/storeOrder/statusUpdate'),
                'placement' => 'left',
                'source' => StoreOrder::model()->getPaidStatusList(),
                'options' => array(
                    StoreOrder::PAID_STATUS_PAID => array('class' => 'btn btn-success'),
                    StoreOrder::PAID_STATUS_NOT_PAID => array('class' => 'btn btn-danger'),
                ),
            ),
        ),
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
