<?php
	$this->breadcrumbs=array(
        'Store Orders'=>array('admin'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	array('label'=>'Create StoreOrder','url'=>array('create')),
	array('label'=>'View StoreOrder','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage StoreOrder','url'=>array('admin')),
	);
    $this->title = "Update StoreOrder #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>