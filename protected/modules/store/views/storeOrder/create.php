<?php
	$this->breadcrumbs=array(
        'Store Orders'=>array('admin'),
        'Create',
    );

    $this->menu=array(
    array('label'=>'Manage StoreOrder','url'=>array('admin')),
    );
    $this->title = "Create StoreOrder";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>