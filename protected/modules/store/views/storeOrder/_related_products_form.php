<?php $this->widget('booster.widgets.TbGridView', array(
    'id' => 'store-product-grid',
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->productSearch(),
    //'filter'=>$model,
    'columns' => array(
        'id',
        'product_id',
        'product_name',
        'price',
        array(
            'class' => 'booster.widgets.TbEditableColumn',
            'name' => 'quantity',
            'editable' => array(
                'type' => 'number',
                'url' => $this->createUrl('/store/StoreOrder/quantityUpdate'),
                'placement' => 'left',
                'success' => 'js:function(response) {
                    if (response.result == true) {
                        $("#total_price").html(response.data.totalPrice);
                        $("#StoreOrder_total_price").val(response.data.totalPrice);
                    }
                }',
            ),
        ),
        'sku',
        array(
            'class' => 'backend.components.ButtonColumn',
            'template' => '{view} {delete}',
            'htmlOptions' => array('width' => '70px'),
            'viewButtonUrl' => function ($data) {
                    return Yii::app()->createUrl("/store/storeProduct/view", array('id' => $data->product_id));
                },
            'deleteButtonUrl' => function ($data) {
                    return Yii::app()->createUrl("/store/storeOrderProduct/delete", array('id' => $data->id));
                },
        ),
    ),
)); ?>

<div class="row">
    <div class="col-sm-10"></div>
    <div class="col-sm-2">
        <h2>Total Price: <span id="total_price"><?php echo $model->total_price; ?></span></h2>
    </div>
</div>

<div class="row" style="margin-top:20px">
    <div class="col-sm-10">
        <div class="form-group">
            <?php $this->widget(
                'booster.widgets.TbSelect2',
                [
                    'name' => 'ProductSelect',
                    'asDropDownList' => false,
                    'options' => [
                        'minimumInputLength' => 2,
                        'placeholder' => 'Select product',
                        'width' => '100%',
                        'allowClear' => true,
                        'ajax' => [
                            'url' => Yii::app()->controller->createUrl('/store/storeOrderProduct/ajaxSearch'),
                            'dataType' => 'json',
                            'data' => 'js:function(term, page) { return {q: term }; }',
                            'results' => 'js:function(data) { return {results: data}; }',
                        ],
                        'formatResult' => 'js:productFormatResult',
                        'formatSelection' => 'js:productFormatSelection',
                    ],
                    'htmlOptions' => [
                        'id' => 'product-select',
                        'class' => 'form-control'
                    ],
                ]
            );?>
        </div>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-primary btn btn-default btn-block" href="#" id="add-product-to-order">
            <?= Yii::t('StoreModule.store','Add'); ?>
        </a>
    </div>
</div>

<script type="text/javascript">
    $('#add-product-to-order').click(function (e) {
        e.preventDefault();
        var productId = $("#product-select").select2("val");
        if (productId) {
            $.ajax({
                url: '<?= Yii::app()->controller->createUrl('/store/storeOrderProduct/productRow')?>',
                type: 'get',
                data: {
                    'product_id': productId,
                    'order_id': <?php echo $model->id; ?>,
                },
                success: function (data) {
                    $.fn.yiiGridView.update('store-product-grid');
                }
            });
        }
    });

    function productFormatSelection(product) {
        return product.name;
    }

    function productFormatResult(product) {
        var markup = "<table class='result'><tr>";
        if (product.thumb !== undefined) {
            markup += "<td width='30px'><img src='" + product.thumb + "' class=''/></td>";
        }
        markup += "<td class='info'><div class='title'>" + product.name + "</div>";
        markup += "</td></tr></table>";
        return markup;
    }
</script>