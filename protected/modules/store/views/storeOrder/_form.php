<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'store-order-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListGroup($model,'status', StoreOrder::model()->getStatusList(), array('class'=>'form-control')); ?>

    <?php echo $form->datePickerGroup($model,'created', array('class'=>'form-control','maxlength'=>255, 'prepend' => '<i class="fa fa-calendar"></i>')); ?>

    <?php echo $form->dropDownSelect2Group($model, 'user_id', CHtml::listData(User::model()->findAll(), 'id', 'name'));  ?>

	<?php /*echo $form->textFieldGroup($model,'delivery_id',array('class'=>'form-control')); */?>

	<?php /*echo $form->textFieldGroup($model,'delivery_price',array('class'=>'form-control','maxlength'=>10)); */?>

	<?php /*echo $form->textFieldGroup($model,'payment_method_id',array('class'=>'form-control')); */?>

    <?php echo $form->checkboxGroup($model,'paid',array('class'=>'form-control')); ?>

	<?php /*echo $form->textFieldGroup($model,'payment_time',array('class'=>'form-control')); */?>

	<?php /*echo $form->textAreaGroup($model,'payment_details',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); */?>

	<?php echo $form->textFieldGroup($model,'total_price',array('class'=>'form-control','maxlength'=>10,'readOnly'=>true)); ?>

	<?php /*echo $form->textFieldGroup($model,'discount',array('class'=>'form-control','maxlength'=>10)); */?>

	<?php /*echo $form->textFieldGroup($model,'coupon_discount',array('class'=>'form-control','maxlength'=>10)); */?>

	<?php /*echo $form->textFieldGroup($model,'separate_delivery',array('class'=>'form-control')); */?>

	<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php echo $form->textFieldGroup($model,'address',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php echo $form->textFieldGroup($model,'phone',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php echo $form->textFieldGroup($model,'email',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php echo $form->textAreaGroup($model,'comment',array('class'=>'form-control','maxlength'=>1024)); ?>

	<?php echo $form->textFieldGroup($model,'ip',array('class'=>'form-control','maxlength'=>15)); ?>

	<?php echo $form->textFieldGroup($model,'url',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php /*echo $form->textFieldGroup($model,'note',array('class'=>'form-control','maxlength'=>1024)); */?>

	<?php /*echo $form->textFieldGroup($model,'updated',array('class'=>'form-control')); */?>


    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>

<?php $this->tabTitle = 'Cart'; ?>
<?php $this->beginClip('left_column_content'); ?>
    <?php if(!$model->isNewRecord) : ?>
        <?php $this->renderPartial('_related_products_form', array('model' => $model)); ?>
    <?php endif; ?>
<?php $this->endClip(); ?>
