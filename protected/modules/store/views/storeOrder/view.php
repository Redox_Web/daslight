<?php
$this->breadcrumbs=array(
    'Store Orders'=>array('admin'),
    $model->name,
);

$this->menu=array(
    array('label'=>'Create StoreOrder','url'=>array('create')),
    array('label'=>'Update StoreOrder','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete StoreOrder','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage StoreOrder','url'=>array('admin')),
);
$this->title = "View StoreOrder # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        /*'delivery_id',
        'delivery_price',
        'payment_method_id',*/
        'paid:boolean',
        /*'payment_time',
        'payment_details',*/
        'total_price',
        'discount',
        /*'coupon_discount',
        'separate_delivery',*/
        'status:orderstatus',
        'user_id',
        'name',
        'address',
        'phone',
        'email',
        'comment',
        'ip',
        'url',
        'note',
        'created',
        'updated',
    ),
)); ?>