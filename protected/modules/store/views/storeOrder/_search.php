<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'delivery_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'delivery_price',array('class'=>'form-control','maxlength'=>10)); ?>

		<?php echo $form->textFieldGroup($model,'payment_method_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'paid',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'payment_time',array('class'=>'form-control')); ?>

		<?php echo $form->textAreaGroup($model,'payment_details',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'total_price',array('class'=>'form-control','maxlength'=>10)); ?>

		<?php echo $form->textFieldGroup($model,'discount',array('class'=>'form-control','maxlength'=>10)); ?>

		<?php echo $form->textFieldGroup($model,'coupon_discount',array('class'=>'form-control','maxlength'=>10)); ?>

		<?php echo $form->textFieldGroup($model,'separate_delivery',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'status_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'date',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'user_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'address',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'phone',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'email',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'comment',array('class'=>'form-control','maxlength'=>1024)); ?>

		<?php echo $form->textFieldGroup($model,'ip',array('class'=>'form-control','maxlength'=>15)); ?>

		<?php echo $form->textFieldGroup($model,'url',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'note',array('class'=>'form-control','maxlength'=>1024)); ?>

		<?php echo $form->textFieldGroup($model,'modified',array('class'=>'form-control')); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
