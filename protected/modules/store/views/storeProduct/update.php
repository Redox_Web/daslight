<?php
	$this->breadcrumbs=array(
        'Store Products'=>array('admin'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	array('label'=>'Create StoreProduct','url'=>array('create')),
	array('label'=>'View StoreProduct','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage StoreProduct','url'=>array('admin')),
	);
    $this->title = "Update StoreProduct <?php echo $model->id; ?>";
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'searchModel' => $searchModel)); ?>