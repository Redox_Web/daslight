<?php
	$this->breadcrumbs=array(
        'Store Products'=>array('admin'),
        $model->name,
    );

    $this->menu=array(
    array('label'=>'Create StoreProduct','url'=>array('create')),
    array('label'=>'Update StoreProduct','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete StoreProduct','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage StoreProduct','url'=>array('admin')),
    );
    $this->title = "View StoreProduct # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'producer_id',
		'category_id',
		'sku',
		'name',
		'price',
		'old_price',
		'short_description',
		'is_special',
		'quantity',
		'status',
		'created',
		'updated',
),
)); ?>
