<?php
	$model->disableDefaultScope()->admin();
	$this->breadcrumbs=array(
        'Store Products'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
    array('label'=>'Create StoreProduct','url'=>array('create')),
    );
    $this->title = "Manage Store Products";
?>

<?php $this->widget('ExtendedGridView',array(
    'id'=>'store-product-grid',
    'type' => 'striped bordered condensed',
    'selectableRows' => 2,
    'skin'=>'nested',
    'nested' => true,
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
            'htmlOptions' => array('class' => 'move-block'),
            'type'=>'raw',
            'value'=>function(){
                echo "<span class='handle-sortable'></span>";
            }
        ),
		'id',
		'producer_id:storeProducer',
		'category_id:storeCategory',
		'sku',
		'name_'.Yii::app()->language,
		'slug',
		/*
		'price',
		'old_price',
		'description',
		'short_description',
		'is_special',
		'quantity',
		'in_stock',
		'status',
		'created',
		'updated',
		'meta_title',
		'meta_keywords',
		'meta_description',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
