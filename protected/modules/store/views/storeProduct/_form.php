<?php $form = new ActiveForm();
    $form->id = 'store-producer-form';
    $form->enableAjaxValidation = false;
    $form->type = 'horizontal';
    $form->htmlOptions = 'array("class" =>"form-horizontal row-border", "enctype"=>"multipart/form-data")';
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListGroup($model,'category_id', StoreProduct::getCategoryTree(),array('class'=>'form-control')); ?>

<?php echo $form->dropDownListGroup($model,'producer_id', CHtml::listData(StoreProducer::model()->findAll(), 'id', 'name'), array('class'=>'form-control')); ?>

<?php echo $form->textFieldGroup($model,'sku',array('class'=>'form-control','maxlength'=>100)); ?>

<?php $this->widget('booster.widgets.TbTabsLang', array(
    'type'=>'pills',
    'labelModel' => $model,
    'labelAttribute' => 'name_ru',
    'htmlOptions'=>array('class'=>'form-group'),
    'tabs'=>array(
        array('label'=>'RU', 'content'=>$form->textField($model,'name_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('name_ru'))),
        array('label'=>'RO', 'content'=>$form->textField($model,'name_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('name_ro'))),
    ),
));?>


<?php echo $form->textFieldGroup($model,'price',array('class'=>'form-control','maxlength'=>19)); ?>

<?php echo $form->textFieldGroup($model,'old_price',array('class'=>'form-control','maxlength'=>19)); ?>

<?php $this->widget('booster.widgets.TbTabsLang', array(
    'type'=>'pills',
    'labelModel' => $model,
    'labelAttribute' => 'short_description_ru',
    'htmlOptions'=>array('class'=>'form-group'),
    'tabs'=>array(
        array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'short_description_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('short_description_ru'))),
        array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'short_description_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('short_description_ro'))),
    ),
)); ?>

<?php $this->widget('booster.widgets.TbTabsLang', array(
    'type'=>'pills',
    'labelModel' => $model,
    'labelAttribute' => 'description_ru',
    'htmlOptions'=>array('class'=>'form-group'),
    'tabs'=>array(
        array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'description_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('description_ru'))),
        array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'description_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('description_ro'))),
    ),
)); ?>

<?php echo $form->fileFieldGroup($model,'pdf',array('class'=>'form-control','maxlength'=>250)); ?>

<?php /*echo $form->tinyMceGroup($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); */?>

<?php /*echo $form->checkboxGroup($model, 'is_special'); */?>

<?php /*echo $form->textFieldGroup($model,'quantity',array('class'=>'form-control')); */?>

<?php echo $form->dropDownListGroup($model,'status', StoreProduct::model()->getInStockList(), array('class'=>'form-control')); ?>

<?php echo $form->checkboxGroup($model, 'is_active',
    array('widgetOptions' =>
        array(
            'htmlOptions' => array(
                'checked'=>$model->isNewRecord ? true : $model->is_active
            ),
        )
    )
); ?>

<?php $this->widget('booster.widgets.TbButton', array(
    'buttonType'=>'formSubmit',
    'htmlOptions' => array('class' => 'btn btn-primary'),
    'label'=>$model->isNewRecord ? 'Create' : 'Save',
)); ?>

<?php $this->beginClip('store_attributes');?>
    <?php $this->renderPartial('_attribute_form', array('category' => $model->category, 'model' => $model)); ?>
<?php $this->endClip(); ?>

<?php $this->beginClip('related_products');?>
    <?php if(!$model->isNewRecord) : ?>
        <?php $this->renderPartial('_related_products_form', array('model' => $model, 'searchModel' => $searchModel)); ?>
    <?php endif; ?>
<?php $this->endClip(); ?>

<?php $this->beginClip('pictures_content');?>
    <?php if(!$model->isNewRecord) : ?>
        <?php $form->uploadifyRow($model, 'path', 'contypeImagesList'); ?>
    <?php endif; ?>
<?php $this->endClip(); ?>

<?php $this->beginClip('meta_tags');?>
    <?php if(!$model->isNewRecord) : ?>
        <?php $this->renderPartial('_meta_tags', array('model' => $model, 'form'=>$form)); ?>
    <?php endif; ?>
<?php $this->endClip(); ?>

<script type="text/javascript">
    $(function () {
        $('#StoreProduct_category_id').on('change',function () {
            var typeId = $(this).val();
            if (typeId) {
                $('#attributeprogress').load('<?= Yii::app()->createUrl('/store/storeProduct/typeAttributesForm');?>/' + typeId);
            }
            else {
                $('#attributeprogress').html('');
            }
        });
    });
</script>