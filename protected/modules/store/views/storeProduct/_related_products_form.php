<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'order-form',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$searchModel->searchNotFor($model->id),
    'ajaxUrl' => array('/store/related/index/'.$model->id),
    'filter'=>$searchModel,
    'columns'=>array(
        'id',
        'name',
        'sku',
        'category_id',
        'price',
        array(
            'class'=>'DToggleColumn',
            // атрибут модели
            'name'=>'related',
            // заголовок столбца
            'header' => false,
            // запрос подтвердждения (если нужен)
            'onImageHtml' => '<span class="icon-on">Related</span>',
            'offImageHtml' => '<span class="icon-off">Related</span>',
            'aTagOn' => 'btn btn-success',
            'aTagOff' => 'btn btn-default-alt',
            'aToggle' => 'btn-success btn-default-alt',
            'linkUrl'=>'Yii::app()->controller->createUrl("storeProduct/related", array("id"=>'.$model->id.', "related_product"=>$data->id, "param"=>StoreRelated::RELATED))',
            'confirmation' => false,
            'value' => '$data->related_button('.$model->id.', StoreRelated::RELATED)',
            // фильтр
            'filter' => false,
            // alt для иконок (так как отличается от стандартного)
            'htmlOptions' => array('style'=>'width:30px'),
        ),
        array(
            'class'=>'DToggleColumn',
            // атрибут модели
            'name'=>'related',
            // заголовок столбца
            'header' => false,
            // запрос подтвердждения (если нужен)
            'onImageHtml' => '<span class="icon-on">Companion</span>',
            'offImageHtml' => '<span class="icon-off">Companion</span>',
            'aTagOn' => 'btn btn-success',
            'aTagOff' => 'btn btn-default-alt',
            'aToggle' => 'btn-success btn-default-alt',
            'linkUrl'=>'Yii::app()->controller->createUrl("storeProduct/related", array("id"=>'.$model->id.', "related_product"=>$data->id, "param"=>StoreRelated::COMPANION))',
            'confirmation' => false,
            'value' => '$data->related_button('.$model->id.', StoreRelated::COMPANION)',
            // фильтр
            'filter' => false,
            // alt для иконок (так как отличается от стандартного)
            'htmlOptions' => array('style'=>'width:30px'),
        ),
    ),
)); ?>