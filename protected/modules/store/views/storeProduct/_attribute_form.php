<?php
    $activeAttr = null;
    if (!empty($category->attribute))
        $activeAttr = $category->attribute(array('order' => 'ord ASC'));
    else {
        $activeAttr = StoreAttribute::model()->findByPk($model->category_id);
    }
?>

<?php if (!empty($activeAttr)) { ?>
    <?php foreach($activeAttr as $attribute) { ?>
        <?php if(isset($attribute->column_name)) { ?>
            <?php $hasError = $model->hasErrors('eav.' . $attribute->column_name); ?>
            <div class="form-group">
                <label for="StoreAttribute_<?php echo $attribute->column_name; ?>" class="col-sm-3 control-label control-label <?php echo $attribute->required ? 'required' : ''?> <?php echo $hasError ? "error" : ""; ?>">
                    <?php echo $attribute->title; ?>
                    <?php if ($attribute->required) { ?>
                        <span class="required">*</span>
                    <?php } ?>
                    <?php if ($attribute->unit) { ?>
                        <span>(<?php echo $attribute->unit; ?>)</span>
                    <?php } ?>
                </label>
                <div class="col-sm-6">
                    <?php echo $attribute->renderField($model->attribute($attribute->column_name), null, array('class' => $hasError ? "error form-control" : "form-control")); ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>

<?php $this->widget('booster.widgets.TbButton', array(
    'buttonType'=>'formSubmit',
    'htmlOptions' => array('class' => 'btn btn-primary'),
    'label'=>$model->isNewRecord ? 'Create' : 'Save',
)); ?>