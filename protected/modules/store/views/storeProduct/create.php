<?php
	$this->breadcrumbs=array(
        'Store Products'=>array('admin'),
        'Create',
    );

    $this->menu=array(
    array('label'=>'Manage StoreProduct','url'=>array('admin')),
    );
    $this->title = "Create StoreProduct";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>