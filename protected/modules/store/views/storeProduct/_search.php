<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'producer_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'category_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'sku',array('class'=>'form-control','maxlength'=>100)); ?>

		<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'slug',array('class'=>'form-control','maxlength'=>150)); ?>

		<?php echo $form->textFieldGroup($model,'price',array('class'=>'form-control','maxlength'=>19)); ?>

		<?php echo $form->textFieldGroup($model,'old_price',array('class'=>'form-control','maxlength'=>19)); ?>

		<?php echo $form->textAreaGroup($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

		<?php echo $form->textAreaGroup($model,'short_description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'is_special',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'quantity',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'in_stock',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'status',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'created',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'updated',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'meta_title',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'meta_keywords',array('class'=>'form-control','maxlength'=>250)); ?>

		<?php echo $form->textFieldGroup($model,'meta_description',array('class'=>'form-control','maxlength'=>250)); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
