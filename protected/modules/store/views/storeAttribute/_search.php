<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'title',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'type',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'unit',array('class'=>'form-control','maxlength'=>30)); ?>

		<?php echo $form->textFieldGroup($model,'required',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'inshort',array('class'=>'form-control')); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
