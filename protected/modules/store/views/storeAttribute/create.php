<?php
	$this->breadcrumbs=array(
        'Store Attributes'=>array('admin'),
        'Create',
    );

    $this->menu=array(
    array('label'=>'Manage StoreAttribute','url'=>array('admin')),
    );
    $this->title = "Create StoreAttribute";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>