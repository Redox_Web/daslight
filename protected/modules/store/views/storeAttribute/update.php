<?php
	$this->breadcrumbs=array(
        'Store Attributes'=>array('admin'),
        $model->title=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	array('label'=>'Create StoreAttribute','url'=>array('create')),
	array('label'=>'View StoreAttribute','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage StoreAttribute','url'=>array('admin')),
	);
    $this->title = "Update StoreAttribute <?php echo $model->id; ?>";
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>