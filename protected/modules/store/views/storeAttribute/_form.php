<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'store-attribute-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListGroup($model,'type', StoreAttribute::getTypesList(),array('class'=>'form-control')); ?>

    <?php echo $form->dropDownListGroup($model,'lang', StoreAttribute::model()->getLangList(),array('class'=>'form-control')); ?>

    <?php echo $form->textFieldGroup($model,'title',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php echo $form->textFieldGroup($model,'unit',array('class'=>'form-control','maxlength'=>30)); ?>

    <?php echo $form->checkboxGroup($model, 'required'); ?>

    <?php echo $form->checkboxGroup($model, 'inshort'); ?>

    <?php $class = !in_array($model->type, StoreAttribute::getTypesWithOptions()) ? ' hidden' : ''; ?>
    <?php echo $form->textAreaGroup($model,'rawOptions',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array(
                    'rows'=>10,
                    'class'=>'form-control',
                    'value' => $model->getRawOptions()
                ),
            ),
            'groupOptions' => array('class' => "options".$class)
        )
    ); ?>


    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(function () {
        $('#StoreAttribute_type').change(function () {
            if ($.inArray(parseInt($(this).val()), [<?php echo implode(',', StoreAttribute::getTypesWithOptions());?>]) >= 0) {
                $('.options').removeClass('hidden');
            }
            else {
                $('.options').addClass('hidden');
            }
        });
    });
</script>