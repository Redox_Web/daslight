<?php
	$this->breadcrumbs=array(
        'Store Attributes'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
    array('label'=>'Create StoreAttribute','url'=>array('create')),
    );
    $this->title = "Manage Store Attributes";
?>

<?php $this->widget('DTbGridView',array(
    'id'=>'store-attribute-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'skin'=>'sizer',
    'sizerVariants' => array(20, 40, 60, 80),
    //'skin'=>'nested',
    'columns'=>array(
        array(
            'htmlOptions' => array('class' => 'move-block'),
            'type'=>'raw',
            'value'=>function(){
                echo "<span class='handle-sortable'></span>";
            }
        ),
		'id',
		'title',
		'type',
		'unit',
		'required',
        'lang:lang',
		/*
		'inshort',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
