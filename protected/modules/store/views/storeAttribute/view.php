<?php
	$this->breadcrumbs=array(
        'Store Attributes'=>array('admin'),
        $model->title,
    );

    $this->menu=array(
    array('label'=>'Create StoreAttribute','url'=>array('create')),
    array('label'=>'Update StoreAttribute','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete StoreAttribute','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage StoreAttribute','url'=>array('admin')),
    );
    $this->title = "View StoreAttribute # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'title',
        'type:storeType',
        'unit',
        'required:boolean',
        'inshort:boolean',
        'lang:lang',
    ),
)); ?>
