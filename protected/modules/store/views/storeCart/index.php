<div class="col-sm-8 col-md-8 col-lg-9">
    <div class="custom-breadcrumbs">
        <ul>
            <li><a href="#">Главная</a></li>
            <li>Корзина</li>
        </ul>
    </div>
    <div class="block-white">
        <div class="static-page">
            <h2><?php echo Yii::t("StoreModule.store", "Shopping Cart");?></h2>
            <div class="steps-3">
                <div class="step-item  active">Шаг 1</div>
                <div class="step-item">Шаг 2</div>
                <div class="step-item">Шаг 3</div>
                <div class="clearfix"></div>
            </div>
            <div class="alert alert-info" role="alert"><?php echo Yii::t("base", "Edit your shopping list, and then click \"Buy \". At the second step you can specify the address where we need to deliver your order.");?></div>
            <table class="table mt10">
                <thead>
                <tr>
                    <th><?php echo Yii::t("StoreModule.store", "Product Name");?></th>
                    <th><?php echo Yii::t("StoreModule.store", "Quantity");?></th>
                    <th><?php echo Yii::t("StoreModule.store", "Price");?></th>
                    <th class="w10"></th>
                </tr>
                </thead>
                <tbody>
                <?php $allItems = Yii::app()->getModule('store')->cart->getAllItems(); ?>
                <?php if($allItems->count()) { ?>
                    <?php foreach($allItems as $key => $item) { ?>
                        <tr class="cart-item">
                            <td>
                                <a href="<?php echo $item->url; ?>">
                                    <?php echo $item->name; ?>
                                </a>
                            </td>
                            <td>
                                <div class="product-count">
                                    <span class="count-button-left cart-quantity-decrease" data-target="#<?php echo CHtml::activeId($item, 'quantity').$key; ?>">
                                        <i class="fa fa-minus"></i>
                                    </span>
                                    <span class="count-button-right cart-quantity-increase" data-target="#<?php echo CHtml::activeId($item, 'quantity').$key; ?>">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                    <div class="block-input">
                                        <input id="<?php echo CHtml::activeId($item, 'quantity').$key; ?>" class="position-count form-control" data-product-id="<?php echo $item->sku; ?>" type="text" value="<?php echo $item->getQuantity(); ?>">
                                </div>
                            </td>
                            <td><?php echo YHelper::formatCurrency($item->resultPrice); ?></td>
                            <td><a class="cart-delete-product" data-product-id="<?php echo $item->sku; ?>"><span class="icon-remove"><i class="fa fa-times"></i></span></a></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <tr>
                    <th colspan="2"></th>
                    <th colspan="2"><?php echo Yii::t("StoreModule.store", "Total");?>: <span id="priceSumCart"><?php echo YHelper::formatCurrency(Yii::app()->getModule('store')->cart->getCost()); ?></span></th>
                </tr>
                </tbody>
            </table>
            <div class="text-right pb10">
                <button type="button" class="button-inline button-green" onclick="location.href='<?php echo Yii::app()->createUrl('store/storeCart/Checkout'); ?>'"><i class="fa fa-shopping-cart"></i> <?php echo Yii::t("StoreModule.store", "Proceed to Checkout");?></button>
            </div>
        </div>
    </div>
</div>