<div class="col-sm-8 col-md-8 col-lg-9">
    <div class="custom-breadcrumbs">
        <ul>
            <li><a href="#">Главная</a></li>
            <li>Корзина</li>
        </ul>
    </div>
    <div class="block-white">
        <div class="static-page">
            <h2><?php echo Yii::t("StoreModule.store", "Contact Information");?></h2>
            <div class="steps-3">
                <div class="step-item"><?php echo Yii::t("StoreModule.store", "Step 1");?></div>
                <div class="step-item active"><?php echo Yii::t("StoreModule.store", "Step 2");?></div>
                <div class="step-item"><?php echo Yii::t("StoreModule.store", "Step 3");?></div>
                <div class="clearfix"></div>
            </div>
            <div role="alert" class="alert alert-info"><?php echo Yii::t("base", "Enter the address at which to deliver.");?></div>
            <?php
                $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'action' => ['/store/order/create'],
                        'id' => 'order-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => false,
                            'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                            'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
                        ),
                        'htmlOptions' => array(
                            'hideErrorMessage' => false,
                            'class' => 'order-form',
                        )
                    )
                );
            ?>
                <div class="form-group">
                    <?php echo $form->textField($order, 'name', array('placeholder' => Yii::t("StoreModule.store", 'Client Name'), 'class' => 'form-control')); ?>
                    <?php echo $form->error($order, 'name'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->textField($order, 'email', array('placeholder' => Yii::t("StoreModule.store", 'Email'), 'class' => 'form-control')); ?>
                    <?php echo $form->error($order, 'email'); ?>
                </div>
                <div class="form-group">
                    <?php
                        $this->widget('CMaskedTextField', array(
                            'model' => $order,
                            'attribute' => 'phone',
                            'mask' => '(999)-999-999',
                            'htmlOptions' => array(
                                'placeholder' => Yii::t("StoreModule.store", 'Phone'),
                                'class' => 'form-control'
                            )
                        ));
                    ?>
                    <?php echo $form->error($order, 'phone'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->textField($order, 'address', array('placeholder' => Yii::t("StoreModule.store", 'Address'), 'class' => 'form-control')); ?>
                    <?php echo $form->error($order, 'address'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->textArea($order, 'comment', array('placeholder' => Yii::t("StoreModule.store", 'comment'), 'class' => 'form-control')); ?>
                </div>
                <div class="nav-buttons mb10">
                    <div class="row">
                        <div class="col-xs-offset-0 col-xs-0 col-sm-offset-0 col-sm-5 col-md-offset-0 col-md-4 col-lg-offset-0 col-lg-4">
                        </div>
                        <div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-7 col-md-offset-2 col-md-6 col-lg-offset-4 col-lg-4 text-right">
                            <a class="button-inline button-green" href="#" onClick="$('#order-form').submit(); return false;">
                                <?php echo Yii::t("StoreModule.store", "Continue");?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>