<section class="products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                <?php $this->renderPartial('_filters'); ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="top-heading">
                            <h2><?php echo Yii::t("base", "Products filtering");?></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="heading">
                            <?php $category = isset($category->title) ? $category->title : Yii::t("base", 'All Products'); ?>
                            <h2><?php echo $category; ?></h2>
                        </div>
                    </div>
                    <?php $this->widget('zii.widgets.CListView', array(
                        'dataProvider' => $dataProvider,
                        'itemView' => '_product_grid', // refers to the partial view named '_post'
                        'pager' => "LinkPager",
                        'template' => "{sorter}\n{items}\n{pager}",
                        'cssFile' => false,
                        'summaryText' => false,
                    ));
                    ?>
                </div>
            </div>

        </div>
    </div>
</section>