﻿<?php
    $this->breadcrumbs=array(
        $product->category->title => $product->category->url,
        $product->name
    );
?>

<div class="col-sm-8 col-md-8 col-lg-9">
<div class="custom-breadcrumbs">
    <ul>
        <li><a href="#">Главная</a></li>
        <li><a href="#">Интерьерное освещение</a></li>
        <li><a href="#">Встраиваемые</a></li>
        <li>SC-SX-PNL-FC3030 RGB+</li>
    </ul>
</div>
<div class="block-white">
    <div class="row mb20">
        <div class="col-md-6 col-lg-5">
            <?php $image = $product->contypeMainImage ? $product->contypeMainImage->path : ''; ?>
            <a class="img-group" rel="lightbox" href="<?php echo YHelper::getImagePath($image); ?>">
                <img alt="<?php echo $product->name; ?>" src="<?php echo YHelper::getImagePath($image, 240, 160); ?>" class="img-responsive mb30">
            </a>
        </div>
        <div class="col-md-6 col-lg-7">
            <h6><a href="#"><?php echo $product->category->title; ?></a></h6>
            <h2>SC-SX-PNL-FC3030 RGB+</h2>
            <h5 class="inline-block"><?php echo Yii::t("base", "Price");?>: <span class="single-product-price"><?php echo YHelper::formatCurrency($product->price); ?></span></h5>
            <p><?php echo $product->short_description; ?></p>
            <div class="item">
                <?php $attributes = $product->IsShortAttributes; if($attributes) { ?>
                    <?php foreach ($attributes as $attribute) { ?>
                        <li>
                            <?php $attr = $attribute->renderValue($product->attribute($attribute->column_name)); ?>
                            <?php if(!empty($attr)) { ?>
                                <?php echo $attribute->title; ?>:
                                <?php echo $attr; ?>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="product-count">
                <span class="count-button-left increase">
                    <i class="fa fa-minus"></i>
                </span>
                <span class="count-button-right decrease">
                    <i class="fa fa-plus"></i>
                </span>
                <div class="block-input">
                    <input id="quantity" class="position-count" data-product-id="<?php echo $cartModel->sku; ?>" type="text" value="<?php echo $cartModel instanceof IECartPosition ? $cartModel->getQuantity() : 1; ?>">
                </div>
            </div>
            <a id="add-product-to-cart" class="button-icon button-green" href="javascript:void(0);" data-product-id="<?php echo $product->sku; ?>">
                <i class="fa fa-shopping-cart"></i> <?php echo Yii::t("StoreModule.store", "Add to Cart"); ?>
            </a>
        </div>
    </div>
    <div class="mb20">
        <table class="table table-striped">
            <thead>
            <tr>
                <th colspan="2"><?php echo Yii::t("base", "Characteristics");?></th>
            </tr>
            </thead>
            <tbody>
                <?php $attributes = $product->IsNotShortAttributes; if($attributes) { ?>
                    <?php foreach ($attributes as $attribute) { ?>
                        <tr>
                            <?php $attr = $attribute->renderValue($product->attribute($attribute->column_name)); ?>
                            <?php if(!empty($attr)) { ?>
                                <td><?php echo $attribute->title; ?>:</td>
                                <td><?php echo $attr; ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
</div>
<?php $this->widget('SameCategory', array('product' => $product)); ?>
</div>