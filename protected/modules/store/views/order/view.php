<div class="col-sm-8 col-md-8 col-lg-9">
    <div class="custom-breadcrumbs">
        <ul>
            <li><a href="#">Главная</a></li>
            <li>Корзина</li>
        </ul>
    </div>
    <div class="block-white">
        <div class="static-page">
            <h1>Спасибо за покупку!</h1>
            <div class="steps-3">
                <div class="step-item">Шаг 1</div>
                <div class="step-item">Шаг 2</div>
                <div class="step-item active">Шаг 3</div>
                <div class="clearfix"></div>
            </div>
            <div role="alert" class="alert alert-success"><?php echo Yii::t("StoreModule.store", "You order was successfully created. Our operator will contact you soon to confirm your order and details. Thank you for your purchase!");?></div>
            <div class="text-right mb10">
                <a class="button-inline button-green" href="<?php echo Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)); ?><?php //echo Yii::app()->homeUrl; ?>">
                    <?php echo Yii::t("StoreModule.store", "Buy something else");?>
                </a>
            </div>
        </div>
    </div>
</div>