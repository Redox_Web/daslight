<html>
<head>
</head>
<body>
<?php $subject = Yii::t(
    'StoreModule.store',
    'Order №{n} in the store "{site}"',
    ['{n}' => $order->id, '{site}' => Yii::app()->name]
); ?>
<h1 style="font-weight:normal;">
    <?php echo CHtml::link(
        Yii::t("StoreModule.store", "Order №:number", array(':number' => $order->id)),
        Yii::app()->createAbsoluteUrl('/order/orderBackend/update', ['id' => $order->id])
    ); ?>
    <?php echo Yii::t("StoreModule.store", "to the amount of");?> <?php echo YHelper::formatCurrency($order->getTotalPrice()); ?>.
</h1>
<table cellpadding="6" cellspacing="0" style="border-collapse: collapse;">
    <tr>
        <td style="padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;">
            <?php echo Yii::t("StoreModule.store", "Status");?>
        </td>
        <td style="padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;">
            <?php echo StoreOrder::model()->getStatusList($order->status); ?>
        </td>
    </tr>
    <tr>
        <td style="padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;">
            <?php echo Yii::t("StoreModule.store", "Payment");?>
        </td>
        <td style="padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;">
            <?= $order->paid ? Yii::t("StoreModule.store", "Yes") : Yii::t("StoreModule.store", "No"); ?>
        </td>
    </tr>
    <?php if ($order->name): ?>
        <tr>
            <td style="padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <?php echo Yii::t("StoreModule.store", "Name, surname");?>
            </td>
            <td style="padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?= CHtml::encode($order->name); ?>
            </td>
        </tr>
    <?php endif; ?>
    <?php if ($order->email): ?>
        <tr>
            <td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <?php echo Yii::t("StoreModule.store", "Email");?>
            </td>
            <td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?= CHtml::encode($order->email); ?>
            </td>
        </tr>
    <?php endif; ?>
    <?php if ($order->phone): ?>
        <tr>
            <td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <?php echo Yii::t("StoreModule.store", "Phone");?>
            </td>
            <td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?= CHtml::encode($order->phone); ?>
            </td>
        </tr>
    <?php endif; ?>
    <?php if ($order->address): ?>
        <tr>
            <td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <?php echo Yii::t("StoreModule.store", "Delivery address");?>
            </td>
            <td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?= CHtml::encode($order->address); ?>
            </td>
        </tr>
    <?php endif; ?>
    <?php if ($order->comment): ?>
        <tr>
            <td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <?php echo Yii::t("StoreModule.store", "Comment");?>
            </td>
            <td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?= nl2br(CHtml::encode($order->comment)); ?>
            </td>
        </tr>
    <?php endif; ?>

</table>

<h1 style="font-weight:normal;"><?php echo Yii::t("StoreModule.store", "Buyer ordered");?>:</h1>

<table cellpadding="6" cellspacing="0" style="border-collapse: collapse;">
    <?php foreach ($order->products as $orderProduct): ?>
        <tr>
            <td align="center" style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?php if($orderProduct->product): ?>
                    <a href="<?= Yii::app()->createAbsoluteUrl($orderProduct->product->url); ?>">
                        <?php $image = $orderProduct->product->contypeMainImage ? $orderProduct->product->contypeMainImage->path : ''; ?>
                        <img border="0" src="<?php echo Yii::app()->getBaseUrl(true).YHelper::getImagePath($image, 50, 50); ?>">
                    </a>
                <?php else: ?>
                    <?= CHtml::encode($orderProduct->product_name); ?>
                <?php endif; ?>
            </td>
            <td style="padding:6px; width:250; padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <a href="<?= Yii::app()->createAbsoluteUrl($orderProduct->product->url); ?>" target="_blank"><?= $orderProduct->product->name; ?></a>
                <?php foreach ($orderProduct->variantsArray as $variant): ?>
                    <h5><?= $variant['attribute_title']; ?>: <?= $variant['optionValue']; ?></h5>
                <?php endforeach; ?>
            </td>
            <td align=right style="padding:6px; text-align:right; width:150; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?= $orderProduct->quantity; ?> &times; <?= YHelper::formatCurrency($orderProduct->price); ?>
            </td>
        </tr>
    <?php endforeach; ?>

    <?php if ($order->delivery && !$order->separate_delivery): ?>
        <tr>
            <td style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;"></td>
            <td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;">
                <?= CHtml::encode($order->delivery->name); ?>
            </td>
            <td align="right" style="padding:6px; text-align:right; width:170; background-color:#ffffff; border:1px solid #e0e0e0;">
                <?php echo YHelper::formatCurrency($order->getDeliveryPrice()); ?>
            </td>
        </tr>
    <?php endif; ?>

    <tr>
        <td style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;"></td>
        <td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-weight:bold;">
            <?php echo Yii::t("StoreModule.store", "Total");?>
        </td>
        <td align="right" style="padding:6px; text-align:right; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-weight:bold;">
            <?= YHelper::formatCurrency($order->getTotalPriceWithDelivery()); ?>
        </td>
    </tr>
</table>
<br/>

</body>
</html>