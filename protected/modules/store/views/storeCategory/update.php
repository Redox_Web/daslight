<?php
	$this->breadcrumbs=array(
        'Store Categories'=>array('admin'),
        $model->title=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	array('label'=>'Create StoreCategory','url'=>array('create')),
	array('label'=>'View StoreCategory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage StoreCategory','url'=>array('admin')),
	);
    $this->title = "Update StoreCategory <?php echo $model->id; ?>";
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>