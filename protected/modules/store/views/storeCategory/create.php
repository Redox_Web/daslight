<?php
	$this->breadcrumbs=array(
        'Store Categories'=>array('admin'),
        'Create',
    );

    $this->menu=array(
    array('label'=>'Manage StoreCategory','url'=>array('admin')),
    );
    $this->title = "Create StoreCategory";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>