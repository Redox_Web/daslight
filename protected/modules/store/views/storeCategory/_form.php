<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'store-category-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'title_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('title_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'title_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('title_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'description_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'description_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('description_ru'))),
            array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'description_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('description_ro'))),
        ),
    )); ?>

    <?php echo $form->fileFieldGroup($model,'image',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php echo $form->fileFieldGroup($model,'title_image',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php echo $form->textFieldGroup($model,'url_static_page',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php echo $form->dropDownListGroup($model,'attribute', CHtml::listData(StoreAttribute::model()->findAll(array('order' => 'ord ASC')), 'id', 'title'), array('class'=>'form-control', 'multiple'=>'multiple','style'=>'height:140px;')); ?>

	<?php /*echo $form->textFieldGroup($model,'meta_title',array('class'=>'form-control','maxlength'=>255)); */?><!--

	<?php /*echo $form->textAreaGroup($model,'meta_description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); */?>

    --><?php /*echo $form->textAreaGroup($model,'meta_keywords',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); */?>

    <?php echo $form->checkboxGroup($model, 'is_active',
        array('widgetOptions' =>
            array(
                'htmlOptions' => array(
                    'checked'=>$model->isNewRecord ? true : $model->is_active
                ),
            )
        )
    ); ?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

    <?php $this->beginClip('meta_tags');?>
    <?php if(!$model->isNewRecord) : ?>
        <?php $this->renderPartial('/storeCategory/_meta_tags', array('model' => $model, 'form'=>$form)); ?>
    <?php endif; ?>
    <?php $this->endClip(); ?>

<?php $this->endWidget(); ?>
