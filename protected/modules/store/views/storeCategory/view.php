<?php
	$this->breadcrumbs=array(
        'Store Categories'=>array('admin'),
        $model->title,
    );

    $this->menu=array(
    array('label'=>'Create StoreCategory','url'=>array('create')),
    array('label'=>'Update StoreCategory','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete StoreCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage StoreCategory','url'=>array('admin')),
    );
    $this->title = "View StoreCategory # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title_ru',
		'description_ru',
		'slug_ru',
		'image:image',
		'is_active:boolean',
),
)); ?>
