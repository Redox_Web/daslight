<?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'meta_header_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'meta_header_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('meta_header_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'meta_header_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('meta_header_ro'))),
        ),
    )); ?>

<?php $this->widget('booster.widgets.TbTabsLang', array(
    'type'=>'pills',
    'labelModel' => $model,
    'labelAttribute' => 'meta_header_h2_ru',
    'htmlOptions'=>array('class'=>'form-group'),
    'tabs'=>array(
        array('label'=>'RU', 'content'=>$form->textField($model,'meta_header_h2_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('meta_header_h2_ru'))),
        array('label'=>'RO', 'content'=>$form->textField($model,'meta_header_h2_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('meta_header_h2_ro'))),
    ),
)); ?>

<?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'meta_title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'meta_title_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('meta_title_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'meta_title_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('meta_title_ro'))),
        ),
    )); ?>


<?php $this->widget('booster.widgets.TbTabsLang', array(
    'type'=>'pills',
    'labelModel' => $model,
    'labelAttribute' => 'meta_keywords_ru',
    'htmlOptions'=>array('class'=>'form-group'),
    'tabs'=>array(
        array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'meta_keywords_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('meta_keywords_ru'))),
        array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'meta_keywords_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('meta_keywords_ro'))),
    ),
)); ?>

<?php $this->widget('booster.widgets.TbTabsLang', array(
    'type'=>'pills',
    'labelModel' => $model,
    'labelAttribute' => 'meta_description_ru',
    'htmlOptions'=>array('class'=>'form-group'),
    'tabs'=>array(
        array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'meta_description_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('meta_description_ru'))),
        array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'meta_description_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('meta_description_ro'))),
    ),
)); ?>


<?php $this->widget('booster.widgets.TbButton', array(
    'buttonType'=>'formSubmit',
    'htmlOptions' => array('class' => 'btn btn-primary'),
    'label'=>$model->isNewRecord ? 'Create' : 'Save',
)); ?>