<?php
    $this->breadcrumbs=array(
        'Store Categories'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create StoreCategory','url'=>array('create')),
    );
    $this->title = "Manage Store Categories";
?>

<p>Перетащите для сортировки и перемещения между элементами списка.</p>

<?php if(!empty($items)){?>
    <div class="tree">
        <?php
        $level = 0;
        foreach ($items as $n => $item) {
            if ($item->level == $level)
                echo CHtml::closeTag('li') . "\n";
            else if ($item->level > $level) {
                if ($item->isRoot())
                    echo CHtml::openTag('ol', array('class' => 'sortable ui-sortable')) . "\n";
                else
                    echo CHtml::openTag('ol') . "\n";
            } else {
                echo CHtml::closeTag('li') . "\n";

                for ($i = $level - $item->level; $i; $i--) {
                    echo CHtml::closeTag('ol') . "\n";
                    echo CHtml::closeTag('li') . "\n";
                }
            }

            echo CHtml::openTag('li', array('class' => 'comment', 'id' => "list_$item->id"));
            ?>

            <div>
                <span class="disclose"><span></span></span>
                <a href="<?php echo Yii::app()->createUrl("store/storeCategory/update", array('id' => $item->id)); ?>"><?php echo strip_tags($item->title); ?></a>
                <b class="panel-w pull-right">
                    <a class="view" rel="tooltip" data-original-title="Просмотреть" href="<?php echo Yii::app()->createUrl('store/storeCategory/view', array('id' => $item->id));?>">
                        <i class="fa-backend fa fa-eye"></i>
                    </a>
                    <a class="update" rel="tooltip" data-original-title="Редактировать" href="<?php echo Yii::app()->createUrl('store/storeCategory/update', array('id' => $item->id));?>">
                        <i class="fa-backend fa fa-edit"></i>
                    </a>
                    <a class="delete" rel="tooltip" data-original-title="Удалить" href="<?php echo Yii::app()->createUrl('store/storeCategory/delete', array('id' => $item->id));?>">
                        <i class="fa-backend fa fa-trash-o"></i>
                    </a>
                </b>
            </div>
            <?php
            $level = $item->level;
        }

        for ($i = $level; $i; $i--) {
            echo CHtml::closeTag('li') . "\n";
            echo CHtml::closeTag('ol') . "\n";
        }
        ?>
    </div>
<?php } else { ?>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        Ничего не найдено. Пожалуйста, нажмите <strong>Создать категорию</strong>.
    </div>
<?php } ?>

<script type="text/javascript">
    $().ready(function () {
        if ($('ol.sortable').length > 0) {
            $('ol.sortable').nestedSortable({
                disableNesting:'no-nest',
                forcePlaceholderSize:true,
                handle:'div',
                helper:'clone',
                items:'li',
                maxLevels:4,
                opacity:.4,
                placeholder:'placeholder',
                revert:250,
                tabSize:25,
                isTree: true,
                expandOnHover: 700,
                startCollapsed: false,
                tolerance:'pointer',
                toleranceElement:'> div',
                update: function(){
                    var arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
                    $.ajax({
                        type:"POST",
                        data:{
                            ids:arraied
                        },
                        url:"<?php echo Yii::app()->createUrl('store/storeCategory/tree'); ?>",
                        success:function (msg) {
                            //$().ready(function(){$.sticky("Изменение порядка было сделано успешно.", {autoclose : 5000, position: "top-right", type: "st-success" });});
                        }
                    });
                }
            });

        }

        jQuery('.pull-right').on('click', '.delete',function () {
            if (!confirm('Вы уверены, что хотите удалить этот пункт?')) return false;
            var th = $(this);

            $.ajax({
                type:"POST",
                url:$(this).attr('href'),
                success:function (msg) {
                    th.closest('li').remove();
                }
            });

            return false;
        });
    });

    $('.disclose').on('click', function() {
        $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
        return false;
    });
</script>
