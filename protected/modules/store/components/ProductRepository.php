<?php
Yii::import('main.widgets.YSort');
/**
 * Class ProductRepository
 */
class ProductRepository extends CComponent
{
    protected $attributeFilter;

    public function init()
    {
        $this->attributeFilter = Yii::app()->getComponent('attributesFilter');
    }

    public function getByFilter(StoreCategory $category, array $mainSearchAttributes, array $eavSearchAttributes, $perPage = null)
    {
        $perPage = empty($perPage) ? Yii::app()->params['itemsPerPage'] : $perPage;
        $model = StoreProduct::model();

        $criteria = new CDbCriteria();
        $criteria->select = 't.*';
        $criteria->params = array();
        $criteria->with = array('category' => array('together' => true));
        $criteria->addCondition('category.id = :category_id OR t.category_id = :category_id OR category.root = :category_id'); //changed row
        $criteria->params[':category_id'] = $category->id;

        foreach(Yii::app()->getModule('store')->attributesFilter->getMainSearchParams() as $param => $field) {
            if(!empty($mainSearchAttributes[$param])) {
                $criteria->addInCondition($field, $mainSearchAttributes[$param]);
                //if($param == 'category')//my changed row
                    //$criteria->addInCondition('category.root', $mainSearchAttributes[$param], 'OR'); //changed row
            }
        }

        if(!empty($mainSearchAttributes[AttributeFilter::MAIN_SEARCH_PARAM_NAME])) {
            $criteria->addSearchCondition('name', $mainSearchAttributes[AttributeFilter::MAIN_SEARCH_PARAM_NAME], true);
        }

        if($eavSearchAttributes){
            $eavCriteria = $model->getFilterByEavAttributesCriteria($eavSearchAttributes);
            $criteria->mergeWith($eavCriteria);
        }

        return new CActiveDataProvider(
            $model,
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => (int)$perPage,
                    'pageVar' => 'page',
                ),
                'sort' => array(
                    'attributes'=>array(
                        'name'=>array(
                            'asc'=>'name_'.Yii::app()->language.' ASC',
                            'desc'=>'name_'.Yii::app()->language.' DESC',
                        ),
                        'price'=>array(
                            'asc'=>'price ASC',
                            'desc'=>'price DESC',
                        )
                    ),
                    'sortVar' => 'sort',
                    'defaultOrder' => 'ord ASC',
                ),
            )
        );
    }


    /**
     * @param int $perPage
     * @return CActiveDataProvider
     */
    public function getListForIndexPage($perPage = null)
    {
        $perPage = empty($perPage) ? Yii::app()->params['itemsPerPage'] : $perPage;

        $criteria = new CDbCriteria();
        $criteria->select = 't.*';
        $criteria->params = array();
        $criteria->addCondition('status = :status');
        $criteria->addCondition('is_active = :is_active');
        $criteria->params['status'] = StoreProduct::STATUS_IN_STOCK;
        $criteria->params['is_active'] = StoreProduct::STATUS_ACTIVE;

        return new CActiveDataProvider(
            StoreProduct::model(),
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => (int)$perPage,
                    'pageVar' => 'page',
                ),
                'sort' => array(
                    'sortVar' => 'sort',
                    'defaultOrder' => 'ord ASC',
                ),
            )
        );
    }

    /**
     * @param StoreCategory $category
     * @param int $perPage
     * @return CActiveDataProvider
     */
    public function getListForCategory(StoreCategory $category, $perPage = 20)
    {
        $criteria = new CDbCriteria();
        $criteria->select = 't.*';
        $criteria->with = array('category' => array('together' => true));
        $criteria->addCondition('category.id = :category_id OR t.category_id = :category_id OR category.root = :category_id'); //changed row
        $criteria->params[':category_id'] = $category->id;

        $sorter = new YSort;
        $sorter->attributes = array(
            'name_'.Yii::app()->language => array(
                'asc'=>'name_'.Yii::app()->language.' ASC',
                'desc'=>'name_'.Yii::app()->language.' DESC',
            ),
            'price' => array(
                'asc'=>'price ASC',
                'desc'=>'price DESC',
            )
        );
        $sorter->sortVar = 'sort';
        $sorter->defaultOrder = 'ord ASC';
        return new CActiveDataProvider(
            StoreProduct::model(),
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => (int)$perPage,
                    'pageVar' => 'page',
                ),
                'sort' => $sorter,
            )
        );
    }

    /**
     * @param $query
     * @return array
     */
    public function search($query)
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        $criteria->addCondition('status = :status');
        $criteria->params['status'] = Product::STATUS_ACTIVE;
        $criteria->addSearchCondition('name', $query, true);
        return Product::model()->findAll($criteria);
    }
}
