<?php

/**
 * Class AttributeFilter
 */
class AttributeFilter extends CComponent
{
    /**
     *
     */
    const MAIN_SEARCH_PARAM_PRODUCER = 'brand';
    /**
     *
     */
    const MAIN_SEARCH_PARAM_CATEGORY = 'category';

    const MAIN_SEARCH_PARAM_NAME = 'name';

    /**
     * @var string
     */
    public $dropdownTemplate = '%s[]';
    /**
     * @var string
     */
    public $checkboxTemplate = '%s';
    /**
     * @var string
     */
    public $numberTemplate = '%s[%s]';

    /**
     * @var string
     */
    public $stringTemplate = '%s';

    /**
     *
     */
    public function init()
    {

    }

    /**
     * @param $paramName
     * @param $value
     * @param CHttpRequest $request
     * @return bool
     */
    public function isMainSearchParamChecked($paramName, $value, CHttpRequest $request)
    {
        $data = $request->getQuery($paramName);

        return !empty($data) && in_array($value, $data);
    }

    /**
     * @return array
     */
    public function getMainSearchParams()
    {
        return array(
            self::MAIN_SEARCH_PARAM_CATEGORY => 'category_id',
            self::MAIN_SEARCH_PARAM_PRODUCER => 'producer_id'
        );
    }

    /**
     * @param Attribute $attribute
     * @param null $mode
     * @return null|string
     */
    public function getFieldName(StoreAttribute $attribute, $mode = null)
    {
        $type = (int)$attribute->type;

        if ($type === StoreAttribute::TYPE_SHORT_TEXT) {
            return sprintf($this->stringTemplate, $attribute->column_name);
        }

        if ($type === StoreAttribute::TYPE_CHECKBOX) {
            return sprintf($this->checkboxTemplate, $attribute->column_name);
        }

        if ($type === StoreAttribute::TYPE_NUMBER) {
            return sprintf($this->numberTemplate, $attribute->column_name, $mode);
        }

        return null;
    }

    /**
     * @param Attribute $attribute
     * @param null $mode
     * @return mixed
     */
    public function getFieldValue(StoreAttribute $attribute, $mode = null)
    {
        if(null !== $mode) {
            $data = Yii::app()->getRequest()->getParam($attribute->column_name);
            return is_array($data) && array_key_exists($mode, $data) ? $data[$mode] : null;
        }
        return Yii::app()->getRequest()->getParam($this->getFieldName($attribute, $mode), null);
    }

    /**
     * @param Attribute $attribute
     * @param null $value
     * @return bool
     */
    public function isFieldChecked(StoreAttribute $attribute, $value = null)
    {
        return Yii::app()->getRequest()->getParam($this->getFieldName($attribute), -1) == $value;
    }


    /**
     * @param AttributeOption $option
     * @return null|string
     */
    public function getDropdownOptionName(StoreAttributeOption $option)
    {
        if ((int)$option->attribute->type === StoreAttribute::TYPE_DROPDOWN) {
            return sprintf($this->dropdownTemplate, $option->attribute->column_name, $option->id);
        }
        return null;
    }

    /**
     * @param AttributeOption $option
     * @param mixed $value
     * @return mixed
     */
    public function getIsDropdownOptionChecked(StoreAttributeOption $option, $value)
    {
        $data = Yii::app()->getRequest()->getQuery($option->attribute->column_name, false);
        return is_array($data) && in_array($value, $data);
    }

    /**
     * @param CHttpRequest $request
     * @param array $append
     * @return array|mixed
     */
    public function getMainAttributesForSearchFromQuery(CHttpRequest $request, array $append = array())
    {
        $result = array();

        foreach ($this->getMainSearchParams() as $param => $field) {
            if ($request->getQuery($param)) {
                $result[$param] = $request->getQuery($param);
            }
        }

        if (!empty($append)) {
            $result = CMap::mergeArray($append, $result);
        }

        return $result;
    }


    /**
     * @param CHttpRequest $request
     * @return array
     */
    public function getEavAttributesForSearchFromQuery(CHttpRequest $request)
    {
        $result = $params = array();

        $attributes = StoreAttribute::model()->cache(Yii::app()->params['cacheTime'])->findAll(
            array('select' => 'column_name')
        );

        foreach ($attributes as $attribute) {

            if ($request->getQuery($attribute->column_name)) {

                $searchParams = $request->getQuery($attribute->column_name);

                if (!is_array($searchParams)) {
                    $result[$attribute->column_name] = $searchParams;
                    continue;
                }

                $isFrom = array_key_exists('from', $searchParams);
                $isTo = array_key_exists('to', $searchParams);

                if (false === $isFrom && false === $isTo) {
                    $result[$attribute->column_name] = $searchParams;
                    continue;
                }

                if (true === $isFrom && !empty($searchParams['from'])) {
                    $result[] = array('>=', $attribute->column_name, (float)$searchParams['from']);
                }

                if (true === $isTo && !empty($searchParams['to'])) {
                    $result[] = array('<=', $attribute->column_name, (float)$searchParams['to']);
                }

            }
        }

        return $result;
    }
}
