<?php

class OrderNotifyService extends CApplicationComponent
{
    protected $view;
    protected $module;

    public function init()
    {
        parent::init();

        $this->view = Yii::app()->controller;

        $this->module = Yii::app()->getModule('order');
    }

    public function sendOrderCreatedAdminNotify(StoreOrder $order)
    {
        $theme = Yii::t(
            'StoreModule.store',
            'New order #{n} in {site} store',
            ['{n}' => $order->id, '{site}' => Yii::app()->name]
        );

        //$to = YHelper::yiisetting('admin_emails');
        $to = YHelper::yiisetting('email');

        $body = $this->view->renderPartial('store.views.order.email.newOrderAdmin', ['order' => $order], true);
        Yii::app()->email->sendEmail($theme, $body, $to);

        return true;
    }

    public function sendOrderCreatedUserNotify(StoreOrder $order)
    {
        $theme = Yii::t(
            'StoreModule.store',
            'New order #{n} in {site} store',
            ['{n}' => $order->id, '{site}' => Yii::app()->name]
        );

        $body = $this->view->renderPartial('store.views.order.email.newOrderUser', ['order' => $order], true);
        Yii::app()->email->sendEmail($theme, $body, $order->email);
    }

    public function sendOrderChangesNotify(StoreOrder $order)
    {
        $theme = Yii::t(
            'StoreModule.store',
            'Order #{n} in {site} store is changed',
            ['{n}' => $order->id, '{site}' => Yii::app()->getModule('yupe')->siteName]
        );

        $from = $this->module->notifyEmailFrom ? : Yii::app()->getModule('yupe')->email;

        $body = $this->view->renderPartial('/order/email/orderChangeStatus', ['order' => $order], true);

        $this->mail->send($from, $order->email, $theme, $body);
    }
}
