<?php

class CAuthHelper {
	static function isProductAvailable($id) {

        $criteria = new CDbCriteria;
        $criteria->condition = 'id = :id';
        $criteria->addCondition('status = :status');
        $criteria->addCondition('is_active = :is_active');

        $criteria->params[':id'] = $id;
        $criteria->params['status'] = StoreProduct::STATUS_IN_STOCK;
        $criteria->params['is_active'] = StoreProduct::STATUS_ACTIVE;

        $article = StoreProduct::model()->count($criteria);
		if($article) return true;

        //Yii::app()->controller->redirect(Yii::app()->homeUrl);
                Yii::app()->controller->redirect(Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)));
	}

    static function isCartAvailable() {

        $products = Yii::app()->getModule('store')->cart->getCount();
        if($products) return true;

        throw new CHttpException(404, 'You must have at least one item in your cart');
        return false;
    }

    static function isActionAvailable($id) {

        $criteria = new CDbCriteria;
        $criteria->condition = 'id = :id';
        $criteria->addInCondition("is_active", arr_language());
        $criteria->params[':id'] = $id;

        $article = Album::model()->count($criteria);
        if($article) return true;

        return false;
    }
}