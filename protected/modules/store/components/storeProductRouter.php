<?php

Yii::import('store.models.StoreCategory');
Yii::import('store.models.StoreProduct');

class storeProductRouter extends CBaseUrlRule
{
    private static $instance;
    private $matches;
    private $mainpage = false;
    private $expectingParams = array('sort', 'page', 'itemsPerPage', 'q');

    public static function Instance()
    {
        if (self::$instance == null) {
            self::$instance = new storeProductRouter();
        }
        return self::$instance;
    }

    public function urltrans($pathInfo)
    {
        $flag = $this->checkUrl($pathInfo);

        if ($flag) {
            return Yii::app()->createUrl($this->getUrl(array('id'=>$_GET['id'])));
        }
        return false;  // не применяем данное правило
    }

    public function createUrl($manager, $route, $params, $ampersand)
    {
        if ($route === 'store/storeCatalog/view' || $route === 'store/storeCatalog/category') {
            $this->mainpage = ($route === 'store/storeCatalog/category') ? true : false;
            if (isset($params['id'])) {
                return $this->getUrl($params);
            }
        }
        return false;  // не применяем данное правило
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        $flag = $this->checkUrl($pathInfo);

        if ($flag) {
            if ($this->mainpage) return 'store/storeCatalog/category';
            else return 'store/storeCatalog/view';
        }

        return false;  // не применяем данное правило
    }

    //========================help functions=====================

    private function checkUrl($pathInfo)
    {
        $flag = false;
        $exploded_path = explode("/", $pathInfo);
        $pathInfo = preg_replace('~^'.Yii::app()->getBaseUrl(true).'/~', '', $pathInfo);

        if ($pathInfo == 'main/default/change') {
            return false;
        }

        if (preg_match('~^backend~', $pathInfo)) {
            return $flag;
        }
        $lang = implode('|', Yii::app()->languages->languages);
        if (!preg_match('~('.$lang.')/~', $pathInfo)) {
            $pathInfo = Yii::app()->languages->defaultLanguage.'/'.$pathInfo;
        }
        $pathInfo = $this->extractParams($pathInfo);

        if (preg_match('~^('.$lang.')?/([a-zA-Z0-9_-]+)/?([a-zA-Z0-9_-]+)?~', $pathInfo, $this->matches)) {
            if (isset($this->matches[1])) {
                $_GET['language'] = $this->matches[1];
            } else {
                $_GET['language'] = Yii::app()->languages->defaultLanguage;
            }

            if (isset($this->matches[2])) {
                if ($section = $this->dbExist('StoreCategory', $this->matches[2])) {
                    $flag = true;
                    $this->mainpage = true;
                    $_GET['id'] = $section;
                }
            }

            if (isset($this->matches[3]) && $flag) {
                if ($id = $this->dbExist('StoreProduct', $this->matches[3])) {
                    $flag = true;
                    $this->mainpage = false;
                    $_GET['id'] = $id;
                } else {
                    $flag = false;
                }
            }
        }

        if (!$flag) {
            $_GET['language'] = Yii::app()->language;
        }

        return $flag;
    }

    private function extractParams($pathInfo)
    {
        $params = implode('|', $this->expectingParams);
        if (preg_match_all('~('.$params.')/([a-zA-Z0-9._-]+)~', $pathInfo, $matches)) {
            if ($matches[0]) {
                foreach ($matches[0] as $key => $paramString) {
                    $_GET[$matches[1][$key]] = $matches[2][$key];
                    $pathInfo = str_replace('/'.$paramString, "", $pathInfo);
                }
            }
        }

        return $pathInfo;
    }

    private function dbExist($db, $slug)
    {
        $_GET['language'] = !empty($_GET['language']) ? $_GET['language'] : Yii::app()->languages->defaultLanguage;
        if ($db == 'StoreCategory') {
            $article = $db::model()->find("slug_" . $_GET['language'] . " = '$slug'");
        } else {
            $article = $db::model()->find("slug = '$slug'");
        }

        if ($article) return $article->id;
        else return false;
    }

    private function getUrl($params)
    {
        if ($this->mainpage) {
            $article = StoreCategory::model()->findByPk($params['id']);
        } else {
            $article = StoreProduct::model()->findByPk($params['id']);
        }

        $lang = Yii::app()->language . '/';
        $urlParam = array();

        if ($article) {
            $returned_val = '';

            if ($this->mainpage) {
                $returned_val = $lang . $article->{'slug_' . Yii::app()->language};
            } elseif (isset($article->category)) {
                $returned_val = $lang . $article->category->{'slug_' . Yii::app()->language} . '/' . $article->slug;
            }

            if (!empty($params)) {
                foreach ($params as $key => $value) {
                    if ($key != 'id' && $key != 'language') {
                        $urlParam[$key] = $value;
                    }
                }
            }

            $returned_val = $this->addAttributes($returned_val, $urlParam);
            $returned_val = ltrim($returned_val, '/');

            return $returned_val;
        } else {
            return false;
        }
    }

    private function addAttributes($route, $attributes)
    {
        if ($attributes) {
            foreach ($attributes as  $attr => $param) {
                $route .= '/'.$attr.'/'.$param;
            }
        }

        return $route;
    }
}
