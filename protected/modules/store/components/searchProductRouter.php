<?php

Yii::import('store.models.StoreCategory');
Yii::import('store.models.StoreProduct');

class searchProductRouter extends CBaseUrlRule
{
    private static $instance;
    private $matches;
    private $expectingParams = array('sort', 'page', 'itemsPerPage', 'q');

    public static function Instance()
    {
        if (self::$instance == null) {
            self::$instance = new searchProductRouter();
        }
        return self::$instance;
    }

    public function urltrans($pathInfo)
    {
        $flag = $this->checkUrl($pathInfo);

        $parts = parse_url($pathInfo);
        parse_str($parts['query'], $params);

        if ($flag) {
            return Yii::app()->createUrl($this->getUrl($params));
        }
        return false;  // не применяем данное правило
    }

    public function createUrl($manager, $route, $params, $ampersand)
    {
        if ($route === 'store/frontSearch/searchResult') {
            return $this->getUrl($params);
        }
        return false;  // не применяем данное правило
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        $flag = $this->checkUrl($pathInfo);

        if ($flag) {
            return 'store/frontSearch/searchResult';
        }

        return false;  // не применяем данное правило
    }

    //========================help functions=====================

    private function checkUrl($pathInfo)
    {
        $flag = false;
        $pathInfo = preg_replace('~^'.Yii::app()->getBaseUrl(true).'/~', '', $pathInfo);

        if ($pathInfo == 'main/default/change') {
            return false;
        }

        if (preg_match('~^backend~', $pathInfo)) {
            return $flag;
        }
        $lang = implode('|', Yii::app()->languages->languages);
        if (!preg_match('~('.$lang.')/~', $pathInfo)) {
            $pathInfo = Yii::app()->languages->defaultLanguage.'/'.$pathInfo;
        }
        $pathInfo = $this->extractParams($pathInfo);

        if (preg_match('~^('.$lang.')?/search~', $pathInfo, $this->matches)) {
            if (isset($this->matches[1])) {
                $_GET['language'] = $this->matches[1];
                $flag = true;
            } else {
                $_GET['language'] = Yii::app()->languages->defaultLanguage;
                $flag = false;
            }
        }

        if (!$flag) {
            $_GET['language'] = Yii::app()->language;
        }

        return $flag;
    }

    private function extractParams($pathInfo)
    {
        $params = implode('|', $this->expectingParams);
        if (preg_match_all('~('.$params.')/([a-zA-Z0-9._-]+)~', $pathInfo, $matches)) {
            if ($matches[0]) {
                foreach ($matches[0] as $key => $paramString) {
                    $_GET[$matches[1][$key]] = $matches[2][$key];
                    $pathInfo = str_replace('/'.$paramString, "", $pathInfo);
                }
            }
        }

        return $pathInfo;
    }

    private function getUrl($params)
    {
        $lang = Yii::app()->language . '/';
        $returned_val = $lang . 'search';

        $returned_val = $this->addAttributes($returned_val, $params);
        $returned_val = ltrim($returned_val, '/');

        return $returned_val;
    }

    private function addAttributes($route, $params)
    {
        $urlParam = array();

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                if ($key != 'id' && $key != 'language') {
                    $urlParam[$key] = $value;
                }
            }
        }

        if ($urlParam) {
            $route = Yii::app()->createUrl($route, $urlParam);
        }

        return $route;
    }
}
