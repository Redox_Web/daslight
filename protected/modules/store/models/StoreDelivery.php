<?php

/**
 * This is the model class for table "store_delivery".
 *
 * The followings are the available columns in table 'store_delivery':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property double $free_from
 * @property double $available_from
 * @property integer $status
 * @property integer $separate_payment
 */
class StoreDelivery extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'store_delivery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
        return array(
            array('name, price, status', 'required'),
            array('name', 'filter', 'filter' => 'trim'),
            array('separate_payment', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('description, payment_methods, free_from, available_from', 'safe'),
            array('status', 'in', 'range' => array_keys($this->getStatusList())),
            array(
                'id, name, status, description, price, free_from, available_from, separate_payment',
                'safe',
                'on' => 'search'
            ),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => 'status = :status',
                'params' => array(':status' => self::STATUS_ACTIVE),
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => Yii::t('StoreModule.store', 'ID'),
            'name' => Yii::t('StoreModule.store', 'Title'),
            'description' => Yii::t('StoreModule.store', 'Description'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'price' => Yii::t('StoreModule.store', 'Price'),
            'free_from' => Yii::t('StoreModule.store', 'Free from'),
            'available_from' => Yii::t('StoreModule.store', 'Available from'),
            'separate_payment' => Yii::t('StoreModule.store', 'Separate payment'),
            'payment_methods' => Yii::t('StoreModule.store', 'Payment methods'),
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('free_from',$this->free_from);
		$criteria->compare('available_from',$this->available_from);
		$criteria->compare('status',$this->status);
		$criteria->compare('separate_payment',$this->separate_payment);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getStatusList()
    {
        return array(
            self::STATUS_ACTIVE => Yii::t("StoreModule.store", 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t("StoreModule.store", 'Not active'),
        );
    }

    public function getStatusTitle()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t("StoreModule.store", '*unknown*');
    }

    public function getCost($totalPrice)
    {
        if (null === $this->free_from) {
            return $this->price;
        }

        return $this->free_from < $totalPrice ? 0 : $this->price;
    }

    public function checkAvailable(Order $order)
    {
        return $order->getProductsCost() >= $this->available_from;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreDelivery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
