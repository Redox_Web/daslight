<?php
Yii::import('store.models.StoreRelated');
/**
 * This is the model class for table "store_product".
 *
 * The followings are the available columns in table 'store_product':
 * @property integer $id
 * @property integer $producer_id
 * @property integer $category_id
 * @property string $sku
 * @property string $name_ru
 * @property string $name_ro
 * @property string $slug
 * @property string $price
 * @property string $old_price
 * @property string $description
 * @property string $short_description_ru
 * @property string $short_description_ro
 * @property integer $is_special
 * @property integer $quantity
 * @property integer $status
 * @property string $created
 * @property string $updated
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class StoreProduct extends ActiveRecord
{
    const STATUS_NOT_IN_STOCK = 0;
    const STATUS_IN_STOCK = 1;
    const STATUS_DELETED = 2;

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    private $_eavAttributes = null;

    private $disableDefaultScope = false;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'store_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_ru, name_ro, sku, producer_id, category_id, price', 'required'),
			array('producer_id, category_id, is_special, quantity, status', 'numerical', 'integerOnly'=>true),
			array('sku', 'length', 'max'=>100),
            array('sku','unique'),
			array('name_ru, name_ro, meta_title, meta_keywords, meta_description, pdf', 'length', 'max'=>250),
            array('pdf', 'file', 'types'=>'jpg, gif, png, jpeg, pdf', 'allowEmpty'=>true, 'safe' => false),
			array('slug', 'length', 'max'=>150),
			array('price, old_price', 'length', 'max'=>19),
			array('description, short_description_ru, short_description_ro, description_ro, description_ru, slug, is_active, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, producer_id, category_id, sku, name_ru, name_ro, slug, price, old_price,
			 description, short_description_ru, short_description_ro, is_special,
			  quantity, status, created, updated, meta_title, meta_keywords,
			   meta_description, meta_title_ru, meta_title_ro, meta_keywords_ru,
			    meta_keywords_ro, meta_description_ru, meta_description_ro,
			     meta_header_ru, meta_header_ro', 'safe', 'on'=>'search'),
            array('created', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => false, 'on' => 'insert, update'),
            array('updated', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => false, 'on' => 'update'),
		);
	}

    public function behaviors()
    {
        return array(
            'eavAttr' => array(
                'class' => 'application.modules.store.components.behaviors.AttributesBehavior',
                'tableName' => 'store_attribute_value',
                'entityField' => 'product_id',
                'preload' => false,
                'valueField' => 'value'
            ),
        );
    }

    public function defaultScope()
    {
        return $this->isDisableDefaultScope() ?
            [] :
            [
                'condition' => $this->getTableAlias(false, false).'.status <> :deleted AND '.$this->getTableAlias(false, false).'.is_active = :active AND '.$this->getTableAlias(false, false).'.category_id IS NOT NULL',
                'params' => [':deleted' => self::STATUS_DELETED, ':active' => StoreProduct::STATUS_ACTIVE],
            ];
    }

    public function scopes()
    {
        return array(
            'admin' => array(
                'condition' => $this->getTableAlias(false, false).'.status <> :deleted',
                'params' => [
                    ':deleted' => self::STATUS_DELETED,
                ],
            ),
        );
    }

    public function beforeSave()
    {
        $this->slug = YText::translit($this->name_ro);
        $proverka = self::model()->findByAttributes(array('slug' => $this->slug));
        if($proverka && $proverka->id != $this->id)
            $this->slug .= '_'.$this->id;

        return parent::beforeSave();
    }

    public function beforeValidate()
    {
        foreach ((array)$this->_eavAttributes as $name => $value) {
            $model = StoreAttribute::model()->getAttributeByName($name);

            if (!$model->isType(StoreAttribute::TYPE_CHECKBOX) && $model->isRequired() && !$value) {
                $this->addError(
                    'eav.' . $name,
                    Yii::t("StoreModule.store", "{title} attribute is required", array('title' => $model->title))
                );
            }
        }

        return parent::beforeValidate();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category' => array(self::BELONGS_TO, 'StoreCategory', 'category_id'),
            'producer' => array(self::BELONGS_TO, 'StoreProducer', 'producer_id'),
            'contypeImagesList' => array(self::HAS_MANY, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type', 'order' => 'id DESC', 'params' => array(':type' => $this->getClass())),
            'contypeMainImage' => array(self::HAS_ONE, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type AND is_main = 1', 'params' => array(':type' => $this->getClass())),
            'contypeNotMainImages' => array(self::HAS_MANY, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type AND is_main = 0', 'params' => array(':type' => $this->getClass())),
            'relatedProducts'=>array(self::MANY_MANY, 'StoreProduct', 'store_related(product_id, related_product_id)', 'condition' => 'relation = :relation', 'params' => array(':relation' => StoreRelated::RELATED)),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'producer_id' => 'Producer',
			'category_id' => 'Category',
			'sku' => 'Sku',
			'name_ru' => Yii::t("StoreModule.store", 'Name'),
			'name_ro' => Yii::t("StoreModule.store", 'Name'),
			'slug' => 'Slug',
			'price' => Yii::t("StoreModule.store", 'Price'),
			'old_price' => 'Old Price',
			'description' => 'Description',
			'short_description_ru' => 'Short Description Ru',
            'short_description_ro' => 'Short Description RO',
			'is_special' => 'Is Special',
			'quantity' => 'Quantity',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
			'meta_title' => 'Meta Title',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
            'pdf' => 'Attached File',
                        'meta_title_ru' => 'Meta Title',
                        'meta_title_ro' => 'Meta Title',
                        'meta_keywords_ru' => 'Meta Keywords',
                        'meta_keywords_ro' => 'Meta Keywords',
                        'meta_description_ru' => 'Meta Description',
                        'meta_description_ro' => 'Meta Description',
                        'meta_header_ru' => 'Meta Header',
                        'meta_header_ro' => 'Meta Header',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        if ($this->producer_id) {
            $crt = new CDbCriteria;
            $crt->select = 'id';
            $crt->compare('name',$this->producer_id,true);
            $info = StoreProducer::model()->findAll($crt);
            $in = CHtml::listData($info, 'id', 'id');

            $criteria->addInCondition('producer_id', $in);
        }

        if ($this->category_id) {
            $crt = new CDbCriteria;
            $crt->select = 'id';
            $crt->compare('title_ru',$this->category_id,true);
            $crt->compare('title_ro',$this->category_id,true, 'OR');
            $info = StoreCategory::model()->findAll($crt);
            $in = CHtml::listData($info, 'id', 'id');

            $criteria->addInCondition('category_id', $in);
        }

		$criteria->compare('id',$this->id);
		$criteria->compare('sku',$this->sku,true);
		$criteria->compare('name_ro',$this->name_ro,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('old_price',$this->old_price,true);
		$criteria->compare('description_ru',$this->description_ru,true);
        $criteria->compare('description_ro',$this->description_ro,true);
		$criteria->compare('short_description_ru',$this->short_description_ru,true);
        $criteria->compare('short_description_ro',$this->short_description_ro,true);
		$criteria->compare('is_special',$this->is_special);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);

        $criteria->addCondition("status <> :deleted");
        $criteria->params[':deleted'] = StoreProduct::STATUS_DELETED;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getInStockList()
    {
        return array(
            self::STATUS_IN_STOCK => Yii::t('StoreModule.store', 'In stock'),
            self::STATUS_NOT_IN_STOCK => Yii::t('StoreModule.store', 'Not in stock'),
        );
    }

    public function saveData(array $attributes, array $typeAttributes)
    {
        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $this->setAttributes($attributes);
            $this->setTypeAttributes($typeAttributes);

            if ($this->save()) {
                $this->updateEavAttributes($this->_eavAttributes);
                $transaction->commit();

                return true;
            }

            return false;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }

    public function setTypeAttributes(array $attributes)
    {
        $this->_eavAttributes = $attributes;
    }

    public function updateEavAttributes($attributes)
    {
        if (!is_array($attributes)) {
            return;
        }
        $this->deleteEavAttributes(array(), true);

        $attributes = array_filter($attributes, 'strlen');
        $this->setEavAttributes($attributes, true);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function attribute($attribute)
    {
        if($this->getIsNewRecord()) {
            return null;
        }

        return isset($this->_eavAttributes[$attribute]) ? $this->_eavAttributes[$attribute] : $this->getEavAttribute($attribute);
    }

    public function getIsShortAttributes()
    {
        if (empty($this->category)) {
            return array();
        }

        return (array)$this->category->attribute(array('condition' => 'inshort = 1 AND lang IN (:lang, "ruro")', 'order' => 'ord ASC', 'params' => array(':lang' => Yii::app()->language)));
    }

    public function getIsNotShortAttributes()
    {
        if (empty($this->category)) {
            return array();
        }

        return (array)$this->category->attribute(array('condition' => 'inshort = 0 AND lang IN (:lang, "ruro")', 'order' => 'ord ASC', 'params' => array(':lang' => Yii::app()->language)));
    }

    public function checkActiveProducts($product)
    {
        if (isset($product->category)) {
            return $product->category->isCatActive();
        }
    }

    public static function getCategoryTree()
    {
        $categories = StoreCategory::model()->active()->findAll(array('order' => 'priority, root, lft, id'));
        if (!empty($categories)) {
            $sign = "-";
            foreach ($categories as $category) {
                /**
                 * @var $category Category
                 */
                $iterator = str_repeat($sign, $category->level - 1);
                $list[$category->id] = $iterator . " " . $category->title;

            }
        }
        return $list;
    }

    public static function getAssocList()
    {
        //TODO: Прочитать http://www.elisdn.ru/blog/16/dcategorybehavior-rabota-s-kategoriiami-i-spiskami-v-yii
        $models = self::model()->findAll(array('order'=>'name_'.Yii::app()->language.' ASC'));
        return CHtml::listData($models, 'id', 'name_'.Yii::app()->language);
    }

    public function getResultPrice()
    {
        return (float)$this->price;
    }

    public function getRelatedProductsDataProvider()
    {
        return new CArrayDataProvider($this->relatedProducts, array('pagination' => false));
    }

    public function getSavePersent()
    {
        if($this->price > 0) {
            $persent = ($this->resultPrice * 100) / $this->old_price;
            return (float)round(100 - $persent, 2);
        } else
            return 0;
    }

    public function getShort_description()
    {
        return $this->{'short_description_'.Yii::app()->language};
    }

    public function getDescription()
    {
        return $this->{'description_'.Yii::app()->language};
    }

    public function getId()
    {
        return 'product_' . $this->id;
    }

    public function getRatingLog() {
        return StoreRatingLog::model()->count('user_id = :user_id AND product_id = :product_id', array(':user_id' => Yii::app()->user->id, 'product_id' => $this->id));
    }

    public function searchByName($name)
    {
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('name_'.Yii::app()->language, $name);
        return $this->findAll($criteria);
    }

    public function getUrl($lang = null)
    {
        if($lang) {
            $oldLang = Yii::app()->language;
            Yii::app()->language = $lang;
        }
        $ret = Yii::app()->createUrl('store/storeCatalog/view', array('id' => $this->id));

        if($lang)
            Yii::app()->language = $oldLang;

        return $ret;
    }

    public function getMetaTitle() {
        return $this->{'meta_title_' . Yii::app()->language} ?: $this->name;
    }

    public function getMetaKeyword() {
        $param = "meta_keywords_".Yii::app()->language;
        return $this->$param;
    }

    public function getMetaDescription()
    {
        $param = "meta_description_".Yii::app()->language;
        $description = $this->$param;
        $description = trim(preg_replace('/\s\s+/', ' ', $description));
        return $this->meta_description ?: YText::wordLimiter($description, 200);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->{'name_'.Yii::app()->language} ?
            $this->{'name_'.Yii::app()->language} :
            $this->{'name_'.Yii::app()->languages->defaultLanguage};
    }

    /**
     * @return bool
     */
    protected function isDisableDefaultScope()
    {
        return $this->disableDefaultScope;
    }

    /**
     * @return $this
     */
    public function disableDefaultScope()
    {
        $this->disableDefaultScope = true;

        return $this;
    }
}
