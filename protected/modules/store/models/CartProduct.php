<?php
Yii::import('store.components.extensions.IECartPosition');

class CartProduct extends StoreProduct implements IECartPosition
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getClass() {
        return 'storeproduct';
    }

    public function getProductModel()
    {
        return StoreProduct::model()->findByPk($this->id);
    }
}