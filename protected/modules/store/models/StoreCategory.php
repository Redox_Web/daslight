<?php

/**
 * This is the model class for table "store_category".
 *
 * The followings are the available columns in table 'store_category':
 * @property integer $id
 * @property string $title_ru
 * @property string $title_ro
 * @property string $description_ru
 * @property string $description_ro
 * @property string $slug_ru
 * @property string $slug_ro
 * @property string $image
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $is_active
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property integer $priority
 */

class StoreCategory extends ActiveRecord
{
    private $_url;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'store_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_ro', 'required'),
            array('image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'safe' => false),
            array('title_image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'safe' => false),
			array('is_active, root, lft, rgt, level, priority', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_ro, slug_ru, slug_ro, image, meta_title, meta_description, meta_keywords, url_static_page', 'length', 'max'=>255),
            array('description_ru, description_ro, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro, meta_header_h2_ru, meta_header_h2_ro, url_static_page', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title_ru, title_ro, description_ru, description_ro, slug_ru, slug_ro, image, meta_title, meta_description, meta_keywords, is_active, root, lft, rgt, level, priority, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro, meta_header_h2_ru, meta_header_h2_ro, url_static_page', 'safe', 'on'=>'search'),
		);
	}

    public function behaviors()
    {
        return array(
            'nestedSetBehavior' => array(
                'class' => 'ext.yiiext.behaviors.model.trees.NestedSetBehavior',
                'hasManyRoots' => true,
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
                'rootAttribute' => 'root',
            ),
        );
    }

    /*public function defaultScope()
    {
        return array(
            'condition'=> $this->getTableAlias(false, false).'.is_active = 1',
        );
    }*/

    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => $this->getTableAlias(false, false).'.is_active = 1',
            ),
        );
    }

    public function beforeSave()
    {
        $this->slug_ru = YText::translit($this->title_ru);
        $proverka = self::model()->findByAttributes(array('slug_ru' => $this->slug_ru));
        if($proverka && $proverka->id != $this->id)
            $this->slug_ru .= '_'.$this->id;

        $this->slug_ro = YText::translit($this->title_ro);
        $proverka = self::model()->findByAttributes(array('slug_ro' => $this->slug_ro));
        if($proverka && $proverka->id != $this->id)
            $this->slug_ro .= '_'.$this->id;

        return parent::beforeSave();
    }

    public function afterSave()
    {
        if (isset($_POST['StoreCategory']['attribute'])) {
            StoreCategoryAttribute::model()->deleteAllByAttributes(array('store_category_id'=>$this->id));
            foreach ($_POST['StoreCategory']['attribute'] as $value) {
                $categoryAttribute = new StoreCategoryAttribute();
                $categoryAttribute->store_category_id = $this->id;
                $categoryAttribute->store_attribute_id = $value;
                $categoryAttribute->save();
            }
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'attribute' => array(self::MANY_MANY, 'StoreAttribute', 'store_category_attribute(store_category_id, store_attribute_id)'),
            'product' => array(self::HAS_MANY, 'StoreProduct', 'category_id'),
            'productCount'=>array(self::STAT, 'StoreProduct', 'category_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ru' => 'Title Ru',
            'title_ro' => 'Title Ro',
			'description_ru' => 'Description Ru',
            'description_ro' => 'Description Ro',
			'slug_ru' => 'Slug Ru',
            'slug_ro' => 'Slug Ro',
			'image' => 'Image',
			'meta_title' => 'Meta Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'is_active' => 'Is Active',
			'root' => 'Root',
			'lft' => 'Lft',
			'rgt' => 'Rgt',
			'level' => 'Level',
			'priority' => 'Priority',
                        'meta_title_ru' => 'Meta Title',
                        'meta_title_ro' => 'Meta Title',
                        'meta_keywords_ru' => 'Meta Keywords',
                        'meta_keywords_ro' => 'Meta Keywords',
                        'meta_description_ru' => 'Meta Description',
                        'meta_description_ro' => 'Meta Description',
                        'meta_header_ru' => 'Meta Header',
                        'meta_header_ro' => 'Meta Header',
                        'meta_header_h2_ru' => 'Meta Header H2',
                        'meta_header_h2_ro' => 'Meta Header H2',
                        'url_static_page' => 'Url Static Page'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
        $criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('description_ru',$this->description_ru,true);
        $criteria->compare('description_ro',$this->description_ro,true);
		$criteria->compare('slug_ru',$this->slug_ru,true);
        $criteria->compare('slug_ro',$this->slug_ro,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('root',$this->root);
		$criteria->compare('lft',$this->lft);
		$criteria->compare('rgt',$this->rgt);
		$criteria->compare('level',$this->level);
		$criteria->compare('priority',$this->priority);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return bool
     */
    public function isCatActive()
    {
        if (!$this->is_active) {
            return false;
        }

        $ancestorCategories = $this->ancestors()->findAll();
        if ($ancestorCategories) {
            foreach ($ancestorCategories as $ancestorCategory) {
                if (!$ancestorCategory->is_active) {
                    return false;
                }
            }
        }

        return true;
    }

    public function checkActiveCategory($category)
    {
        if (isset($category)) {
            return $category->isCatActive();
        }
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getTitle()
    {
        return $this->{'title_'.Yii::app()->language};
    }

    public function getDescription()
    {
        return $this->{'description_'.Yii::app()->language};
    }

    public function getUrl($force_lang=false)
    {
        if ($this->_url === null && $this->url_static_page && is_int((int)$this->url_static_page)) {
            $this->_url = Yii::app()->createUrl('main/default/page', array('id' => $this->url_static_page));
        } elseif ($this->_url === null && $this->url_static_page) {
            $this->_url = Yii::app()->createUrl($this->url_static_page);
        } elseif ($this->_url === null || $force_lang) {
            $this->_url = Yii::app()->createUrl('store/storeCatalog/category', array('id' => $this->id));
        }

        return $this->_url;
    }

    public function getMetaTitle() {
        return $this->{'meta_title_' . Yii::app()->language} ?: $this->{'title_' . Yii::app()->language};
    }

    public function getMenuTitle() {
        return $this->{'menu_title_' . Yii::app()->language} ?: $this->{'title_' . Yii::app()->language};
    }

    public function getMetaKeyword() {
        $param = "meta_keywords_".Yii::app()->language;
        return $this->$param;
    }

    public function getMetaDescription()
    {
        $param = "meta_description_".Yii::app()->language;
        $description = $this->$param;
        $description = trim(preg_replace('/\s\s+/', ' ', $description));
        return $this->meta_description ?: YText::wordLimiter($description, 200);
    }

    public function getProductsQuantity()
    {
        if ($this->isRoot()) {
            $descendants = $this->descendants()->active()->findAll();
            $descendants = array_filter($descendants, array(new StoreCategory(), "checkActiveCategory"));
            $ids = CHtml::listData($descendants, 'id', 'id');
            $ids = array_values($ids);

            $criteria= new CDbCriteria;
            $criteria->addInCondition('category_id', $ids);
            return StoreProduct::model()->count($criteria);
        }

        return $this->productCount;
    }

    public function getBreabCrumbs()
    {
        $links = array();
        $categoryRoots = $this->ancestors()->findAll();
        if ($categoryRoots) {
            foreach ($categoryRoots as $categoryRoot) {
                $links[$categoryRoot->title] = $categoryRoot->url;
            }
        }
        $links[] = $this->title;
        return $links;
    }
}
