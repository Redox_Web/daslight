<?php

/**
 * @property integer $id
 * @property integer  $delivery_id
 * @property double  $delivery_price
 * @property integer  $payment_method_id
 * @property integer  $paid
 * @property string  $payment_time
 * @property string $payment_details
 * @property double $total_price
 * @property integer $separate_delivery
 * @property integer $status
 * @property string $date
 * @property integer $user_id
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $ip
 * @property string $url
 * @property string $note
 * @property string $modified
 *
 * @property OrderProduct[] $products
 * @property Delivery $delivery
 * @property Payment $payment
 * @property User $user
 *
 */

class StoreOrder extends ActiveRecord
{
    const PAID_STATUS_NOT_PAID = 0;
    const PAID_STATUS_PAID = 1;

    const SCENARIO_USER = 'front';
    const SCENARIO_ADMIN = 'admin';

    const STATUS_NEW = 'status_new';
    const STATUS_ACCEPTED = 'status_accepted';
    const STATUS_FINISHED = 'status_finished';
    const STATUS_DELETED = 'status_deleted';

    public $flash_messages = false;
    /**
     * @var OrderProduct[]
     */
    private $_orderProducts = array();

    private $hasProducts = false; // ставим в true, когда в сценарии front добавляем хотя бы один продукт

    protected $oldAttributes;

    private $productsChanged = false; // менялся ли список продуктов в заказе

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'store_order';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, email, address, phone', 'required', 'on' => self::SCENARIO_USER),
            array('name, email, address, phone', 'filter', 'filter' => 'trim'),
            array('email', 'email'),
            array('delivery_id, separate_delivery, payment_method_id, paid, user_id', 'numerical', 'integerOnly' => true),
            array('delivery_price, total_price, status', 'safe'),
            array('name, address, phone, email', 'length', 'max' => 255),
            array('comment, note', 'length', 'max' => 1024),
            array('url', 'unique'),
            array('created', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => false, 'on' => self::SCENARIO_USER),
            array('user_id, paid, payment_time, payment_details, total_price, separate_delivery, status, date, ip, url, modified',
                'unsafe',
                'on' => self::SCENARIO_USER),
            array('id, delivery_id, delivery_price, payment_method_id, paid, payment_time, payment_details, total_price, separate_delivery, status, date, user_id, name, address, phone, email, comment, ip, url, note, modified',
                'safe',
                'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'storeproducts' => array(self::MANY_MANY, 'StoreProduct', 'store_order_product(order_id, product_id)'),
            'products' => array(self::HAS_MANY, 'StoreOrderProduct', 'order_id', 'order' => 'products.id ASC'),
            'delivery' => array(self::BELONGS_TO, 'StoreDelivery', 'delivery_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function scopes()
    {
        return array(
            'new' => array(
                'condition' => 't.status = :status',
                'params' => array(':status' => OrderStatus::STATUS_NEW),
            ),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
                'createAttribute' => 'created',
                'updateAttribute' => 'updated',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('StoreModule.store', 'id'),
            'delivery_id' => Yii::t('StoreModule.store', 'Delivery'),
            'delivery_price' => Yii::t('StoreModule.store', 'Delivery price'),
            'payment_method_id' => Yii::t('StoreModule.store', 'Payment'),
            'paid' => Yii::t('StoreModule.store', 'Paid'),
            'payment_time' => Yii::t('StoreModule.store', 'Paid date'),
            'payment_details' => Yii::t('StoreModule.store', 'Payment details'),
            'total_price' => Yii::t('StoreModule.store', 'Total price'),
            'separate_delivery' => Yii::t('StoreModule.store', 'Separate delivery payment'),
            'status' => Yii::t('StoreModule.store', 'Status'),
            'date' => Yii::t('StoreModule.store', 'Date'),
            'user_id' => Yii::t('StoreModule.store', 'User'),
            'name' => Yii::t('StoreModule.store', 'Client'),
            'address' => Yii::t('StoreModule.store', 'Address'),
            'phone' => Yii::t('StoreModule.store', 'Phone'),
            'email' => Yii::t('StoreModule.store', 'Email'),
            'comment' => Yii::t('StoreModule.store', 'Comment'),
            'ip' => Yii::t('StoreModule.store', 'IP'),
            'url' => Yii::t('StoreModule.store', 'Url'),
            'note' => Yii::t('StoreModule.store', 'Note'),
            'modified' => Yii::t('StoreModule.store', 'Update date'),
        );
    }

    public function productSearch() {
        $search = $this->products;
        $orderProduct = new StoreOrderProduct();

        $sort = new CSort;
        $sort->defaultOrder = 'id DESC';
        $sort->attributes = array('product_id' => array('label' => $orderProduct->getAttributeLabel('product_id')),
            'user_id' => array('label' => $orderProduct->getAttributeLabel('user_id')),
            'product_name' => array('label' => $orderProduct->getAttributeLabel('product_name')),
            'variants_text' => array('label' => $orderProduct->getAttributeLabel('variants_text')),
            'price' => array('label' => $orderProduct->getAttributeLabel('price')),
            'quantity' => array('label' => $orderProduct->getAttributeLabel('quantity')),
            'sku' => array('label' => $orderProduct->getAttributeLabel('sku')),
        );

        return new CArrayDataProvider($search, array(
                'pagination' => array('Pagesize' => Yii::app()->params['defaultPageSize']),
                'sort' => $sort
            )
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('delivery_id', $this->delivery_id, false);
        $criteria->compare('delivery_price', $this->delivery_price, false);
        $criteria->compare('payment_method_id', $this->payment_method_id, false);
        $criteria->compare('paid', $this->paid, false);
        $criteria->compare('payment_time', $this->payment_time, true);
        $criteria->compare('payment_details', $this->payment_details, true);
        $criteria->compare('total_price', $this->total_price, false);
        $criteria->compare('separate_delivery', $this->separate_delivery, false);
        $criteria->compare('status', $this->status, false);
        $criteria->compare('user_id', $this->user_id, false);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('updated', $this->updated, true);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
                'sort' => array('defaultOrder' => $this->getTableAlias() . '.id DESC'),
            )
        );
    }

    public function afterFind()
    {
        $this->oldAttributes = $this->getAttributes();
        parent::afterFind();
    }

    public function beforeValidate()
    {
        if ($this->getScenario() === self::SCENARIO_USER) {
            if (!$this->hasProducts) {
                $this->addError('products', Yii::t('StoreModule.store', 'There are no selected products'));
            }
        }

        return parent::beforeValidate();
    }

    public function getProductsCost()
    {
        $cost = 0;
        $products = $this->productsChanged ? $this->_orderProducts : $this->products;

        foreach ($products as $op) {
            $cost += $op->price * $op->quantity;
        }

        return $cost;
    }

    public function getCoupons()
    {
        return $this->coupons;
    }

    public function getDeliveryCost()
    {
        $cost = $this->delivery_price;
        return $cost;
    }

    public function saveData(array $attributes, $status = StoreOrder::STATUS_NEW)
    {
        $transaction = Yii::app()->getDb()->beginTransaction();

        try {
            $this->status = $status;
            $this->setAttributes($attributes);

            if ($this->getScenario() == self::SCENARIO_USER) {
                $this->setProducts(Yii::app()->getModule('store')->cart->getAllItems());
            }

            if (!$this->save()) {
                return false;
            }

            $transaction->commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }

    public function beforeSave()
    {
        $productsCost = $this->getProductsCost();

        if ($this->isNewRecord) {
            $this->url = md5(uniqid(time(), true));
            $this->ip = Yii::app()->request->userHostAddress;
            if ($this->getScenario() === self::SCENARIO_USER) {
                $this->user_id = Yii::app()->user->id;
            }
        }

        $this->total_price = $productsCost;

        return parent::beforeSave();
    }

    public function afterDelete()
    {
        foreach ($this->products as $product) {
            $product->delete();
        }
        parent::afterDelete();
    }

    public function getPaidStatusList($value = null)
    {
        $arr = array(
            self::PAID_STATUS_PAID => Yii::t("StoreModule.store", 'Paid'),
            self::PAID_STATUS_NOT_PAID => Yii::t("StoreModule.store", 'Not paid'),
        );

        if($value) return $arr[$value];
        return $arr;
    }

    public function getStatusList($value = null)
    {
        $arr = array(
            self::STATUS_NEW => Yii::t("StoreModule.store", 'Status new'),
            self::STATUS_ACCEPTED => Yii::t("StoreModule.store", 'Status accepted'),
            self::STATUS_FINISHED => Yii::t("StoreModule.store", 'Status finished'),
            self::STATUS_DELETED => Yii::t("StoreModule.store", 'Status deleted'),
        );

        if($value) return $arr[$value];
        return $arr;
    }

    public function getLabels($value = null)
    {
        $arr = array(
            self::STATUS_NEW => array('class' => 'btn btn-default'),
            self::STATUS_ACCEPTED => array('class' => 'btn btn-primary'),
            self::STATUS_FINISHED => array('class' => 'btn btn-success'),
            self::STATUS_DELETED => array('class' => 'btn btn-danger'),
        );

        if($value) return $arr[$value];
        return $arr;
    }

    public function getPaidStatus()
    {
        $data = $this->getPaidStatusList();
        return isset($data[$this->paid]) ? $data[$this->paid] : Yii::t("StoreModule.store", '*unknown*');
    }


    /**
     *
     * Формат массива:
     * <pre>
     * array(
     *    '45' => array( //реальный id или сгенерированный новый, у новых внутри массива нет id
     *        'id' => '10', //если нет id, то новый
     *        'variant_ids' => array('10', '20, '30'), // массив с id вариантов
     *        'quantity' = > '5',
     *        'price' => '1000',
     *        'product_id' => '123',
     *    )
     * )
     * </pre>
     * @param $orderProducts Array
     */
    public function setProducts($orderProducts)
    {
        $this->productsChanged = true;
        $orderProductsObjectsArray = array();
        if ($orderProducts) {
            foreach ($orderProducts as $product) {
                $this->hasProducts = true;

                $orderProduct = new StoreOrderProduct();
                $orderProduct->product_id = $product->id;
                $orderProduct->product_name = $product->name;
                $orderProduct->sku = $product->sku;
                $orderProduct->quantity = $product->getQuantity();
                $orderProduct->price = $product->getResultPrice();

                $orderProductsObjectsArray[] = $orderProduct;
            }
            $this->_orderProducts = $orderProductsObjectsArray;
        }
    }

    /**
     * Массив объектов OrderProduct
     * @param $products
     */
    private function updateOrderProducts($products)
    {
        if (!$this->productsChanged) {
            return;
        }

        $validOrderProductIds = array();

        foreach ($products as $var) {
            /* @var $var OrderProduct */
            if ($var->isNewRecord) {
                $var->order_id = $this->id;
            }

            if ($var->save()) {
                $validOrderProductIds[] = $var->id;
            }
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('order_id = :order_id');
        $criteria->params = array(':order_id' => $this->id);
        $criteria->addNotInCondition('id', $validOrderProductIds);
        StoreOrderProduct::model()->deleteAll($criteria);
    }

    public function afterSave()
    {
        $this->updateOrderProducts($this->_orderProducts);
        parent::afterSave();
    }


    public function getTotalPrice()
    {
        return (float)$this->total_price;
    }

    public function getDeliveryPrice()
    {
        return (float)$this->delivery_price;
    }

    public function getTotalPriceWithDelivery()
    {
        $price = $this->getTotalPrice();

        if (!$this->separate_delivery) {
            $price += $this->getDeliveryPrice();
        }

        return $price;
    }

    public function isPaid()
    {
        return (int)$this->paid === static::PAID_STATUS_PAID;
    }

    public function pay(Payment $payment)
    {
        if ($this->isPaid()) {
            return true;
        }

        $this->paid = static::PAID_STATUS_PAID;
        $this->payment_method_id = $payment->id;
        $this->payment_time = date('Y-m-d H:i:s');

        $result = $this->save();

        return $result;
    }

    public function findByUrl($url)
    {
        return $this->findByAttributes(array('url' => $url));
    }

    public function findByNumber($number)
    {
        return $this->findByPk($number);
    }
}