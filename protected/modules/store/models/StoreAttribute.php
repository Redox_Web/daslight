<?php

/**
 * This is the model class for table "store_attribute".
 *
 * The followings are the available columns in table 'store_attribute':
 * @property integer $id
 * @property string $title
 * @property integer $type
 * @property string $unit
 * @property integer $required
 * @property integer $inshort
 *
 * The followings are the available model relations:
 * @property StoreAttributeValue[] $storeAttributeValues
 */
class StoreAttribute extends ActiveRecord
{
    public $flash_messages = false;

    const TYPE_TEXT = 0;
    const TYPE_SHORT_TEXT = 1;
    const TYPE_DROPDOWN = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_CHECKBOX_LIST = 4;
    const TYPE_IMAGE = 5;
    const TYPE_NUMBER = 6;

    public $rawOptions;

    const RU = 'ru';
    const RO = 'ro';
    const RURO = 'ruro';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'store_attribute';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type, required, inshort', 'required'),
            array('title', 'unique'),
			array('type, required, inshort', 'numerical', 'integerOnly'=>true),
			array('title, column_name', 'length', 'max'=>255),
			array('unit', 'length', 'max'=>30),
            array('rawOptions, lang', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, type, unit, required, inshort', 'safe', 'on'=>'search'),
		);
	}

    public function beforeSave()
    {
        $this->column_name = YText::translit($this->title);
        $proverka = self::model()->findByAttributes(array('column_name' => $this->column_name));
        if($proverka && $proverka->id != $this->id)
            $this->column_name .= '_'.$this->id;

        return parent::beforeSave();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'attribute_values' => array(self::HAS_MANY, 'StoreAttributeValue', 'attribute'),
            'options' => array(self::HAS_MANY, 'StoreAttributeOption', 'attribute_id', 'order' => 'position ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'type' => 'Type',
			'unit' => 'Unit',
			'required' => 'Required',
			'inshort' => 'Inshort',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('required',$this->required);
		$criteria->compare('inshort',$this->inshort);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getTypesList()
    {
        return array(
            self::TYPE_SHORT_TEXT => Yii::t('StoreModule.main', 'Short text (up to 250 characters)'),
            self::TYPE_TEXT => Yii::t('StoreModule.main', 'Text'),
            self::TYPE_DROPDOWN => Yii::t('StoreModule.main', 'Dropdown list'),
            self::TYPE_CHECKBOX => Yii::t('StoreModule.main', 'Checkbox'),
            self::TYPE_NUMBER => Yii::t('StoreModule.main', 'Number'),
        );
    }

    public function getLangList($value = null)
    {
        $arr = array(
            self::RU => 'RU',
            self::RO => 'RO',
            self::RURO => 'RU/RO',
        );

        if($value) return $arr[$value];
        return $arr;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StoreAttribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function type($type_id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 't.type_id=:type',
            'params'=>array(':type'=>$type_id),
        ));
        return $this;
    }

    protected function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->attribute_values as $value)
                $value->delete();

            return true;
        }

        return false;
    }

    public static function getTypesWithOptions()
    {
        return array(self::TYPE_DROPDOWN);
    }

    public function afterSave()
    {
        if ($this->type == self::TYPE_DROPDOWN && $this->scenario != 'sort') {
            // список новых значений опций атрибута, не пустые, без лишних пробелов по бокам, уникальные
            $newOptions = array_unique(
                array_filter(
                    array_map('trim', explode("\n", $this->rawOptions))
                )
            );

            // в нижнем регистре, чтобы не надо было переназначать привязку атрибутов в товарах
            $newOptionsLower = array_map(
                function ($x) {
                    return mb_strtolower($x, 'utf-8');
                },
                $newOptions
            );

            $oldOptionsLower = array(); // список имен опций, которые уже сохранены

            // удалим те из них, которых нет, в остальных обновим значение и позицию
            foreach ((array)$this->options as $option) {
                /* @var $option AttributeOption */
                $position = array_search(mb_strtolower($option->value), $newOptionsLower);
                // опция была удалена
                if ($position === false) {
                    $option->delete();
                } else {
                    $oldOptionsLower[] = mb_strtolower($option->value, 'utf-8');
                    $option->value = $newOptions[$position]; // если поменяли регистр опции
                    $option->position = $position;
                    $option->save();
                }
            }

            // добавим оставшиеся
            foreach (array_diff($newOptionsLower, $oldOptionsLower) as $position => $value) {
                $option = new StoreAttributeOption();
                $option->attribute_id = $this->id;
                $option->value = $newOptions[$position];
                $option->position = $position;
                $option->save();
            }
        }

        parent::afterSave();
    }

    public function getRawOptions()
    {
        $tmp = '';
        foreach ((array)$this->options as $option) {
            $tmp .= $option->value . "\n";
        }
        return $tmp;
    }

    public function renderField($value = null, $name = null, $htmlOptions = array('class' => 'form-control'))
    {
        $name = $name ?: 'StoreAttribute[' . $this->column_name . ']';
        switch ($this->type) {
            case self::TYPE_SHORT_TEXT:
                return CHtml::textField($name, $value, $htmlOptions);
                break;
            case self::TYPE_TEXT:
                return CHtml::textArea($name, $value, $htmlOptions);
                break;
            case self::TYPE_DROPDOWN:
                $data = CHtml::listData($this->options, 'id', 'value');
                return CHtml::dropDownList($name, $value, $data, array_merge($htmlOptions, ($this->required ? array() : array('empty' => '---'))));
                break;
            case self::TYPE_CHECKBOX_LIST:
                $data = CHtml::listData($this->options, 'id', 'value');
                return CHtml::checkBoxList($name . '[]', $value, $data, $htmlOptions);
                break;
            case self::TYPE_CHECKBOX:
                return CHtml::checkBox($name, $value, CMap::mergeArray(array('uncheckValue' => 0), $htmlOptions));
                break;
            case self::TYPE_NUMBER:
                $htmlOptions += array('min' => 0);
                $value = empty($value) ? 0 : $value;
                return CHtml::numberField($name, $value, $htmlOptions);
                break;
            case self::TYPE_IMAGE:
                return CHtml::fileField($name, null, $htmlOptions);
                break;
        }

        return null;
    }

    public function renderValue($value)
    {
        $unit = $this->unit ? ' ' . $this->unit : '';
        $res = '';
        switch ($this->type) {
            case self::TYPE_TEXT:
            case self::TYPE_SHORT_TEXT:
            case self::TYPE_NUMBER:
                $res = $value;
                break;
            case self::TYPE_DROPDOWN:
                $data = CHtml::listData($this->options, 'id', 'value');
                if (!is_array($value) && isset($data[$value])) {
                    $res = $data[$value];
                }
                break;
            case self::TYPE_CHECKBOX:
                $res = $value ? Yii::t("StoreModule.store", "Yes") : Yii::t("StoreModule.store", "No");
                break;
        }

        return $res . $unit;
    }

    public function getAttributeByName($name)
    {
        return $name ? self::model()->findByAttributes(array('column_name' => $name)) : null;
    }

    public function isType($type)
    {
        return $type == $this->type;
    }

    public function isRequired()
    {
        return $this->required;
    }
}
