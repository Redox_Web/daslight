<?php

class ProductSearch extends StoreProduct
{
    public $related;

    public function searchNotFor($id = null)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('sku', $this->sku);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('price', $this->price);
        $criteria->addNotInCondition('id', array($id));

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC')
        ));
    }

    public function related_button($product_id, $relation)
    {
        $storeRelated = StoreRelated::model()->findByAttributes(array('product_id' => $product_id, 'related_product_id' => $this->id, 'relation' => $relation));

        if($storeRelated)
            return true;
        else
            return false;
    }
}
