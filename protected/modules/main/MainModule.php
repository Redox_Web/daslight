<?php
Yii::import('backend.components.WebModule');

class MainModule extends WebModule
{
    public $sidebar_tab = "action-pop";
    public $_localAssetsUrl;

	public function init()
	{
        parent::init();

		$this->setImport(array(
			'main.models.*',
			'main.components.*',
            'main.widgets.*',
		));
	}

    public static function urlRules()
    {
        return array(
            array('class' => 'main.components.mainPageRouter'),
            array('class' => 'main.components.mainUrlRouter'),
            //'contact' => 'main/default/feedback',
            //'calculator' => 'main/default/calculator',
            //'partners' => 'main/default/partners'
        );
    }

    public static function menuItem()
    {
        return array(
            'icon' => 'fa fa-folder-open-o',
            'label' => Yii::t('MainModule.main', 'Main content'),
            'items' => array(
                array(
                    'label' => Yii::t('MainModule.main', 'Static Pages'),
                    'url' => 'main/mainPages/admin',
                    'sidebar_tab' => 'main-pages',
                ),
                array(
                    'label' => Yii::t('MainModule.main', 'Feedback'),
                    'url' => 'main/mainFeedback/admin',
                    'sidebar_tab' => 'main-feedback',
                ),
                array(
                    'label' => Yii::t('MainModule.main', 'Contact Info'),
                    'url' => 'main/contactInfo/admin',
                    'sidebar_tab' => 'contact-info',
                ),
                array(
                    'label' => Yii::t('MainModule.main', 'Work type'),
                    'url' => 'main/workType/admin',
                    'sidebar_tab' => 'work-type',
                ),
                array(
                    'label' => Yii::t('MainModule.main', 'Action Popup'),
                    'url' => 'main/actionPopup/admin',
                    'sidebar_tab' => 'action-pop',
                ),
            )
        );
    }

    public static function getLayoutAlias($layoutName = '')
    {
        return 'application.modules.main.views.layouts.'.($layoutName ? $layoutName : 'main');
    }
}
