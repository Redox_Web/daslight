<?php
class DefaultController extends Frontend
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
            'webhook' => 'application.components.Webhook',
        );
    }

    public function actionChange()
    {
        if (isset($_GET['lang'])) {
            Yii::app()->session['language'] = $_GET['lang'];
        }
        if (isset(Yii::app()->session['language'])) {
            $cookie = new CHttpCookie('language', $_GET['lang']);
            $cookie->expire = time() + (60*60*24*365); // (1 year)
            Yii::app()->request->cookies['language'] = $cookie;

            Yii::app()->language = Yii::app()->session['language'];
            Yii::app()->user->setState('language', $_GET['lang']);
        }
        $baseUrl = Yii::app()->request->getBaseUrl(true)."/";
        $refferer = Yii::app()->request->urlReferrer;
        $expl_refer = explode('/', $refferer);

        if ($baseUrl==$refferer || in_array(end($expl_refer), Yii::app()->languages->languages)) {
            $link = $this->createUrl("/main/default/index", array("lang"=>Yii::app()->language));
            if ($link == '/') {
                $link = $link . Yii::app()->language;
            }
            $this->redirect($link);
        } else {
            $this->redirect($this->urltrans(Yii::app()->request->urlReferrer));
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        if (Yii::app()->language != Yii::app()->user->language) {
            $this->redirect('/' . Yii::app()->user->language);
        }
        $this->render('index');
    }

    public function actionFeedback()
    {
        $model = new MainFeedback();
        $model->scenario = 'registerwcaptcha';

        /*if (isset($_POST['ajax']) && $_POST['ajax'] === 'feedback-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }*/

        if (isset($_POST["MainFeedback"])) {
            $model->attributes = $_POST["MainFeedback"];

            if($model->save()) {
                if($this->sendMailContact($model)){
                    Yii::app()->getUser()->setFlash(
                        FlashMessages::SUCCESS_MESSAGE(),
                        YHelper::yiisetting('success_message_contact_'.Yii::app()->language)
                    );
                    $model->unsetAttributes();
                }else{
                    throw new CException("Error on send email!");
                }
            }
        }

        $this->render('feedback', array('model' => $model));
    }

    public function sendMailContact($model){
        $emails_settings = YHelper::yiisetting('emails_list_contact');
        if($emails_settings){
            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->AddReplyTo($model->email);
            $emails_arr = preg_split("/\\r\\n|\\r|\\n/", $emails_settings);
            foreach($emails_arr as $email){
                $mailer->AddAddress($email);
            }
            $mailer->FromName = Yii::app()->name;
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = "Сообщение со страницы контактов с сайта ".Yii::app()->name;
            $mailer->IsHTML(true);  // set email format to HTML
            $body = "Сообщение со страницы контактов с сайта ".Yii::app()->name."<br><br>"
                    . "Имя: {$model->name} <br>"
                    . "Email: {$model->email} <br>"
                    . "Сообщение: {$model->feedback} <br>";
            $mailer->Body = $body;

            if($mailer->Send()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public function actionPage($id){
        $model = MainPages::model()->findByPk($id);
        if(!$model)
            throw new CHttpException(404,Yii::t("base","The requested page does not exist!"));

        $this->render("page",array('model'=>$model));
    }

    public function actionPartners(){
        $this->render("partners");
    }

    public function actionCalculator(){
        $this->render("calculator");
    }

    public function actionDownload(){
        $filepath = 'files/document.txt';

        $fpath = explode("/", $filepath);
        $fileName = end($fpath);

        if (file_exists($filepath))
            return Yii::app()->getRequest()->sendFile($fileName, @file_get_contents($filepath));
        else
            throw new CHttpException(404, 'This file does not exist.');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionPriceCount()
    {
        $model = new PriceCount();

        if (isset($_POST['PriceCount'])) {
            $model->attributes=$_POST['PriceCount'];
            if ($model->validate()) {
                $blogArticle = $_FILES['PriceCount'];

                $theme = 'DASLIGHT расчет стоймости';
                $body = $this->renderPartial('main.views.default._email', ['model' => $model], true);
                Yii::app()->email->sendEmail($theme, $body, YHelper::yiisetting('email'), $model->email, $blogArticle);

                Yii::app()->getUser()->setFlash(
                    FlashMessages::SUCCESS_MESSAGE(),
                    YHelper::yiisetting('success_message_calculation_'.Yii::app()->language)
                );

                $this->refresh(true);
            }
        }

        $this->render('price_count',array(
            'model'=>$model,
        ));
    }
}