<?php
Yii::import('backend.components.BackendController');

class ActionPopupController extends BackendController
{
    public $sidebar_tab = "action-popup";

    public function actions()
    {
        return array(
            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update'=> 'DUpdateAction',
        );
    }

    public function createModel()
    {
        return new ActionPopup();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ActionPopup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='action-popup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionClearCache()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $actionPopup = ActionPopup::model()->findByPk(1);
            $actionPopup->cache = YHelper::generateStr();
            if ($actionPopup->saveAttributes(array('cache'))) {
                Yii::app()->ajax->success();
            }

            Yii::app()->ajax->failure();
        }
    }
}