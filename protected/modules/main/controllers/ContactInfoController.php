<?php
Yii::import('backend.components.BackendController');

class ContactInfoController extends BackendController
{
    public $sidebar_tab = "contact-info";

    public function actions()
    {
        return array(
            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => array(
                'class' => 'DCreateAction',
                'redirect' => 'manage',
            ),
            'delete' => 'DDeleteAction',
            'update' => array(
                'class' => 'DUpdateAction',
                'redirect' => 'manage',
            ),
        );
    }

    public function createModel()
    {
        return new ContactInfo();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ContactInfo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionCreateinfo()
    {
        $model=new ContactInfo('info');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['ContactInfo'])) {
            $model->attributes=$_POST['ContactInfo'];
            if($model->saveNode())
                $this->redirect(array('admin'));
        }

        $this->render('create',array(
            'model'=>$model,
            'info' => true
        ));
    }

    public function actionUpdateinfo($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'info';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['ContactInfo'])) {
            $model->attributes=$_POST['ContactInfo'];
            if($model->saveNode())
                $this->redirect(array('admin'));
        }

        $this->render('update',array(
            'model'=>$model,
            'info' => true
        ));
    }

    public function actionTree()
    {
        if (isset($_POST["ids"])) {
            $ids = $_POST["ids"];
            foreach ($ids as $key => $id) {
                $menu = ContactInfo::model()->findByPk((int)$id["item_id"]);
                if(isset($menu->flash_messages))
                    $menu->flash_messages = false;

                if ($menu && (int)$id["item_id"] != 0) {
                    $menu->priority = $key;
                    $menu->saveNode();
                    if (!empty($id["parent_id"]) && $id["parent_id"] != "root") {
                        $root = ContactInfo::model()->findByPk((int)$id["parent_id"]);
                        $menu->moveAsLast($root);
                    } else {
                        if (!$menu->isRoot())
                            $menu->moveAsRoot();
                    }
                }
            }
            echo json_encode($ids);
        }
    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='main-links-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}