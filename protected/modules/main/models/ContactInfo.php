<?php

/**
 * This is the model class for table "main_links".
 *
 * The followings are the available columns in table 'main_links':
 * @property integer $id
 * @property integer $page_id
 * @property integer $level
 * @property string $title_ru
 * @property string $title_ro
 * @property string $info_ru
 * @property string $info_ro
 * @property integer $active
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $priority
 */
class ContactInfo extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contact_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_ro', 'required'),
            array('info_ru, info_ro', 'required', 'on' => 'info'),
			array('page_id, level, active, root, lft, rgt, priority', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_ro', 'length', 'max'=>150),
			array('info', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, page_id, level, title_ru, title_ro, info_ru, info_ro, active, root, lft, rgt, priority', 'safe', 'on'=>'search'),
		);
	}

    public function behaviors()
    {
        return array(
            'nestedSetBehavior' => array(
                'class' => 'ext.yiiext.behaviors.model.trees.NestedSetBehavior',
                'hasManyRoots' => true,
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
                'rootAttribute' => 'root',
            ),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'level' => 'Level',
			'title_ru' => 'Title Ru',
            'title_ro' => 'Title Ro',
			'info_ru' => 'Info Ru',
            'info_ro' => 'Info Ro',
			'active' => 'Active',
			'root' => 'Root',
			'lft' => 'Lft',
			'rgt' => 'Rgt',
			'priority' => 'Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('level',$this->level);
		$criteria->compare('title_ru',$this->title_ru,true);
        $criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('info_ru',$this->info_ru,true);
        $criteria->compare('info_ro',$this->info_ro,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('root',$this->root);
		$criteria->compare('lft',$this->lft);
		$criteria->compare('rgt',$this->rgt);
		$criteria->compare('priority',$this->priority);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MainLinks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getTitle()
    {
        return $this->{'title_'.Yii::app()->language};
    }

    public function getInfo()
    {
        return $this->{'info_'.Yii::app()->language};
    }
}
