<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $title_ru
 * @property string $title_ro
 * @property string $footer_title_ru
 * @property string $footer_title_ro
 * @property string $content_ru
 * @property string $content_ro
 * @property string $slug
 */
class MainPages extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'main_pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_ro, content_ru, content_ro, slug', 'required'),
			array('title_ru, title_ro, slug, footer_title_ru, footer_title_ro', 'length', 'max'=>255),
            array('title_image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'safe' => false),
            array('meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'length', 'max'=>250),
            array('ogimage', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'safe' => false),
            array('slug', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title_ru, title_ro, content_ru, content_ro, slug, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title_ru' => 'Title Ru',
            'title_ro' => 'Title Ro',
			'content_ru' => 'Content Ru',
            'content_ro' => 'Content Ro',
			'slug' => 'Slug',
                        'meta_title_ru' => 'Meta Title',
                        'meta_title_ro' => 'Meta Title',
                        'meta_keywords_ru' => 'Meta Keywords',
                        'meta_keywords_ro' => 'Meta Keywords',
                        'meta_description_ru' => 'Meta Description',
                        'meta_description_ro' => 'Meta Description',
                        'meta_header_ru' => 'Meta Header',
                        'meta_header_ro' => 'Meta Header',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
        $criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('content_ru',$this->content_ru,true);
        $criteria->compare('content_ro',$this->content_ro,true);
		$criteria->compare('slug',$this->slug,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getTitle() {
        return $this->{'title_' . Yii::app()->language};
    }

    public function getFooterTitle() {
        return $this->{'footer_title_' . Yii::app()->language} ? : $this->getTitle();
    }

    public function getContent() {
        return $this->{'content_' . Yii::app()->language};
    }

    public function getMetaTitle() {
        return $this->{'meta_title_' . Yii::app()->language} ?: $this->{'title_' . Yii::app()->language};
    }

    public function getMetaKeyword() {
        return $this->{'meta_keywords_' . Yii::app()->language};
    }

    public function getMetaDescription() {
        return $this->{'meta_description_' . Yii::app()->language};
    }

    public function getUrl() {
        return Yii::app()->createUrl('/main/default/page', array('id' => $this->id));
    }
}