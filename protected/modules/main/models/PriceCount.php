<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PriceCount extends CFormModel
{
    public $type;
    public $name;
    public $description;
    public $phone;
    public $email;
    public $file;
    protected $calculationPage;

    /**
     * @return MainPages|null
     */
    public function getCalculationPage()
    {
        /** @var MainPages $model */
        $model = MainPages::model();
        $calculationPage = $model->findByAttributes(array('slug' => 'calculation-page'));
        return $calculationPage;
    }

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('type, name, description, phone, email', 'required'),
            array('name, description, phone, email','filter','filter'=>'strip_tags'),
            array('file', 'file', 'types'=>'jpg, jpeg, gif,png,doc,docx,pdf,txt', 'maxSize'=>1024 * 1024 * 25, 'safe' => false, 'allowEmpty' => true),
            array('phone', 'match', 'pattern'=>'/^[-+()0-9 ]+$/', 'message' => Yii::t("base",'Wrong phone format')),
            array('email', 'email'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'type' => Yii::t("base", 'Work type'),
            'name' => Yii::t("base", 'Name'),
            'description' => Yii::t("base", 'Description'),
            'phone' => Yii::t("base", 'Phone'),
            'email' => Yii::t("base", 'E-mail'),
        );
    }

    public function getAllTypes($value = null)
    {
        $work_types = WorkType::model()->findAll();
        $retArr = CHtml::listData($work_types, 'id', 'name');

        if ($value) {
            return $retArr[$value];
        }

        return $retArr;
    }
}
