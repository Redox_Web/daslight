<?php
Yii::import('main.models.ActionPopup');

class ActionPop extends CWidget
{
    public $view = 'popup_view';

    public function run()
    {
        $actionPop = ActionPopup::model()->findByPk(1);

        if ($actionPop && $this->checkCookie($actionPop->cache) && $actionPop->is_active) {
            $this->registerScript();
            $this->writeCookie($actionPop->cache);
            $this->render($this->view, array('actionPop' => $actionPop));
        }
    }

    private function registerScript() {
        Yii::app()->clientScript->registerScriptFile(
            $this->controller->getAssetsUrl().'/js/fancybox/jquery.fancybox-1.3.4.pack.js',
            CClientScript::POS_END
        );
    }

    private function checkCookie($cookieName) {
        $datetime1 = $this->getCookie($cookieName);

        if ($datetime1) {
            $datetime2 = new DateTime();
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            return $days >= 14;
        }

        return true;
    }

    private function getCookie($cookieName) {
        $cookie = (string) Yii::app()->request->cookies[$cookieName];
        if ($cookie) {
            return new DateTime($cookie);
        }

        return false;
    }

    private function writeCookie($cookieName) {
        $date = date('Y-m-d');
        $cookie = new CHttpCookie($cookieName, $date);
        $cookie->expire = time()+60*60*24*180;
        Yii::app()->request->cookies[$cookieName] = $cookie;
    }
}