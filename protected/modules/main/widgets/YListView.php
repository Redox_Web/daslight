<?php
Yii::import('zii.widgets.CListView');

class YListView extends CListView
{
    public $sizerVariants = array(9, 18, 27, 'All');
    public $sizerCssClass = 'mt-textbox';
    public $sizerHeader = 'View';
    public $sizerAttribute = 'itemsPerPage';
    public $sizerFooter = '';

    /**
     * Renders the sorter.
     */
    public function renderSorter()
    {
    	$this->summaryCssClass = 'mt-textbox';
        $this->sorterCssClass = 'mt-shoplist-header';

        if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes)) {
            return;
        }

        echo CHtml::openTag('header',array('class'=>$this->sorterCssClass))."\n";
            echo '<div class="btn-box">';
                echo "<ul class=\"list-inline\">\n";
	            echo '<li>';
	                echo '<a href="#" class="drop-link">';
	                    echo $this->getFilterTitle().'&nbsp;';
	                    echo '<i aria-hidden="true" class="fa fa-angle-down"></i>';
	                echo '</a>';

                $sort=$this->dataProvider->getSort();

                echo '<div class="drop">';
                    echo '<ul class="list-unstyled">';

                    foreach($this->sortableAttributes as $name=>$label) {
                        if (is_integer($name)) {
                            echo '<li>'.$sort->link($label, null, array($label => 1)).'</li>';
                            echo '<li>'.$sort->link($label, null, array($label => 0)).'</li>';
                        } else {
                            echo '<li>'.$sort->link($name,Yii::t("MainModule.main", $label), array($name => 1)).'</li>';
                            echo '<li>'.$sort->link($name,Yii::t("MainModule.main", $label), array($name => 0)).'</li>';
                        }
                    }
                    echo '</ul>';
                echo '</div>';
	            echo '</li>';
                echo "</ul>";
            echo "</div>";
        echo $this->sorterFooter;
        $this->renderSizer();
        echo CHtml::closeTag('header');
    }

    private function getFilterTitle()
    {
        $defaultString = Yii::t("MainModule.main", "Default Sorting");

        $sort = Yii::app()->request->getQuery('sort');
        if ($sort == 'price.desc') {
            $defaultString = Yii::t("MainModule.main", 'Price') . ' <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        } elseif ($sort == 'price') {
            $defaultString = Yii::t("MainModule.main", 'Price') . ' <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        } elseif ($sort == 'name_ru' || $sort == 'name_ro') {
            $defaultString = Yii::t("MainModule.main", 'Name') . ' <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        } elseif ($sort == 'name_ru.desc' || $sort == 'name_ro.desc') {
            $defaultString = Yii::t("MainModule.main", 'Name') . ' <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        }

        return $defaultString;
    }

    public function renderSizer()
    {
        $itemCount = $this->dataProvider->getTotalItemCount();
        if ($itemCount <= 0 || $itemCount < $this->sizerVariants[0])
            return;

        $render = null;
        $pageVar = $this->dataProvider->getPagination()->pageVar;

        $render .= CHtml::openTag('div', array('class' => $this->sizerCssClass)) . "\n";
        $render .= $this->renderSummary();
        $render .= '<p>';
        $render .= Yii::t("MainModule.main", $this->sizerHeader).' ';

        $data = array();
        $link = preg_replace('~(/'.$this->sizerAttribute.'/[a-zA-Z0-9._-]+)~', '', Yii::app()->request->url);

        foreach($this->sizerVariants as $count)
        {
            $params = array_replace($_GET, array($this->sizerAttribute => $count));
            if (isset($params[$pageVar]))
                unset($params[$pageVar]);

            if ($count == 'All') {
                $data[] = CHtml::link(Yii::t("MainModule.main", $count), Yii::app()->createUrl($link, array(
                    $this->sizerAttribute => $itemCount
                )));
            } else {
                $data[] = CHtml::link($count, Yii::app()->createUrl($link, array($this->sizerAttribute => $count)));
            }
        }

        $render .= implode('/', $data);

        $render .= $this->sizerFooter;
        $render .= '</p>';
        $render .= CHtml::closeTag('div');

        if(preg_match('~{summary}~', $this->template) && $this->inSummary)
            $this->summaryText .= $render;
        else
            echo $render;
    }

    public function renderSummary()
    {
        if(($count=$this->dataProvider->getItemCount())<=0)
            return;

//        echo CHtml::openTag($this->summaryTagName, array('class'=>$this->summaryCssClass));
        if($this->enablePagination)
        {
            $pagination=$this->dataProvider->getPagination();
            $total=$this->dataProvider->getTotalItemCount();
            $start=$pagination->currentPage*$pagination->pageSize+1;
            $end=$start+$count-1;
            if($end>$total)
            {
                $end=$total;
                $start=$end-$count+1;
            }
            if(($summaryText=$this->summaryText)===null)
                $summaryText=Yii::t('zii','Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.',$total);
            return strtr($summaryText,array(
                '{start}'=>$start,
                '{end}'=>$end,
                '{count}'=>$total,
                '{page}'=>$pagination->currentPage+1,
                '{pages}'=>$pagination->pageCount,
            ));
        }
        else
        {
            if(($summaryText=$this->summaryText)===null)
                $summaryText=Yii::t('zii','Total 1 result.|Total {count} results.',$count);
            return strtr($summaryText,array(
                '{count}'=>$count,
                '{start}'=>1,
                '{end}'=>$count,
                '{page}'=>1,
                '{pages}'=>1,
            ));
        }
//        echo CHtml::closeTag($this->summaryTagName);
    }
}