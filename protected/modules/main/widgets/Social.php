<?php
class Social extends CWidget
{
    private $allow = 0;

    public function run()
    {
        $socials = array('facebook_href', 'twitter_href', 'pinterest_href', 'googleplus_href', 'youtube_href');

        $social = $this->checkSocial($socials);

        if ($this->allow)
            $this->render("social", $social);
    }

    private function checkSocial($socials)
    {
        $socialList = array();
        foreach($socials as $social)
        {
            $socialList[$social] = YHelper::yiisetting($social, '');

            if($socialList[$social] != '#' && $socialList[$social] != '')
                $this->allow = 1;
        }

        return $socialList;
    }
}