<?php
class ContactinfoWidget extends  CWidget
{
    public $view = 'contact_info';

    public function run() {
        $model = ContactInfo::model()->findAll(array('order'=>'priority,root,lft,id'));
        $this->render($this->view, array('model' => $model));
    }
}