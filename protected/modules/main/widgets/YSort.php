<?php
class YSort extends CSort
{
    public function link($attribute,$label=null, $directions = array(), $htmlOptions=array())
    {
        if($label===null)
            $label=$this->resolveLabel($attribute);
        if(($definition=$this->resolveAttribute($attribute))===false)
            return $label;

        if (!$directions)
            $directions=$this->getDirections();

        if(isset($directions[$attribute]))
        {
            $class=$directions[$attribute] ? 'desc' : 'asc';
            if(isset($htmlOptions['class']))
                $htmlOptions['class'].=' '.$class;
            else
                $htmlOptions['class']=$class;
            $descending=!$directions[$attribute];
            unset($directions[$attribute]);
        }
        elseif(is_array($definition) && isset($definition['default']))
            $descending=$definition['default']==='desc';
        else
            $descending=false;

        if($this->multiSort)
            $directions=array_merge(array($attribute=>$descending),$directions);
        else
            $directions=array($attribute=>$descending);

        $url=$this->createUrl(Yii::app()->getController(),$directions);

        $icon = $descending ? ' <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>'
            : ' <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';

        return $this->createLink($attribute,$label.$icon,$url,$htmlOptions);
    }
}