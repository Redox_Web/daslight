<?php
    $categoryRoot = $category == 'all' ? : $category->parent()->find();
    if(!$categoryRoot) $categoryRoot = $category;
?>

<div class="row">
<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class=""></div>
    <div class="promo-image" style="background-image: url('<?php echo YHelper::getImagePath($category->title_image,0,0, Yii::app()->params['header_image']); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6  col-sm-12 text-left-xs text-center-sm ">
                    <?php $arr_url_params = explode("/", Yii::app()->request->requestUri); ?>
                    <?php $page_param = end($arr_url_params); ?>
                    <?php $page_param = filter_var($page_param, FILTER_VALIDATE_INT) !== false ? (int) $page_param : 1; ?>
                    <?php if($category != 'all') { ?>
                        <h1 class="cart-page"><?php echo $category->{"meta_header_".Yii::app()->language} ?: $category->title; ?></h1>
                    <?php } else { ?>
                        <h1 class="cart-page"><?php echo Yii::t("base", "All products");?></h1>
                    <?php } ?>

                    <?php if ($categoryRoot == 'all') {
                        $links = array(Yii::t("base", "All products"));
                    } else {
                        $links = $category->getBreabCrumbs();
                    } ?>

                    <!-- Breadcrumbs of the Page -->
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>$links
                    )); ?>
                    <!-- Breadcrumbs of the Page end -->
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container pt-3em pb-3em">
    <div class="row">
        <?php $this->widget('main.widgets.LeftMenu'); ?>


        <div class="col-xs-12 col-sm-8 col-md-9 wow fadeInRight" data-wow-delay="0.4s">
            <!-- mt shoplist header start here -->
            <?php $this->widget(
                'main.widgets.YListView',
                array(
                    'id' => 'list-item',
                    'dataProvider' => $dataProvider,
                    'itemView' => '_product_grid',
	                'template' => '{sorter}{items}{pager}',
                    'itemsTagName' => 'ul',
                    'itemsCssClass' => 'mt-productlisthold list-inline',
                    'enableHistory' => true,
                    'cssFile' => false,
                    'summaryText' => Yii::t("MainModule.main", '[p]Showing [strong]{start}–{end}[/strong] of [strong]{count}[/strong] results[/p]', array('[p]' => '<p>', '[/p]' => '</p>', '[strong]' => '<strong>', '[/strong]' => '</strong>',)),
                    'pager'=> "LinkPager",
                    'ajaxUpdate'=>false,
                    'sortableAttributes'=>array(
                        'name_'.Yii::app()->language,
                        'price',
                    ),
                    'emptyText' => '<div class="col-xs-12 pb30"><div class="alert alert-info" role="alert">'.Yii::t("base", "No results").'</div></div>'
                )
            ); ?>
            <?php $arr_url_params = explode("/", Yii::app()->request->requestUri); ?>
            <?php $page_param = end($arr_url_params); ?>
            <?php $page_param = filter_var($page_param, FILTER_VALIDATE_INT) !== false ? (int) $page_param : 1; ?>
            <?php if($category != 'all') { ?>
                <?php if($page_param==1 && !empty($category->description)){ ?>
                <div class="pb-2em home-text">
                    <?php echo $category->description; ?>
					<div class="home-text-inner"></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center article-announce home-bottom-block pt-4em">
                        <a class="btn-shop home-read" href="#" style="font-size: 15px;">
                            <span><?php echo Yii::t("base", "More"); ?></span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            <?php } else { ?>
                <h2><?php echo Yii::t("base", "All products");?></h2>
            <?php } ?>
            <!-- mt productlisthold end here -->
        </div>
    </div>
</div>