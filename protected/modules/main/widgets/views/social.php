<ul class="social-icons list-unstyled">
    <?php if($facebook_href)
        echo '<li>'.CHtml::link('', $facebook_href, array('class' => 'fa fa-facebook')).'</li>';
    if($twitter_href)
        echo '<li>'.CHtml::link('', $twitter_href, array('class' => 'fa fa-twitter')).'</li>';
    if($pinterest_href)
        echo '<li>'.CHtml::link('', $pinterest_href, array('class' => 'fa fa-pinterest')).'</li>';
    if($googleplus_href)
        echo '<li>'.CHtml::link('', $googleplus_href, array('class' => 'fa fa-google-plus')).'</li>';
    if($youtube_href)
        echo '<li>'.CHtml::link('', $youtube_href, array('class' => 'fa-youtube')).'</li>'; ?>
</ul>