<li>
    <!-- mt product1 large start here -->
    <div class="mt-product1 large">
        <div class="box">
            <div class="b1">
                <div class="b2">
                    <?php $image = $data->contypeMainImage ? $data->contypeMainImage->path : ''; ?>
                    <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/view', array('id' => $data->id)); ?>">
                        <img src="<?php echo YHelper::getImagePath($image, 258, 258); ?>" alt="<?php echo $data->name; ?>">
                    </a>
                    <ul class="links">
                        <li><a href="#" id="add-product-to-cart" data-product-id="<?php echo $data->sku; ?>">
                                <i class="icon-handbag"></i>
                                <span><?php echo Yii::t("base", "To Cart"); ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="txt text-center">
            <div class="title-product">
                <strong class="title"><a href="<?php echo Yii::app()->createUrl('store/storeCatalog/view', array('id' => $data->id)); ?>"><?php echo $data->name; ?></a></strong>
            </div>
			<span class=" sku-title">Code <span class="orange"><?php echo $data->sku; ?> </span></span>
	        <?php if ($data->old_price) { ?>
				<span class="old-price">
					<span>
						<?php echo YHelper::formatCurrency($data->old_price); ?>
					</span>
				</span>
	        <?php } ?>
			<span class="price">
				<span>
					<?php echo YHelper::formatCurrency($data->getResultPrice()); ?>
				</span>
			</span>
        </div>
    </div><!-- mt product1 center end here -->
</li>