<?php
    //$this->pageTitle = 'DASLight';
    //$this->metaDescription = strip_tags($article->description);
    //$this->ogImage = $article->image;
    //$this->ogType = 'website';
    //$this->ogUrl = Yii::app()->request->requestUri;
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center section-title">
            <h3><?php echo Yii::t("MainModule.main", "Categorii produse"); ?></h3>
            <p class="subtitle"><?php echo YHelper::yiisetting('main_page_subtitle_'.Yii::app()->language); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <?php
            $categories_root = array();
             foreach($categories as $key => $category) {
                 if($category->isRoot()) {
                     $categories_root[] = $category;
                }
            }
            $categories = $categories_root;
            ?>
            <div class="banner-frame">
                <div class="col-xs-12 col-sm-4 col-md-4">
                <?php if (isset($categories[0])) { ?>
                <!-- banner-1 start here -->
                <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/category', array('id' => $categories[0]->id)); ?>" class="banner-1 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                    <div>
                        <img class="w100" alt="<?php echo $categories[0]->title; ?>" src="<?php echo YHelper::getImagePath($categories[0]->image, 385, 477); ?>">
                    </div>
                    <div class="holder">
                        <h2><?php echo $categories[0]->title; ?></h2>
                        <div class="txts">
                            <div class="btn-shop" >
                                <span><?php echo Yii::t("base", "More"); ?></span>
                                <i class="fa fa-angle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
                <!-- banner-1 end here -->
                <?php } ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                <!-- banner-box first start here -->
                <div  class="banner-box first">
                    <?php if (isset($categories[1])) { ?>
                    <!-- banner-2 start here -->
                    <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/category', array('id' => $categories[1]->id)); ?>" class="banner-2 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <img class="w100" alt="<?php echo $categories[1]->title; ?>" src="<?php echo YHelper::getImagePath($categories[1]->image, 386, 223); ?>">
                        <div class="holder">
                            <h2><?php echo $categories[1]->title; ?></h2>
                            <div class="txts">
                                <div class="btn-shop" >
                                    <span><?php echo Yii::t("base", "More"); ?></span>
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- banner-2 end here -->
                    <?php } ?>

                    <?php if (isset($categories[2])) { ?>
                    <!-- banner-3 start here -->
                    <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/category', array('id' => $categories[2]->id)); ?>" class="banner-3 right wow fadeInDown" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInDown;">
                        <img class="w100" alt="<?php echo $categories[2]->title; ?>" src="<?php echo YHelper::getImagePath($categories[2]->image, 386, 223); ?>">
                        <div class="holder">
                            <h2><?php echo $categories[2]->title; ?></h2>
                            <div class="txts">
                                <div class="btn-shop" >
                                    <span><?php echo Yii::t("base", "More"); ?></span>
                                    <i class="fa fa-angle-right"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                    <!-- banner-3 end here -->
                </div>
                <!-- banner-box first end here -->
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                <?php if (isset($categories[3])) { ?>
                <!-- banner-4 start here -->
                <a href="<?php echo $categories[3]->getUrl(); ?>" class="banner-4 wow fadeInRight w100" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                    <img class="w100" alt="<?php echo $categories[3]->title; ?>" src="<?php echo YHelper::getImagePath($categories[3]->image, 385, 477); ?>">
                    <div class="holder">
                        <h2><?php echo $categories[3]->title; ?></h2>
                        <div class="txts">
                            <div class="btn-shop" >
                                <span><?php echo Yii::t("base", "More"); ?></span>
                                <i class="fa fa-angle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
                <!-- banner-4 end here -->
                <?php } ?>
                </div>
            </div>
            <?php $this->widget('store.widgets.RandomProducts'); ?>
        </div>
    </div>
</div>

<?php $this->widget('blog.widgets.LastNews'); ?>

<?php $custom_text = YHelper::yiisetting('custom_text_'.Yii::app()->language); ?>
<?php if(Url::isFrontPage() && $custom_text){ ?>
    <div class="mt-smallproducts wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
        <div class="container ">
            <div class="row">
                <div class="col-xs-12 home-text">
                    <?php echo $custom_text; ?>
					<div class="home-text-inner"></div>
                </div>
            </div>
	        <?php if(Url::isFrontPage()){ ?>
			<div class="row">
				<div class="col-xs-12 text-center article-announce home-bottom-block pt-4em">
					<a class="btn-shop home-read" href="#" style="font-size: 15px;">
						<span><?php echo Yii::t("base", "More"); ?></span>
					</a>
				</div>
			</div>

	        <?php } ?>
            <!-- Popup Holder of the Page -->
            <div class="popup-holder">
                <!-- Popup of the Page -->
                <div id="popup3" class="lightbox">
                    <!-- Mt Newsletter Popup of the Page -->
                    <section class="mt-newsletter-popup">
                        <div class="holder">
                            <div class="txt-holder">
                                <h1>JOIN OUR NEWSLETTER</h1>
                                <span class="txt">Subscribe now to get <b>15%</b> off on any product!</span>
                                <!-- Newsletter Form of the Page -->
                                <form action="#" class="newsletter-form">
                                    <fieldset>
                                        <input type="email" class="form-control" placeholder="Enter your e-mail">
                                        <button type="submit">SUBSCRIBE</button>
                                    </fieldset>
                                </form><!-- Newsletter Form of the Page -->
                            </div>
                            <div class="img-holder">
                                <img class="img-responsive" src="<?php echo Yii::app()->getBaseUrl(false); ?>/static/images/light-stripe.png" alt="image description">
                            </div>
                        </div><!-- Popup Form of the Page -->
                        <form action="#" class="popup-form">
                            <fieldset>
                                <input type="checkbox" class="form-control">Don’t show this popup again
                            </fieldset>
                        </form><!-- Popup Form of the Page end -->
                    </section><!-- Mt Newsletter Popup of the Page -->
                </div><!-- Popup of the Page end -->
            </div><!-- Popup Holder of the Page end -->
        </div>
    </div>
<?php } ?>
