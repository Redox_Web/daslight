
<?php foreach ($model as $category) { ?>
    <?php if ($category->isRoot()) { ?>
        <div class="col-sm-12 col-xs-12 txt-wrap">
            <div class="pl-0_5em">
                <h2><?php echo $category->title; ?></h2>
            </div>

        <?php $children = $category->children()->findAll(); ?>
        <?php if($children) { ?>
		<div class="partners">
            <table class="table">
                <tbody>
                    <?php foreach($children as $child) { ?>
                        <tr>
                            <td class="col-xs-5 col-sm-5"><?php echo $child->title; ?>:</td>
                            <td><?php echo $child->info; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
		</div>
        <?php } ?>
        </div>
    <?php } ?>
<?php } ?>