<div class="row">
    <section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s">
        <div class=""></div>
        <div class="promo-image" style="background-image: url('<?php echo YHelper::getImagePath($category->title_image,0,0, Yii::app()->params['header_image']); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                        <?php $arr_url_params = explode("/", Yii::app()->request->requestUri); ?>
                        <?php $page_param = end($arr_url_params); ?>
                        <?php $page_param = filter_var($page_param, FILTER_VALIDATE_INT) !== false ? (int) $page_param : 1; ?>
                        <?php if($category != 'all') {
                                if ($category->isRoot()) { ?>
                                <h1 class="c-light-blue f-bold">
                                    <?php echo $category->{"meta_header_".Yii::app()->language} ?: $category->title; ?>
                                </h1>
                            <?php } else { ?>
                                <h1 class="no-transform c-light-blue f-bold">
                                    <?php echo $category->{"meta_header_".Yii::app()->language} ?: $category->title; ?>
                                </h1>
                            <?php }
                            } else { ?>
                            <h1><?php echo Yii::t("base", "All products");?></h1>
                        <?php } ?>

                        <!-- Breadcrumbs of the Page -->
                        <?php $this->widget('Breadcrumbs', array(
                            'links'=>$category->getBreabCrumbs()
                        )); ?>
                        <!-- Breadcrumbs of the Page end -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    $description = "";
    if ($page_param == 1) { ?>
        <?php $description =  $category->description; ?>
    <?php } ?>

    <div class="container pt-3em pb-3em">
        <div class="row">
            <?php $this->widget('main.widgets.LeftMenu'); ?>

            <div class="col-xs-12 col-sm-8 col-md-9 wow fadeInRight" data-wow-delay="0.4s">
                <ul class="mt-productlisthold list-inline">
                    <?php foreach($categories as $key => $category) { ?>
                        <li>
                            <!-- mt product1 large start here -->
                            <div class="mt-product1 large">
                                <div class="box">
                                    <div class="b1">
                                        <div class="b2">
                                            <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/category', array('id' => $category->id));?>">
                                                <img src="<?php echo YHelper::getImagePath($category->image, 258, 258); ?>" alt="<?php echo $category->title; ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="txt">
                                    <strong class="title">
                                        <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/category', array('id' => $category->id));?>"><?php echo $category->title; ?>
                                        </a>
                                    </strong>
                                </div>
                            </div><!-- mt product1 center end here -->
                        </li>
                    <?php } ?>
                </ul>

                <?php if ($page_param==1 && !empty($description)) { ?>
                    <div class="pb-2em home-text">
                        <?php echo $description; ?>
						<div class="home-text-inner"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center article-announce home-bottom-block pt-4em">
                            <a class="btn-shop home-read" href="#" style="font-size: 15px;">
                                <span><?php echo Yii::t("base", "More"); ?></span>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
