<?php if ($categories[0]) { ?>
<div class="footer-widget-column col-xs-12 col-sm-6 col-md-4">
    <aside>
        <h3 class="f-widget-heading"><?php echo Yii::t("base", "Our goods");?></h3>
        <div class="menu-categories-container">
            <div class="menu child-pb-0_5em">
                <?php foreach($categories[0] as $key => $category) { ?>
                    <div id="menu-item-618" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-618">
                        <a href="<?php echo $category->url; ?>">
                            <?php echo $category->getMenuTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </aside>
</div>
<?php } ?>

<?php if ($categories[1]) { ?>
<div class="footer-widget-column col-xs-12 col-sm-6 col-md-4">
    <aside>
        <h3 class="f-widget-heading">&nbsp;</h3>
        <div class="menu-categories-container">
            <div class="menu child-pb-0_5em">
                <?php foreach($categories[1] as $key => $category) { ?>
                    <div id="menu-item-618" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-618">
                        <a href="<?php echo $category->url; ?>">
                            <?php echo $category->getMenuTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </aside>
</div>
<?php } ?>

<div class="footer-widget-column col-xs-12 col-sm-6 col-md-4">
    <aside>
        <h3 class="f-widget-heading"><?php echo Yii::t("base", "Helpful information");?></h3>
        <div class="menu-categories-container">
            <div class="menu child-pb-0_5em">
                <div id="menu-item-618" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-618">
                    <a href="<?php echo Yii::app()->createUrl('/main/default/partners'); ?>">
                        <?php echo Yii::t("base", "For Partners");?>
                    </a>
                </div>
                <div id="menu-item-619" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-619">
                    <a href="<?php echo Yii::app()->createUrl('/main/default/calculator'); ?>">
                        <?php echo Yii::t("base", "Energy efficiency");?>
                    </a>
                </div>
                <?php if ($static = $staticPages[3]) { ?>
                <div id="menu-item-620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-620">
                    <a href="<?php echo $static->getUrl(); ?>">
                        <?php echo $static->getFooterTitle(); ?>
                    </a>
                </div>
                <?php } ?>
                <?php if ($static = $staticPages[5]) { ?>
                    <div id="menu-item-620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-620">
                        <a href="<?php echo $static->getUrl(); ?>">
                            <?php echo $static->getFooterTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
                <?php if ($static = $staticPages[8]) { ?>
                    <div id="menu-item-620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-620">
                        <a href="<?php echo $static->getUrl(); ?>">
                            <?php echo $static->getFooterTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
                <?php if ($static = $staticPages[4]) { ?>
                    <div id="menu-item-620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-620">
                        <a href="<?php echo $static->getUrl(); ?>">
                            <?php echo $static->getFooterTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
                <?php if ($static = $staticPages[6]) { ?>
                    <div id="menu-item-620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-620">
                        <a href="<?php echo $static->getUrl(); ?>">
                            <?php echo $static->getFooterTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
                <?php if ($static = $staticPages[7]) { ?>
                    <div id="menu-item-620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-620">
                        <a href="<?php echo Yii::app()->createUrl('main/default/priceCount'); ?>">
                            <?php echo $static->getFooterTitle(); ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </aside>
</div>


