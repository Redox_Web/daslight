<li class="drop">
    <a href="#"><?php echo Yii::t("base", "Products");?>
        <i class="fa fa-angle-down" aria-hidden="true"></i>
    </a>
    <!-- mt dropmenu start here -->
    <div class="mt-dropmenu text-left">
        <div class="mt-dropmenu-inner">
            <!-- mt frame start here -->
            <div class="mt-frame">
                <!-- mt f box start here -->
                <div class="mt-f-box">
                    <?php foreach ($categories as $key => $category) { ?>
                        <?php if ($category->isRoot()) { ?>
                            <?php /*if ($this->emptyCheck($category)) { */?>
                                <?php
                                $titleArr = explode('/', $category->title);
                                $title = implode('<br>/', $titleArr);
                                ?>
                                <!-- mt col4 start here -->
                                <div class="mt-col-4">
                                    <div class="sub-dropcont">
                                        <strong class="title">
                                            <a href="<?php echo $category->url; ?>" class="mt-subopener">
                                                <?php echo $title; ?>
                                            </a>
                                        </strong>
                                        <div class="sub-drop">
                                            <ul>
                                            <?php $subCategory = $category->children()->active()->findAll(); ?>
                                            <?php if ($subCategory) { // && $category->id != 21 ?>
                                                <?php foreach ($subCategory as $key => $child) { ?>
                                                    <?php /*if ($this->emptyCheck($child)) { */?>
                                                    <li>
                                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        <a href="<?php echo $child->url; ?>"><?php echo $child->title; ?>
                                                        </a>
                                                    </li>
                                                    <?php /*} */?>
                                                <?php } ?>
                                            <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php /*} */?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <!-- mt f box end here -->
            </div>
        </div>
        <!-- mt frame end here -->
    </div>
    <!-- mt dropmenu end here -->
    <!--<span class="mt-mdropover"></span>-->
</li>