<header class="mt-shoplist-header">
    <!-- btn-box start here -->
    <div class="btn-box">
        <ul class="list-inline">
            <li>
                <a href="#" class="drop-link">
                    <?php echo Yii::t("MainModule.main", "Default Sorting"); ?>
                    <i aria-hidden="true" class="fa fa-angle-down"></i>
                </a>
                <div class="drop">
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo Yii::app()->createUrl(); ?>" class="current-sort">
                                Price<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
                            </a>
                        </li>

                        <li><a href="#" class="current-sort"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i></a></li>
                        <li><a href="#">Price</a></li>
                        <li><a href="#">Relevance</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div><!-- btn-box end here -->
    <!-- mt-textbox start here -->
    <div class="mt-textbox">
        <p>Showing  <strong>1–9</strong> of  <strong>65</strong> results</p>
        <p>View   <a href="#">9</a> / <a href="#">18</a> / <a href="#">27</a> / <a href="#">All</a></p>
    </div><!-- mt-textbox end here -->
</header><!-- mt shoplist header end here -->