<!-- sidebar of the Page start here -->
<aside id="sidebar" class="col-xs-12 col-sm-4 col-md-3 wow fadeInLeft" data-wow-delay="0.4s">
    <!-- shop-widget of the Page start here -->
    <section class="shop-widget">
        <h2 class="c-light-blue"><?php echo Yii::t("base", "Find It Fast"); ?></h2>
        <!-- category list start here -->
        <ul class="list-unstyled category-list">
            <?php $categories_main = StoreCategory::model()->active()->findAll(array('order'=>'priority,root,lft,id')); ?>
            <?php foreach($categories_main as $key => $category) { ?>
                <?php
                    if($category->isRoot()) {
                        if ($category->url_static_page && !empty($staticPages[$category->url_static_page])) {
                            $static = $staticPages[$category->url_static_page];
                            ?>
                            <li>
                                <a href="<?php echo $static->getUrl(); ?>">
                                    <div class="flex space-between items-center nowrap">
                                        <span class="name">
                                            <?php echo $static->title; ?>
                                        </span>
                                        <span class="num"><?php echo $category->getProductsQuantity(); ?></span>
                                    </div>
                                </a>
                            </li>
                            <?php
                        } else {
                    ?>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('store/storeCatalog/category', array('id' => $category->id)); ?>">
                                <div class="flex space-between items-center nowrap">
                                    <span class="name"><?php echo $category->title; ?></span>
                                    <span class="num"><?php echo $category->getProductsQuantity(); ?></span>
                                </div>
                            </a>
                        </li>
                    <?php
                            }
                    ?>
                <?php } ?>
            <?php } ?>
        </ul><!-- category list end here -->
    </section><!-- shop-widget of the Page end here -->
</aside><!-- sidebar of the Page end here -->