<?php
class FooterCategories extends CWidget
{
    public $view = 'footer-categories';

    public function run()
    {
        $categories = StoreCategory::model()->active()->findAll(
            array(
                'order'=>'priority,root,lft,id',
                'condition' => 'root <> id AND root <> 61 AND level = 2',
                'limit' => 16
            )
        );

        $exclusive = StoreCategory::model()->find(
            array(
                'condition' => 'id = 61'
            )
        );

        $staticPages = MainPages::model()->findAll();
        if ($staticPages) {
            foreach ($staticPages as $staticPage) {
                $staticPages[$staticPage->id] = $staticPage;
            }
        }

        if ($categories || $staticPages) {
            $categories = array_filter($categories, array(new StoreCategory(), "checkActiveCategory"));
            $categories = array_chunk($categories, 8);
            $this->render(
                $this->view,
                array('categories' => $categories, 'staticPages' => $staticPages, 'exclusive' => $exclusive)
            );
        }
    }

    public function emptyCheck($category)
    {
        $descendants = $category->descendants()->findAll();
        $ids = CHtml::listData($descendants, 'id', 'id');
        $ids = array_values($ids);
        array_push($ids, $category->id);

        $criteria= new CDbCriteria;
        $criteria->addInCondition('category_id', $ids);
        return StoreProduct::model()->count($criteria) > 0;
    }
}