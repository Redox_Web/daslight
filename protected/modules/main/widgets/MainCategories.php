<?php
class MainCategories extends CWidget
{
    public $view = 'main-categories';
    public $parmArr = array();
    public $category = null;
    public $dataProvider = null;

    public function run() {
        if (!$this->category) {
            $categories = StoreCategory::model()->active()->findAll(array('order'=>'priority,root,lft,id'));
            $this->parmArr = array('categories' => $categories);
        } elseif ($this->category == 'all') {
            $this->view = 'products';
            $this->parmArr = array('category' => $this->category, 'dataProvider' => $this->dataProvider);
        } elseif ($this->category->children()->active()->findAll()) {
            $this->view = 'main-subcategory';
            $categories = $this->category->children()->active()->findAll();
            $this->parmArr = array('category' => $this->category, 'categories' => $categories);
        } else {
            $this->view = 'products';
            $this->parmArr = array('category' => $this->category, 'dataProvider' => $this->dataProvider);
        }

        $this->render($this->view, $this->parmArr);
    }
}