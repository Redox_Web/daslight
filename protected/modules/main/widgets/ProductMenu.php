<?php

class ProductMenu extends CWidget
{
    public $view = 'product-menu';

    public function run()
    {
        $categories = StoreCategory::model()->active()->findAll(
            array(
                'condition' => 'id <> 61',
                'order'=>'priority,root,lft,id'
            )
        );
        if($categories)
            $this->render($this->view, array('categories' => $categories));
    }

    public function emptyCheck($category)
    {
        $descendants = $category->descendants()->findAll();
        $ids = CHtml::listData($descendants, 'id', 'id');
        $ids = array_values($ids);
        array_push($ids, $category->id);

        $criteria= new CDbCriteria;
        $criteria->addInCondition('category_id', $ids);
        // check if category have products
        $not_empty = (boolean) (StoreProduct::model()->count($criteria) > 0);
        $stroreCategories = StoreCategory::model()->active()->findAllByPk($ids);
        
        if(!$not_empty){
            // check category if have some url for static page
            foreach($stroreCategories as $one){
                if(!empty($one->url_static_page)){
                    $not_empty = true;
                    break;
                }  
            }
        }
        return $not_empty;
    }
}