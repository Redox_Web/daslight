<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
    'Checkbox' => 'Чекбокс',
    'Dropdown list' => 'Выпадающий список',
    'Feedback' => 'Отзыв',
    'Main content' => 'Основной контент',
    'Number' => 'Количество',
    'Search' => 'Поиск',
    'Short text (up to 250 characters)' => 'Короткий текст (до 250 символов)',
    'Static Pages' => 'Статичные страницы',
    'Text' => 'Текст',
    'Send' => 'Отправить',
    'Contact form' => 'Контактная форма',
    'Name' => 'Название',
    'Contact Name' => 'Имя',
    'Email' => 'Email',
    'Message' => 'Сообщение',
    'Categorii produse' => 'Категории продуктов',
    'Articole utile' => 'Полезные статьи',
    'Articole care le va ajuta cu alegerea produselor' => 'МАТЕРИАЛЫ КОТОРЫЕ ПОМОГУТ ВАМ С ВЫБОРОМ ТОВАРОВ',
    'Price' => 'Цена',
    'View' => 'Элементов на странице',
    'All' => 'Все',
    '[p]Showing [strong]{start}–{end}[/strong] of [strong]{count}[/strong] results[/p]' =>
        '[p]Элементы [strong]{start}–{end}[/strong] из [strong]{count}[/strong][/p]',
    'Default Sorting' => 'Сортировка',
);
