<?php
	$this->breadcrumbs=array(
        'Main Feedbacks'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage MainFeedback','url'=>array('admin')),
    );
    $this->title = "Create MainFeedback";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>