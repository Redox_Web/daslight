<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'main-feedback-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'name',array('class'=>'form-control','maxlength'=>80)); ?>

	<?php echo $form->textFieldGroup($model,'email',array('class'=>'form-control','maxlength'=>255)); ?>

	<?php echo $form->textAreaGroup($model,'feedback',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>


    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>
