<?php
	$this->breadcrumbs=array(
        'Main Feedbacks'=>array('admin'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create MainFeedback','url'=>array('create')),
	    array('label'=>'View MainFeedback','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage MainFeedback','url'=>array('admin')),
	);
    $this->title = "Update MainFeedback #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>