<?php
	$this->breadcrumbs=array(
        'Main Feedbacks'=>array('admin'),
        $model->name,
    );

    $this->menu=array(
        array('label'=>'Create MainFeedback','url'=>array('create')),
        array('label'=>'Update MainFeedback','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete MainFeedback','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage MainFeedback','url'=>array('admin')),
    );
    $this->title = "View MainFeedback # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
		'email',
		array(
			'name' => 'feedback',
			'type' => 'raw',
			'value' => function($data) {
				return nl2br(strip_tags($data->feedback));
			}
		),
),
)); ?>
