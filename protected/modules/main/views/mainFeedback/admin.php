<?php
	$this->breadcrumbs=array(
        'Main Feedbacks'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create MainFeedback','url'=>array('create')),
    );
    $this->title = "Manage Main Feedbacks";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'main-feedback-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'name',
		'email',
		'feedback',
        array(
            'class' => 'backend.components.ButtonColumn',
            'template' => '{view} {delete}',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
