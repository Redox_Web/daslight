<?php
	$this->breadcrumbs=array(
        'Work Types'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage WorkType','url'=>array('admin')),
    );
    $this->title = "Create WorkType";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>