<?php
	$this->breadcrumbs=array(
        'Work Types'=>array('admin'),
        $model->name,
    );

    $this->menu=array(
        array('label'=>'Create WorkType','url'=>array('create')),
        array('label'=>'Update WorkType','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete WorkType','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage WorkType','url'=>array('admin')),
    );
    $this->title = "View WorkType # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name_ru',
        'name_ro',
),
)); ?>
