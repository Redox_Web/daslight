<?php
	$this->breadcrumbs=array(
        'Work Types'=>array('admin'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create WorkType','url'=>array('create')),
	    array('label'=>'View WorkType','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage WorkType','url'=>array('admin')),
	);
    $this->title = "Update WorkType #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>