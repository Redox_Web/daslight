<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'work-type-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

	<?php $this->widget('booster.widgets.TbTabsLang', array(
		'type'=>'pills',
		'labelModel' => $model,
		'labelAttribute' => 'name_ru',
		'htmlOptions'=>array('class'=>'form-group'),
		'tabs'=>array(
			array('label'=>'RU', 'content'=>$form->textField($model,'name_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('name_ru'))),
			array('label'=>'RO', 'content'=>$form->textField($model,'name_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('name_ro'))),
		),
	));?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>
