<?php
	$this->breadcrumbs=array(
        'Work Types'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create WorkType','url'=>array('create')),
    );
    $this->title = "Manage Work Types";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'work-type-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'name_ru',
        'name_ro',
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
