<?php
	$this->breadcrumbs=array(
        'Action Popups'=>array('admin'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'Update ActionPopup','url'=>array('update','id'=>$model->id)),
        array('label'=>'Manage ActionPopup','url'=>array('admin')),
    );
    $this->title = "View ActionPopup # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
    'id',
    'content_ru',
    'content_ro',
    'is_active:boolean',
),
)); ?>
