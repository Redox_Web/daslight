<?php
	$this->breadcrumbs=array(
        'Action Popups'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
    );
    $this->title = "Manage Action Popups";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'action-popup-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'content_ru',
		'content_ro',
        array(
            'template' => '{update}',
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
