<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'action-popup-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

	<?php $this->widget('booster.widgets.TbTabsLang', array(
		'type'=>'pills',
		'labelModel' => $model,
		'labelAttribute' => 'content_ru',
		'htmlOptions'=>array('class'=>'form-group'),
		'tabs'=>array(
			array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'content_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('content_ru'))),
			array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'content_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('content_ro'))),
		),
	)); ?>

	<?php echo $form->checkboxGroup($model, 'is_active',
		array('widgetOptions' =>
			array(
				'htmlOptions' => array(
					'checked'=>$model->isNewRecord ? true : $model->is_active
				),
			)
		)
	); ?>

	<div class="form-group">
		<span class="col-sm-3"></span>
		<div class="col-sm-6">
			<div class="checkbox checkbox-success">
                <?php echo CHtml::ajaxButton('Сброс кэша', '/main/actionPopup/clearCache',
                    array(
                        'type'=>'POST',
                        'dataType'=>'json',
                        'success'=>'function(data){
                            if (data.result) {
                                $.pnotify({title: \'Alert!\',text: \'Кэш успешно сброшен\',type: \'success\'});
                            } else {
                                $.pnotify({title: \'Alert!\',text: \'Ошибка при сохранении\',type: \'error\'});
                            }
                        }',

                        'async' => true,
                    ),
                    array(
                        'class' => 'btn btn-primary btn btn-magenta',
                    )
                ); ?>
			</div>
		</div>
	</div>

	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'formSubmit',
		'htmlOptions' => array('class' => 'btn btn-primary'),
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>

<?php $this->endWidget(); ?>
