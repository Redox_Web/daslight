<?php
	$this->breadcrumbs=array(
        'Action Popups'=>array('admin'),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Manage ActionPopup','url'=>array('admin')),
	);
    $this->title = "Update ActionPopup #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>