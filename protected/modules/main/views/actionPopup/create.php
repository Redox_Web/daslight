<?php
	$this->breadcrumbs=array(
        'Action Popups'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage ActionPopup','url'=>array('admin')),
    );
    $this->title = "Create ActionPopup";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>