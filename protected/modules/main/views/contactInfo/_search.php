<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'page_id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'level',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'title',array('class'=>'form-control','maxlength'=>150)); ?>

		<?php echo $form->textFieldGroup($model,'url',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'active',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'root',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'lft',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'rgt',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'priority',array('class'=>'form-control')); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
