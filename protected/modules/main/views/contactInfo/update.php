<?php
	$this->breadcrumbs=array(
        'Main Links'=>array('admin'),
        $model->title=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create MainLinks','url'=>array('create')),
	    array('label'=>'View MainLinks','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage MainLinks','url'=>array('admin')),
	);
    $this->title = "Update MainLinks #".$model->id;
    $info = !empty($info) ? : false;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model, 'info' => $info)); ?>