<?php
	$this->breadcrumbs=array(
        'Main Links'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage MainLinks','url'=>array('admin')),
    );
    $this->title = "Create MainLinks";

    if(!isset($link)) $link = false;
    $info = !empty($info) ? : false;
?>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'info' => $info)); ?>