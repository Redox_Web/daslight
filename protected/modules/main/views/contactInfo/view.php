<?php
	$this->breadcrumbs=array(
        'Main Links'=>array('admin'),
        $model->title,
    );

    $this->menu=array(
        array('label'=>'Create MainLinks','url'=>array('create')),
        array('label'=>'Update MainLinks','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete MainLinks','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage MainLinks','url'=>array('admin')),
    );
    $this->title = "View MainLinks # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		'url',
),
)); ?>
