<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'main-links-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border'),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'title_ru',array('class'=>'form-control','maxlength'=>150)), 'active'=>($model->hasErrors('name_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'title_ro',array('class'=>'form-control','maxlength'=>150)), 'active'=>($model->hasErrors('name_ro'))),
        ),
    ));?>

    <?php if(!empty($info))
        $this->widget('booster.widgets.TbTabsLang', array(
            'type'=>'pills',
            'labelModel' => $model,
            'labelAttribute' => 'info_ru',
            'htmlOptions'=>array('class'=>'form-group'),
            'tabs'=>array(
                array('label'=>'RU', 'content'=>$form->textArea($model,'info_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('name_ru'))),
                array('label'=>'RO', 'content'=>$form->textArea($model,'info_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('name_ro'))),
            ),
        ));
    ?>

	<?php /*echo $form->textFieldGroup($model,'active',array('class'=>'form-control')); */?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>
