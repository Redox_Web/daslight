<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class="promo-image" style="background-image: url('/static/images/contacts.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm ">
                <h1 class="cart-page">
                    <?php echo Yii::t("base", "Contacts"); ?>
                </h1>
                <?php $this->widget('Breadcrumbs', array(
                    'links'=>array(
                        Yii::t("base", "Contacts")
                    ),
                )); ?>
            </div>
        </div>
    </div>
    </div>
</section>

<div class="mt-bestseller text-center wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 page-text text-left">
                <p>
                <div style="height: 450px; width: 100%;">
                    <?php Yii::app()->GM->f()?>
                </div>
                <!--<iframe width="100%" height="450" frameborder="0" allowfullscreen="" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2721.3939169088208!2d28.844259350146462!3d46.99323783803861!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c97eb010b8eef7%3A0x2932f2ac1d16e9bf!2sBulevardul+Dacia+2%2C+Chi%C5%9Fin%C4%83u%2C+Moldova!5e0!3m2!1sen!2s!4v1447418081651" class="mb30 mt10"></iframe>-->
                <div class="row">
                    <br>
                    <?php $this->widget('ContactinfoWidget'); ?>
                    <div class="col-lg-12 pb10">
                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'feedback-form',
                            'enableAjaxValidation' => false,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => false,
                                'inputContainer' => 'fieldset',
                            ),
                        )); ?>
                        <h5><?php echo Yii::t("MainModule.main", "Contact form");?></h5>
                        <div class="pb10">
                            <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => Yii::t("MainModule.main", "Name"))); ?>
                            <?php echo $form->error($model, 'name'); ?>
                        </div>
                        <div class="pb10">
                            <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => Yii::t("MainModule.main", "Email"))); ?>
                            <?php echo $form->error($model, 'email'); ?>
                        </div>
                        <div class="pb10">
                            <?php echo $form->textArea($model, 'feedback', array('class' => 'form-control', 'rows' => 8, 'placeholder' => Yii::t("MainModule.main", "Message"))); ?>
                            <?php echo $form->error($model, 'feedback'); ?>
                        </div>
                        <div class="pb10">
                            <?php $this->widget('application.extensions.recaptcha.EReCaptcha',
                                array('model'=>$model, 'attribute'=>'validacion',
                                    'theme'=>'red', 'language'=>'en_EN',
                                    'publicKey'=>'6LfVThcTAAAAAAzyzRI3-0SZ_L2g4zw1K6t5Gok3')) ?>
                            <?php echo CHtml::error($model, 'validacion'); ?>
                        </div>
                        <div class="text-right">
                            <button class="button-inline button-green" type="submit"><?php echo Yii::t("MainModule.main", "Send");?></button>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                </p>
            </div>
        </div>
    </div>
</div>