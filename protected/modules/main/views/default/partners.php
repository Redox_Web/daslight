<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s">
    <div class="promo-image" style="background-image: url('/static/images/contacts.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                <h1 class="cart-page">
                    <?php echo YHelper::yiisetting('partners_title_'.Yii::app()->language); ?>
                </h1>
                <?php $this->widget('Breadcrumbs', array(
                    'links'=>array(
                        YHelper::yiisetting('partners_title_'.Yii::app()->language)
                    ),
                )); ?>
            </div>
        </div>
    </div>
    </div>
</section>

<div class="pt-5em pb-5em text-center wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
    <div class="container">
        <?php echo YHelper::yiisetting('partners_'.Yii::app()->language); ?>
    </div>
</div>
