<?php
//    if (Yii::app()->language == 'ro'){
//        $this->pageTitle = 'DASLight - iluminarea LED Nr1 în Moldova';
//        $this->metaDescription = 'DASLight oferă o gamă largă de becuri LED, benzi cu LED-uri, profile de aluminiu, precum și servicii de instalare.';
//    } else {
//        $this->pageTitle = 'DASLight - LED освещение Nr1 в Молдове';
//        $this->metaDescription = 'Компания DASLight предлагает богатый ассортимент LED ламп, светодиодных лент, аллюминевых профилей, а также услуги по их установке.';
//    }
    $this->pageTitle = YHelper::yiisetting('meta_title_'.Yii::app()->language);
    $this->metaKeywords = YHelper::yiisetting('meta_keywords_'.Yii::app()->language);
    $this->metaDescription = YHelper::yiisetting('meta_description_'.Yii::app()->language);
    $this->ogImage = 'static/images/daslogo.png';
    $this->ogUrl = Yii::app()->request->requestUri;
?>

<?php $this->widget('MainCategories'); ?>