<li>
    <a class="" href="<?php echo Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)); ?><?php //echo Yii::app()->homeUrl; ?>">
        <?php echo Yii::t("base", "Main");?>
    </a>
</li>
<!-- Start Product menu dropdown -->
<?php $this->widget('main.widgets.ProductMenu'); ?>
<!-- End Product menu dropdown -->
<li>
    <a href="<?php echo $this->createUrl('/main/default/priceCount'); ?>">
        <?php echo Yii::t("base", "Shares");?>
    </a>
</li>
<li>
    <a href="<?php echo $this->createUrl('/blog/frontBlog/category', array('id' => 1)); ?>">
        <?php echo Yii::t("base", "About light-emitting diodes");?>
    </a>
</li>
<li>
    <a href="<?php echo $this->createUrl('/main/default/page', array('id' => 2)); ?>">
        <?php echo Yii::t("base", "About company");?>
    </a>
</li>
<li>
    <a href="<?php echo $this->createUrl('/blog/frontBlog/category', array('id' => 7)); ?>">
        <?php echo Yii::t("base", "Our projects");?>
    </a>
</li>
<li>
    <a href="<?php echo $this->createUrl('/main/default/feedback'); ?>"><?php echo Yii::t("base", "Contacts");?></a>
</li>