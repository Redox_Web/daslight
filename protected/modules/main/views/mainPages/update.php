<?php
	$this->breadcrumbs=array(
        'Main Pages'=>array('admin'),
        $model->title=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create MainPages','url'=>array('create')),
	    array('label'=>'View MainPages','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage MainPages','url'=>array('admin')),
	);
    $this->title = "Update MainPages #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>