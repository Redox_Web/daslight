<?php
	$this->breadcrumbs=array(
        'Main Pages'=>array('admin'),
        $model->title,
    );

    $this->menu=array(
        array('label'=>'Create MainPages','url'=>array('create')),
        array('label'=>'Update MainPages','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete MainPages','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage MainPages','url'=>array('admin')),
    );
    $this->title = "View MainPages # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		'content',
		'slug',
),
)); ?>
