<?php
	$this->breadcrumbs=array(
        'Main Pages'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage MainPages','url'=>array('admin')),
    );
    $this->title = "Create MainPages";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>