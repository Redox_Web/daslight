<?php
	$this->breadcrumbs=array(
        'Main Pages'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create MainPages','url'=>array('create')),
    );
    $this->title = "Manage Main Pages";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'main-pages-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'title_ru',
		'slug',
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
