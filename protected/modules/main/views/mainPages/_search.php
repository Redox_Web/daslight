<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'title',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textAreaGroup($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'slug',array('class'=>'form-control','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
