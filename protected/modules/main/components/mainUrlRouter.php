<?php

class mainUrlRouter extends CBaseUrlRule
{
    private static $instance;
    private $matches;
    public $transArray = array(
        'partners' => array('language' => array('ro', 'ru'), 'link' => 'main/default/partners'),
        'contact' => array('language' => array('ro', 'ru'), 'link' => 'main/default/feedback'),
        'calculator' => array('language' => array('ro', 'ru'), 'link' => 'main/default/calculator'),
        'calculation' => array('language' => array('ro', 'ru'), 'link' => 'main/default/priceCount'),
        'cart' => array('language' => array('ro', 'ru'), 'link' => 'store/storeCart/index'),
        'checkout' => array('language' => array('ro', 'ru'), 'link' => 'store/storeCart/Checkout'),
        'addtocart' => array('language' => array('ro', 'ru'), 'link' => 'store/storeCart/add'),
        'updatecart' => array('language' => array('ro', 'ru'), 'link' => 'store/storeCart/update'),
        'deletecart' => array('language' => array('ro', 'ru'), 'link' => 'store/storeCart/delete'),
        'widgetcart' => array('language' => array('ro', 'ru'), 'link' => 'store/storeCart/widget'),
        'order' => array('language' => array('ro', 'ru'), 'link' => 'store/order/create'),
//        'search' => array('language' => array('ro', 'ru'), 'link' => 'store/frontSearch/searchResult'),
        '' => array('language' => array('ro', 'ru'), 'link' => 'main/default/index'),
    );

    public static function Instance() {
        if (self::$instance == null)
            self::$instance = new mainUrlRouter();

        return self::$instance;
    }

    public function urltrans($pathInfo)
    {
        $flag = $this->checkUrl($pathInfo);
        if($flag)
            return Yii::app()->createUrl($this->getUrl($flag));

        return false;  // не применяем данное правило
    }

    public function createUrl($manager,$route,$params,$ampersand)
    {
        if (isset($route))
            return $this->getUrl($route);

        return false;  // не применяем данное правило
    }

    public function parseUrl($manager,$request,$pathInfo,$rawPathInfo)
    {
        $flag = $this->checkUrl($pathInfo);

        if ($flag) {
            return $flag;
        }

        return false;  // не применяем данное правило
    }

    //========================help functions=====================

    private function checkUrl($pathInfo)
    {
        $flag = false;
        $pathInfo = preg_replace('~^'.Yii::app()->getBaseUrl(true).'/~', '', $pathInfo);

        if (preg_match('~^backend~', $pathInfo)) return $flag;

        $lang = implode('|', Yii::app()->languages->languages);
        $defaultLanguage = (isset(Yii::app()->user) && isset(Yii::app()->user->language)) ?
            Yii::app()->user->language : Yii::app()->languages->defaultLanguage;

        if (!preg_match('~('.$lang.')/~', $pathInfo) && !in_array($pathInfo, Yii::app()->languages->languages)) {
            $pathInfo = $defaultLanguage;
            if (!in_array($pathInfo, Yii::app()->languages->languages)) {
                $pathInfo = $defaultLanguage.'/'.$pathInfo;
            }
        }

        if (preg_match('~('.$lang.')?/([a-zA-Z0-9_-]+)?$~', $pathInfo, $this->matches)) {
            if (isset($this->matches[1])) {
                $_GET['language'] = $this->matches[1];
            } else {
                $_GET['language'] = $defaultLanguage;
            }

            if (isset($this->matches[2])) {
                if (@in_array($_GET['language'], $this->transArray[$this->matches[2]]['language'])) {
                    $flag = $this->transArray[$this->matches[2]]['link'];
                }
            } elseif (in_array($pathInfo, Yii::app()->languages->languages)) {
                if (@in_array($_GET['language'], $this->transArray[$this->matches[2]]['language'])) {
                    $flag = $this->transArray['']['link'];
                }
            }
        }

        if (!$flag) {
            $_GET['language'] = Yii::app()->language;
        }

        return $flag;
    }

    private function getUrl($route)
    {
        $result = $this->search('link', $route, Yii::app()->language);
        if(empty($result)) return false;

        return Yii::app()->language.'/'.$result;
    }

    public function search($key, $value, $language)
    {
        $result = '';
        if (is_array($this->transArray)) {
            foreach ($this->transArray as $subkey => $subarray) {
                if (isset($subarray[$key]) && $subarray[$key] == $value && in_array($language, $subarray['language'])) {
                    $result = $subkey;
                    break;
                }
            }
        }

        return $result;
    }
}
