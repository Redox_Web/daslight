<?php
Yii::import('main.models.MainPages');

class mainPageRouter extends CBaseUrlRule
{
    private static $instance;
    private $matches;
    private $mainpage = false;

    public static function Instance() {
        if (self::$instance == null)
            self::$instance = new mainPageRouter();

        return self::$instance;
    }

    public function createUrl($manager,$route,$params,$ampersand)
    {
        if(!empty($params)) {
            foreach($params as $key => $value)
                if(preg_match('~_page~', $key))
                    return false;
        }

        if ($route==='main/default/page') {
            if (isset($params['id']) && !isset($params['language']) && !isset($params['page'])) {
                return $this->getUrl($params['id']);
            }
        }
        return false;  // не применяем данное правило
    }

    public function urltrans($pathInfo)
    {
        $flag = $this->checkUrl($pathInfo);
        if($flag)
            return Yii::app()->createUrl($this->getUrl($_GET['id']));

        return false;  // не применяем данное правило
    }

    public function parseUrl($manager,$request,$pathInfo,$rawPathInfo)
    {
        $flag = $this->checkUrl($pathInfo);

        if ($flag) {
            return '/main/default/page';
        }

        return false;  // не применяем данное правило
    }

    //========================help functions=====================

    private function checkUrl($pathInfo)
    {
        $flag = false;

        $pathInfo = preg_replace('~^'.Yii::app()->getBaseUrl(true).'/~', '', $pathInfo);

        if(preg_match('~^backend~', $pathInfo))
            return $flag;

        $lang = implode('|', Yii::app()->languages->languages);
        if (!preg_match('~('.$lang.')/~', $pathInfo)) {
            $pathInfo = Yii::app()->languages->defaultLanguage.'/'.$pathInfo;
        }

        if(preg_match('~('.$lang.')?/([a-zA-Z0-9_-]+)?$~', $pathInfo, $this->matches)) {
            if(isset($this->matches[1]))
                $_GET['language'] = $this->matches[1];
            else
                $_GET['language'] = Yii::app()->languages->defaultLanguage;

            if(isset($this->matches[2])) {
                if($section = $this->dbExist('MainPages', $this->matches[2])) {
                    $flag = true;
                    $_GET['id'] = $section;
                } else
                    $flag = false;
            }
        }

        if(!$flag)
            $_GET['language'] = Yii::app()->language;

        return $flag;
    }

    private function dbExist($db, $slug) {
        $blog = $db::model()->find("slug = '$slug'");

        if ($blog)
            return $blog->id;
        else
            return false;
    }

    private function getUrl($id) {
        $blog = MainPages::model()->findByPk($id);

        if ($blog)
            return Yii::app()->language.'/'.$blog->slug;
        else
            return false;
    }
}