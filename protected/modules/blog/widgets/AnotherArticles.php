<?php
class AnotherArticles extends CWidget
{
    /**
     * @var string Id of elements
     */
    public $article = null;
    public $view = 'another_articles';

    public function run()
    {
        $articles = $this->article->category->articles(array('condition' => 'id <> :curid', 'params' => array(':curid' => $this->article->id)));
        if($articles)
            $this->render($this->view, array('articles' => $articles));
    }
}