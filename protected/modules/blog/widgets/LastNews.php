<?php
class LastNews extends CWidget
{
    /**
     * @var string Id of elements
     */
    public $view = 'last-news';

    public function run()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'category_id = 1';
        $criteria->order = "id Desc";
        $criteria->limit = 2;

        $articles = BlogArticle::model()->findAll($criteria);

        if($articles)
            $this->render($this->view, array('articles' => $articles));
    }
}