<div class="mt-bestseller text-center wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 mt-heading text-uppercase">
                <h1 class="heading"><?php echo Yii::t("MainModule.main", "Articole utile"); ?></h1>
                <p><?php echo Yii::t("MainModule.main", "Articole care le va ajuta cu alegerea produselor"); ?></p>
                <a class="btn-shop more-articles" href="<?php echo Yii::app()->createUrl('/blog/frontBlog/category', array('id' => 1)); ?>">
                    <span><?php echo Yii::t("base", "All articles");?></span>
                </a>
            </div>
        </div>
        <div class="row">
        <?php foreach($articles as $article) { ?>
            <div class="col-xs-12 col-md-6 article-announce">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <img src="<?php echo YHelper::getImagePath($article->image, 197, 204); ?>" alt="light">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7 text-left">
                        <h3><?php echo  $article->title; ?></h3>
                        <p>
                            <?php echo YText::characterLimiter($article->preview, 100); ?>
                        </p>
                        <a class="btn-shop" href="<?php echo $article->url; ?>">
                            <span><?php echo Yii::t("base", "More"); ?></span>
                        </a>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
