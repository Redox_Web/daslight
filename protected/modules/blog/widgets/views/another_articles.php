<div class="container pb-4em">
	<div class="posts-list wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;" >
		<h3 class="mb-2em"><?php echo Yii::t("base", "Another articles");?></h3>

		<div class="row tabs-sliderlg">
			<?php foreach($articles as $article) { ?>
				<div class="col-xs-12 col-sm-6 col-md-3 article-announce">
					<div class="row">
						<div class="col-xs-12 col-sm-12 mb-2em">
							<a href="<?php echo $article->url; ?>">
								<img src="<?php echo YHelper::getImagePath($article->image, 246, 162); ?>" alt="light">
							</a>
						</div>
						<div class="col-xs-12 col-sm-12 text-left">
							<h3><?php echo  $article->title; ?></h3>
							<p>
								<?php echo YText::characterLimiter($article->preview, 100); ?>
							</p>
							<a class="btn-shop" href="<?php echo $article->url; ?>">
								<span><?php echo Yii::t("base", "More"); ?></span>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>