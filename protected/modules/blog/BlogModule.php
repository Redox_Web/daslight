<?php
Yii::import('backend.components.WebModule');

class BlogModule extends WebModule
{
    public $_localAssetsUrl;

    public function init()
    {
        parent::init();

        $this->setImport(array(
            'blog.models.*',
            'blog.components.*',
            'blog.widgets.*',
        ));
    }

    public static function menuItem()
    {
        return array(
            'icon' => 'fa fa-envelope',
            'label' => Yii::t('BlogModule.blog', 'Blog'),
            'items' => array(
                array(
                    'label' => Yii::t('BlogModule.blog', 'Category'),
                    'url' => 'blog/blogCategory/admin',
                    'sidebar_tab' => 'blog-category',
                ),
                array(
                    'label' => Yii::t('BlogModule.blog', 'Articles'),
                    'url' => 'blog/blogArticle/admin',
                    'sidebar_tab' => 'blog-article',
                ),
            )
        );
    }

    public static function urlRules()
    {
        return array(
            array('class' => 'blog.components.blogArticleRouter'),
        );
    }
}