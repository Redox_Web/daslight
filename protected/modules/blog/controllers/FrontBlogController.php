<?php
class FrontBlogController extends Frontend
{
    public function actionView($id)
    {
        $article = BlogArticle::model()->findByPk($id);
        $this->pageTitle = $article->getMetaTitle();
        $this->metaDescription = $article->getMetaDescription();
        $this->metaKeywords = $article->getMetaKeyword();
        if($article)
            $this->render('article', array('article' => $article));
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    public function actionCategory($id)
    {
        $articleCategory = BlogCategory::model()->findByPk($id);
        $this->pageTitle = $articleCategory->getMetaTitle();
        $this->metaDescription = $articleCategory->getMetaDescription();
        $this->metaKeywords = $articleCategory->getMetaKeyword();
        
        $this->render('category', array('articleCategory' => $articleCategory));
    }

    public function actionSlidesCategory($id)
    {
        $articleCategory = BlogCategory::model()->findByPk($id);
        $this->render('slidesCategory', array('articleCategory' => $articleCategory));
    }

    public function actionViewSlide($id)
    {
        $article = BlogArticle::model()->findByPk($id);
        if($article)
            $this->render('articleSlide', array('article' => $article));
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }
}