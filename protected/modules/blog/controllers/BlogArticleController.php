<?php
Yii::import('backend.components.BackendController');
Yii::import('backend.components.multiapload.*');

class BlogArticleController extends BackendController
{
    public $sidebar_tab = "blog-article";

    public function actions()
    {
        $ret = array(
            'upload' => 'UploadAction',
            'gallery'=>'GalleryAction',
            'imagedel'=>'ImagedelAction',
            'mainimage'=>'MainimageAction',

            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update'=> array(
                'class' => 'DUpdateAction',
                'layout' => Yii::app()->getModule('backend')->getBackendLayoutAlias('layout_tabs'),
            ),
        );

        if(isset($_GET['id'])) {
            $article = BlogArticle::model()->findByPk($_GET['id']);
            if(isset($article) && $article->category_id != 7)
                $ret = CMap::mergeArray($ret, array('update' => 'DUpdateAction'));
        }

        return $ret;
    }

    public function createModel()
    {
        return new BlogArticle();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=BlogArticle::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blog-article-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}