<?php

/**
 * This is the model class for table "blog_article".
 *
 * The followings are the available columns in table 'blog_article':
 * @property integer $id
 * @property integer $category_id
 * @property string $title_ru
 * @property string $title_ro
 * @property string $slug_ru
 * @property string $slug_ro
 * @property string $preview_ru
 * @property string $preview_ro
 * @property string $description_ru
 * @property string $description_ro
 * @property string $image
 * @property string $created
 * @property string $updated
 * @property integer $author
 * @property integer $is_active
 */
class BlogArticle extends ActiveRecord
{
    protected $_url;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ro, title_ru', 'required'),
			array('category_id, author, is_active', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_ro, head_title_ru, head_title_ro, slug_ru, slug_ro, image', 'length', 'max'=>255),
			array('description_ru, description_ro, preview_ru, preview_ro, created, updated, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'safe'),
            array('image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'safe' => false),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, title_ru, title_ro, slug_ru, slug_ro, preview_ru, preview_ro, description_ru, description_ro, image, created, updated, author, is_active, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'safe', 'on'=>'search'),
            array('author', 'default', 'value' => Yii::app()->user->id,'setOnEmpty' => false, 'on' => 'insert, update'),
            array('updated', 'default', 'value' => date('Y-m-d H:i:s'),'setOnEmpty' => false, 'on' => 'update'),
            array('created, updated', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => false, 'on' => 'insert'),
		);
	}

    public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'is_active = 1',
            ),
        );
    }

    public function beforeSave()
    {
        $this->slug_ru = YText::translit($this->title_ru);
        $proverka = self::model()->findByAttributes(array('slug_ru' => $this->title_ru));
        if($proverka && $proverka->id != $this->id)
            $this->slug_ru .= '_'.$this->id;

        $this->slug_ro = YText::translit($this->title_ro);
        $proverka = self::model()->findByAttributes(array('slug_ro' => $this->title_ro));
        if($proverka && $proverka->id != $this->id)
            $this->slug_ro .= '_'.$this->id;

        return parent::beforeSave();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category' => array(self::BELONGS_TO, 'BlogCategory', 'category_id'),
            'contypeImagesList' => array(self::HAS_MANY, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type', 'order' => 'id DESC', 'params' => array(':type' => $this->getClass())),
            'contypeImagesList' => array(self::HAS_MANY, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type', 'order' => 'id DESC', 'params' => array(':type' => $this->getClass())),
            'contypeMainImage' => array(self::HAS_ONE, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type AND is_main = 1', 'params' => array(':type' => $this->getClass())),
            'contypeNotMainImages' => array(self::HAS_MANY, 'MultipleImages', 'item_id', 'condition' => 'content_type = :type AND is_main = 0', 'params' => array(':type' => $this->getClass())),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'title_ru' => 'Title Ru',
			'title_ro' => 'Title Ro',
			'slug_ru' => 'Slug Ru',
			'slug_ro' => 'Slug Ro',
			'preview_ru' => 'Preview Ru',
			'preview_ro' => 'Preview Ro',
			'description_ru' => 'Description Ru',
			'description_ro' => 'Description Ro',
			'image' => 'Image',
			'created' => 'Created',
			'updated' => 'Updated',
			'author' => 'Author',
			'is_active' => 'Is Active',
                        'meta_title_ru' => 'Meta Title',
                        'meta_title_ro' => 'Meta Title',
                        'meta_keywords_ru' => 'Meta Keywords',
                        'meta_keywords_ro' => 'Meta Keywords',
                        'meta_description_ru' => 'Meta Description',
                        'meta_description_ro' => 'Meta Description',
                        'meta_header_ru' => 'Meta Header',
                        'meta_header_ro' => 'Meta Header',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('slug_ru',$this->slug_ru,true);
		$criteria->compare('slug_ro',$this->slug_ro,true);
		$criteria->compare('preview_ru',$this->preview_ru,true);
		$criteria->compare('preview_ro',$this->preview_ro,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('description_ro',$this->description_ro,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('author',$this->author);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogArticle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getCategoryList()
    {
        return CHtml::listData(BlogCategory::model()->findAll(), 'id', 'name_'.Yii::app()->language);
    }

    public function getTitle()
    {
        return $this->{'title_'.Yii::app()->language};
    }

    public function getHeadTitle()
    {
        return $this->{'head_title_'.Yii::app()->language};
    }

    public function getPreview()
    {
        return $this->{'preview_'.Yii::app()->language};
    }

    public function getDescription()
    {
        return $this->{'description_'.Yii::app()->language};
    }

    public function getUrl()
    {
        //if ($this->_url === null)
            $this->_url = Yii::app()->createUrl('/blog/frontBlog/view', array('id' => $this->id));
        return $this->_url;
    }
    
    public function getMetaTitle() {
        return $this->{'meta_title_' . Yii::app()->language} ?: $this->{'title_' . Yii::app()->language};
    }

    public function getMetaKeyword() {
        return $this->{'meta_keywords_' . Yii::app()->language};
    }

    public function getMetaDescription() {
        return $this->{'meta_description_' . Yii::app()->language};
    }
}
