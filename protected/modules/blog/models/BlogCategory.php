<?php

/**
 * This is the model class for table "blog_category".
 *
 * The followings are the available columns in table 'blog_category':
 * @property integer $id
 * @property string $name_ro
 * @property string $name_ru
 * @property string $description_ru
 * @property string $description_ro
 * @property string $slug_ru
 * @property string $slug_ro
 * @property string $is_active
 */
class BlogCategory extends ActiveRecord
{
    private $_url;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_ro, name_ru', 'required'),
			array('name_ro, name_ru, slug_ru, slug_ro', 'length', 'max'=>255),
			array('is_active', 'length', 'max'=>1),
            array('description_ru, description_ro, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_ro, name_ru, slug_ru, slug_ro, is_active, meta_title_ru, meta_title_ro, meta_keywords_ru, meta_keywords_ro, meta_description_ru, meta_description_ro, meta_header_ru, meta_header_ro', 'safe', 'on'=>'search'),
		);
	}

    public function beforeSave()
    {
        $this->slug_ru = YText::translit($this->name_ru);
        $proverka = self::model()->findByAttributes(array('slug_ru' => $this->name_ru));
        if($proverka && $proverka->id != $this->id)
            $this->slug_ru .= '_'.$this->id;

        $this->slug_ro = YText::translit($this->name_ro);
        $proverka = self::model()->findByAttributes(array('slug_ro' => $this->name_ro));
        if($proverka && $proverka->id != $this->id)
            $this->slug_ro .= '_'.$this->id;

        return parent::beforeSave();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'articles' => array(self::HAS_MANY, 'BlogArticle', 'category_id', 'scopes' => 'active'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_ro' => 'Name Ro',
			'name_ru' => 'Name Ru',
			'slug_ru' => 'Slug Ru',
			'slug_ro' => 'Slug Ro',
			'is_active' => 'Is Active',
                        'meta_title_ru' => 'Meta Title',
                        'meta_title_ro' => 'Meta Title',
                        'meta_keywords_ru' => 'Meta Keywords',
                        'meta_keywords_ro' => 'Meta Keywords',
                        'meta_description_ru' => 'Meta Description',
                        'meta_description_ro' => 'Meta Description',
                        'meta_header_ru' => 'Meta Header',
                        'meta_header_ro' => 'Meta Header',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_ro',$this->name_ro,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('slug_ru',$this->slug_ru,true);
		$criteria->compare('slug_ro',$this->slug_ro,true);
		$criteria->compare('is_active',$this->is_active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getCategoryData()
    {
        return new CArrayDataProvider($this->articles,
            array(
                'sort' => array('defaultOrder' => 'id DESC'),
                'pagination' => array('Pagesize' => Yii::app()->params['itemsPerPage'])
            )
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getName()
    {
        return $this->{'name_'.Yii::app()->language};
    }

    public function getDescription()
    {
        return $this->{'description_'.Yii::app()->language};
    }

    public function getUrl()
    {
        //if ($this->_url === null)
            $this->_url = Yii::app()->createUrl('/blog/frontBlog/category', array('id'=>$this->id));
        return $this->_url;
    }
    
    public function getMetaTitle() {
        return $this->{'meta_title_' . Yii::app()->language} ?: $this->{'name_' . Yii::app()->language};
    }

    public function getMetaKeyword() {
        return $this->{'meta_keywords_' . Yii::app()->language};
    }

    public function getMetaDescription() {
        return $this->{'meta_description_' . Yii::app()->language};
    }
}
