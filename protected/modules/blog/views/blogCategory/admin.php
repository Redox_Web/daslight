<?php
	$this->breadcrumbs=array(
        'Blog Categories'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create BlogCategory','url'=>array('create')),
    );
    $this->title = "Manage Blog Categories";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'blog-category-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'name_ro',
		'name_ru',
		'slug_ru',
		'slug_ro',
		'is_active:boolean',
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
