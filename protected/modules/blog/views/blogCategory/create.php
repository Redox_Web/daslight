<?php
	$this->breadcrumbs=array(
        'Blog Categories'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage BlogCategory','url'=>array('admin')),
    );
    $this->title = "Create BlogCategory";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>