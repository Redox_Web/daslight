<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'name_ro',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'name_ru',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'slug_ru',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'slug_ro',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'is_active',array('class'=>'form-control','maxlength'=>1)); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
