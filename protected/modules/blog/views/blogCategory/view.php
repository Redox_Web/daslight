<?php
	$this->breadcrumbs=array(
        'Blog Categories'=>array('admin'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'Create BlogCategory','url'=>array('create')),
        array('label'=>'Update BlogCategory','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete BlogCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage BlogCategory','url'=>array('admin')),
    );
    $this->title = "View BlogCategory # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name_ro',
		'name_ru',
		'slug_ru',
		'slug_ro',
		'is_active:boolean',
),
)); ?>
