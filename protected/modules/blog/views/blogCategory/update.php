<?php
	$this->breadcrumbs=array(
        'Blog Categories'=>array('admin'),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create BlogCategory','url'=>array('create')),
	    array('label'=>'View BlogCategory','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage BlogCategory','url'=>array('admin')),
	);
    $this->title = "Update BlogCategory #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>