<?php
    $this->pageTitle = 'DASLight | ' . $article->title;
    $this->metaDescription = strip_tags($article->metaDescription);
    $this->metaKeywords = strip_tags($article->metaKeyword);
    $this->ogImage = $article->image;
    $this->ogType = 'article';
    $this->ogUrl = Yii::app()->request->requestUri;
?>

<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s">
    <div class="promo-image" style="background-image: url('/static/images/contacts.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm ">
                    <h1 class="cart-page">
                        <?php echo $article->title; ?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            $article->category->name => array('/blog/frontBlog/slidesCategory', 'id' => 7),
                            $article->title
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="mt-about-sec text-center wow fadeInUp text-left" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
    <div class="container">
        <div class="txt">
            <?php echo $article->description; ?>
            <i><?php echo Yii::t("base", "Implemented");?>: <?php echo YHelper::formatDate($article->updated); ?></i>
        </div>
        <div class="row">
            <?php $attributes = $article->contypeImagesList; if($attributes) { ?>
                <?php foreach ($attributes as $attribute) { ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 pb-2em">
                        <div class="photo-item">
                            <a class="img-group" rel="lightbox" href="<?php echo YHelper::getImagePath($attribute->path); ?>">
                                <img alt="project photos 1" src="<?php echo YHelper::getImagePath($attribute->path, 254, 204); ?>">
                            </a>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php $this->widget('AnotherArticles', array('article' => $article)); ?>
    </div>
</div>
