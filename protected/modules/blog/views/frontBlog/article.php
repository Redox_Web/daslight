<?php
    $this->pageTitle = 'DASLight | ' . $article->title;
    $this->metaDescription = strip_tags($article->metaDescription);
    $this->metaKeywords = strip_tags($article->metaKeyword);
    $this->ogImage = $article->image;
    $this->ogType = 'article';
    $this->ogUrl = Yii::app()->request->requestUri;
?>
<?php Yii::app()->clientScript->registerScriptFile('//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f0de5962e75fe9b',CClientScript::POS_END); ?>

<section class="mt-contact-banner mt-banner-22" data-wow-delay="0.4s">
    <div class="promo-image" style="background-image: url('/static/images/contacts.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                    <h1 class="cart-page">
                        <?php echo $article->headTitle; ?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            $article->category->name => $article->category->url,
                            $article->title
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mt-blog-detail style4">
    <div class="container">
        <article class="blog-post style3">
            <div class="img-holder">

                <img src="<?php echo YHelper::getImagePath($article->image, 1200, 570); ?>" alt="light"/>
                <time class="time" datetime="<?php echo $article->created; ?>">
                    <strong><?php echo YHelper::formatDate('dd', $article->created, 'yyyy-MM-dd HH:mm:ss'); ?></strong>
                    <?php echo ucfirst(rtrim(YHelper::formatDate('LLL', $article->created, 'yyyy-MM-dd HH:mm:ss'), '.')); ?>
                </time>
            </div>
            <div class="blog-txt">
                <h2><?php echo $article->{"meta_header_".Yii::app()->language} ?: $article->{"title_".Yii::app()->language}; ?></h2>
                <ul class="list-unstyled blog-nav">
                    <li><i class="fa fa-clock-o"></i><?php echo $article->created; ?></li>
                </ul>
                <?php echo $article->description; ?>
                <div class="addthis_sharing_toolbox"></div>
            </div>
        </article>
    </div>
</section>

<?php $this->widget('AnotherArticles', array('article' => $article)); ?>