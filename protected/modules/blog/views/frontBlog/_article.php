<article class="post-blog wow fadeInLeft" data-wow-delay="0.4s" style="position: absolute; left: 0px; top: 0px; visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
    <div class="img-holder">
        <a href="<?php echo $data->url; ?>">
            <img src="<?php echo YHelper::getImagePath($data->image, 400, 300); ?>" alt="<?php echo $data->title; ?>">
        </a>
    </div>
    <h2 class=""><a href="<?php echo $data->url; ?>"><?php echo $data->title; ?></a></h2>
    <time class="time">
        <strong><?php echo YHelper::formatDate('dd', $data->created, 'yyyy-MM-dd HH:mm:ss'); ?></strong>
        <?php echo ucfirst(rtrim(YHelper::formatDate('LLL', $data->created, 'yyyy-MM-dd HH:mm:ss'), '.')); ?>
    </time>
    <a href="<?php echo $data->url; ?>" class="btn-more"><i class="fa fa-angle-right"></i> <?php echo Yii::t("base", "More"); ?></a>
</article>