<div class="row">
    <div class="col-xs-12 pb10">
        <h5 class="mb0">
            <a href="<?php echo $data->url; ?>">
                <?php echo $data->title; ?>
            </a>
        </h5>
        <i class="color-gray"><?php echo Yii::t("base", "Implemented");?>: <?php echo YHelper::formatDate($data->updated); ?></i>
    </div>

    <?php $attributes = $data->contypeImagesList(array('limit' => 3)); if($attributes) { ?>
        <?php foreach ($attributes as $attribute) { ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="photo-item">
                    <a href="<?php echo $data->url; ?>">
                        <img alt="project photos 1" src="<?php echo YHelper::getImagePath($attribute->path, 254, 204); ?>">
                    </a>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>