<?php
$this->pageTitle = 'DASLight | ' . $articleCategory->name;
$this->metaDescription = strip_tags($articleCategory->metaDescription);
$this->metaKeywords = strip_tags($articleCategory->metaKeyword);
?>
<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class="promo-image" style="background-image: url('/static/images/contacts.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm ">
                <h1 class="cart-page">
                    <?php echo $articleCategory->name;?>
                </h1>
                <p class="cart-page">
                    <?php echo $articleCategory->description;?>
                </p>
                <!-- Breadcrumbs of the Page -->
                <?php $this->widget('Breadcrumbs', array(
                    'links'=>array(
                        $articleCategory->name
                    ),
                )); ?>
            </div>

        </div>
    </div>
    </div>
</section>

<div class="mt-blog-detail style4 pt-3em">
    <div class="container">
        <div class="mt-iso" id="blog-isotops" style="position: relative;">
            <?php $this->widget(
                'zii.widgets.CListView',
                array(
                    'dataProvider' => $articleCategory->getCategoryData(),
                    'itemView' => '_article',
                    'enableHistory' => true,
                    'cssFile' => false,
                    'summaryText' => false,
                    'pager'=> "LinkPager",
                    'ajaxUpdate'=>false,
                )
            ); ?>
        </div>
    </div>
</div>
