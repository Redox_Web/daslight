<?php
	$this->breadcrumbs=array(
        'Blog Articles'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create BlogArticle','url'=>array('create')),
    );
    $this->title = "Manage Blog Articles";
?>

<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'blog-article-grid',
    'type' => 'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'category_id',
		'title_ru',
		'title_ro',
		'slug_ru',
		'slug_ro',
		/*
		'preview_ru',
		'preview_ro',
		'description_ru',
		'description_ro',
		'image',
		'created',
		'updated',
		'author',
		'is_active',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '70px'),
        ),
    ),
)); ?>
