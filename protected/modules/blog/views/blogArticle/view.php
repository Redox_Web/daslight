<?php
	$this->breadcrumbs=array(
        'Blog Articles'=>array('admin'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'Create BlogArticle','url'=>array('create')),
        array('label'=>'Update BlogArticle','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete BlogArticle','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage BlogArticle','url'=>array('admin')),
    );
    $this->title = "View BlogArticle # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'category_id',
		'title_ru',
		'title_ro',
		'slug_ru',
		'slug_ro',
		'preview_ru',
		'preview_ro',
		'description_ru',
		'description_ro',
		'image',
		'created',
		'updated',
		'author:user',
		'is_active:boolean',
),
)); ?>
