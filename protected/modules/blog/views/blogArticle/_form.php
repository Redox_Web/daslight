<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'blog-article-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListGroup($model,'category_id', BlogArticle::model()->categoryList,array('class'=>'form-control')); ?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'title_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('title_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'title_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('title_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'head_title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'head_title_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('head_title_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'head_title_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('head_title_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'preview_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'preview_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('preview_ru'))),
            array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'preview_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('preview_ro'))),
        ),
    )); ?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'description_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->tinyMceGroup($model,'description_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('description_ru'))),
            array('label'=>'RO', 'content'=>$form->tinyMceGroup($model,'description_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control'), true), 'active'=>($model->hasErrors('description_ro'))),
        ),
    )); ?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'meta_header_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'meta_header_ru',array('class'=>'form-control','maxlength'=>250)), 'active'=>($model->hasErrors('meta_header_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'meta_header_ro',array('class'=>'form-control','maxlength'=>250)), 'active'=>($model->hasErrors('meta_header_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'meta_title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'meta_title_ru',array('class'=>'form-control','maxlength'=>250)), 'active'=>($model->hasErrors('meta_title_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'meta_title_ro',array('class'=>'form-control','maxlength'=>250)), 'active'=>($model->hasErrors('meta_title_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'meta_keywords_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'meta_keywords_ru',array('class'=>'form-control','maxlength'=>250)), 'active'=>($model->hasErrors('meta_keywords_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'meta_keywords_ro',array('class'=>'form-control','maxlength'=>250)), 'active'=>($model->hasErrors('meta_keywords_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'meta_description_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textArea($model,'meta_description_ru', array('rows'=>6, 'cols'=>50, 'class'=>'form-control','maxlength'=>250), true), 'active'=>($model->hasErrors('meta_description_ru'))),
            array('label'=>'RO', 'content'=>$form->textArea($model,'meta_description_ro', array('rows'=>6, 'cols'=>50, 'class'=>'form-control','maxlength'=>250), true), 'active'=>($model->hasErrors('meta_description_ro'))),
        ),
    ));?>

    <?php echo $form->fileFieldGroup($model,'image',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php echo $form->checkboxGroup($model, 'is_active',
        array('widgetOptions' =>
            array(
                'htmlOptions' => array(
                    'checked'=>$model->isNewRecord ? true : $model->is_active
                ),
            )
        )
    ); ?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>

<?php $this->beginClip('left_column_content');?>
    <?php if(!$model->isNewRecord) : ?>
        <?php $form->uploadifyRow($model, 'path', 'contypeImagesList'); ?>
    <?php endif; ?>
<?php $this->endClip(); ?>
