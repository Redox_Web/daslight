<?php
	$this->breadcrumbs=array(
        'Blog Articles'=>array('admin'),
        $model->id=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create BlogArticle','url'=>array('create')),
	    array('label'=>'View BlogArticle','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage BlogArticle','url'=>array('admin')),
	);
    $this->title = "Update BlogArticle #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>