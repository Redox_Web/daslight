<?php
	$this->breadcrumbs=array(
        'Blog Articles'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage BlogArticle','url'=>array('admin')),
    );
    $this->title = "Create BlogArticle";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>