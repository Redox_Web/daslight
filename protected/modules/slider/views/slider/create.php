<?php
	$this->breadcrumbs=array(
        'Sliders'=>array('admin'),
        'Create',
    );

    $this->menu=array(
        array('label'=>'Manage Slider','url'=>array('admin')),
    );
    $this->title = "Create Slider";
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>