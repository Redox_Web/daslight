<?php $form=$this->beginWidget('backend.components.ActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class' =>'form-horizontal row-border', "enctype"=>"multipart/form-data"),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->fileFieldGroup($model,'image',array('class'=>'form-control','maxlength'=>255)); ?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'link_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'link_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('link_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'link_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('link_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'subtitle_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'subtitle_ru',array('class'=>'form-control','maxlength'=>80)), 'active'=>($model->hasErrors('subtitle_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'subtitle_ro',array('class'=>'form-control','maxlength'=>80)), 'active'=>($model->hasErrors('subtitle_ro'))),
        ),
    ));?>

    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'title_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'title_ru',array('class'=>'form-control','maxlength'=>80)), 'active'=>($model->hasErrors('title_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'title_ro',array('class'=>'form-control','maxlength'=>80)), 'active'=>($model->hasErrors('title_ro'))),
        ),
    ));?>


    <?php $this->widget('booster.widgets.TbTabsLang', array(
        'type'=>'pills',
        'labelModel' => $model,
        'labelAttribute' => 'description_ru',
        'htmlOptions'=>array('class'=>'form-group'),
        'tabs'=>array(
            array('label'=>'RU', 'content'=>$form->textField($model,'description_ru',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('description_ru'))),
            array('label'=>'RO', 'content'=>$form->textField($model,'description_ro',array('class'=>'form-control','maxlength'=>255)), 'active'=>($model->hasErrors('description_ro'))),
        ),
    ));?>

    <?php echo $form->dropDownListGroup($model, 'target', Slider::model()->getMenuTargetList(), array('class'=>'form-control'));  ?>

	<?php /*echo $form->textFieldGroup($model,'title',array('class'=>'form-control','maxlength'=>80)); */?><!--

	--><?php /*echo $form->textFieldGroup($model,'description',array('class'=>'form-control','maxlength'=>255)); */?>

    <?php echo $form->checkboxGroup($model, 'is_active',
        array('widgetOptions' =>
            array(
                'htmlOptions' => array(
                    'checked'=>$model->isNewRecord ? true : $model->is_active
                ),
            )
        )
    ); ?>

    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'formSubmit',
        'htmlOptions' => array('class' => 'btn btn-primary'),
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>

<?php $this->endWidget(); ?>
