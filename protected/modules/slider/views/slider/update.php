<?php
	$this->breadcrumbs=array(
        'Sliders'=>array('admin'),
        $model->title=>array('view','id'=>$model->id),
        'Update',
    );

	$this->menu=array(
	    array('label'=>'Create Slider','url'=>array('create')),
	    array('label'=>'View Slider','url'=>array('view','id'=>$model->id)),
	    array('label'=>'Manage Slider','url'=>array('admin')),
	);
    $this->title = "Update Slider #".$model->id;
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>