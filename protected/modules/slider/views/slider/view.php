<?php
	$this->breadcrumbs=array(
        'Sliders'=>array('admin'),
        $model->title,
    );

    $this->menu=array(
        array('label'=>'Create Slider','url'=>array('create')),
        array('label'=>'Update Slider','url'=>array('update','id'=>$model->id)),
        array('label'=>'Delete Slider','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage Slider','url'=>array('admin')),
    );
    $this->title = "View Slider # $model->id";
?>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'image:image',
		'link_ru',
		'link_ro',
		'title_ru',
    	'title_ro',
		'description_ru',
    	'description_ro',
		'target:target',
		'is_active:boolean',
),
)); ?>
