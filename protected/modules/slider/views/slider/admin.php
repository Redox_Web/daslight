<?php
	$this->breadcrumbs=array(
        'Sliders'=>array('admin'),
        'Manage',
    );

    $this->menu=array(
        array('label'=>'Create Slider','url'=>array('create')),
    );
    $this->title = "Manage Sliders";
?>

<?php $this->widget('ExtendedGridView',array(
    'id'=>'slider-grid',
    'type' => 'striped bordered condensed',
    'selectableRows' => 2,
    'skin'=>'nested',
    'nested' => true,
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
            'htmlOptions' => array('class' => 'move-block'),
            'type'=>'raw',
            'value'=>function(){
                echo "<span class='handle-sortable'></span>";
             }
        ),
		'id',
        'is_active:boolean',
		'image',
		'link_ru',
		'link_ro',
		'target:target',
		/*
		'title',
		'description',
		'is_active',
		'ord',
		*/
        array(
            'class' => 'backend.components.ButtonColumn',
            'htmlOptions' => array('width' => '80px'),
        ),
    ),
)); ?>
