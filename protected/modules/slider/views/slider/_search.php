<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'id',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'image',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'link_ru',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'link_ro',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'link_en',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'target',array('class'=>'form-control','maxlength'=>10)); ?>

		<?php echo $form->textFieldGroup($model,'title',array('class'=>'form-control','maxlength'=>80)); ?>

		<?php echo $form->textFieldGroup($model,'description',array('class'=>'form-control','maxlength'=>255)); ?>

		<?php echo $form->textFieldGroup($model,'is_active',array('class'=>'form-control')); ?>

		<?php echo $form->textFieldGroup($model,'ord',array('class'=>'form-control')); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
