<div class="mt-main-slider">
    <!-- slider banner-slider start here -->
    <div class="slider banner-slider">
        <?php foreach($slides as $slide) { ?>
            <!-- holder start here -->
            <div class="holder text-center" style="background-image: url('<?php echo Yii::app()->getBaseUrl(false)."/".$slide->image; ?>');">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text centerize">
                                <strong class="title">
                                    <?php echo $slide->getSubtitle(); ?>
                                </strong>

                                <h1><?php echo $slide->getMainTitle(0); ?></h1>
                                <h2><?php echo $slide->getMainTitle(1); ?></h2>
                                <div class="txt">
                                    <p><?php echo $slide->getDescription(); ?></p>
                                </div>
                                <a href="<?php echo $slide->link; ?>" class="shop"><?php echo Yii::t("base", "More");?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- holder end here -->
        <?php } ?>
    </div>
    <!-- slider regular end here -->
</div><!-- mt main slider end here -->

<?php Yii::app()->clientScript->registerScript('customBanner',"
    jQuery(\".banner-slider\").slick({
            dots: true,
            arrows: false,
            infinite: true,
            adaptiveHeight: true,
            autoplaySpeed: ".YHelper::yiisetting('slider_speed', 5000).",
            autoplay: true
        });
", CClientScript::POS_READY); ?>