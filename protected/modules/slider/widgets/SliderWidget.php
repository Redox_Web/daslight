<?php
Yii::import('slider.models.Slider');

class SliderWidget extends CWidget
{
    public $view = 'slider-view';

    public function run()
    {
        $slides = Slider::model()->active()->findAll(array('order' => 'ord'));
        if ($slides) {
            $this->render($this->view, array('slides' => $slides));
        }
    }
}
