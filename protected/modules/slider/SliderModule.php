<?php
Yii::import('backend.components.WebModule');

class SliderModule extends WebModule
{
    public $_localAssetsUrl;

	public function init()
	{
        parent::init();

		$this->setImport(array(
			'slider.models.*',
			'slider.components.*',
            'slider.widgets.*',
		));
	}

    public static function urlRules()
    {
        return array(

        );
    }

    public static function menuItem()
    {
        return array(
            'icon' => 'fa fa-folder-open-o',
            'label' => Yii::t('SliderModule.slider', 'Slider'),
            'items' => array(
                array(
                    'label' => Yii::t('SliderModule.slider', 'Images'),
                    'url' => 'slider/slider/admin',
                    'sidebar_tab' => 'slider',
                ),
            )
        );
    }
}
