<?php
Yii::import('backend.components.BackendController');

class SliderController extends BackendController
{
    public $sidebar_tab = "slider";

    public function actions()
    {
        return array(
            'admin'=>'DAdminAction',
            'view' => 'DViewAction',
            'create' => 'DCreateAction',
            'delete' => 'DDeleteAction',
            'update'=> 'DUpdateAction',
            'sort'=> array(
                'class' => 'DSortAction',
            ),
        );
    }

    public function createModel()
    {
        return new Slider();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Slider::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='slider-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}