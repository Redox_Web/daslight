<?php

/**
 * This is the model class for table "slider".
 *
 * The followings are the available columns in table 'slider':
 * @property integer $id
 * @property string $image
 * @property string $link_ru
 * @property string $link_ro
 * @property string $link_en
 * @property string $target
 * @property string $title_ru
 * @property string $title_ro
 * @property string $subtitle_ru
 * @property string $subtitle_ro
 * @property string $description_ru
 * @property string $description_ro
 * @property integer $is_active
 * @property integer $ord
 */
class Slider extends ActiveRecord
{
    public $flash_messages = false;

    const SELF = '_self';
    const BLANK = '_blank';
    const TOP = '_top';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'slider';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'safe' => false),
			array('is_active, ord', 'numerical', 'integerOnly'=>true),
			array('image, link_ru, link_ro, link_en, description_ru, description_ro', 'length', 'max'=>255),
			array('target', 'length', 'max'=>10),
			array('title_ru, title_ro, subtitle_ru, subtitle_ro', 'length', 'max'=>80),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image, link_ru, link_ro, link_en, target, title_ru, title_ro, subtitle_ru, subtitle_ro,description_ru, description_ro, is_active, ord', 'safe', 'on'=>'search'),
		);
	}

    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => $this->getTableAlias(false, false).'.is_active = 1',
            ),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'link_ru' => 'Link Ru',
			'link_ro' => 'Link Ro',
			'link_en' => 'Link En',
			'target' => 'Target',
			'title_ru' => 'Title Ru',
            'title_ro' => 'Title Ro',
            'subtitle_ru' => 'Subtitle Ru',
            'subtitle_ro' => 'Subtitle Ro',
			'description_ru' => 'Description Ru',
            'description_ro' => 'Description Ro',
			'is_active' => 'Is Active',
			'ord' => 'Ord',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('link_ru',$this->link_ru,true);
		$criteria->compare('link_ro',$this->link_ro,true);
		$criteria->compare('link_en',$this->link_en,true);
		$criteria->compare('target',$this->target,true);
		$criteria->compare('title_ru',$this->title_ru,true);
        $criteria->compare('title_ro',$this->title_ro,true);
        $criteria->compare('subtitle_ru',$this->title_ru,true);
        $criteria->compare('subtitle_ro',$this->title_ro,true);
		$criteria->compare('description_ru',$this->description_ru,true);
        $criteria->compare('description_ro',$this->description_ro,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('ord',$this->ord);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Slider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getMenuTargetList($value = null)
    {
        $arr = array(
            self::SELF => Yii::t("SliderModule.slider", 'In the current window'),
            self::BLANK => Yii::t("SliderModule.slider", 'In a new browser tab'),
            self::TOP => Yii::t("SliderModule.slider", 'In a new browser window'),
        );

        if($value) return $arr[$value];
        return $arr;
    }

    public function getLink()
    {
        return $this->{'link_'.Yii::app()->language};
    }

    public function getSubtitle()
    {
        return $this->{'subtitle_'.Yii::app()->language};
    }

    public function getTitle()
    {
        return $this->{'title_'.Yii::app()->language};
    }

    public function getDescription()
    {
        return $this->{'description_'.Yii::app()->language};
    }

    public function getMainTitle($index)
    {
        $title = explode('<br>', $this->getTitle());

        return isset($title[$index]) ? $title[$index] : '';
    }
}
