<?php
/** @var MainPages $staticPage */
$staticPage = $model->getCalculationPage();
$this->pageTitle = $staticPage->getTitle();
$this->metaDescription = $staticPage->getMetaDescription();
$this->metaKeywords = $staticPage->getMetaKeyword();
?>
<section class="mt-contact-banner mt-banner-22 pb-5em pt-2em">
    <div class="container">
        <div class="flex">
            <div class="col-xs-12 col-sm-12">
                <?php $this->widget('Breadcrumbs', array(
                    'breadcrumbsClass' => 'breadcrumbs',
                    'links'=>array(
                        $staticPage->getTitle()
                    ),
                )); ?>
                <div>
                    <h1 class="f-light"><?php echo $staticPage->getTitle();?></h1>
                </div>
                <?php $form=$this->beginWidget('CActiveForm',array(
                    'id'=>'main-calculator-form',
                    'enableAjaxValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => false
                    ),
                    'htmlOptions' => array('class' =>'calculator-form', "enctype"=>"multipart/form-data"),
                )); ?>
                    <div class="row">
						<?php $this->widget('FlashMessages'); ?>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <?php echo $form->dropDownList($model, "type", $model->getAllTypes(), array('class' => 'form-control', 'prompt' => Yii::t("base", 'Work type'))); ?>
                                <?php echo $form->error($model, 'type'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Description'))); ?>
                                <?php echo $form->error($model, 'description'); ?>
                            </div>
                            <div class="form-group">
                                <span class="btn btn-file">
                                    <span class="flex items-center">
                                        <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i>
                                        <span><?php echo Yii::t("base", "Upload");?></span>
                                        <?php echo $form->fileField($model, 'file'); ?>
                                    </span>
                                </span>
	                            <?php echo $form->error($model, 'file'); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Name'))); ?>
                                <?php echo $form->error($model, 'name'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $form->textField($model, 'phone', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Phone'))); ?>
                                <?php echo $form->error($model, 'phone'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => Yii::t("base", 'Email'))); ?>
                                <?php echo $form->error($model, 'email'); ?>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-submit btn-block" type="submit">
                                    <?php echo Yii::t("base", "Send");?>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</section>