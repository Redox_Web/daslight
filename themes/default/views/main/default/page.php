<?php
    $this->pageTitle = $model->getMetaTitle();
    $this->metaDescription = $model->getMetaDescription();
    $this->metaKeywords = $model->getMetaKeyword();
    $this->ogImage = $model->ogimage;
?>
<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class="promo-image" style="background-image: url('<?php echo YHelper::getImagePath($model->title_image,0,0, Yii::app()->params['header_image']); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                    <h1 class="cart-page">
                        <?php echo $model->{"meta_header_".Yii::app()->language} ?: $model->{"title_".Yii::app()->language}; ?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            $model->{"meta_header_".Yii::app()->language} ?: $model->{"title_".Yii::app()->language}
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="mt-about-sec text-center wow fadeInUp text-left" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
    <div class="container">
        <div class="txt">
            <?php echo $model->content; ?>
        </div>
    </div>
</div>
