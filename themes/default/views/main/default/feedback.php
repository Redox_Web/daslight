<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s">
    <div class="promo-image" style="background-image: url('<?php echo YHelper::getImagePath(
            YHelper::yiisetting('contactusinfo_title_image'),0,0, Yii::app()->params['header_image']); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm ">
                    <h1 class="cart-page">
                        <?php echo Yii::t("base", "Contacts"); ?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            Yii::t("base", "Contacts")
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="mt-contact-detail wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
    <div class="container">
        <div class="flex-1em child-col-xs-12 child-col-sm-6">
            <div>
                <?php echo YHelper::yiisetting('contactusinfo_'.Yii::app()->language); ?>
            </div>
            <div>
                <?php $this->widget('FlashMessages'); ?>
                <div class="contact-form">
                    <h2><?php echo Yii::t("MainModule.main", "Contact form");?></h2>
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'feedback-form',
                        'enableAjaxValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'inputContainer' => 'fieldset',
                        ),
                    )); ?>
                    <div class="pb-1em">
                        <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => Yii::t("MainModule.main", "Contact Name"))); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>
                    <div class="pb-1em">
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => Yii::t("MainModule.main", "Email"))); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </div>
                    <div class="pb-1em">
                        <?php echo $form->textArea($model, 'feedback', array('class' => 'form-control', 'rows' => 8, 'placeholder' => Yii::t("MainModule.main", "Message"))); ?>
                        <?php echo $form->error($model, 'feedback'); ?>
                    </div>
                    <div class="pb-1em">
                        <?php $this->widget('application.extensions.recaptcha.EReCaptcha',
                            array('model'=>$model, 'attribute'=>'validacion',
                                'theme'=>'red', 'language'=>'en_EN',
                                'publicKey'=>'6LfVThcTAAAAAAzyzRI3-0SZ_L2g4zw1K6t5Gok3')) ?>
                        <?php echo CHtml::error($model, 'validacion'); ?>
                    </div>
                    <div>
                        <button class="btn-type3" type="submit"><?php echo Yii::t("MainModule.main", "Send");?></button>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mt-contact-detail bg-grey pt-3em pb-3em">
    <div class="container">
        <div class="flex-2em">
            <?php $this->widget('ContactinfoWidget'); ?>
        </div>

    </div>
</div>
<div style="height: 500px; width: 100%;">
    <?php Yii::app()->GM->f()?>
</div>