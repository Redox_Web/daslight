<div class="social-icons">
    <?php if($facebook_href)
        echo '<li>'.CHtml::link('&nbsp;', $facebook_href, array('class' => 'fa fa-facebook', 'target' => "_blank")).'</li>';
    if($twitter_href)
        echo '<li>'.CHtml::link('&nbsp;', $twitter_href, array('class' => 'fa fa-twitter', 'target' => "_blank")).'</li>';
    if($pinterest_href)
        echo '<li>'.CHtml::link('&nbsp;', $pinterest_href, array('class' => 'fa fa-pinterest', 'target' => "_blank")).'</li>';
    if($googleplus_href)
        echo '<li>'.CHtml::link('&nbsp;', $googleplus_href, array('class' => 'fa fa-google-plus', 'target' => "_blank")).'</li>';
    if($youtube_href)
        echo '<li>'.CHtml::link('&nbsp;', $youtube_href, array('class' => 'fa fa-youtube', 'target' => "_blank")).'</li>'; ?>
</div>