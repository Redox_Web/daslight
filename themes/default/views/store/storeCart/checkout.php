<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class=""></div>
    <div class="promo-image" style="background-image: url('/<?php echo YHelper::yiisetting('cart_title_image', Yii::app()->params['header_image']); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                    <h1 class="cart-page">
                        <?php echo Yii::t("StoreModule.store", "Shopping Cart");?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            Yii::t("base", "Cart")
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="mt-process-sec wow fadeInUp" data-wow-delay="0.4s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-unstyled process-list step-2">
                    <li class="active">
                        <span class="counter">1</span>
                        <strong class="title"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                    </li>
                    <li class="active">
                        <span class="counter">2</span>
                        <strong class="title"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                    </li>
                    <li>
                        <span class="counter">3</span>
                        <strong class="title"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div><!-- Mt Process Section of the Page end -->

<!-- Mt Product Table of the Page -->
<div class="mt-product-table wow fadeInUp" data-wow-delay="0.4s">
    <div class="container">
        <div class="row">
            <?php $form = $this->beginWidget(
                'CActiveForm',
                array(
                    'id' => 'order-form',
                    'action' => array('/store/order/create'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,
                        'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                        'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
                    ),
                    'htmlOptions' => array(
                        'hideErrorMessage' => false,
                        'class' => 'order-form',
                    )
                )
            ); ?>
            <div class="col-xs-12 col-md-6">
                <div class="row">
                    <div class="col-xs-12 form-input">
                        <?php echo $form->textField($order, 'name', array('placeholder' => Yii::t("StoreModule.store", 'Ваше имя'), 'class' => 'form-control')); ?>
                        <?php echo $form->error($order, 'name'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-input">
                        <?php echo $form->textField($order, 'email', array('placeholder' => Yii::t("StoreModule.store", 'Email'), 'class' => 'form-control')); ?>
                        <?php echo $form->error($order, 'email'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-input">
                        <?php
                        $this->widget('CMaskedTextField', array(
                            'model' => $order,
                            'attribute' => 'phone',
                            'mask' => '(999)-999-999',
                            'htmlOptions' => array(
                                'placeholder' => Yii::t("StoreModule.store", 'Номер телефона'),
                                'class' => 'form-control'
                            )
                        ));
                        ?>
                        <?php echo $form->error($order, 'phone'); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="row">
                    <div class="col-xs-12 form-input">
                        <?php echo $form->textField($order, 'address', array('placeholder' => Yii::t("StoreModule.store", 'Адрес доставки'), 'class' => 'form-control')); ?>
                        <?php echo $form->error($order, 'address'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $form->textArea($order, 'comment', array('placeholder' => Yii::t("StoreModule.store", 'Комментарий к покупке'), 'class' => 'form-control')); ?>
                        <?php echo $form->error($order, 'comment'); ?>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="row">
            <div class="col-xs-12 page-navigation-block">
                <a class="left" href="<?php echo Yii::app()->createUrl('store/storeCart/Index'); ?>"> <?php echo Yii::t("StoreModule.store", "Back");?></a>
                <a class="right" href="javascript:void(0);" onClick="$('#order-form').submit(); return false;"><?php echo Yii::t("StoreModule.store", "Continue");?></a>
            </div>
        </div>
    </div>
</div><!-- Mt Product Table of the Page end -->