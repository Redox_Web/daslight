<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class=""></div>
    <div class="promo-image" style="background-image: url('/<?php echo YHelper::yiisetting('cart_title_image', Yii::app()->params['header_image']); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                    <h1 class="cart-page">
                        <?php echo Yii::t("StoreModule.store", "Shopping Cart");?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            Yii::t("base", "Cart")
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $allItems = Yii::app()->getModule('store')->cart->getAllItems(); ?>
<?php if($allItems->count()) { ?>

    <div class="mt-process-sec wow fadeInUp" data-wow-delay="0.4s">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-unstyled process-list step-1">
                        <li class="active">
                            <span class="counter">1</span>
                            <strong class="title up"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                        </li>
                        <li>
                            <span class="counter">2</span>
                            <strong class="title up"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                        </li>
                        <li>
                            <span class="counter">3</span>
                            <strong class="title up"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- Mt Process Section of the Page end -->

    <!-- Mt Product Table of the Page -->
    <div class="mt-product-table wow fadeInUp" data-wow-delay="0.4s">
        <div class="container">
            <div class="row border-2 table-header">
                <div class="col-xs-12 col-sm-8">
                    <strong class="title"><?php echo Yii::t("StoreModule.store", "Product Name");?></strong>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <strong class="title"><?php echo Yii::t("StoreModule.store", "Quantity");?></strong>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <strong class="title"><?php echo Yii::t("StoreModule.store", "Price");?></strong>
                </div>
            </div>

            <?php foreach($allItems as $key => $item) { ?>
                <div class="row border cart-item">
                    <div class="col-xs-12 col-sm-8">
                        <strong class="display-xs"><?php echo Yii::t("StoreModule.store", "Product Name");?></strong>
                        <strong class="product-name"><a href="<?php echo $item->url; ?>"><?php echo $item->name; ?></a></strong>
                        <strong class="code-title">Code  <span class="orange">6126 </span></strong>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <strong class="display-xs"><?php echo Yii::t("StoreModule.store", "Quantity");?></strong>
                        <div class="counter-outer">
                            <div class="counter">
                                <button type="button" class="but counterBut dec cart-quantity-decrease" data-target="#<?php echo CHtml::activeId($item, 'quantity').$key; ?>"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
                                <input id="<?php echo CHtml::activeId($item, 'quantity').$key; ?>" class="position-count form-control field fieldCount" data-product-id="<?php echo $item->sku; ?>" type="text" value="<?php echo $item->getQuantity(); ?>" data-min="1" data-max="20">
                                <button type="button" class="but counterBut inc cart-quantity-increase" data-target="#<?php echo CHtml::activeId($item, 'quantity').$key; ?>"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <strong class="display-xs"><?php echo Yii::t("StoreModule.store", "Price");?></strong>
                        <strong class="price"><span class="blue"><?php echo YHelper::formatCurrency($item->resultPrice); ?></span></strong>
                        <a href="javascript:void(0);" class="cart-delete-product" data-product-id="<?php echo $item->sku; ?>"><i class="fa fa-close" ></i></a>
                    </div>
                </div>
            <?php } ?>

            <div class="row">
                <div class="pt-2em">
                    <div class="col-xs-12 col-sm-6 text-left">
                        <form action="#" class="coupon-form buy-form">
                            <a class="button" href="<?php echo $conditiiLivrare->getUrl(); ?>" target="_blank">&nbsp;
                                <?php echo Yii::t("base", "Delivery conditions");?>
                            </a>
                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-right">
                        <form action="#" class="coupon-form">
                            <div class="cart-total">
                                <span><?php echo Yii::t("StoreModule.store", "Total");?>:</span>&nbsp;&nbsp;
                                <span id="priceSumCart" class="blue">
                                    <?php echo YHelper::formatCurrency(Yii::app()->getModule('store')->cart->getCost()); ?>
                                </span>
                            </div>
                        </form>
                        <form action="#" class="buy-form">
                            <a class="button" href="<?php echo Yii::app()->createUrl('store/storeCart/Checkout'); ?>" ><span class="icon-handbag"></span>&nbsp;&nbsp;
                                <?php echo Yii::t("StoreModule.store", "Buy");?>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Mt Product Table of the Page end -->
<?php } else { ?>
    <div class="mt-product-table wow fadeInUp" data-wow-delay="0.4s">
        <div class="container">
            <div class="alert alert-info" role="alert">
                <?php echo Yii::t("StoreModule.store", "Ваша корзина пуста, сделайте покупки");?>
            </div>
        </div>
    </div>
<?php }?>