<?php
    $this->pageTitle = Yii::t('StoreModule.store', 'Search result products: [:q]',
        array('[:q]' => $searchPhrase));
?>
<div class="related-products  wow fadeInUp" data-wow-delay="0.4s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>
                    <?php echo Yii::t('StoreModule.store', 'Search result products: [:q]',
                        array('[:q]' => $searchPhrase)
                    ) ?>
                </h2>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        if ($dataSearch) {
                            $dataSearch->pagination->pageSize=30;
                            $this->widget('zii.widgets.CListView', array(
                                'dataProvider' => $dataSearch,
                                'itemView' => '_search_result',
                                'summaryText' => false,
                                'pager'=> "LinkPager",
                                'ajaxUpdate'=>false,
                                'cssFile' => false
                            ));
                        } else {
                            echo Yii::t("base", "Your query is too short!");
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- related products end here -->
</div>

