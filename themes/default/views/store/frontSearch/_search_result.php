<!-- mt product1 center start here -->
<div class="mt-product1 mt-paddingbottom20">
    <div class="box">
        <div class="b1">
            <div class="b2">
                <?php $image = $data->contypeMainImage ? $data->contypeMainImage->path : ''; ?>
                <a href="<?php echo $data->url; ?>">
                    <img src="<?php echo YHelper::getImagePath($image, 300, 300); ?>" alt="<?php echo $data->name; ?>">
                </a>
                <!--  <span class="caption">
                      <span class="new">NEW</span>
                  </span>-->
                <ul class="mt-stars">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star-o"></i></li>
                </ul>
                <ul class="links">
                    <li>
                        <a href="javascript:void(0);" class="add-product-to-cart" data-product-id="<?php echo $data->sku; ?>">
                            <i class="icon-handbag"></i>
                            <span><?php echo Yii::t("base", "To Cart"); ?></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="txt">
        <strong class="title"><a href="<?php echo $data->url; ?>"><?php echo $data->name; ?></a></strong>
        <strong class="title">Code <span class="orange"><?php echo $data->sku; ?> </span></strong>
        <?php if ($data->old_price) { ?>
            <span class="old-price">
                <span><?php echo YHelper::formatCurrency($data->old_price); ?></span>
            </span>
        <?php } ?>
        <span class="price">
            <span>
                <?php echo YHelper::formatCurrency($data->getResultPrice()); ?>
            </span>
        </span>
    </div>
</div>
