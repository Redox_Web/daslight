<?php if(Yii::app()->request->getUserHostAddress() == '46.55.16.53') { ?>
    <div class="filter-products">
        <form id="store-filter" name="store-filter" method="get">
            <?php $this->widget('store.widgets.filters.CategoryFilterWidget', array('dataProvider' => $dataProvider)); ?>
            <div class="pt20 pb20 pl20 pr20">
                <input class="button-block button-red" type="submit" value="<?php echo Yii::t("base","Show");?>" class="button"/>
            </div>
        </form>
    </div>
    <div class="banner-block">
        <!--<a href="#">
            <img src="<?php /*echo $this->assetsUrl; */?>/img/banner-1.jpg" alt="banner" />
        </a>-->
        <?php $this->widget("banner.widgets.Banners", array('position' => 'left', 'limit' => 1)); ?>
    </div>
<?php } ?>