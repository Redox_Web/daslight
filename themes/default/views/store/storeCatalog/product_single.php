<?php
$mainImagePath = $product->contypeMainImage ? $product->contypeMainImage->path : '';
$this->pageTitle = $product->getMetaTitle();
$this->metaKeywords = $product->getMetaKeyword();
$this->metaDescription = $product->getMetaDescription();
$this->ogImage = $mainImagePath;
?>
<?php $categoryRoot = $product->category->parent()->find();?>
<div class="single-product">
    <section class="mt-product-detial wow fadeInUp" data-wow-delay="0.4s">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Slider of the Page -->
                    <div class="slider">
                        <!-- Comment List of the Page -->
                        <ul class="list-unstyled comment-list">
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <!-- Comment List of the Page end -->
                        <!-- Product Slider of the Page -->

                        <div class="product-slider pt-3em-sm">
                            <div class="slide">
                                <img src="<?php echo YHelper::getImagePath($mainImagePath, 500, 500); ?>" alt="<?php echo $product->name; ?>">
                            </div>
                            <?php if ($product->contypeNotMainImages) { ?>
                                <?php foreach ($product->contypeNotMainImages as $image) { ?>
                                    <div class="slide">
                                        <img src="<?php echo YHelper::getImagePath($image->path, 550, 550); ?>" alt="">
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <!-- Product Slider of the Page end -->
                        <!-- Pagg Slider of the Page -->
                        <?php
                        if($product->contypeNotMainImages): ?>
                            <ul class="list-unstyled slick-slider pagg-slider">
                                <li>
                                    <div class="img">
                                        <img src="<?php echo YHelper::getImagePath($mainImagePath, 150, 150); ?>" alt="<?php echo $product->name; ?>">
                                    </div>
                                </li>
                                <?php foreach ($product->contypeNotMainImages as $image) { ?>
                                    <li><div class="img">
                                        <img src="<?php echo YHelper::getImagePath($image->path, 150, 150); ?>" alt="<?php echo $product->name; ?>">
                                    </div></li>
                                <?php } ?>
                            </ul>
                        <?php endif; ?>
                        <!-- Pagg Slider of the Page end -->
                    </div>
                    <!-- Slider of the Page end -->

                    <!-- Detail Holder of the Page -->
                    <div class="detial-holder">
                        <?php $this->widget('Breadcrumbs', array(
                            'links'=>array(
                                $categoryRoot->title => $categoryRoot->url,
                                $product->category->title => $product->category->url,
                                $product->name
                            ),
                        )); ?>

                        <!-- Breadcrumbs of the Page end -->
                        <h2><?php echo $product->{"meta_header_".Yii::app()->language} ?: $product->name; ?></h2>
                        <div class="code-title">Code: <span class="orange"><?php echo $product->sku; ?></span></div>
                        <div class="txt-wrap">
                            <?php foreach ($product->IsNotShortAttributes as $attribute) { ?>
                                <?php $attr = $attribute->renderValue($product->attribute($attribute->column_name)); ?>
                                <?php if(!empty($attr)) { ?>
                                    <div class="product-row">
                                        <div><?php echo $attribute->title; ?>:  </div>
                                        <div><?php echo $attr; ?></div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="product-row">
                                <div><?php echo Yii::t("base", "State"); ?>:  </div>
                                <?php if (!$product->status) : ?>
                                    <div class="c-light-blue f-bold"><?php echo Yii::t("base", "La comanda"); ?></div>
                                <?php else : ?>
                                    <div class="c-light-blue f-bold"><?php echo Yii::t("base", "Disponibil"); ?></div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="text-holder">
                <span class="price">
                    <?php echo YHelper::formatCurrency($product->getResultPrice()); ?>
                    <?php if ($product->old_price) { ?>
                        <del><?php echo YHelper::formatCurrency($product->old_price); ?></del>
                    <?php } ?>
                </span>
                        </div>
                        <!-- Product Form of the Page -->
                        <form action="#" class="product-form">
                            <fieldset>
                                <div class="counter">
                                    <label for="count-input">QTY:</label>
                                    <button type="button" class="but counterBut dec increase"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
                                    <input type="text" id="count-input" class="field fieldCount quantity" value="1" data-min="1" data-max="200" data-product-id="<?php echo $cartModel->sku; ?>">
                                    <button type="button" class="but counterBut inc decrease"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
                                </div>
                                <div class="row-val">
                                    <button class="add-product-to-cart" data-product-id="<?php echo $product->sku; ?>" type="submit"><?php echo Yii::t("base", "To Cart"); ?></button>
                                </div>
                            </fieldset>
                        </form>
                        <!-- Product Form of the Page end -->
                    </div>
                    <!-- Detail Holder of the Page end -->
                </div>
            </div>
        </div>
    </section><!-- Mt Product Detial of the Page end -->

    <?php if($product->description): ?>
    <div class="related-products wow fadeInUp" data-wow-delay="0.4s">
        <div class="container">
            <h2 href="#tab1" class="c-light-blue title-border"><?php echo Yii::t("base", "Description"); ?></h2>
            <div class="pt-2em"><?php echo $product->description; ?></div>
        </div>
    </div>
    <?php endif; ?>


    <?php $this->widget('SameCategory', array('product' => $product)); ?>

</div>
