<section class="mt-contact-banner mt-banner-22 wow fadeInUp" data-wow-delay="0.4s" >
    <div class="promo-image" style="background-image: url('/<?php echo YHelper::yiisetting('cart_title_image', Yii::app()->params['header_image']); ?>');">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-12 text-left-xs text-center-sm">
                    <h1 class="cart-page">
                        <?php echo Yii::t("StoreModule.store", "Shopping Cart");?>
                    </h1>
                    <?php $this->widget('Breadcrumbs', array(
                        'links'=>array(
                            Yii::t("base", "Cart")
                        ),
                    )); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="mt-process-sec wow fadeInUp" data-wow-delay="0.4s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-unstyled process-list step-3">
                    <li class="active">
                        <span class="counter">1</span>
                        <strong class="title"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                    </li>
                    <li class="active">
                        <span class="counter">2</span>
                        <strong class="title"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                    </li>
                    <li class="active">
                        <span class="counter">3</span>
                        <strong class="title"><?php echo Yii::t("StoreModule.store", "Step");?></strong>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- Mt Process Section of the Page end -->

<!-- Mt Product Table of the Page -->
<div class="wow fadeInUp" data-wow-delay="0.4s">
    <div class="container success-checkout">
        <div class="row">
            <div class="col-xs-12  text-uppercase text-center">
                <h2 class="heading">
                    <?php echo Yii::t("StoreModule.store", "You order was successfully created"); ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <p class="heading"><?php echo Yii::t("StoreModule.store", "Our operator will contact you soon to confirm your order and details"); ?></p>
                <p class="orange-bold"><?php echo Yii::t("StoreModule.store", "Thank you for your purchase!"); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 page-continue-block">
                <a class="right" href="<?php echo Yii::app()->controller->createUrl('/main/default/index', array('lang' => Yii::app()->language)); ?>">
                    <?php echo Yii::t("StoreModule.store", "Buy something else");?>
                </a>
            </div>
        </div>
    </div>
</div><!-- Mt Product Table of the Page end -->