<li>
    <?php $params = array('rel' => 'nofollow', 'class' => 'lang'); ?>
    <?php if(Yii::app()->language == 'ru') { $params['class'] .= ' active'; } ?>
    <?php echo CHtml::link('Ru', Yii::app()->createUrl('main/default/change', array('lang' => 'ru')), $params); ?>
</li>
<li>
    <?php $params = array('rel' => 'nofollow', 'class' => 'lang'); ?>
    <?php if(Yii::app()->language == 'ro') { $params['class'] .= ' active'; } ?>
    <?php echo CHtml::link('Ro', Yii::app()->createUrl('main/default/change', array('lang' => 'ro')), $params); ?>
</li>