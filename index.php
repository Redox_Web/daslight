<?php
date_default_timezone_set('Europe/Kiev');
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

$whitelist = array('127.0.0.1', '::1');

defined('LOCALHOST') or define('LOCALHOST', in_array($_SERVER['REMOTE_ADDR'], $whitelist));
if (LOCALHOST) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
}

// specify how many levels of call stack should be shown in each log message
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
require_once('protected/myfunctions.php');

require_once($yii);
Yii::createWebApplication($config)->run();