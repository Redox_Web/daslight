function lampset(num)
{
    var count = $('#simple'+num).val();
    $('#led'+num).val(count);

    sumpower();
}

function sumpower()
{
    var count = $('#lamp-count').val();
    var simple = 0;
    var led = 0;

    for(var i = 1; i <= count; i++)
    {
        simple += $('#simple'+i).val() * $('#simple-power'+i).val();
        led += $('#led'+i).val() * $('#led-power'+i).val();
    }
    $('#simple-power').val(simple);
    $('#led-power').val(led);

    allsumpower();
}

function allsumpower()
{
    $('#led-hours').val($('#simple-hours').val());

    var simple_total = $('#simple-power').val() * $('#simple-hours').val();
    var led_total = $('#led-power').val() * $('#led-hours').val();

    $('#simple-allpower').val(simple_total);
    $('#led-allpower').val(led_total);

    allmoney();
}

function allmoney()
{

    $('#led-cost').val($('#simple-cost').val());

    var simple_money = $('#simple-cost').val() * $('#simple-allpower').val();
    var led_money = $('#led-cost').val() * $('#led-allpower').val();

    $('#allsum-simple').html(Math.round(simple_money).toFixed(2));
    $('#allsum-led').html(Math.round(led_money,2).toFixed(2));

    drawgr(Math.round(simple_money,0).toFixed(2),Math.round(led_money,0).toFixed(2));
}

function drawgr(simple, led)
{
    var v1 = parseInt(simple);
    var v2 = parseInt(led);
    var data = new google.visualization.DataTable();
    data.addColumn('string', '');
    data.addColumn('number', conventional_lamp);
    data.addColumn('number', led_lamp);
    data.addRows([
      ['', v1, v2]
    ]);

    var options = {
      width: 800, height: 100,
      title: '',
      colors:['#FBC2C4','#76C376'],
      chartArea:{left:100,top:20,width:500}
    };

    var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
    chart.draw(data, options);

}

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
var data = new google.visualization.DataTable();
data.addColumn('string', '');
data.addColumn('number', conventional_lamp);
data.addColumn('number', led_lamp);
data.addRows([
  ['', 0, 0]
]);

var options = {
  width: 800, height: 100,
  title: '',
  colors:['#FBC2C4','#76C376'],
  chartArea:{left:100,top:20,width:500}
};

var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
chart.draw(data, options);
}