$(document).ajaxError(function () {
    /*$('#notifications').notify({message: {text: 'Произошла ошибка =('}, 'type': 'danger'}).show();*/
});

$(document).ready(function () {
    var cartWidgetSelector = '#shopping-cart-widget';
    var priceSumCart = '#priceSumCart';
    var checkFailed = true;

    function updateCartWidget() {
        $(cartWidgetSelector).load('/'+curentLanguage+'/widgetcart');
    }

    function updatePositionSumPrice($sum) {
        $(priceSumCart).html($sum);
    }

    function changePositionQuantity(productId, quantity) {
        var data = {'quantity': quantity, 'id': productId};
        $.ajax({
            url: '/'+curentLanguage+'/updatecart',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                    updatePositionSumPrice(data.data);
                }
            }
        });
    }

    $('body').on('click', '.cart-delete-product',function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'id': el.data('product-id')};
        console.log('test')
        $.ajax({
            url: '/'+curentLanguage+'/deletecart',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log(data)
                if (data.result) {
                    el.closest('.cart-item').remove();
                    updateCartWidget();
                    updatePositionSumPrice(data.data);
                }
            }
        });
    });

    $(".position-count").keyup(function () {
        if(!checkFailed) {
            var quantity = $(this).val();
            var productId = $(this).data('product-id');
            changePositionQuantity(productId, quantity);
        }
    });

    $("#quantity, .position-count").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || //, 190
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            checkFailed = false;
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))
            e.preventDefault();
        else
            checkFailed = false;
    });

    $('#add-product-to-cart, .add-product-to-cart').click(function (e) {
        e.preventDefault();
        var button = $(this);
        button.button('loading');
        var quantity = $(this).closest('.product-form').find('.quantity').val() || 1;
        var data = {'Product[id]': button.data('product-id'), 'Product[quantity]': quantity};
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: data,
            url: '/'+curentLanguage+'/addtocart',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                //showNotify(button, data.result ? 'success' : 'danger', data.data);
            }
        }).always(function () {
            button.button('reset');
        });
    });

    $('#add-product-to-wishlist').click(function (e) {
        e.preventDefault();
        var button = $(this);
        button.button('loading');
        var data = {'Product[id]': button.data('product-id')};
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: data,
            url: '/'+curentLanguage+'/store/frontWishlist/add',
            success: function (data) {
                if (data.sum) {
                    $('.mylist-counter').html(data.sum);
                }
            }
        }).always(function () {
            button.button('reset');
        });
    });

    $('body').on('click', '.quick-add-product-to-cart', function (e) {
        e.preventDefault();
        var el = $(this);
        var data = {'Product[id]': el.data('product-id')};
        /*data[atlantTokenName] = yupeToken;*/
        $.ajax({
            url: '/'+curentLanguage+'/addtocart',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    updateCartWidget();
                }
                //showNotify(el, data.result ? 'success' : 'danger', data.data);
            }
        });
    });

    $('.cart-quantity-increase').click(function() {
        var target = $($(this).data('target'));
        var quantity = parseInt(target.val()) + 1;
        target.val(quantity);

        var productId = target.data('product-id');
        changePositionQuantity(productId, quantity);
    });

    $('.cart-quantity-decrease').click(function() {
        var target = $($(this).data('target'));
        var quantity = parseInt(target.val()) - 1;
        if (parseInt(target.val()) > 1) {
            target.val(quantity);
        }

        var productId = target.data('product-id');
        changePositionQuantity(productId, quantity);
    });

    $(".increase").click(function () {
        var value = $(this).parent().find("input").val();
        if (value != 1) {
            value--;
            $(this).parent().find("input").val(value);
        }
    });
    $(".decrease").click(function () {
        var value = $(this).parent().find("input").val();
        value++;
        $(this).parent().find("input").val(value);
    });

    //=====================filter==================

    /*$('#store-filter').on('mouseup', 'input', function (e) {
     e.preventDefault();

     $.fn.yiiListView.update('list-item', {
     data: $('#store-filter').serialize()
     });
     return false;
     });*/
});
